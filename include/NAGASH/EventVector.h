//***************************************************************************************
/// @file EventVector.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Result.h"

namespace NAGASH
{
    /** @class NAGASH::EventVector EventVector.h "NAGASH/EventVector.h"
        @brief Deprecated. It's better to use user defined event vector.
    */
    class EventVector : public Result
    {
    public:
        EventVector(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "") : Result(MSG, c, rname, fname) {}

        void BookVariable(const TString &name, int *value);
        void BookVariable(const TString &name, long *value);
        void BookVariable(const TString &name, double *value);
        void BookVariable(const TString &name, bool *value);
        void BookVariable(const TString &name, TString *value);
        void BookVariable(const TString &name, TLorentzVector *value);
        void BookVariable(const TString &name, std::vector<int> *value);
        void BookVariable(const TString &name, std::vector<long> *value);
        void BookVariable(const TString &name, std::vector<double> *value);
        void BookVariable(const TString &name, std::vector<bool> *value);
        void BookVariable(const TString &name, std::vector<TString> *value);
        void BookVariable(const TString &name, std::vector<TLorentzVector> *value);

        void SetVariableAddress(const TString &name, int *value);
        void SetVariableAddress(const TString &name, long *value);
        void SetVariableAddress(const TString &name, double *value);
        void SetVariableAddress(const TString &name, bool *value);
        void SetVariableAddress(const TString &name, TString *value);
        void SetVariableAddress(const TString &name, TLorentzVector *value);
        void SetVariableAddress(const TString &name, std::vector<int> *value);
        void SetVariableAddress(const TString &name, std::vector<long> *value);
        void SetVariableAddress(const TString &name, std::vector<double> *value);
        void SetVariableAddress(const TString &name, std::vector<bool> *value);
        void SetVariableAddress(const TString &name, std::vector<TString> *value);
        void SetVariableAddress(const TString &name, std::vector<TLorentzVector> *value);

        void GetEvent(int ie);
        void Fill();
        int Size();
        void Combine(std::shared_ptr<Result> result) override;

    private:
        std::map<TString, std::vector<int>> ValueMapInt;
        std::map<TString, std::vector<long>> ValueMapLong;
        std::map<TString, std::vector<double>> ValueMapDouble;
        std::map<TString, std::vector<bool>> ValueMapBool;
        std::map<TString, std::vector<TString>> ValueMapTString;
        std::map<TString, std::vector<TLorentzVector>> ValueMapTLorentzVector;
        std::map<TString, std::vector<std::vector<int>>> ValueMapIntVec;
        std::map<TString, std::vector<std::vector<long>>> ValueMapLongVec;
        std::map<TString, std::vector<std::vector<double>>> ValueMapDoubleVec;
        std::map<TString, std::vector<std::vector<bool>>> ValueMapBoolVec;
        std::map<TString, std::vector<std::vector<TString>>> ValueMapTStringVec;
        std::map<TString, std::vector<std::vector<TLorentzVector>>> ValueMapTLorentzVectorVec;

        std::map<int *, std::vector<int> *> BookVarMapInt;
        std::map<long *, std::vector<long> *> BookVarMapLong;
        std::map<double *, std::vector<double> *> BookVarMapDouble;
        std::map<bool *, std::vector<bool> *> BookVarMapBool;
        std::map<TString *, std::vector<TString> *> BookVarMapTString;
        std::map<TLorentzVector *, std::vector<TLorentzVector> *> BookVarMapTLorentzVector;
        std::map<std::vector<int> *, std::vector<std::vector<int>> *> BookVarMapIntVec;
        std::map<std::vector<long> *, std::vector<std::vector<long>> *> BookVarMapLongVec;
        std::map<std::vector<double> *, std::vector<std::vector<double>> *> BookVarMapDoubleVec;
        std::map<std::vector<bool> *, std::vector<std::vector<bool>> *> BookVarMapBoolVec;
        std::map<std::vector<TString> *, std::vector<std::vector<TString>> *> BookVarMapTStringVec;
        std::map<std::vector<TLorentzVector> *, std::vector<std::vector<TLorentzVector>> *> BookVarMapTLorentzVectorVec;

        std::map<int *, std::vector<int> *> SetVarMapInt;
        std::map<long *, std::vector<long> *> SetVarMapLong;
        std::map<double *, std::vector<double> *> SetVarMapDouble;
        std::map<bool *, std::vector<bool> *> SetVarMapBool;
        std::map<TString *, std::vector<TString> *> SetVarMapTString;
        std::map<TLorentzVector *, std::vector<TLorentzVector> *> SetVarMapTLorentzVector;
        std::map<std::vector<int> *, std::vector<std::vector<int>> *> SetVarMapIntVec;
        std::map<std::vector<long> *, std::vector<std::vector<long>> *> SetVarMapLongVec;
        std::map<std::vector<double> *, std::vector<std::vector<double>> *> SetVarMapDoubleVec;
        std::map<std::vector<bool> *, std::vector<std::vector<bool>> *> SetVarMapBoolVec;
        std::map<std::vector<TString> *, std::vector<std::vector<TString>> *> SetVarMapTStringVec;
        std::map<std::vector<TLorentzVector> *, std::vector<std::vector<TLorentzVector>> *> SetVarMapTLorentzVectorVec;

        int size = 0;
    };

    inline int EventVector::Size()
    {
        return size;
    }
} // namespace NAGASH
