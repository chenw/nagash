#ifndef NAGASH_LOOPHEPMC_HEADER
#define NAGASH_LOOPHEPMC_HEADER

#ifdef NAGASH_HEPMC
// The following should be defined with HepMC3

#include "NAGASH/ConfigTool.h"
#include "NAGASH/MSGTool.h"
#include "NAGASH/Job.h"
#include "NAGASH/EventVector.h"

#include "HepMC3/GenEvent.h"
#include "HepMC3/Reader.h"
#include "HepMC3/ReaderAscii.h"
#include "HepMC3/ReaderAsciiHepMC2.h"
#include "HepMC3/ReaderRoot.h"
#include "HepMC3/ReaderRootTree.h"
#include "HepMC3/ReaderLHEF.h"
#include "HepMC3/ReaderHEPEVT.h"

#include "TString.h"
namespace NAGASH
{
    class LoopHepMC : public Job
    {
    public:
        LoopHepMC(const ConfigTool &c, const TString &type);
        virtual ~LoopHepMC();
        virtual StatusCode InitializeData() = 0;
        virtual StatusCode InitializeUser() = 0;
        virtual StatusCode Execute() = 0;
        virtual StatusCode Finalize() = 0;
        StatusCode OpenHepMCFile(const TString &);
        StatusCode Run();
        void SetEventVector(std::shared_ptr<EventVector> ev);

        template <typename LE>
        static StatusCode Process(const ConfigTool &, const TString &, std::shared_ptr<ResultGroup>, std::shared_ptr<ResultGroup>, const TString &);

    protected:
        std::shared_ptr<HepMC3::Reader> ReaderUser();
        int CurrentEntry();
        const TString &InputFileName();
        std::shared_ptr<EventVector> EventVectorUser();
        HepMC3::GenEvent *EventUser();

    private:
        std::shared_ptr<HepMC3::Reader> fReader = nullptr;
        int NEventsToLoop = 0;
        std::shared_ptr<EventVector> eventvector;
        TString inputfilename;
        int entry = -1;
        TString hepmctype;
        HepMC3::GenEvent evt;
    };

    inline LoopHepMC::LoopHepMC(const ConfigTool &c, const TString &type) : Job(c) { hepmctype = type; }
    inline std::shared_ptr<HepMC3::Reader> LoopHepMC::ReaderUser() { return fReader; }
    inline int LoopHepMC::CurrentEntry() { return entry; }
    inline void LoopHepMC::SetEventVector(std::shared_ptr<EventVector> ev) { eventvector = ev; }
    inline std::shared_ptr<EventVector> LoopHepMC::EventVectorUser() { return eventvector; }
    inline const TString &LoopHepMC::InputFileName() { return inputfilename; }
    inline HepMC3::GenEvent *LoopHepMC::EventUser() { return &evt; }

    template <typename LE>
    StatusCode LoopHepMC::Process(const ConfigTool &config_child, const TString &filetype, std::shared_ptr<ResultGroup> result, std::shared_ptr<ResultGroup> wholeresult, const TString &inputfilename)
    {
        static_assert(!std::is_pointer<LE>::value, "LoopHepMC::Process : type can not be a pointer");
        static_assert(std::is_base_of<LoopHepMC, LE>::value, "LoopHepMC::Process : Input type must be or be inherited from LoopHepMC class");

        Job::DefaultMutex.lock();
        auto childLE = std::make_shared<LE>(config_child, filetype);
        if (childLE->OpenHepMCFile(inputfilename) == StatusCode::FAILURE)
        {
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        auto msg_child = childLE->MSGUser();
        TString threadsuffix;
        if (childLE->JobNumber() >= 0)
            threadsuffix.Form("(Job %d)", childLE->JobNumber());

        msg_child->StartTitle("LE::InitializeData" + threadsuffix);
        if (childLE->InitializeData() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();

        msg_child->StartTitle("LE::InitializeUser" + threadsuffix);
        if (childLE->InitializeUser() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();

        Job::DefaultMutex.unlock();

        msg_child->StartTitle("LE::Run" + threadsuffix);
        if (childLE->Run() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            Job::DefaultMutex.lock();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();

        Job::DefaultMutex.lock();
        // Add Child Result to Mother
        msg_child->StartTitle("LE::Finalize" + threadsuffix);
        if (childLE->Finalize() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();
        wholeresult->Merge(childLE->ResultGroupUser());
        childLE.reset();
        Job::DefaultMutex.unlock();

        return StatusCode::SUCCESS;
    }

} // namespace NAGASH

#endif
#endif
