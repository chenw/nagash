//***************************************************************************************
/// @file LoopEvent.cxx
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#include "NAGASH/LoopEvent.h"

using namespace NAGASH;
using namespace std;

/// @brief open the ROOT file with the given filename
StatusCode LoopEvent::OpenRootFile(const TString &filename)
{
    infile = new TFile(filename, "READ");
    inputrootfilename = filename;

    if (infile == nullptr)
    {
        MSGUser()->StartTitle("OpenRootFile");
        MSGUser()->MSG(MSGLevel::ERROR, "input ROOT file ", filename, " does not exist");
        MSGUser()->EndTitle();
        return StatusCode::FAILURE;
    }

    if (infile->IsZombie())
    {
        MSGUser()->StartTitle("OpenRootFile");
        MSGUser()->MSG(MSGLevel::ERROR, "input ROOT file ", filename, " is corrupted");
        MSGUser()->EndTitle();
        delete infile;
        infile = nullptr;
        return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
}

StatusCode LoopEvent::Run()
{
    Job::DefaultMutex.lock();
    TimerUser()->Record("(Start Run)");
    MSGUser()->MSG(MSGLevel::INFO, "Start Loop File ", InputRootFileName());
    long Nentries = RootTreeUser()->GetEntries();

    auto originlevel = MSGUser()->OutputLevel;
    MSGUser()->OutputLevel = MSGLevel::SILENT;
    long EvtMax, EvtMin;
    bool EvtMaxExist = true, EvtMinExist = true;
    EvtMax = ConfigUser()->GetPar<long>("EvtMax", &EvtMaxExist);
    EvtMin = ConfigUser()->GetPar<long>("EvtMin", &EvtMinExist);
    if (!EvtMaxExist)
        EvtMax = Nentries - 1;
    if (!EvtMinExist)
        EvtMin = 0;
    MSGUser()->OutputLevel = originlevel;

    if (EvtMin > EvtMax)
        std::swap(EvtMin, EvtMax);

    EvtMax = std::min(EvtMax, Nentries - 1);
    EvtMin = std::max(0L, EvtMin);
    MSGUser()->MSG(MSGLevel::INFO, "Will loop from ", EvtMin, " to ", EvtMax, " entry in the file (", Nentries, " entries in total)");
    Job::DefaultMutex.unlock();

    for (int ie = EvtMin; ie <= EvtMax; ie++)
    {
        entry = ie;
        TString num;
        num.Form("(Execute Event %d)", ie);
        auto titlegurad = MSGUser()->StartTitleWithGuard(num);
        if (RootTreeUser()->GetEntry(ie) <= 0)
        {
            MSGUser()->MSG(MSGLevel::WARNING, "Entry ", ie, " is corrupted");
            continue;
        }
        // should add StatusCode::RECOVERABLE branch
        if (Execute() != StatusCode::SUCCESS)
        {
            MSGUser()->MSG(MSGLevel::ERROR, " Failed here, please check");
            return StatusCode::FAILURE;
        }
    }
    TimerUser()->Record("(End Run)");

    Job::DefaultMutex.lock();
    TimerUser()->Duration("(Start Run)", "(End Run)");
    // fChain->PrintCacheStats();
    delete fChain;
    delete infile;
    fChain = nullptr;
    infile = nullptr;
    Job::DefaultMutex.unlock();

    return StatusCode::SUCCESS;
}

LoopEvent::~LoopEvent()
{
    if (fChain != nullptr)
    {
        delete fChain;
    }

    if (infile != nullptr)
    {
        delete infile;
    }
}
