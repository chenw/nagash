//***************************************************************************************
/// @file HistTool.h
/// @author Luxin Zhang, Wenhao Ma, Zihan Zhao
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Uncertainty.h"

namespace NAGASH
{
    /// @ingroup Histograms ToolClasses
    /// @class NAGASH::HistTool HistTool.h "NAGASH/HistTool.h"
    /// @brief provide tools for manipulating ROOT histograms
    class HistTool : public Tool
    {
    public:
        HistTool(std::shared_ptr<MSGTool> MSG) : Tool(MSG), unc(MSG){}; ///< @brief constructor
        // Hist Link
        // use TH1 To make it possible for TH1D TH2D TH3D etc.
        void ProcessHistLink(const TString &type, const std::vector<TH1 *> &input, TH1 *res);
        void ProcessHistLink(std::function<double(const std::vector<double> &)>, std::function<double(const std::vector<double> &, const std::vector<double> &)>, const std::vector<TH1 *> &input, TH1 *res);
        void ProcessHistLink(std::function<void(const std::vector<TH1 *> &, TH1 *)>, const std::vector<TH1 *> &input, TH1 *res);
        void Asymmetry(TH1 *A, TH1 *B, TH1 *Asy);
        void Delta(TH1 *A, TH1 *B, TH1 *D, bool dosep = false);
        void Ratio(TH1 *A, TH1 *B, TH1 *R, bool dosep = false);
        void Chi2(TH1 *A, TH1 *B, TH1 *C);
        void Pull(TH1 *A, TH1 *B, TH1 *P);
        double Chi2(TH1D *A, TH1D *B, TH2D *CorrA = nullptr, TH2D *CorrB = nullptr);
        void ConvertCovToCorr(TH2D *covar, TH2D *corr);
        void ConvertCorrToCov(TH2D *corr, TH1D *h, TH2D *cov);
        void ConvertCovToError(TH2D *hcov, TH1D *herr);

        // vector hist convertion
        // maybe write a version of Rvalue references for better performance
        void ConvertVectorToTH1D(TH1D *h1, const std::vector<double> &v1, const std::vector<double> &v1err = std::vector<double>());
        void ConvertVectorToTH2D(TH2D *h2, const std::vector<std::vector<double>> &v2, const std::vector<std::vector<double>> &v2err = std::vector<std::vector<double>>());
        void ConvertVectorToTH3D(TH3D *h3, const std::vector<std::vector<std::vector<double>>> &v3, const std::vector<std::vector<std::vector<double>>> &v3err = std::vector<std::vector<std::vector<double>>>());
        std::vector<double> ConvertTH1DToVector(TH1D *h1d);
        std::vector<double> ConvertTH1DErrorToVector(TH1D *h1d);
        std::vector<std::vector<double>> ConvertTH2DToVector(TH2D *h2d, const TString &priority = "XY");
        std::vector<std::vector<double>> ConvertTH2DErrorToVector(TH2D *h2d, const TString &priority = "XY");
        std::vector<std::vector<std::vector<double>>> ConvertTH3DToVector(TH3D *h3d, const TString &priority = "XYZ");
        std::vector<std::vector<std::vector<double>>> ConvertTH3DErrorToVector(TH3D *h3d, const TString &priority = "XYZ");

        // dimension change
        void ConvertVectorTH1DToTH1D(const std::vector<TH1D *> &vh1d, TH1D *targeth1d);
        void ConvertTH2DToVectorTH1D(TH2D *h2d, const std::vector<TH1D *> &vh1d, const TString &priority = "XY");
        void ConvertTH3DToVectorTH1D(TH3D *h3d, const std::vector<std::vector<TH1D *>> &vh1d, const TString &priority = "XYZ");

        // convert To matrix
        TMatrixD ConvertTH2DToMatrix(TH2D *h2d);
        void ConvertMatrixToTH2D(const TMatrixD &mat, TH2D *h2d);

        template <typename T>
        static std::vector<T> ConvertVector2DToVector1D(const std::vector<std::vector<T>> &v2d);
        template <typename T>
        static std::vector<T> ConvertVector2DToVector1D(std::vector<std::vector<T>> &&v2d);

    protected:
        Uncertainty unc;
    };

    /// @brief Flatten 2D vector to a 1D vector.
    /// @tparam T type of the vector.
    /// @param v2d input 2D vector with const reference.
    /// @return flattened 1D vector.
    template <typename T>
    inline std::vector<T> HistTool::ConvertVector2DToVector1D(const std::vector<std::vector<T>> &v2d)
    {
        std::vector<T> returnv1d;
        for (auto &v1d : v2d)
            for (auto &content : v1d)
                returnv1d.emplace_back(content);

        return returnv1d;
    }

    /// @brief Flatten 2D vector to a 1D vector.
    /// @tparam T type of the vector.
    /// @param v2d input 2D vector with right value reference.
    /// @return flattened 1D vector.
    template <typename T>
    inline std::vector<T> HistTool::ConvertVector2DToVector1D(std::vector<std::vector<T>> &&v2d)
    {
        std::vector<T> returnv1d;
        for (auto &v1d : v2d)
            for (auto &content : v1d)
                returnv1d.emplace_back(content);

        return returnv1d;
    }

} // namespace NAGASH
