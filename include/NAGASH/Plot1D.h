//***************************************************************************************
/// @file Plot1D.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/NGHist.h"

namespace NAGASH
{
    /// @ingroup Histograms
    /// @brief alias for frequently used NGHist<TH1D>.
    using Plot1D = NGHist<TH1D>;

    /// @brief partial specialization for the constructor of NGHist<TH1D>.
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name the name of the histogram.
    /// @param filename  the file name of the histogram to be saved in.
    /// @param bindiv the binning of the histogram.
    template <>
    template <>
    inline NGHist<TH1D>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const std::vector<double> &bindiv)
        : HistBase(MSG, c, name, filename)
    {
        Nominal = new TH1D(GetResultName(), GetResultName(), (int)bindiv.size() - 1, bindiv.data());
        Nominal->SetDirectory(0);
        Nominal->Sumw2();
        FillHist = Nominal;
    }

}
