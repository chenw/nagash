//***************************************************************************************
/// @file EventVector.cxx
/// @author Wenhao Ma
//***************************************************************************************

#include "NAGASH/EventVector.h"

using namespace NAGASH;

void EventVector::Combine(std::shared_ptr<Result> result)
{
    auto subEV = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
    if (subEV != nullptr)
    {
        for (auto &t : ValueMapInt)
        {
            auto re = subEV->ValueMapInt.find(t.first);
            if (re != subEV->ValueMapInt.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapLong)
        {
            auto re = subEV->ValueMapLong.find(t.first);
            if (re != subEV->ValueMapLong.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapDouble)
        {
            auto re = subEV->ValueMapDouble.find(t.first);
            if (re != subEV->ValueMapDouble.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapBool)
        {
            auto re = subEV->ValueMapBool.find(t.first);
            if (re != subEV->ValueMapBool.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapTString)
        {
            auto re = subEV->ValueMapTString.find(t.first);
            if (re != subEV->ValueMapTString.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapTLorentzVector)
        {
            auto re = subEV->ValueMapTLorentzVector.find(t.first);
            if (re != subEV->ValueMapTLorentzVector.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapIntVec)
        {
            auto re = subEV->ValueMapIntVec.find(t.first);
            if (re != subEV->ValueMapIntVec.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapLongVec)
        {
            auto re = subEV->ValueMapLongVec.find(t.first);
            if (re != subEV->ValueMapLongVec.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapDoubleVec)
        {
            auto re = subEV->ValueMapDoubleVec.find(t.first);
            if (re != subEV->ValueMapDoubleVec.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapBoolVec)
        {
            auto re = subEV->ValueMapBoolVec.find(t.first);
            if (re != subEV->ValueMapBoolVec.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapTStringVec)
        {
            auto re = subEV->ValueMapTStringVec.find(t.first);
            if (re != subEV->ValueMapTStringVec.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }
        for (auto &t : ValueMapTLorentzVectorVec)
        {
            auto re = subEV->ValueMapTLorentzVectorVec.find(t.first);
            if (re != subEV->ValueMapTLorentzVectorVec.end())
            {
                t.second.insert(t.second.end(), re->second.begin(), re->second.end());
            }
        }

        this->size += subEV->size;
    }
}

void EventVector::BookVariable(const TString &name, int *value)
{
    if (ValueMapInt.find(name) == ValueMapInt.end())
    {
        ValueMapInt.emplace(std::pair<TString, std::vector<int>>(name, std::vector<int>{}));
        auto temp = &(ValueMapInt.find(name)->second);
        BookVarMapInt.emplace(std::pair<int *, std::vector<int> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, long *value)
{
    if (ValueMapLong.find(name) == ValueMapLong.end())
    {
        ValueMapLong.emplace(std::pair<TString, std::vector<long>>(name, std::vector<long>{}));
        auto temp = &(ValueMapLong.find(name)->second);
        BookVarMapLong.emplace(std::pair<long *, std::vector<long> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, double *value)
{
    if (ValueMapDouble.find(name) == ValueMapDouble.end())
    {
        ValueMapDouble.emplace(std::pair<TString, std::vector<double>>(name, std::vector<double>{}));
        auto temp = &(ValueMapDouble.find(name)->second);
        BookVarMapDouble.emplace(std::pair<double *, std::vector<double> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, bool *value)
{
    if (ValueMapBool.find(name) == ValueMapBool.end())
    {
        ValueMapBool.emplace(std::pair<TString, std::vector<bool>>(name, std::vector<bool>{}));
        auto temp = &(ValueMapBool.find(name)->second);
        BookVarMapBool.emplace(std::pair<bool *, std::vector<bool> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, TString *value)
{
    if (ValueMapTString.find(name) == ValueMapTString.end())
    {
        ValueMapTString.emplace(std::pair<TString, std::vector<TString>>(name, std::vector<TString>{}));
        auto temp = &(ValueMapTString.find(name)->second);
        BookVarMapTString.emplace(std::pair<TString *, std::vector<TString> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, TLorentzVector *value)
{
    if (ValueMapTLorentzVector.find(name) == ValueMapTLorentzVector.end())
    {
        ValueMapTLorentzVector.emplace(std::pair<TString, std::vector<TLorentzVector>>(name, std::vector<TLorentzVector>{}));
        auto temp = &(ValueMapTLorentzVector.find(name)->second);
        BookVarMapTLorentzVector.emplace(std::pair<TLorentzVector *, std::vector<TLorentzVector> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, std::vector<int> *value)
{
    if (ValueMapIntVec.find(name) == ValueMapIntVec.end())
    {
        ValueMapIntVec.emplace(std::pair<TString, std::vector<std::vector<int>>>(name, std::vector<std::vector<int>>()));
        auto temp = &(ValueMapIntVec.find(name)->second);
        BookVarMapIntVec.emplace(std::pair<std::vector<int> *, std::vector<std::vector<int>> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, std::vector<long> *value)
{
    if (ValueMapLongVec.find(name) == ValueMapLongVec.end())
    {
        ValueMapLongVec.emplace(std::pair<TString, std::vector<std::vector<long>>>(name, std::vector<std::vector<long>>()));
        auto temp = &(ValueMapLongVec.find(name)->second);
        BookVarMapLongVec.emplace(std::pair<std::vector<long> *, std::vector<std::vector<long>> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, std::vector<double> *value)
{
    if (ValueMapDoubleVec.find(name) == ValueMapDoubleVec.end())
    {
        ValueMapDoubleVec.emplace(std::pair<TString, std::vector<std::vector<double>>>(name, std::vector<std::vector<double>>()));
        auto temp = &(ValueMapDoubleVec.find(name)->second);
        BookVarMapDoubleVec.emplace(std::pair<std::vector<double> *, std::vector<std::vector<double>> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, std::vector<bool> *value)
{
    if (ValueMapBoolVec.find(name) == ValueMapBoolVec.end())
    {
        ValueMapBoolVec.emplace(std::pair<TString, std::vector<std::vector<bool>>>(name, std::vector<std::vector<bool>>()));
        auto temp = &(ValueMapBoolVec.find(name)->second);
        BookVarMapBoolVec.emplace(std::pair<std::vector<bool> *, std::vector<std::vector<bool>> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, std::vector<TString> *value)
{
    if (ValueMapTStringVec.find(name) == ValueMapTStringVec.end())
    {
        ValueMapTStringVec.emplace(std::pair<TString, std::vector<std::vector<TString>>>(name, std::vector<std::vector<TString>>()));
        auto temp = &(ValueMapTStringVec.find(name)->second);
        BookVarMapTStringVec.emplace(std::pair<std::vector<TString> *, std::vector<std::vector<TString>> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::BookVariable(const TString &name, std::vector<TLorentzVector> *value)
{
    if (ValueMapTLorentzVectorVec.find(name) == ValueMapTLorentzVectorVec.end())
    {
        ValueMapTLorentzVectorVec.emplace(std::pair<TString, std::vector<std::vector<TLorentzVector>>>(name, std::vector<std::vector<TLorentzVector>>()));
        auto temp = &(ValueMapTLorentzVectorVec.find(name)->second);
        BookVarMapTLorentzVectorVec.emplace(std::pair<std::vector<TLorentzVector> *, std::vector<std::vector<TLorentzVector>> *>(value, temp));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::BookVariable");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " has already been booked, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, int *value)
{
    auto re = ValueMapInt.find(name);
    if (re != ValueMapInt.end())
    {
        SetVarMapInt.emplace(std::pair<int *, std::vector<int> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, long *value)
{
    auto re = ValueMapLong.find(name);
    if (re != ValueMapLong.end())
    {
        SetVarMapLong.emplace(std::pair<long *, std::vector<long> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, double *value)
{
    auto re = ValueMapDouble.find(name);
    if (re != ValueMapDouble.end())
    {
        SetVarMapDouble.emplace(std::pair<double *, std::vector<double> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, bool *value)
{
    auto re = ValueMapBool.find(name);
    if (re != ValueMapBool.end())
    {
        SetVarMapBool.emplace(std::pair<bool *, std::vector<bool> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, TString *value)
{
    auto re = ValueMapTString.find(name);
    if (re != ValueMapTString.end())
    {
        SetVarMapTString.emplace(std::pair<TString *, std::vector<TString> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, TLorentzVector *value)
{
    auto re = ValueMapTLorentzVector.find(name);
    if (re != ValueMapTLorentzVector.end())
    {
        SetVarMapTLorentzVector.emplace(std::pair<TLorentzVector *, std::vector<TLorentzVector> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, std::vector<int> *value)
{
    auto re = ValueMapIntVec.find(name);
    if (re != ValueMapIntVec.end())
    {
        SetVarMapIntVec.emplace(std::pair<std::vector<int> *, std::vector<std::vector<int>> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, std::vector<long> *value)
{
    auto re = ValueMapLongVec.find(name);
    if (re != ValueMapLongVec.end())
    {
        SetVarMapLongVec.emplace(std::pair<std::vector<long> *, std::vector<std::vector<long>> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, std::vector<double> *value)
{
    auto re = ValueMapDoubleVec.find(name);
    if (re != ValueMapDoubleVec.end())
    {
        SetVarMapDoubleVec.emplace(std::pair<std::vector<double> *, std::vector<std::vector<double>> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, std::vector<bool> *value)
{
    auto re = ValueMapBoolVec.find(name);
    if (re != ValueMapBoolVec.end())
    {
        SetVarMapBoolVec.emplace(std::pair<std::vector<bool> *, std::vector<std::vector<bool>> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, std::vector<TString> *value)
{
    auto re = ValueMapTStringVec.find(name);
    if (re != ValueMapTStringVec.end())
    {
        SetVarMapTStringVec.emplace(std::pair<std::vector<TString> *, std::vector<std::vector<TString>> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::SetVariableAddress(const TString &name, std::vector<TLorentzVector> *value)
{
    auto re = ValueMapTLorentzVectorVec.find(name);
    if (re != ValueMapTLorentzVectorVec.end())
    {
        SetVarMapTLorentzVectorVec.emplace(std::pair<std::vector<TLorentzVector> *, std::vector<std::vector<TLorentzVector>> *>(value, &(re->second)));
    }
    else
    {
        MSGUser()->StartTitle("EventVector::SetVariableAddress");
        MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
    }
}

void EventVector::GetEvent(int ie)
{
    if (size < ie || ie < 0)
    {
        MSGUser()->StartTitle("EventVector::GetEvent");
        MSGUser()->MSG(MSGLevel::ERROR, "Event ", ie, " doesn't exist, this call will be ignored");
        MSGUser()->EndTitle();
        return;
    }

    for (auto &t : SetVarMapInt)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapLong)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapDouble)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapBool)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapTString)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapTLorentzVector)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapIntVec)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapLongVec)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapDoubleVec)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapBoolVec)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapTStringVec)
        *(t.first) = t.second->at(ie);
    for (auto &t : SetVarMapTLorentzVectorVec)
        *(t.first) = t.second->at(ie);
}

void EventVector::Fill()
{
    for (auto &t : BookVarMapInt)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapLong)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapDouble)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapBool)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapTString)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapTLorentzVector)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapIntVec)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapLongVec)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapDoubleVec)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapBoolVec)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapTStringVec)
        t.second->emplace_back(*(t.first));
    for (auto &t : BookVarMapTLorentzVectorVec)
        t.second->emplace_back(*(t.first));

    size++;
}
