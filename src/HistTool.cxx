//***************************************************************************************
/// @file HistTool.cxx
/// @author Luxin Zhang, Wenhao Ma, Zihan Zhao
//***************************************************************************************

#include "NAGASH/HistTool.h"

using namespace NAGASH;
using namespace std;

/// @brief Create a new histogram based on the input histograms.
/// @param[in] type the kind of the histogram you want to create. Possible values are: `Asymmetry`, `Delta`, `Ratio`, `Chi2` and `Pull`.
/// @param[in] input the input set of histograms.
/// @param[out] res output histogram.
void HistTool::ProcessHistLink(const TString &type, const std::vector<TH1 *> &input, TH1 *res)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ProcessHistLink");
    if (type.TString::EqualTo("Asymmetry"))
    {
        if (input.size() != 2)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Hist Link Asymmetry needs two input hists, but now it is ", input.size());
            return;
        }
        Asymmetry(input[0], input[1], res);
    }
    else if (type.TString::EqualTo("Delta"))
    {
        if (input.size() != 2)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Hist Link Delta needs two input hists, but now it is ", input.size());
            return;
        }
        Delta(input[0], input[1], res);
    }
    else if (type.TString::EqualTo("Ratio"))
    {
        if (input.size() != 2)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Hist Link Ratio needs two input hists, but now it is ", input.size());
            return;
        }
        Ratio(input[0], input[1], res);
    }
    else if (type.TString::EqualTo("Chi2"))
    {
        if (input.size() != 2)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Hist Link Chi2 needs two input hists, but now it is ", input.size());
            return;
        }
        Chi2(input[0], input[1], res);
    }
    else if (type.TString::EqualTo("Pull"))
    {
        if (input.size() != 2)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Hist Link Pull needs two input hists, but now it is ", input.size());
            return;
        }
        Pull(input[0], input[1], res);
    }
    else
        MSGUser()->MSG(MSGLevel::ERROR, "Hist Link ", type.TString::Data(), " not supported!");
}

/// @brief Create a new histogram based on the input histograms.
/// @param[in] contentfunction the function to calculate the new histogram content.
/// @param[in] errorfunction the function to calculate the new histogram error.
/// @param[in] input the input set of histograms.
/// @param[out] res the output histogram.
void HistTool::ProcessHistLink(std::function<double(const std::vector<double> &)> contentfunction, std::function<double(const std::vector<double> &, const std::vector<double> &)> errorfunction, const std::vector<TH1 *> &input, TH1 *res)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("HistTool::ProcessHistLink");
    // check if any hist is nullptr
    for (size_t i = 0; i < input.size(); i++)
        if (!input[i])
        {
            MSGUser()->MSG_ERROR("input hist ", i, " is nullptr");
            return;
        }

    if (!res)
    {
        MSGUser()->MSG_ERROR("result hist is nullptr");
        return;
    }

    // check the size of hists
    int binnumx = res->GetNbinsX(), binnumy = res->GetNbinsY(), binnumz = res->GetNbinsZ();
    for (size_t i = 0; i < input.size(); i++)
        if (input[i]->GetNbinsX() != binnumx || input[i]->GetNbinsY() != binnumy || input[i]->GetNbinsZ() != binnumz)
        {
            MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
            return;
        }

    // fill content and error
    for (int i = 1; i <= binnumx; i++)
        for (int j = 1; j <= binnumy; j++)
            for (int k = 1; k <= binnumz; k++)
            {
                std::vector<double> vcontent(input.size());
                std::vector<double> verror(input.size());
                for (size_t m = 0; m < input.size(); m++)
                {
                    vcontent[m] = input[m]->GetBinContent(input[m]->GetBin(i, j, k));
                    verror[m] = input[m]->GetBinError(input[m]->GetBin(i, j, k));
                }
                res->SetBinContent(res->GetBin(i, j, k), contentfunction(vcontent));
                res->SetBinError(res->GetBin(i, j, k), errorfunction(vcontent, verror));
            }
}

/// @brief Create a new histogram based on the input histograms.
/// @param[in] func the function to calculate the new histogram.
/// @param[in] input the input set of histograms.
/// @param[out] res the output histogram.
void HistTool::ProcessHistLink(std::function<void(const std::vector<TH1 *> &, TH1 *)> func, const std::vector<TH1 *> &input, TH1 *res)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("HistTool::ProcessHistLink");
    // check if any hist is nullptr
    for (size_t i = 0; i < input.size(); i++)
        if (!input[i])
        {
            MSGUser()->MSG_ERROR("input hist ", i, " is nullptr");
            return;
        }

    if (!res)
    {
        MSGUser()->MSG_ERROR("result hist is nullptr");
        return;
    }

    func(input, res);
}

/// @brief Calculate the asymmetry between two histograms.
/// @param[in] A histogram A.
/// @param[in] B histogram B.
/// @param[out] Asy the asymmetry histogram \f$(A+B)/(A-B)\f$.
void HistTool::Asymmetry(TH1 *A, TH1 *B, TH1 *Asy)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::Asymmetry");
    if (A == nullptr || B == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (Asy == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = A->GetNbinsX(), binnumy = A->GetNbinsY(), binnumz = A->GetNbinsZ();
    if (B->GetNbinsX() != binnumx || B->GetNbinsY() != binnumy || B->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return;
    }
    if (Asy->GetNbinsX() != binnumx || Asy->GetNbinsY() != binnumy || Asy->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (int i = 1; i <= A->GetNbinsX(); i++)
    {
        for (int j = 1; j <= A->GetNbinsY(); j++)
        {
            for (int k = 1; k <= A->GetNbinsZ(); k++)
            {
                double ContentA = A->GetBinContent(A->GetBin(i, j, k)), ContentB = B->GetBinContent(B->GetBin(i, j, k));
                double ErrorA = A->GetBinError(A->GetBin(i, j, k)), ErrorB = B->GetBinError(B->GetBin(i, j, k));
                if (ContentA != 0 || ContentB != 0)
                {
                    Asy->SetBinContent(Asy->GetBin(i, j, k), (ContentA - ContentB) / (ContentA + ContentB));
                    Asy->SetBinError(Asy->GetBin(i, j, k), unc.AminusBoverAplusB(ContentA, ContentB, ErrorA, ErrorB));
                }
                else
                {
                    Asy->SetBinContent(Asy->GetBin(i, j, k), 0);
                    Asy->SetBinError(Asy->GetBin(i, j, k), 0);
                }
            }
        }
    }
}

/// @brief Calculate the difference between two histograms.
/// @param[in] A histogram A.
/// @param[in] B histogram B.
/// @param[out] D the difference histogram \f$(A-B)\f$.
/// @param[in] dosep seperate the error or not, if do seperate, ignore A's error.
void HistTool::Delta(TH1 *A, TH1 *B, TH1 *D, bool dosep)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::Delta");
    if (A == nullptr || B == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (D == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = A->GetNbinsX(), binnumy = A->GetNbinsY(), binnumz = A->GetNbinsZ();
    if (B->GetNbinsX() != binnumx || B->GetNbinsY() != binnumy || B->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return;
    }
    if (D->GetNbinsX() != binnumx || D->GetNbinsY() != binnumy || D->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (int i = 1; i <= binnumx; i++)
    {
        for (int j = 1; j <= binnumy; j++)
        {
            for (int k = 1; k <= binnumz; k++)
            {
                double val = A->GetBinContent(A->GetBin(i, j, k)) - B->GetBinContent(B->GetBin(i, j, k));
                double val_err = sqrt(A->GetBinError(A->GetBin(i, j, k)) * A->GetBinError(A->GetBin(i, j, k)) + B->GetBinError(B->GetBin(i, j, k)) * B->GetBinError(B->GetBin(i, j, k)));

                if (A == B && !dosep)
                    val_err = 0;

                if (A == B && dosep)
                    val_err = A->GetBinError(A->GetBin(i, j, k));

                if (A != B && dosep)
                    val_err = B->GetBinError(A->GetBin(i, j, k));

                D->SetBinContent(D->GetBin(i, j, k), val);
                D->SetBinError(D->GetBin(i, j, k), val_err);
            }
        }
    }
}

/// @brief Calculate the ratio between two histograms.
/// @param[in] A histogram A.
/// @param[in] B histogram B.
/// @param[out] R the ratio histogram \f$(A/B)\f$.
/// @param[in] dosep seperate the error or not, if do seperate, ignore A's error.
void HistTool::Ratio(TH1 *A, TH1 *B, TH1 *R, bool dosep)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::Ratio");
    if (A == nullptr || B == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (R == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = A->GetNbinsX(), binnumy = A->GetNbinsY(), binnumz = A->GetNbinsZ();
    if (B->GetNbinsX() != binnumx || B->GetNbinsY() != binnumy || B->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return;
    }
    if (R->GetNbinsX() != binnumx || R->GetNbinsY() != binnumy || R->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    bool NeedWARN = false;

    double val;
    double val_err;
    for (int i = 1; i <= binnumx; i++)
    {
        for (int j = 1; j <= binnumy; j++)
        {
            for (int k = 1; k <= binnumz; k++)
            {
                if (B->GetBinContent(B->GetBin(i, j, k)) == 0)
                {
                    if (A != B)
                        NeedWARN = true;
                    if (A == B)
                    {
                        R->SetBinContent(R->GetBin(i, j, k), 1);
                        R->SetBinError(R->GetBin(i, j, k), 0);
                    }
                    continue;
                }
                else
                {
                    val = A->GetBinContent(A->GetBin(i, j, k)) / B->GetBinContent(B->GetBin(i, j, k));
                    val_err = unc.AoverB(A->GetBinContent(A->GetBin(i, j, k)), B->GetBinContent(B->GetBin(i, j, k)),
                                         A->GetBinError(A->GetBin(i, j, k)), B->GetBinError(B->GetBin(i, j, k)));
                }

                if (A == B && !dosep)
                    val_err = 0;

                if (A == B && dosep)
                    val_err = A->GetBinError(A->GetBin(i, j, k)) / A->GetBinContent(A->GetBin(i, j, k));

                if (A != B && dosep)
                    val_err = unc.AoverB(A->GetBinContent(A->GetBin(i, j, k)), B->GetBinContent(B->GetBin(i, j, k)),
                                         0, B->GetBinError(B->GetBin(i, j, k)));

                R->SetBinContent(R->GetBin(i, j, k), val);
                R->SetBinError(R->GetBin(i, j, k), val_err);
            }
        }
    }

    if (NeedWARN)
        MSGUser()->MSG_WARNING("Can not define a ratio for some bin!");
}

/// @brief Calculate the \f$\chi^2\f$ between two histograms.
/// @param[in] A histogram A.
/// @param[in] B histogram B.
/// @param[out] C \f$(A-B)^2/(\sigma^2(A)+\sigma^2(B))\f$.
void HistTool::Chi2(TH1 *A, TH1 *B, TH1 *C)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::Chi2");
    if (A == nullptr || B == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (C == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = A->GetNbinsX(), binnumy = A->GetNbinsY(), binnumz = A->GetNbinsZ();
    if (B->GetNbinsX() != binnumx || B->GetNbinsY() != binnumy || B->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return;
    }
    if (C->GetNbinsX() != binnumx || C->GetNbinsY() != binnumy || C->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    bool NeedWARN = false;
    for (int i = 1; i <= A->GetNbinsX(); i++)
    {
        for (int j = 1; j <= A->GetNbinsY(); j++)
        {
            for (int k = 1; k <= A->GetNbinsZ(); k++)
            {
                double ContentA = A->GetBinContent(A->GetBin(i, j, k)), ContentB = B->GetBinContent(B->GetBin(i, j, k));
                double ErrorA = A->GetBinError(A->GetBin(i, j, k)), ErrorB = B->GetBinError(B->GetBin(i, j, k));
                if (ErrorA != 0 || ErrorB != 0)
                    C->SetBinContent(C->GetBin(i, j, k), pow((ContentA - ContentB) / unc.AminusB(ErrorA, ErrorB), 2));
                else
                {
                    C->SetBinContent(C->GetBin(i, j, k), 0);
                    NeedWARN = true;
                }
                C->SetBinError(C->GetBin(i, j, k), 0);
            }
        }
    }

    if (NeedWARN)
        MSGUser()->MSG_WARNING("Can not define a chi2 for some bin!");
}

/// @brief Calculate the pull between two histograms.
/// @param[in] A histogram A.
/// @param[in] B histogram B.
/// @param[out] P pull histogram \f$(A-B)/\sqrt{\sigma^2(A)+\sigma^2(B)}\f$.
void HistTool::Pull(TH1 *A, TH1 *B, TH1 *P)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::Pull");
    if (A == nullptr || B == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (P == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = A->GetNbinsX(), binnumy = A->GetNbinsY(), binnumz = A->GetNbinsZ();
    if (B->GetNbinsX() != binnumx || B->GetNbinsY() != binnumy || B->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return;
    }
    if (P->GetNbinsX() != binnumx || P->GetNbinsY() != binnumy || P->GetNbinsZ() != binnumz)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    bool NeedWARN = false;
    for (int i = 1; i <= A->GetNbinsX(); i++)
    {
        for (int j = 1; j <= A->GetNbinsY(); j++)
        {
            for (int k = 1; k <= A->GetNbinsZ(); k++)
            {
                double ContentA = A->GetBinContent(A->GetBin(i, j, k)), ContentB = B->GetBinContent(B->GetBin(i, j, k));
                double ErrorA = A->GetBinError(A->GetBin(i, j, k)), ErrorB = B->GetBinError(B->GetBin(i, j, k));
                if (ErrorA != 0 || ErrorB != 0)
                    P->SetBinContent(P->GetBin(i, j, k), (ContentA - ContentB) / unc.AminusB(ErrorA, ErrorB));
                else
                {
                    P->SetBinContent(P->GetBin(i, j, k), 0);
                    NeedWARN = true;
                }
                P->SetBinError(P->GetBin(i, j, k), 0);
            }
        }
    }
    if (NeedWARN)
        MSGUser()->MSG_WARNING("Can not define a pull for some bin!");
}

/// @brief Calculate the \f$\chi^2\f$ between two histograms.
/// @param A histogram A.
/// @param B histogram B.
/// @param CorrA covariance matrix of A.
/// @param CorrB covariance matrix of B.
/// @return \f$\chi^2\f$ value.
double HistTool::Chi2(TH1D *A, TH1D *B, TH2D *CorrA, TH2D *CorrB)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::Chi2");
    if (A == nullptr || B == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return 0;
    }
    int binnum = A->GetXaxis()->GetNbins();
    if (B->GetXaxis()->GetNbins() != binnum)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return 0;
    }

    if (CorrA)
        if (CorrA->GetNbinsX() != binnum || CorrA->GetNbinsY() != binnum)
        {
            MSGUser()->MSG_ERROR("Input correlation histogram definition incorrect!!!");
            return 0;
        }

    if (CorrB)
        if (CorrB->GetNbinsX() != binnum || CorrB->GetNbinsY() != binnum)
        {
            MSGUser()->MSG_ERROR("Input correlation histogram definition incorrect!!!");
            return 0;
        }

    TMatrixD covA(binnum, binnum);
    TMatrixD covB(binnum, binnum);
    TMatrixD totalcov(binnum, binnum);
    if (CorrA)
        covA = ConvertTH2DToMatrix(CorrA);
    else
    {
        for (int i = 0; i < binnum; i++)
            for (int j = 0; j < binnum; j++)
            {
                if (i != j)
                    covA(i, j) = 0;
                else
                    covA(i, j) = 1;
            }
    }

    if (CorrB)
        covB = ConvertTH2DToMatrix(CorrB);
    else
    {
        for (int i = 0; i < binnum; i++)
            for (int j = 0; j < binnum; j++)
            {
                if (i != j)
                    covB(i, j) = 0;
                else
                    covB(i, j) = 1;
            }
    }

    for (int i = 0; i < binnum; i++)
        for (int j = 0; j < binnum; j++)
        {
            covA(i, j) *= A->GetBinError(i + 1) * A->GetBinError(j + 1);
            covB(i, j) *= B->GetBinError(i + 1) * B->GetBinError(j + 1);
        }
    totalcov = covA + covB;

    auto invertedcov = totalcov.InvertFast();

    // need to be modified to make correlation possible
    double chi2 = 0;
    for (int ibin = 0; ibin < binnum; ibin++)
        for (int jbin = 0; jbin < binnum; jbin++)
            chi2 += (A->GetBinContent(ibin + 1) - B->GetBinContent(ibin + 1)) * (A->GetBinContent(jbin + 1) - B->GetBinContent(jbin + 1)) * invertedcov(ibin, jbin);

    return chi2;
}

/// @brief Convert covariance matrix to correlation matrix
/// @param[in] covar input covariance matrix.
/// @param[out] corr output correlation matrix.
void HistTool::ConvertCovToCorr(TH2D *covar, TH2D *corr)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertCovToCorr");
    if (covar == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (corr == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = covar->GetXaxis()->GetNbins(), binnumy = covar->GetYaxis()->GetNbins();
    if (binnumx != binnumy)
    {
        MSGUser()->MSG_ERROR("Covariance matrix needs to be square matrix!!!");
        return;
    }
    if (corr->GetXaxis()->GetNbins() != binnumx || corr->GetYaxis()->GetNbins() != binnumy)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (int i = 0; i < covar->GetXaxis()->GetNbins(); i++)
    {
        for (int j = 0; j < covar->GetYaxis()->GetNbins(); j++)
        {
            if (covar->GetBinContent(i + 1, i + 1) == 0 || covar->GetBinContent(j + 1, j + 1) == 0)
                corr->SetBinContent(i + 1, j + 1, 0);
            else
                corr->SetBinContent(i + 1, j + 1, covar->GetBinContent(i + 1, j + 1) / sqrt(covar->GetBinContent(i + 1, i + 1) * covar->GetBinContent(j + 1, j + 1)));
            corr->SetBinError(i + 1, j + 1, 0);
        }
    }
}

/// @brief Convert covariance matrix to error histogram.
/// @param[in] hcov the input covariance matrix.
/// @param[out] herr the output error.
void HistTool::ConvertCovToError(TH2D *hcov, TH1D *herr)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertCovToError");
    if (hcov == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (herr == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = hcov->GetXaxis()->GetNbins(), binnumy = hcov->GetYaxis()->GetNbins();
    if (binnumx != binnumy)
    {
        MSGUser()->MSG_ERROR("Covariance matrix needs to be square matrix!!!");
        return;
    }
    if (herr->GetXaxis()->GetNbins() != binnumx)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (int i = 1; i <= hcov->GetNbinsX(); i++)
    {
        herr->SetBinContent(i, sqrt(hcov->GetBinContent(i, i)));
        herr->SetBinError(i, 0);
    }
}

/// @brief Convert correlation matrix to covariance matrix.
/// @param[in] corr Input correlation matrix.
/// @param[in] h the histogram whose error would be used to calculate the covariance matrix.
/// @param[out] cov the output covariance matrix.
void HistTool::ConvertCorrToCov(TH2D *corr, TH1D *h, TH2D *cov)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertCorrToCov");
    if (corr == nullptr || h == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (cov == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int binnumx = corr->GetXaxis()->GetNbins(), binnumy = corr->GetYaxis()->GetNbins();
    if (binnumx != binnumy)
    {
        MSGUser()->MSG_ERROR("Correlation coefficient matrix needs to be square matrix!!!");
        return;
    }
    if (h->GetXaxis()->GetNbins() != binnumx)
    {
        MSGUser()->MSG_ERROR("Input hist definition incorrect!!!");
        return;
    }
    if (cov->GetXaxis()->GetNbins() != binnumx || cov->GetYaxis()->GetNbins() != binnumy)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (int i = 1; i <= cov->GetNbinsX(); i++)
    {
        for (int j = 1; j <= cov->GetNbinsY(); j++)
        {
            cov->SetBinContent(i, j, corr->GetBinContent(i, j) * h->GetBinError(i) * h->GetBinError(j));
            cov->SetBinError(i, j, 0);
        }
    }
}

/// @brief Convert vectors to a histogram.
/// @param[out] h1 output histogram.
/// @param[in] v1 vector of the contents of the histogram.
/// @param[in] v1err vector of the errors of the histogram.
void HistTool::ConvertVectorToTH1D(TH1D *h1, const std::vector<double> &v1, const std::vector<double> &v1err)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertVectorToTH1D");
    if (v1.empty())
    {
        MSGUser()->MSG_ERROR("Input vector is empty!!!");
        return;
    }
    if (h1 == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int vecnum1 = v1.size();
    if (vecnum1 != h1->GetXaxis()->GetNbins())
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (size_t i = 0; i < v1.size(); i++)
    {
        h1->SetBinContent(i + 1, v1[i]);
        h1->SetBinError(i + 1, 0);
    }

    for (size_t i = 0; i < v1err.size(); i++)
    {
        h1->SetBinError(i + 1, v1err[i]);
    }
}

/// @brief Convert vectors to a 2D histogram.
/// @param[out] h2 output histogram.
/// @param[in] v2 the contents of the histogram.
/// @param[in] v2err the errors of the histogram.
void HistTool::ConvertVectorToTH2D(TH2D *h2, const std::vector<std::vector<double>> &v2, const std::vector<std::vector<double>> &v2err)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertVectorToTH2D");
    if (v2.empty())
    {
        MSGUser()->MSG_ERROR("Input vector is empty!!!");
        return;
    }
    if (h2 == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int v2numx = v2.size();
    int binnumx = h2->GetXaxis()->GetNbins(), binnumy = h2->GetYaxis()->GetNbins();
    if (v2numx != binnumx)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (size_t i = 0; i < v2.size(); i++)
    {
        if ((int)v2.at(i).size() != binnumy)
        {
            MSGUser()->MSG_ERROR("Result hist definition incorrect or input vector incorrect!!!");
            return;
        }
    }

    for (size_t i = 0; i < v2.size(); i++)
    {
        for (size_t j = 0; j < v2.at(i).size(); j++)
        {
            h2->SetBinContent(i + 1, j + 1, v2[i][j]);
            h2->SetBinError(i + 1, j + 1, 0);
        }
    }

    for (size_t i = 0; i < v2err.size(); i++)
    {
        for (size_t j = 0; j < v2err.at(i).size(); j++)
        {
            h2->SetBinError(i + 1, j + 1, v2err[i][j]);
        }
    }
}

/// @brief Convert vectors to a 3D histogram.
/// @param[out] h3 output histogram.
/// @param[in] v3 the contents of the histogram.
/// @param[in] v3err the errors of the histogram.
void HistTool::ConvertVectorToTH3D(TH3D *h3, const std::vector<std::vector<std::vector<double>>> &v3, const std::vector<std::vector<std::vector<double>>> &v3err)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertVectorToTH3D");
    if (v3.empty())
    {
        MSGUser()->MSG_ERROR("Input vector is empty!!!");
        return;
    }
    if (h3 == nullptr)
    {
        MSGUser()->MSG_ERROR("Result hist not defined!!!");
        return;
    }

    int v3numx = v3.size();
    int binnumx = h3->GetXaxis()->GetNbins(), binnumy = h3->GetYaxis()->GetNbins(), binnumz = h3->GetZaxis()->GetNbins();
    if (v3numx != binnumx)
    {
        MSGUser()->MSG_ERROR("Result hist definition incorrect!!!");
        return;
    }

    for (size_t i = 0; i < v3.size(); i++)
    {
        if (v3.at(i).empty())
        {
            MSGUser()->MSG_ERROR("Input vector is empty!!!");
            return;
        }
        if ((int)v3.at(i).size() != binnumy)
        {
            MSGUser()->MSG_ERROR("Result hist definition incorrect or input vector incorrect!!!");
            return;
        }
        for (size_t j = 0; j < v3.at(i).at(j).size(); j++)
        {
            if ((int)v3.at(i).at(j).size() != binnumz)
            {
                MSGUser()->MSG_ERROR("Result hist definition incorrect or input vector incorrect!!!");
                return;
            }
        }
    }

    for (size_t i = 0; i < v3.size(); i++)
    {
        for (size_t j = 0; j < v3.at(i).size(); j++)
        {
            for (size_t k = 0; k < v3.at(j).at(j).size(); k++)
            {
                h3->SetBinContent(i + 1, j + 1, k + 1, v3[i][j][k]);
                h3->SetBinError(i + 1, j + 1, k + 1, 0);
            }
        }
    }

    for (size_t i = 0; i < v3err.size(); i++)
    {
        for (size_t j = 0; j < v3err.at(i).size(); j++)
        {
            for (size_t k = 0; k < v3err.at(i).at(j).size(); k++)
            {
                h3->SetBinError(i + 1, j + 1, k + 1, v3err[i][j][k]);
            }
        }
    }
}

/// @brief Convert the contents of a 1D histogram to a vector.
/// @param h1d the input histogram.
/// @return the vector of the contents of the histogram.
std::vector<double> HistTool::ConvertTH1DToVector(TH1D *h1d)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH1DToVector");

    std::vector<double> returnv1d;

    if (h1d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return returnv1d;
    }

    for (int i = 1; i <= h1d->GetNbinsX(); i++)
        returnv1d.emplace_back(h1d->GetBinContent(i));

    return returnv1d;
}

/// @brief Convert the errors of a 1D histogram to a vector.
/// @param h1d the input histogram.
/// @return the vector of the errors of the histogram.
std::vector<double> HistTool::ConvertTH1DErrorToVector(TH1D *h1d)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH1DErrorToVector");

    std::vector<double> returnv1d;

    if (h1d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return returnv1d;
    }

    for (int i = 1; i <= h1d->GetNbinsX(); i++)
        returnv1d.emplace_back(h1d->GetBinError(i));

    return returnv1d;
}

/// @brief Convert the contents of a 2D histogram to a vector.
/// @param h2d the input histogram.
/// @param priority the priority of the axis to be converted, can be either "XY" or "YX".
/// @return  the contents of the histogram.
std::vector<std::vector<double>> HistTool::ConvertTH2DToVector(TH2D *h2d, const TString &priority)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH2DToVector");

    std::vector<std::vector<double>> returnv2d;

    if (h2d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return returnv2d;
    }

    if (priority == "XY")
    {
        for (int i = 1; i <= h2d->GetNbinsX(); i++)
        {
            std::vector<double> tempv1d;
            for (int j = 1; j <= h2d->GetNbinsY(); j++)
            {
                tempv1d.emplace_back(h2d->GetBinContent(i, j));
            }
            returnv2d.emplace_back(tempv1d);
        }
    }
    else if (priority == "YX")
    {
        for (int j = 1; j <= h2d->GetNbinsY(); j++)
        {
            std::vector<double> tempv1d;
            for (int i = 1; i <= h2d->GetNbinsX(); i++)
            {
                tempv1d.emplace_back(h2d->GetBinContent(i, j));
            }
            returnv2d.emplace_back(tempv1d);
        }
    }
    else
    {
        returnv2d = ConvertTH2DToVector(h2d);
    }

    return returnv2d;
}

/// @brief Convert the errors of a 2D histogram to a vector.
/// @param h2d the input histogram.
/// @param priority the priority of the axis to be converted, can be either "XY" or "YX".
/// @return  the errors of the histogram.
std::vector<std::vector<double>> HistTool::ConvertTH2DErrorToVector(TH2D *h2d, const TString &priority)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH2DErrorToVector");

    std::vector<std::vector<double>> returnv2d;

    if (h2d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return returnv2d;
    }

    if (priority == "XY")
    {
        for (int i = 1; i <= h2d->GetNbinsX(); i++)
        {
            std::vector<double> tempv1d;
            for (int j = 1; j <= h2d->GetNbinsY(); j++)
            {
                tempv1d.emplace_back(h2d->GetBinError(i, j));
            }
            returnv2d.emplace_back(tempv1d);
        }
    }
    else if (priority == "YX")
    {
        for (int j = 1; j <= h2d->GetNbinsY(); j++)
        {
            std::vector<double> tempv1d;
            for (int i = 1; i <= h2d->GetNbinsX(); i++)
            {
                tempv1d.emplace_back(h2d->GetBinError(i, j));
            }
            returnv2d.emplace_back(tempv1d);
        }
    }
    else
    {
        returnv2d = ConvertTH2DErrorToVector(h2d);
    }

    return returnv2d;
}

/// @brief Convert the contents of a 3D histogram to a vector.
/// @param h3d the input histogram.
/// @param priority the priority of the axis to be converted, can be either "XYZ", XZY", "ZXY", "ZYX", "YZX" or "YXZ".
/// @return  the contents of the histogram.
std::vector<std::vector<std::vector<double>>> HistTool::ConvertTH3DToVector(TH3D *h3d, const TString &priority)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH3DToVector");

    std::vector<std::vector<std::vector<double>>> returnv3d;

    if (h3d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return returnv3d;
    }

    if (priority == "XYZ")
    {
        for (int i = 1; i <= h3d->GetNbinsX(); i++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int j = 1; j <= h3d->GetNbinsY(); j++)
            {
                std::vector<double> tempv1d;
                for (int k = 1; k <= h3d->GetNbinsZ(); k++)
                {
                    tempv1d.emplace_back(h3d->GetBinContent(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "XZY")
    {
        for (int i = 1; i <= h3d->GetNbinsX(); i++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int k = 1; k <= h3d->GetNbinsZ(); k++)
            {
                std::vector<double> tempv1d;
                for (int j = 1; j <= h3d->GetNbinsY(); j++)
                {
                    tempv1d.emplace_back(h3d->GetBinContent(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "ZXY")
    {
        for (int k = 1; k <= h3d->GetNbinsZ(); k++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int i = 1; i <= h3d->GetNbinsX(); i++)
            {
                std::vector<double> tempv1d;
                for (int j = 1; j <= h3d->GetNbinsY(); j++)
                {
                    tempv1d.emplace_back(h3d->GetBinContent(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "ZYX")
    {
        for (int k = 1; k <= h3d->GetNbinsZ(); k++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int j = 1; j <= h3d->GetNbinsY(); j++)
            {
                std::vector<double> tempv1d;
                for (int i = 1; i <= h3d->GetNbinsX(); i++)
                {
                    tempv1d.emplace_back(h3d->GetBinContent(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "YZX")
    {
        for (int j = 1; j <= h3d->GetNbinsY(); j++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int k = 1; k <= h3d->GetNbinsZ(); k++)
            {
                std::vector<double> tempv1d;
                for (int i = 1; i <= h3d->GetNbinsX(); i++)
                {
                    tempv1d.emplace_back(h3d->GetBinContent(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "YXZ")
    {
        for (int j = 1; j <= h3d->GetNbinsY(); j++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int i = 1; i <= h3d->GetNbinsX(); i++)
            {
                std::vector<double> tempv1d;
                for (int k = 1; k <= h3d->GetNbinsZ(); k++)
                {
                    tempv1d.emplace_back(h3d->GetBinContent(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else
    {
        returnv3d = ConvertTH3DToVector(h3d);
    }

    return returnv3d;
}

/// @brief Convert the errors of a 3D histogram to a vector.
/// @param h3d the input histogram.
/// @param priority the priority of the axis to be converted, can be either "XYZ", XZY", "ZXY", "ZYX", "YZX" or "YXZ".
/// @return  the errors of the histogram.
std::vector<std::vector<std::vector<double>>> HistTool::ConvertTH3DErrorToVector(TH3D *h3d, const TString &priority)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH3DErrorToVector");

    std::vector<std::vector<std::vector<double>>> returnv3d;

    if (h3d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return returnv3d;
    }

    if (priority == "XYZ")
    {
        for (int i = 1; i <= h3d->GetNbinsX(); i++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int j = 1; j <= h3d->GetNbinsY(); j++)
            {
                std::vector<double> tempv1d;
                for (int k = 1; k <= h3d->GetNbinsZ(); k++)
                {
                    tempv1d.emplace_back(h3d->GetBinError(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "XZY")
    {
        for (int i = 1; i <= h3d->GetNbinsX(); i++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int k = 1; k <= h3d->GetNbinsZ(); k++)
            {
                std::vector<double> tempv1d;
                for (int j = 1; j <= h3d->GetNbinsY(); j++)
                {
                    tempv1d.emplace_back(h3d->GetBinError(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "ZXY")
    {
        for (int k = 1; k <= h3d->GetNbinsZ(); k++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int i = 1; i <= h3d->GetNbinsX(); i++)
            {
                std::vector<double> tempv1d;
                for (int j = 1; j <= h3d->GetNbinsY(); j++)
                {
                    tempv1d.emplace_back(h3d->GetBinError(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "ZYX")
    {
        for (int k = 1; k <= h3d->GetNbinsZ(); k++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int j = 1; j <= h3d->GetNbinsY(); j++)
            {
                std::vector<double> tempv1d;
                for (int i = 1; i <= h3d->GetNbinsX(); i++)
                {
                    tempv1d.emplace_back(h3d->GetBinError(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "YZX")
    {
        for (int j = 1; j <= h3d->GetNbinsY(); j++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int k = 1; k <= h3d->GetNbinsZ(); k++)
            {
                std::vector<double> tempv1d;
                for (int i = 1; i <= h3d->GetNbinsX(); i++)
                {
                    tempv1d.emplace_back(h3d->GetBinError(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else if (priority == "YXZ")
    {
        for (int j = 1; j <= h3d->GetNbinsY(); j++)
        {
            std::vector<std::vector<double>> tempv2d;
            for (int i = 1; i <= h3d->GetNbinsX(); i++)
            {
                std::vector<double> tempv1d;
                for (int k = 1; k <= h3d->GetNbinsZ(); k++)
                {
                    tempv1d.emplace_back(h3d->GetBinError(i, j, k));
                }
                tempv2d.emplace_back(tempv1d);
            }
            returnv3d.emplace_back(tempv2d);
        }
    }
    else
    {
        returnv3d = ConvertTH3DErrorToVector(h3d);
    }

    return returnv3d;
}

/// @brief Fuse a vector of 1D histograms into one 1D histogram.
/// @param[in] vh1d the input vector of 1D histograms.
/// @param[out] targeth1d the output 1D histogram.
void HistTool::ConvertVectorTH1DToTH1D(const std::vector<TH1D *> &vh1d, TH1D *targeth1d)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertVectorTH1DToTH1D");
    if (vh1d.empty())
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }

    std::vector<std::vector<double>> vc2d;
    std::vector<std::vector<double>> ve2d;

    for (auto &h1d : vh1d)
    {
        vc2d.emplace_back(ConvertTH1DToVector(h1d));
        ve2d.emplace_back(ConvertTH1DErrorToVector(h1d));
    }

    auto vc1d = ConvertVector2DToVector1D(vc2d);
    auto ve1d = ConvertVector2DToVector1D(ve2d);

    ConvertVectorToTH1D(targeth1d, vc1d, ve1d);
}

/// @brief Convert a 2D histogram into a vector of 1D histograms.
/// @param[in] h2d the input 2D histogram.
/// @param[out] vh1d the output vector of 1D histograms.
/// @param[in] priority the priority of the axis to be converted, can be either "XY" or "YX".
void HistTool::ConvertTH2DToVectorTH1D(TH2D *h2d, const std::vector<TH1D *> &vh1d, const TString &priority)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH2DToVectorTH1D");
    if (h2d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (vh1d.empty())
    {
        MSGUser()->MSG_ERROR("Result vector is empty!!!");
        return;
    }

    auto vc2d = ConvertTH2DToVector(h2d, priority);
    auto ve2d = ConvertTH2DErrorToVector(h2d, priority);

    if (vh1d.size() != vc2d.size())
    {
        MSGUser()->MSG_ERROR("Result vector size incorrect or input hist incorrect!!!");
        return;
    }

    for (size_t i = 0; i < vh1d.size(); i++)
    {
        ConvertVectorToTH1D(vh1d[i], vc2d[i], ve2d[i]);
    }
}

/// @brief Convert a 3D histogram into a 2D vector of 1D histograms.
/// @param[in] h3d the input 3D histogram.
/// @param[out] vh1d the output 2D vector of 1D histograms.
/// @param[in] priority the priority of the axis to be converted, can be either "XYZ", "XZY", "ZXY", "ZYX", "YZX", or "YXZ".
void HistTool::ConvertTH3DToVectorTH1D(TH3D *h3d, const std::vector<std::vector<TH1D *>> &vh1d, const TString &priority)
{
    auto msgguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH3DToVectorTH1D");
    if (h3d == nullptr)
    {
        MSGUser()->MSG_ERROR("Input hist not defined!!!");
        return;
    }
    if (vh1d.empty())
    {
        MSGUser()->MSG_ERROR("Result vector is empty!!!");
        return;
    }

    auto vc3d = ConvertTH3DToVector(h3d, priority);
    auto ve3d = ConvertTH3DErrorToVector(h3d, priority);

    if (vh1d.size() != vc3d.size())
    {
        MSGUser()->MSG_ERROR("Result vector size incorrect or input hist incorrect!!!");
        return;
    }

    for (size_t i = 0; i < vh1d.size(); i++)
    {
        if (vh1d[i].size() != vc3d[i].size())
        {
            MSGUser()->MSG_ERROR("Result vector size incorrect or input hist incorrect!!!");
            return;
        }

        for (size_t j = 0; j < vh1d[i].size(); j++)
        {
            ConvertVectorToTH1D(vh1d[i][j], vc3d[i][j], ve3d[i][j]);
        }
    }
}

/// @brief Convert a 2D histogram to a matrix.
/// @param h2d the input 2D histogram.
/// @return the output matrix.
TMatrixD HistTool::ConvertTH2DToMatrix(TH2D *h2d)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertTH2DToMatrix");
    if (!h2d)
    {
        MSGUser()->MSG_ERROR("Target TH2D does not exist");
        return TMatrixD(0, 0);
    }

    TMatrixD mat(h2d->GetNbinsX(), h2d->GetNbinsY());
    for (int i = 0; i < h2d->GetNbinsX(); i++)
        for (int j = 0; j < h2d->GetNbinsY(); j++)
            mat(i, j) = h2d->GetBinContent(i + 1, j + 1);

    return mat;
}

/// @brief Convert a matrix to a 2D histogram.
/// @param mat the input matrix.
/// @param h2d the output 2D histogram.
void HistTool::ConvertMatrixToTH2D(const TMatrixD &mat, TH2D *h2d)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("HistTool::ConvertMatrixToTH2D");
    if (!h2d)
    {
        MSGUser()->MSG_ERROR("Target TH2D does not exist");
        return;
    }
    if (h2d->GetNbinsX() != mat.GetNrows() || h2d->GetNbinsY() != mat.GetNcols())
    {
        MSGUser()->MSG_ERROR("Target TH2D does not match input matrix");
        return;
    }

    for (int i = 0; i < h2d->GetNbinsX(); i++)
        for (int j = 0; j < h2d->GetNbinsY(); j++)
            h2d->SetBinContent(i + 1, j + 1, mat(i, j));
}
