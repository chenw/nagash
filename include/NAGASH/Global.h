//***************************************************************************************
/// @file Global.h
/// @brief Some global definitions.
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

// header from ROOT
#include <TMath.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>
#include <THStack.h>
#include <TAxis.h>
#include <TF1.h>
#include <TBranch.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TRandom3.h>
#include <TMinuit.h>
#include <TApplication.h>
#include <TEnv.h>
#include <TComplex.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TSystem.h>
#include <TMath.h>
#include <TLatex.h>
#include <TString.h>
#include <TComplex.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TMatrixD.h>
#include <TList.h>
#include <TKey.h>

// header from c++ std libraries
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <algorithm>
#include <functional>
#include <ctime>
#include <vector>
#include <math.h>
#include <unistd.h>
#include <any>
#include <type_traits>
#include <mutex>
#include <memory>
#include <utility>
#include <random>
#include <chrono>
#include <regex>
#include <iterator>
#include <array>
#include <shared_mutex>
#include <filesystem>
#include <ranges>

namespace NAGASH
{
    enum class StatusCode
    {
        FAILURE,
        SUCCESS,
        RECOVERABLE
    };

    // version number
    inline const size_t NAGASHMainVersion = 0;
    inline const size_t NAGASHSubVersion = 9;
    inline const size_t NAGASHSubSubVersion = 8;
    inline int GlobalRootConfig = []()
    {
// config root to enable multi-threading
#ifndef __APPLE__
        ROOT::EnableThreadSafety();
#else
        std::cout << "Warning: ROOT::EnableThreadSafety() is not automatically supported on MacOS. User should call it manually at the beginning of the program." << std::endl;
#endif
        TDirectory::AddDirectory(kFALSE);
        TH1::AddDirectory(kFALSE);
        return 1;
    }();

    class PlotGroup;
    class LoopEvent;
    class ConfigTool;
    class Analysis;
} // namespace NAGASH
