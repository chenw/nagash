#include "NAGASH/NAGASH.h"
using namespace NAGASH;

auto config = std::make_shared<ConfigTool>();
auto msg = config->MSGUser();
void Write()
{
    // auto hist = std::make_shared<NGHist<TH1D>>(msg, config, "test", "save.root", 10, 0, 10);
    auto blank = std::make_shared<NGHist<TH1D>>(msg, config, "test ", "save.root", 5, 0, 5);
    // hist->BookSystematicVariation("syst");
    blank->BookSystematicVariation("syst");
    // hist->WriteToFile();
    blank->WriteToFile();
}

void Read()
{
    // auto hist = std::make_shared<NGHist<TH1D>>(msg, config, "test");
    auto blank = std::make_shared<NGHist<TH1D>>(msg, config, "test ");
    auto file = new TFile("save.root");
    // hist->Recover(file);
    blank->Recover(file);
    // std::cout << hist->GetNominalHist()->GetNbinsX() << "  " << hist->GetFinalHist() << "  " << hist->GetVariation("syst") << "  " << hist->GetNVariations() << "  " << hist->GetCovariance() << "  " << hist->GetCorrelation() << std::endl;
    std::cout << blank->GetNominalHist()->GetNbinsX() << "  " << blank->GetFinalHist() << "  " << blank->GetVariation("syst") << "  " << blank->GetNVariations() << "  " << blank->GetCovariance() << "  " << blank->GetCorrelation() << std::endl;
}

int main(int argc, char **argv)
{
    if (!std::strcmp(argv[1], "write"))
        Write();
    else if (!std::strcmp(argv[1], "read"))
        Read();
    return 0;
}