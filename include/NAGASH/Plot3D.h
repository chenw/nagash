//***************************************************************************************
/// @file Plot3D.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/NGHist.h"

namespace NAGASH
{
    /// @ingroup Histograms
    /// @brief alias for frequently used NGHist<TH3D>.
    using Plot3D = NGHist<TH3D>;

    /// @brief partial specialization for the constructor of NGHist<TH3D>.
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name the name of the histogram.
    /// @param filename  the file name of the histogram to be saved in.
    /// @param bindivx binning in the x axis.
    /// @param bindivy binning in the y axis.
    /// @param bindivz binning in the z axis.
    template <>
    template <>
    inline NGHist<TH3D>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const std::vector<double> &bindivx, const std::vector<double> &bindivy, const std::vector<double> &bindivz)
        : HistBase(MSG, c, name, filename)
    {
        Nominal = new TH3D(GetResultName(), GetResultName(), (int)bindivx.size() - 1, bindivx.data(), (int)bindivy.size() - 1, bindivy.data(), (int)bindivz.size() - 1, bindivz.data());
        Nominal->Sumw2();
        Nominal->SetDirectory(0);
        FillHist = Nominal;
    }

}
