#include "NAGASH/NAGASHFile.h"
#include "NAGASH/Global.h"

using namespace NAGASH;

int main(int argc, char **argv)
{
    if (argc != 2 && argc != 3)
    {
        std::cout << "Usage: NAGASHls TARGET (output file name)" << std::endl;
        return 0;
    }

    NAGASHFile infile(std::make_shared<MSGTool>(), argv[1], NAGASHFile::Mode::READ);
    if (!infile.IsOpen())
        return -1;

    if (argc == 3)
    {
        std::ofstream outfile;
        outfile.open(argv[2]);
        infile.PrintListOfHists(outfile);
    }
    else
    {
        infile.PrintListOfHists();
    }

    return 0;
}