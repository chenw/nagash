//***************************************************************************************
/// @file TemplateFitter.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

#include "Minuit2/FCNBase.h"

namespace NAGASH
{
    class TemplateFitter : public Tool
    {
    public:
        TemplateFitter(std::shared_ptr<MSGTool> MSG) : Tool(MSG) {}

        void SetTarget(TH1D *h);
        void SetTemplate(TH1D *h, double SF = 1, bool fix = false);
        void SetTemplate(TH1D *h, double SF, double min, double max);
        void Fit();
        void SetRelation(std::function<double(const std::vector<double> &, const std::vector<double> &)>,
                         std::function<double(const std::vector<double> &, const std::vector<double> &, const std::vector<double> &)>);
        void SetFitRange(int min, int max);
        double GetSF(uint64_t i);
        double GetSFError(uint64_t i);
        double GetChi2();
        void Clear();

    private:
        class TemplateFitterFCN : public ROOT::Minuit2::FCNBase
        {
        public:
            TemplateFitterFCN(TH1D *, const std::vector<TH1D *> &, int, int);
            void SetRelation(std::function<double(const std::vector<double> &, const std::vector<double> &)>,
                             std::function<double(const std::vector<double> &, const std::vector<double> &, const std::vector<double> &)>);
            double operator()(const std::vector<double> &) const override;
            // error defination
            double Up() const { return 1.0; }

        private:
            std::function<double(const std::vector<double> &, const std::vector<double> &)> contentfunction;
            std::function<double(const std::vector<double> &, const std::vector<double> &, const std::vector<double> &)> errorfunction;

            TH1D *h_target = nullptr;
            std::vector<TH1D *> h_template;
            int min = 0;
            int max = 0;
        };

        std::vector<double> sf;
        std::vector<double> sfmin; // user set min sf
        std::vector<double> sfmax; // user set min sf
        std::vector<double> sferr;
        std::vector<bool> isfixed;
        double chi2 = 0;
        int FitRangeMin = 0;
        int FitRangeMax = 0;

        std::function<double(const std::vector<double> &, const std::vector<double> &)> contentfunction;
        std::function<double(const std::vector<double> &, const std::vector<double> &, const std::vector<double> &)> errorfunction;

        TH1D *ht = nullptr;
        std::vector<TH1D *> vh;

        bool Check();
    };
} // namespace NAGASH
