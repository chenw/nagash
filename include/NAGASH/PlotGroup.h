//***************************************************************************************
/// @file PlotGroup.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/MapTool.h"
#include "NAGASH/Plot1D.h"
#include "NAGASH/Plot2D.h"
#include "NAGASH/Plot3D.h"
#include "NAGASH/Result.h"

namespace NAGASH
{
    class PlotGroup : public Result
    {
    private:
        std::map<TString, std::shared_ptr<HistBase>> PlotMap;
        std::vector<std::shared_ptr<HistBase>> PlotVector;

        MapTool maptool;

    public:
        PlotGroup(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "");

        template <typename... Args>
        std::shared_ptr<Plot1D> BookPlot1D(const TString &name, Args &&...args);
        template <typename... Args>
        std::shared_ptr<Plot2D> BookPlot2D(const TString &name, Args &&...args);
        template <typename... Args>
        std::shared_ptr<Plot3D> BookPlot3D(const TString &name, Args &&...args);

        std::shared_ptr<Plot1D> GetPlot1D(const TString &name);
        std::shared_ptr<Plot2D> GetPlot2D(const TString &name);
        std::shared_ptr<Plot3D> GetPlot3D(const TString &name);

        void AddPlot1D(const TString &name, std::shared_ptr<Plot1D> plot);
        void AddPlot2D(const TString &name, std::shared_ptr<Plot2D> plot);
        void AddPlot3D(const TString &name, std::shared_ptr<Plot3D> plot);

        // for general purpose
        template <typename HistType, typename... Args>
        std::shared_ptr<NGHist<HistType>> BookNGHist(const TString &name, Args &&...args);
        template <typename HistType>
        std::shared_ptr<NGHist<HistType>> GetNGHist(const TString &name);
        template <typename HistType>
        void AddNGHist(const TString &name, std::shared_ptr<NGHist<HistType>> plot);

        int GetNPlots();
        void Process();
        void ForEachPlot(std::function<void(std::shared_ptr<HistBase>)>);
        void SetUnprocessed();
        void Scale(double);
        void ScaleVariation(const TString &, double);
        void SetLinkType(const TString &);
        void SetLinkType(std::function<double(const std::vector<double> &)>, std::function<double(const std::vector<double> &, const std::vector<double> &)>);
        void SetLinkType(std::function<void(const std::vector<TH1 *> &, TH1 *)>);
        void SetLinkPlotGroup(uint64_t, std::shared_ptr<PlotGroup>);
        void ClearLinkPlotGroup();
        void Reset();
        void Recover(const TString &filename);
        void Recover(TFile *file);
        void Recover(std::shared_ptr<TFileHelper>);
        std::shared_ptr<PlotGroup> Clone(const TString &name);
        void BookSystematicVariation(const TString &name, double corr_factor = 0, HistBase::SystPolicy policy = HistBase::SystPolicy::Maximum);
        void BookSystematicVariation(const TString &name1, const TString &name2, double corr_factor = 0, HistBase::SystPolicy policy = HistBase::SystPolicy::Maximum);
        void BookSystematicVariation(const std::vector<TString> &names, double corr_factor = 0, HistBase::SystPolicy policy = HistBase::SystPolicy::Maximum);
        void RegroupSystematicVariation(const std::vector<TString> &names, HistBase::SystPolicy policy);
        void RemoveSystematicVariation(const TString &name);
        void RenameSystematicVariation(const TString &name_old, const TString &name_new);
        void SetSystematicVariation(const TString &name = "Nominal");
        void SetSystematicVariation(int index);
        void Combine(std::shared_ptr<Result> result) override;
        void WriteToFile() override;
        virtual ~PlotGroup() = default;
    }; // class PlotGroup

    /// @brief Book NGHist and store in this PlotGroup
    /// @tparam HistType the template parameter passed to NGHist.
    /// @tparam ...Args can be deduced by compiler.
    /// @param name the name of the NGHist.
    /// @param ...args the binning parameters of NGHist.
    /// @return the booked NGHist.
    template <typename HistType, typename... Args>
    inline std::shared_ptr<NGHist<HistType>> PlotGroup::BookNGHist(const TString &name, Args &&...args)
    {
        if (PlotMap.find(name) == PlotMap.end())
        {
            auto tempplot = std::make_shared<NGHist<HistType>>(MSGUser(), ConfigUser(), GetResultName() + "_" + name, "", std::forward<Args>(args)...);
            PlotMap.emplace(std::pair<TString, std::shared_ptr<HistBase>>(name, tempplot));
            PlotVector.emplace_back(tempplot);
            return tempplot;
        }
        else
        {
            auto guard = MSGUser()->StartTitleWithGuard("PlotGroup::BookNGHist");
            MSGUser()->MSG_WARNING("Plot ", name, " has already been booked, returning nullptr");
            return nullptr;
        }
    }

    /// @brief Get booked NGHist with given name.
    /// @tparam HistType the template parameter passed to NGHist.
    /// @param name the name of the NGHist.
    /// @return the pointer to the booked NGHist, if not found, nullptr is returned.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> PlotGroup::GetNGHist(const TString &name)
    {
        auto guard = MSGUser()->StartTitleWithGuard("PlotGroup::GetNGHist");
        std::shared_ptr<NGHist<HistType>> plot = nullptr;
        auto findresult = maptool.FindMostSimilar(PlotMap, name);
        if (findresult != PlotMap.end())
            plot = std::dynamic_pointer_cast<NGHist<HistType>>(findresult->second);

        if (plot != nullptr)
            return plot;
        else
        {
            MSGUser()->MSG_WARNING("Plot ", name, " does not exist, returning nullptr");
            return nullptr;
        }
    }

    /// @brief Append a NGHist to this PlotGroup.
    /// @tparam HistType the template parameter passed to NGHist.
    /// @param name the name of the NGHist.
    /// @param plot the NGHist to be appended.
    template <typename HistType>
    inline void PlotGroup::AddNGHist(const TString &name, std::shared_ptr<NGHist<HistType>> plot)
    {
        PlotMap.emplace(std::pair<TString, std::shared_ptr<HistBase>>(name, plot));
        PlotVector.emplace_back(plot);
    }

    // legacy

    /// @brief Book NGHist<TH1D>
    /// @tparam ...Args deduced by compiler.
    /// @param name name of the NGHist.
    /// @param ...args the binning parameters of NGHist.
    /// @return the booked NGHist<TH1D>.
    template <typename... Args>
    inline std::shared_ptr<Plot1D> PlotGroup::BookPlot1D(const TString &name, Args &&...args)
    {
        return BookNGHist<TH1D>(name, std::forward<Args>(args)...);
    }

    /// @brief Book NGHist<TH2D>
    /// @tparam ...Args deduced by compiler.
    /// @param name name of the NGHist.
    /// @param ...args the binning parameters of NGHist.
    /// @return the booked NGHist<TH2D>.
    template <typename... Args>
    inline std::shared_ptr<Plot2D> PlotGroup::BookPlot2D(const TString &name, Args &&...args)
    {
        return BookNGHist<TH2D>(name, std::forward<Args>(args)...);
    }

    /// @brief Book NGHist<TH3D>
    /// @tparam ...Args deduced by compiler.
    /// @param name name of the NGHist.
    /// @param ...args the binning parameters of NGHist.
    /// @return the booked NGHist<TH3D>.
    template <typename... Args>
    inline std::shared_ptr<Plot3D> PlotGroup::BookPlot3D(const TString &name, Args &&...args)
    {
        return BookNGHist<TH3D>(name, std::forward<Args>(args)...);
    }

    /// @brief Get NGHist<TH1D>
    /// @param name name of the NGHist.
    /// @return the pointer to the NGHist<TH1D>, if not found, nullptr is returned.
    inline std::shared_ptr<Plot1D> PlotGroup::GetPlot1D(const TString &name)
    {
        return GetNGHist<TH1D>(name);
    }

    /// @brief Get NGHist<TH2D>
    /// @param name name of the NGHist.
    /// @return the pointer to the NGHist<TH2D>, if not found, nullptr is returned.
    inline std::shared_ptr<Plot2D> PlotGroup::GetPlot2D(const TString &name)
    {
        return GetNGHist<TH2D>(name);
    }

    /// @brief Get NGHist<TH3D>
    /// @param name name of the NGHist.
    /// @return the pointer to the NGHist<TH3D>, if not found, nullptr is returned.
    inline std::shared_ptr<Plot3D> PlotGroup::GetPlot3D(const TString &name)
    {
        return GetNGHist<TH3D>(name);
    }

    /// @brief Append a NGHist<TH1D> to this PlotGroup.
    /// @param name name of the NGHist.
    /// @param plot the pointer to the NGHist<TH1D> to be appended.
    inline void PlotGroup::AddPlot1D(const TString &name, std::shared_ptr<Plot1D> plot)
    {
        AddNGHist<TH1D>(name, plot);
    }

    /// @brief Append a NGHist<TH2D> to this PlotGroup.
    /// @param name name of the NGHist.
    /// @param plot the pointer to the NGHist<TH2D> to be appended.
    inline void PlotGroup::AddPlot2D(const TString &name, std::shared_ptr<Plot2D> plot)
    {
        AddNGHist<TH2D>(name, plot);
    }

    /// @brief Append a NGHist<TH3D> to this PlotGroup.
    /// @param name name of the NGHist.
    /// @param plot the pointer to the NGHist<TH3D> to be appended.
    inline void PlotGroup::AddPlot3D(const TString &name, std::shared_ptr<Plot3D> plot)
    {
        AddNGHist<TH3D>(name, plot);
    }

} // namespace NAGASH
