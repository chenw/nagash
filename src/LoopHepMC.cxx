#include "NAGASH/LoopHepMC.h"

#ifdef NAGASH_HEPMC

using namespace NAGASH;
using namespace std;

StatusCode LoopHepMC::OpenHepMCFile(const TString &filename)
{
    if (hepmctype.TString::EqualTo("Ascii") ||
        hepmctype.TString::EqualTo("AsciiHepMC3"))
        fReader = std::make_shared<HepMC3::ReaderAscii>(filename.TString::Data());
    else if (hepmctype.TString::EqualTo("AsciiHepMC2"))
        fReader = std::make_shared<HepMC3::ReaderAsciiHepMC2>(filename.TString::Data());
    else if (hepmctype.TString::EqualTo("HEPEVT"))
        fReader = std::make_shared<HepMC3::ReaderHEPEVT>(filename.TString::Data());
    else if (hepmctype.TString::EqualTo("LHEF"))
        fReader = std::make_shared<HepMC3::ReaderLHEF>(filename.TString::Data());
    else if (hepmctype.TString::EqualTo("Root"))
        fReader = std::make_shared<HepMC3::ReaderRoot>(filename.TString::Data());
    else if (hepmctype.TString::EqualTo("RootTree"))
        fReader = std::make_shared<HepMC3::ReaderRootTree>(filename.TString::Data());
    else
    {
        MSGUser()->StartTitle("OpenHepMCFile");
        MSGUser()->MSG(MSGLevel::ERROR, "HepMC Type should be Ascii(AsciiHepMC3), AsciiHepMC2, HEPEVT, LHEF, Root or RootTree ");
        MSGUser()->EndTitle();
        return StatusCode::FAILURE;
    }
    inputfilename = filename;

    if (fReader == nullptr)
    {
        MSGUser()->StartTitle("OpenRootFile");
        MSGUser()->MSG(MSGLevel::ERROR, "input HepMC3 file ", filename, " does not exist");
        MSGUser()->EndTitle();
        return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
}

StatusCode LoopHepMC::Run()
{
    Job::DefaultMutex.lock();
    TimerUser()->Record("(Start Run)");
    MSGUser()->MSG(MSGLevel::INFO, "Start Loop File ", InputFileName());

    auto originlevel = MSGUser()->OutputLevel;
    MSGUser()->OutputLevel = MSGLevel::SILENT;
    int EvtSkip;
    bool EvtSkipExist;
    EvtSkip = ConfigUser()->GetPar<int>("EvtSkip", &EvtSkipExist);
    if (!EvtSkipExist)
        EvtSkip = 0;
    MSGUser()->OutputLevel = originlevel;

    if (EvtSkipExist && EvtSkip > 0)
        MSGUser()->MSG(MSGLevel::INFO, "Will loop 1 event and then skip the next ", EvtSkip, " events");
    Job::DefaultMutex.unlock();

    entry = 0;
    while (!fReader->failed())
    {
        fReader->read_event(evt);
        TString num;
        num.Form("(Execute Event %d)", entry);
        auto titlegurad = MSGUser()->StartTitleWithGuard(num);
        if (fReader->failed())
        {
            MSGUser()->MSG(MSGLevel::WARNING, "Entry ", entry, " is corrupted");
            continue;
        }
        // should add StatusCode::RECOVERABLE branch
        if (Execute() != StatusCode::SUCCESS)
        {
            MSGUser()->MSG(MSGLevel::ERROR, " Failed here, please check");
            return StatusCode::FAILURE;
        }
        entry = entry + 1;
        if (EvtSkipExist && EvtSkip > 0)
        {
            fReader->skip(EvtSkip);
            entry = entry + EvtSkip;
        }
    }
    TimerUser()->Record("(End Run)");

    Job::DefaultMutex.lock();
    TimerUser()->Duration("(Start Run)", "(End Run)");
    fReader = nullptr;
    Job::DefaultMutex.unlock();

    return StatusCode::SUCCESS;
}

LoopHepMC::~LoopHepMC()
{
}

#endif
