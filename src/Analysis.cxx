//**************************************************************************************
/// @file Analysis.cxx
/// @author Wenhao Ma
//**************************************************************************************

#include "NAGASH/Analysis.h"

using namespace NAGASH;
using namespace std;

/** @class NAGASH::Analysis Analysis.h "NAGASH/Analysis.h"
    @ingroup AnalysisClasses
    @brief Provide multi-thread interface to manipulate with \link Job \endlink.
*/

/// @brief Constructor of the Analysis class.
/// @param c Input \link ConfigTool \endlink object.
Analysis::Analysis(const ConfigTool &c)
{
    config = std::make_shared<ConfigTool>(c);
    msg = config->MSGUser();
    timer = std::make_shared<Timer>(msg);
    msg->StartTitle("Analysis");
    ConfigThreadPool();
}

/// @brief Constructor of the Analysis class.
/// @param configfilename path to the configuration file.
Analysis::Analysis(const char *configfilename)
{
    config = std::make_shared<ConfigTool>(configfilename);
    msg = config->MSGUser();
    timer = std::make_shared<Timer>(msg);
    msg->StartTitle("Analysis");
    ConfigThreadPool();
}

/// @brief Default constructor of the Analysis class.
Analysis::Analysis()
{
    config = std::make_shared<ConfigTool>();
    msg = config->MSGUser();
    timer = std::make_shared<Timer>(msg);
    msg->StartTitle("Analysis");
    ConfigThreadPool();
}

/// @brief Config the thread pool using the info from \link ConfigTool \endlink.
void Analysis::ConfigThreadPool()
{
    // read thread number from file
    auto origin = msg->OutputLevel;
    bool existthreadnum = false;
    msg->OutputLevel = MSGLevel::SILENT;
    int threadnum = config->GetPar<int>("Nthread", &existthreadnum);
    msg->OutputLevel = origin;
    if (threadnum <= 0 && existthreadnum)
    {
        msg->MSG(MSGLevel::WARNING, "Thread number is set to less than one, will be ignored");
    }
    else if (existthreadnum)
    {
        ThreadNumber = threadnum;
        pool = make_shared<ThreadPool>(ThreadNumber);
    }
    else if (!existthreadnum)
    {
        msg->MSG(MSGLevel::WARNING, "Nthread in ConfigTool is not set, please call Analysis::SetThreadNumber to set thread number");
    }
}

/// @brief destructor of the Analysis class.
Analysis::~Analysis()
{
    if (vf.size() > 0)
        msg->MSG_WARNING("Threads are not finished during the destruction of Analysis, you should manually call Analysis::Join in the end");
    Join(); // finish unfinished jobs
    msg->EndTitle();
}

/// @brief Set the thread number of the thread pool. Can only be set once.
/// @param num Number of threads.
void Analysis::SetThreadNumber(int num)
{
    if (pool != nullptr)
    {
        msg->MSG(MSGLevel::WARNING, "Thread pool has been defined, the size of thread pool won't be changed");
    }
    else
    {
        ThreadNumber = num;
        pool = make_shared<ThreadPool>(ThreadNumber);
    }
}

/// @brief Join the threads in the thread pool.
void Analysis::Join()
{
    if (pool != nullptr && vf.size() > 0)
    {
        for (size_t i = 0; i < vf.size(); i++)
        {
            auto status = vf[i].get();
            if (status == StatusCode::FAILURE)
                msg->MSG(MSGLevel::WARNING, "Job ", SubmittedJobCount[i], " failed");
        }
        vf.clear();
        for (size_t i = 0; i < ResultsToSave.size(); i++)
        {
            if (ResultsToSave[i] != nullptr)
                ResultsToSave[i]->Write();
        }
        ResultsToSave.clear();
        msg->MSG(MSGLevel::INFO, "Finished Job ", SubmittedJobCount.front(), "~", SubmittedJobCount.back());
        TimerUser()->PrintCurrent("Finished Time : ");
        TString record1, record2;
        record1.Form("(Finished Submitted Job %d~%d)", SubmittedJobCount.front(), SubmittedJobCount.back());
        record2.Form("(Submit Job %d)", SubmittedJobCount.front());
        TimerUser()->Record(record1);
        TimerUser()->Duration(record2, record1);
        SubmittedJobCount.clear();
    }
}
