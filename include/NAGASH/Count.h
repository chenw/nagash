//***************************************************************************************
/// @file Count.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Result.h"

namespace NAGASH
{
    /** @class NAGASH::CountBase Count.h "NAGASH/Count.h"
        @ingroup ResultClasses
        @brief Base class for Count.
        This class provide a base class for Count. You can also use it if you don't need other functions from NAGASH::Result.
    */
    class CountBase
    {
    public:
        void Add(const CountBase &);
        void Add(double x = 1, double w = 1);
        double GetSum() const;
        double GetSum2() const;
        double GetSumW() const;
        double GetSumW2() const;
        double GetSumStatUnc() const;
        double GetMean() const;
        double GetMeanStatUnc() const;
        double GetVar() const;
        double GetStdVar() const;
        double Chi2(const CountBase &) const;
        void Clear();

    protected:
        double SumW = 0;
        double SumW2 = 0;
        double SumX = 0;
        double SumX2 = 0;
    };

    /// @brief Add another CountBase object to this one.
    /// @param subCountBase the CountBase object to be added.
    inline void CountBase::Add(const CountBase &subCountBase)
    {
        SumW += subCountBase.SumW;
        SumW2 += subCountBase.SumW2;
        SumX += subCountBase.SumX;
        SumX2 += subCountBase.SumX2;
    }

    /// @brief Add a value along with its weight.
    /// @param x the value to be added.
    /// @param w the weight of the value.
    inline void CountBase::Add(double x, double w)
    {
        SumW += w;
        SumW2 += w * w;
        SumX += x * w;
        SumX2 += x * x * w;
    }

    /// @brief Calculate the \f$\chi^2\f$ with this and another CountBase object.
    /// @param count the CountBase object that you want to calculate the \f$\chi^2\f$ with.
    /// @return
    inline double CountBase::Chi2(const CountBase &count) const
    {
        return pow(this->GetMean() - count.GetMean(), 2) / (pow(this->GetMeanStatUnc(), 2) + pow(count.GetMeanStatUnc(), 2));
    }

    /// @brief Clear this CountBase object.
    inline void CountBase::Clear()
    {
        SumW = 0;
        SumW2 = 0;
        SumX = 0;
        SumX2 = 0;
    }

    inline double CountBase::GetSum() const { return SumX; }                           ///< Get the sum of the values \f$\sum x\cdot w\f$.
    inline double CountBase::GetSum2() const { return SumX2; }                         ///< Get the sum of the values squared \f$\sum x^2\cdot w\f$.
    inline double CountBase::GetSumW() const { return SumW; }                          ///< Get the sum of the weights \f$\sum w\f$.
    inline double CountBase::GetSumW2() const { return SumW2; }                        ///< Get the sum of the weights squared \f$\sum w^2\f$.
    inline double CountBase::GetSumStatUnc() const { return GetMean() * sqrt(SumW2); } ///< Get the statistical uncertainty of the sum \f$\bar{x}\cdot\sqrt{\sum w^2}\f$.
    inline double CountBase::GetMean() const { return SumW != 0 ? SumX / SumW : 0; }   ///< Get the mean \f$\bar{x}\f$.

    /// @brief Get the statistical uncertainty of the mean \f$\sigma(x)\cdot\sqrt{\sum w^2} / \sum w\f$
    inline double CountBase::GetMeanStatUnc() const { return SumW != 0 ? GetStdVar() * sqrt(SumW2) / SumW : 0; }
    /// @brief Get the variance \f$\sigma^2(x)\f$.
    inline double CountBase::GetVar() const { return SumW != 0 ? (SumX2 / SumW - (SumX / SumW) * (SumX / SumW)) : 0; }
    /// @brief Get the standard variance \f$\sigma(x)\f$.
    inline double CountBase::GetStdVar() const { return sqrt(fabs(GetVar())); }

    /// @class NAGASH::Count Count.h "NAGASH/Count.h"
    /// @ingroup ResultClasses
    /// @brief  Use CountBase with the interface of provided by NAGASH::Result.
    class Count : public Result, public CountBase
    {
    public:
        Count(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "") : Result(MSG, c, rname, fname), CountBase(){};
        ~Count() = default;
        void Combine(std::shared_ptr<Result> result);
    };

    /// @brief Combine with another Count object, interface provided by NAGASH::Result.
    /// @param result the Count object to be combined with this one.
    inline void Count::Combine(std::shared_ptr<Result> result)
    {
        auto subCount = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
        if (subCount != nullptr)
        {
            SumW += subCount->SumW;
            SumW2 += subCount->SumW2;
            SumX += subCount->SumX;
            SumX2 += subCount->SumX2;
        }
    }
} // namespace NAGASH
