//***************************************************************************************
/// @file ResultGroup.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Result.h"
#include "NAGASH/Toolkit.h"
#include "NAGASH/MapTool.h"
#include "NAGASH/SystTool.h"

namespace NAGASH
{
    /// @class NAGASH::ResultGroup ResultGroup.h "NAGASH/ResultGroup.h"
    /// @ingroup AnalysisClasses ResultClasses
    /// @brief Used to manipulate a group of Result objects., especially inside NAGASH::Job.
    class ResultGroup
    {
    public:
        /// @brief Constructor.
        /// @param MSG given NAGASH::MSGTool.
        /// @param c given NAGASH::ConfigTool.
        ResultGroup(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c) : config(c), maptool(MSG), msg(MSG) {}
        ResultGroup() = delete;
        ~ResultGroup() = default;

        /// @brief Add a result to the group.
        /// @param result the pointer of the result.
        void AddResult(std::shared_ptr<Result> result);
        /// @brief Merge two ResultGroup into one.
        /// @param subGroup the ResultGroup to be merged into this one.
        void Merge(std::shared_ptr<ResultGroup> subGroup);
        /// @brief Write all the results inside, i.e. call Result::WriteToFile() for each result.
        void Write();

        template <typename R, typename... Args>
        std::shared_ptr<R> BookResult(const TString &name, const TString &fname = "", Args &&...args);
        template <typename R>
        std::shared_ptr<R> Get(const TString &name);

    private:
        std::map<TString, std::shared_ptr<Result>> ResultMap;
        std::shared_ptr<ConfigTool> config;
        MapTool maptool;
        std::shared_ptr<MSGTool> msg;
    };

    /// @brief Book a result in this ResultGroup.
    /// @tparam R the type of the result.
    /// @tparam ...Args types of other arguments to construct the result. Can be automatically deduced.
    /// @param rname the name of the result.
    /// @param fname the output file name of the result.
    /// @param ...args other arguments to construct the result.
    /// @return
    template <typename R, typename... Args>
    inline std::shared_ptr<R> ResultGroup::BookResult(const TString &rname, const TString &fname, Args &&...args)
    {
        static_assert(!std::is_pointer<R>::value, "class ResultGroup: Can not book result in pointer type");
        static_assert(std::is_base_of<Result, R>::value, "class ResulGroup: Input type must be or be inherited from Result class");

        auto result = std::make_shared<R>(msg, config, rname, fname, std::forward<Args>(args)...);

        if (ResultMap.find(rname) != ResultMap.end())
        {
            msg->StartTitle("ResultGroup::BookResult");
            msg->MSG(MSGLevel::WARNING, "Result ", rname, " already exists, this book will be ignored and return null");
            msg->EndTitle();
            return nullptr;
        }
        else
        {
            ResultMap.emplace(std::pair<TString, std::shared_ptr<Result>>(rname, std::dynamic_pointer_cast<R>(result)));
            return result;
        }
    }

    /// @brief Get the result with the given name.
    /// If the given name is not find, the search will turn to fuzzy search mode, see MapTool::FindMostSimilar().
    /// @tparam R type of the result.
    /// @param name name of the result.
    /// @return the pointer of the result. Will return nullptr if the result does not exist.
    template <typename R>
    inline std::shared_ptr<R> ResultGroup::Get(const TString &name)
    {
        static_assert(!std::is_pointer<R>::value, "class ResultGroup: Can not book result in pointer type");
        static_assert(std::is_base_of<Result, R>::value, "class ResultGroup: Input type must be or be inherited from Result class");

        auto titleguard = msg->StartTitleWithGuard("ResultGroup::Get");
        std::shared_ptr<R> result = nullptr;
        auto findresult = maptool.FindMostSimilar(ResultMap, name);
        if (findresult != ResultMap.end())
            result = std::dynamic_pointer_cast<R>(findresult->second);
        if (result == nullptr)
            msg->MSG(MSGLevel::ERROR, "the input key ", name, " not found, returning nullptr");

        return result;
    }

} // namespace NAGASH
