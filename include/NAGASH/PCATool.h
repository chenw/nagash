//***************************************************************************************
/// @file PCATool.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/MSGTool.h"
#include "NAGASH/Tool.h"
#include "Eigen/Dense"

namespace NAGASH
{
    /** @class NAGASH::PCATool PCATool.h "NAGASH/PCATool.h"
        @ingroup ToolClasses
        @brief PCATool : Principal Component Analysis (PCA)

    Provide the uncertainty and the correlation of one or several hists and then provide its variations. Specifically,
    this tool use eigenvalue decomposition to diagonalize the covariance matrix to get a set of new observables and their
    uncorrelated variations.
     */
    class PCATool : public Tool
    {
    public:
        PCATool(std::shared_ptr<MSGTool> MSG, int size);

        void SetObservable(int index, double val);
        void SetCovariance(int index_i, int index_j, double cov);
        void SetUncertainty(int index, double unc);
        // These functions will set isProcessed to false
        // If unc or cov not set then it is set to be 0

        // These functions will check isProcessed
        // if it not processed then call function
        // Processed
        int GetNPCAVariation();
        double GetPCAResult(int ivar, int index, double nsigma);
        double GetPCAResult(int index);
        double GetPCAUpVariation(int ivar, int index);
        double GetPCADownVariation(int ivar, int index);
        double GetEigenValue(int ivar);

    private:
        bool isProcessed = false;

        void Process();

        Eigen::MatrixXd Covariance_Matrix;
        Eigen::MatrixXd Transform_Matrix;
        Eigen::MatrixXd New_Covariance_Matrix;

        Eigen::VectorXd Observable_Vector;
        Eigen::VectorXd New_Observable_Vector;

        Eigen::VectorXd EigenValue;

        std::vector<Eigen::VectorXd> New_Observable_UpVariations;
        std::vector<Eigen::VectorXd> New_Observable_DownVariations;

        int NVariation = -1;
        int Size = 0;
    };
} // namespace NAGASH
