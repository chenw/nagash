#include "LoopEventTest.h"

using namespace NAGASH;
using namespace std;

StatusCode LoopEventTest::InitializeData()
{
    // RootTreeUser() = (TTree *)RootFileUser()->Get("Tree");
    RootFileUser()->GetObject("Tree", RootTreeUser()); // maybe write a get tree function ?
    if (RootTreeUser() == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Wrong TTree name");
        delete RootFileUser();
        return StatusCode::FAILURE;
    }
    RootTreeUser()->SetBranchAddress("MuonPxFinal", &MuonPxFinal, &b_MuonPxFinal);
    RootTreeUser()->SetBranchAddress("MuonPyFinal", &MuonPyFinal, &b_MuonPyFinal);
    RootTreeUser()->SetBranchAddress("MuonPzFinal", &MuonPzFinal, &b_MuonPzFinal);
    RootTreeUser()->SetBranchAddress("MuonEFinal", &MuonEFinal, &b_MuonEFinal);
    RootTreeUser()->SetBranchAddress("MuonPx", &MuonPx, &b_MuonPx);
    RootTreeUser()->SetBranchAddress("MuonPy", &MuonPy, &b_MuonPy);
    RootTreeUser()->SetBranchAddress("MuonPz", &MuonPz, &b_MuonPz);
    RootTreeUser()->SetBranchAddress("MuonE", &MuonE, &b_MuonE);
    RootTreeUser()->SetBranchAddress("MuonCharge", &MuonCharge, &b_MuonCharge);
    RootTreeUser()->SetBranchAddress("NeutrinoPx", &NeutrinoPx, &b_NeutrinoPx);
    RootTreeUser()->SetBranchAddress("NeutrinoPy", &NeutrinoPy, &b_NeutrinoPy);
    RootTreeUser()->SetBranchAddress("NeutrinoPz", &NeutrinoPz, &b_NeutrinoPz);
    RootTreeUser()->SetBranchAddress("NeutrinoE", &NeutrinoE, &b_NeutrinoE);
    RootTreeUser()->SetBranchAddress("WbosonPx", &WbosonPx, &b_WbosonPx);
    RootTreeUser()->SetBranchAddress("WbosonPy", &WbosonPy, &b_WbosonPy);
    RootTreeUser()->SetBranchAddress("WbosonPz", &WbosonPz, &b_WbosonPz);
    RootTreeUser()->SetBranchAddress("WbosonE", &WbosonE, &b_WbosonE);
    RootTreeUser()->SetBranchAddress("QuarkFlavour", QuarkFlavour, &b_QuarkFlavour);
    RootTreeUser()->SetBranchAddress("QuarkPx", QuarkPx, &b_QuarkPx);
    RootTreeUser()->SetBranchAddress("QuarkPy", QuarkPy, &b_QuarkPy);
    RootTreeUser()->SetBranchAddress("QuarkPz", QuarkPz, &b_QuarkPz);
    RootTreeUser()->SetBranchAddress("QuarkE", QuarkE, &b_QuarkE);

    return StatusCode::SUCCESS;
}

StatusCode LoopEventTest::InitializeUser()
{
    CountNEvents = ResultGroupUser()->BookResult<Count>("NEvents");
    CountMass = ResultGroupUser()->BookResult<Count>("Mass");
    CountTest = ResultGroupUser()->BookResult<Count>("Test");

    ResultGroupUser()->BookResult<PlotGroup>("MassHist", "AnalysisTest_MassHist.root");

    std::shared_ptr<PlotGroup> MyPlotGroup = ResultGroupUser()->Get<PlotGroup>("MassHist");
    PlotWMass = MyPlotGroup->BookPlot1D("WMass", 200, 0, 100);
    PlotWPt = MyPlotGroup->BookPlot1D("WPt", 200, 0, 100);
    PlotWRapidity = MyPlotGroup->BookPlot1D("WRapidity", 100, -5, 5);
    PlotWRapidity_LeptonEta = MyPlotGroup->BookPlot2D("WRapidity_LeptonEta", 25, 0, 2.5, 25, 0, 2.5);
    PlotWp_LeptonPt_MET_LeptonEta = MyPlotGroup->BookPlot3D("Wp_LeptonPt_MET_LeptonEta", 20, 0, 200, 10, 0, 100, 25, 0, 2.5);
    PlotWm_LeptonPt_MET_LeptonEta = MyPlotGroup->BookPlot3D("Wm_LeptonPt_MET_LeptonEta", 20, 0, 200, 10, 0, 100, 25, 0, 2.5);
    PlotWp_LeptonEta = MyPlotGroup->BookPlot1D("Wp_LeptonEta", 25, 0, 2.5);
    PlotWm_LeptonEta = MyPlotGroup->BookPlot1D("Wm_LeptonEta", 25, 0, 2.5);
    PlotAsym_LeptonEta = MyPlotGroup->BookPlot1D("Asym_LeptonEta", 25, 0, 2.5);
    PlotAsym_LeptonEta->SetLinkPlot(0, PlotWp_LeptonEta);
    PlotAsym_LeptonEta->SetLinkPlot(1, PlotWm_LeptonEta);
    PlotAsym_LeptonEta->SetLinkType("Asymmetry");
    PlotX_LeptonEta = ResultGroupUser()->BookResult<NGHist<TProfile>>("PlotX_LeptonEta", "AnalysisTest_MassHist.root", 25, 0., 2.5, 0., 1.);

    return StatusCode::SUCCESS;
}

StatusCode LoopEventTest::Execute()
{
    // in real analysis, you should avoid using ResultGroup::Get method,
    // since it involves a map::find and a dynamic_cast,
    // thus will slow down your program if you get them for each entry of TTree
    // you should explicitly store the results as member variables and get them directly
    TLorentzVector WbosonP4;
    WbosonP4.SetPxPyPzE(WbosonPx, WbosonPy, WbosonPz, WbosonE);
    CountNEvents->Add();
    CountMass->Add(WbosonP4.M());
    CountTest->Add(ToolkitUser()->GetTool<Uncertainty>()->AplusB(1, 1));

    std::shared_ptr<PlotGroup> MyPlotGroup = ResultGroupUser()->Get<PlotGroup>("MassHist");
    PlotWMass->Fill(WbosonP4.M());
    PlotWPt->Fill(WbosonP4.Pt());
    PlotWRapidity->Fill(WbosonP4.Rapidity());

    TLorentzVector Muon, Neutrino;
    Muon.SetPxPyPzE(MuonPx, MuonPy, MuonPz, MuonE);
    Neutrino.SetPxPyPzE(NeutrinoPx, NeutrinoPy, NeutrinoPz, NeutrinoE);
    double mt = Kinematics::MT(Muon, Neutrino);
    if (Muon.Pt() > 25 && Neutrino.Pt() > 25 && mt > 50)
    {
        PlotWRapidity_LeptonEta->Fill(WbosonP4.Rapidity(), Muon.Eta());
        if (MuonCharge > 0)
            PlotWp_LeptonEta->Fill(Muon.Eta());
        else
            PlotWm_LeptonEta->Fill(Muon.Eta());

        if (MuonCharge > 0)
        {
            double xlarge = fabs(QuarkPz[0]) > fabs(QuarkPz[1]) ? fabs(QuarkPz[0]) : fabs(QuarkPz[1]);
            PlotX_LeptonEta->Fill(fabs(Muon.Eta()), xlarge / 13000);
        }
    }

    if (mt > 50)
    {
        if (MuonCharge > 0)
            PlotWp_LeptonPt_MET_LeptonEta->Fill(Muon.Pt(), Neutrino.Pt(), Muon.Eta());
        else
            PlotWm_LeptonPt_MET_LeptonEta->Fill(Muon.Pt(), Neutrino.Pt(), Muon.Eta());
    }

    MSGUser()->MSG(MSGLevel::DEBUG, WbosonP4.M());

    return StatusCode::SUCCESS;
}

StatusCode LoopEventTest::Finalize()
{
    /*
    MSGUser()->MSG(MSGLevel::INFO, "Events Looped : ", ResultGroupUser()->Get<Count>("NEvents")->GetSum());
    MSGUser()->MSG(MSGLevel::INFO, "Mass Mean Unc : ", ResultGroupUser()->Get<Count>("Mass")->GetMeanStatUnc());

    ResultGroupUser()->Get<PlotGroup>("MassHist")->SetSystematicVariation("Nominal");
    MSGUser()->MSG(MSGLevel::INFO, "Nominal");
    MSGUser()->MSG(MSGLevel::INFO, "Mass Mean From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WMass")->GetNominalHist()->GetMean());
    MSGUser()->MSG(MSGLevel::INFO, "WPt Mean From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WPt")->GetNominalHist()->GetMean());
    MSGUser()->MSG(MSGLevel::INFO, "WRapidity Mean From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WRapidity")->GetNominalHist()->GetMean());
    MSGUser()->MSG(MSGLevel::INFO, "WRapidity Total Number From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WRapidity")->GetNominalHist()->Integral());

    ResultGroupUser()->Get<PlotGroup>("MassHist")->SetSystematicVariation("WPtModel");
    MSGUser()->MSG(MSGLevel::INFO, "Syst: WPtModel");
    MSGUser()->MSG(MSGLevel::INFO, "Mass Mean From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WMass")->GetVariation("WPtModel")->GetMean());
    MSGUser()->MSG(MSGLevel::INFO, "WPt Mean From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WPt")->GetVariation("WPtModel")->GetMean());
    MSGUser()->MSG(MSGLevel::INFO, "WRapidity Mean From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WRapidity")->GetVariation("WPtModel")->GetMean());
    MSGUser()->MSG(MSGLevel::INFO, "WRapidity Total Number From Hist : ", ResultGroupUser()->Get<PlotGroup>("MassHist")->GetPlot1D("WRapidity")->GetVariation("WPtModel")->Integral());
    */

    return StatusCode::SUCCESS;
}
