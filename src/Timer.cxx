//***************************************************************************************
/// @file Timer.cxx
/// @author Wenhao Ma
//***************************************************************************************

#include "NAGASH/Timer.h"

using namespace std;
using namespace NAGASH;

/** @class NAGASH::Timer Timer.h "NAGASH/Timer.h"
    @ingroup ToolClasses
    @brief calculate the time interval between two time stamps.
 */

/// @brief Print the current time.
/// @param prefix some message you want to print before the current time.
void Timer::PrintCurrent(const TString &prefix)
{
    auto titleguard = MSGUser()->StartTitleWithGuard(TimerName + "::PrintCurrent");
    auto now = std::chrono::system_clock::now();
    auto t_c = std::chrono::system_clock::to_time_t(now);
    MSGUser()->MSG(MSGLevel::INFO, prefix, std::put_time(std::localtime(&t_c), "%F %T"));
}

/// @brief Record the current time.
/// @param Name name of the time stamp.
void Timer::Record(const TString &Name)
{
    if (TimeIndexMap.find(Name) == TimeIndexMap.end())
    {
        TimeIndexMap.emplace(pair<TString, int>(Name, TimeRecord.size()));
        TimeRecordName.emplace_back(Name);
        TimeRecord.emplace_back(std::chrono::system_clock::now());
    }
    else
    {
        auto guard = MSGUser()->StartTitleWithGuard("Timer::Record");
        MSGUser()->MSG_ERROR("duplicate record name ", Name, ", this won't be recorded");
    }
}

/// @brief Print the time interval between two time stamps.
/// @param Name1 name of the first time stamp.
/// @param Name2 name of the second time stamp.
void Timer::Duration(const TString &Name1, const TString &Name2)
{
    MSGUser()->StartTitle(TimerName + "::Duration");
    auto r1 = TimeIndexMap.find(Name1);
    auto r2 = TimeIndexMap.find(Name2);
    if (r1 != TimeIndexMap.end() && r2 != TimeIndexMap.end())
    {
        int id1 = r1->second, id2 = r2->second;
        if (id1 < id2)
            std::swap(id1, id2);

        double duration_h = std::chrono::duration_cast<std::chrono::hours>(TimeRecord[id1] - TimeRecord[id2]).count();
        double duration_m = std::chrono::duration_cast<std::chrono::minutes>(TimeRecord[id1] - TimeRecord[id2]).count();
        double duration_s = std::chrono::duration_cast<std::chrono::seconds>(TimeRecord[id1] - TimeRecord[id2]).count();
        double duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(TimeRecord[id1] - TimeRecord[id2]).count();
        double duration_us = std::chrono::duration_cast<std::chrono::microseconds>(TimeRecord[id1] - TimeRecord[id2]).count();
        int duration_h_int = (int)duration_h;
        int duration_m_int = (int)duration_m;
        int duration_s_int = (int)duration_s;
        int duration_ms_int = (int)duration_ms;
        if (duration_ms_int == 0)
        {
            MSGUser()->MSG(MSGLevel::INFO, "Duration between ", Name1, " and ", Name2, " : ", duration_us, " us");
        }
        else if (duration_s_int == 0)
        {
            MSGUser()->MSG(MSGLevel::INFO, "Duration between ", Name1, " and ", Name2, " : ", duration_ms, " ms");
        }
        else if (duration_m_int == 0)
        {
            duration_ms_int = duration_ms_int % 1000;
            double duration_double = duration_s + duration_ms_int / 1000.0;
            MSGUser()->MSG(MSGLevel::INFO, "Duration between ", Name1, " and ", Name2, " : ", duration_double, " s");
        }
        else if (duration_h_int == 0)
        {
            duration_s_int = duration_s_int % 60;
            MSGUser()->MSG(MSGLevel::INFO, "Duration between ", Name1, " and ", Name2, " : ", duration_m_int, " m ", duration_s_int, " s");
        }
        else
        {
            duration_m_int = duration_m_int % 60;
            MSGUser()->MSG(MSGLevel::INFO, "Duration between ", Name1, " and ", Name2, " : ", duration_h_int, " h ", duration_m_int, " m");
        }
    }

    if (r1 == TimeIndexMap.end())
        MSGUser()->MSG(MSGLevel::WARNING, "Record ", Name1, " not found");
    if (r2 == TimeIndexMap.end())
        MSGUser()->MSG(MSGLevel::WARNING, "Record ", Name2, " not found");

    MSGUser()->EndTitle();
}

/// @brief Clear all the time stamps.
void Timer::Clear()
{
    TimeRecord.clear();
    TimeIndexMap.clear();
    TimeRecordName.clear();
}
