#include "NAGASH/SystTool.h"

using namespace std;
using namespace NAGASH;

/** @class NAGASH::SystTool SystTool.h "NAGASH/SystTool.h"
    @ingroup Histograms ToolClasses
    @brief Manage systematic variations.

    Here is a demonstration of how to use the SystTool:
    @code{.cxx}
    #include "NAGASH.h"

    using namespace NAGASH;

    int main()
    {
        auto msg = std::make_shared<MSGTool>();
        auto config = std::make_shared<ConfigTool>();
        ResultGroup MyResult(msg, config);
        // prepare for data
        auto MyEV = MyResult.BookResult<EventVector>("MyEV");
        double Energy_Nominal;
        double Energy_Resolution;
        double Energy_ScaleUp;
        double Energy_ScaleDown;

        MyEV->BookVariable("Energy_Nominal", &Energy_Nominal);
        MyEV->BookVariable("Energy_Resolution", &Energy_Resolution);
        MyEV->BookVariable("Energy_ScaleUp", &Energy_ScaleUp);
        MyEV->BookVariable("Energy_ScaleDown", &Energy_ScaleDown);

        TRandom3 myrnd(12345);
        for (int i = 0; i < 1e4; i++)
        {
            Energy_Nominal = myrnd.Gaus(10, 3);
            Energy_Resolution = Energy_Nominal * myrnd.Gaus(1, 0.02);
            Energy_ScaleUp = Energy_Nominal * 1.001;
            Energy_ScaleDown = Energy_Nominal * 0.999;
            MyEV->Fill();
        }

        // define variables
        MyEV->SetVariableAddress("Energy_Nominal", &Energy_Nominal);
        MyEV->SetVariableAddress("Energy_Resolution", &Energy_Resolution);
        MyEV->SetVariableAddress("Energy_ScaleUp", &Energy_ScaleUp);
        MyEV->SetVariableAddress("Energy_ScaleDown", &Energy_ScaleDown);

        // set systematic variations
        SystTool systtool(msg);
        systtool.BookVariation<double>("E", "Nominal", &Energy_Nominal);
        systtool.BookVariation<double>("E", "Resolution", &Energy_Resolution);
        systtool.BookVariation<double>("E", "ScaleUp", &Energy_ScaleUp);
        systtool.BookVariation<double>("E", "ScaleDown", &Energy_ScaleDown);

        auto systlist = systtool.GetListOfVariations();
        auto EnergyPlot = MyResult.BookResult<Plot1D>("EnergyPlot", "output.root", 20, 0, 20);
        for (int i = 1; i < systlist.size(); i++)                   // start from index one since the first variation is always the nominal(need not to book)
            EnergyPlot->BookSystematicVariation(systlist.at(i), 1); // the first argument is the name of variation
                                                                    // the second is the correlation factor of this variation

        for (int i = 0; i < 1e4; i++)
        {
            MyEV->GetEvent(i);
            for (int j = 0; j < systlist.size(); j++)
            {
                systtool.SetToVariation(systlist[j]);
                EnergyPlot->SetSystematicVariation(j); // the zeroth variation is nominal
                                                    // the order of variations in Plot1D is the order when booking them
                                                    // use index to save the time of switching between variations
                EnergyPlot->Fill(systtool.Get<double>("E"));
            }
        }

        EnergyPlot->WriteToFile();

        return 1;
    }
    @endcode

    You can run the code and check the content of the output file `output.root`.
 */

/// @brief Get the name of the current systematic variation.
const TString &SystTool::CurrentVariation()
{
    if (!isSetVar)
    {
        MSGUser()->StartTitle("SystTool::CurrentVariation");
        MSGUser()->MSG(MSGLevel::DEBUG, "No variation is set, will set to nominal");
        MSGUser()->EndTitle();

        SetToVariation("Nominal");
        currentvar = "Nominal";
    }
    return currentvar;
}

/// @brief Set the correlation factor of the given systematic variation.
/// @param name the name of the systematic variation.
/// @param correlation the correlation factor of the systematic variation.
void SystTool::SetVariationCorrelation(const TString &name, double correlation)
{
    auto findresult = CorrMap.find(name);
    if (findresult != CorrMap.end())
    {
        findresult->second = correlation;
    }
    else
    {
        MSGUser()->StartTitle("SystTool::SetVariationCorrelation");
        MSGUser()->MSG(MSGLevel::WARNING, "no variation named ", name, ", this call will be ignored");
        MSGUser()->EndTitle();
    }
}

/// @brief Get the correlation factor of the given systematic variation.
/// @param name the name of the systematic variation.
double SystTool::GetVariationCorrelation(const TString &name)
{
    auto findresult = CorrMap.find(name);
    if (findresult != CorrMap.end())
    {
        return findresult->second;
    }
    else
    {
        MSGUser()->StartTitle("SystTool::GetVariationCorrelation");
        MSGUser()->MSG(MSGLevel::WARNING, "no variation named ", name, ", returning zero");
        MSGUser()->EndTitle();
        return 0;
    }
}

/// @brief get the list of all systematic variations.
const std::vector<TString> &SystTool::GetListOfVariations()
{
    if (isSystListSorted)
    {
        return SystList;
    }
    else
    {
        SystList.clear();
        for (auto &t : CorrMap)
        {
            SystList.emplace_back(t.first);
            // move Nominal to the front
            if (t.first == "Nominal")
                std::swap(SystList[0], SystList.back());
        }

        isSystListSorted = true;
        return SystList;
    }
}

/// @brief get the list of all systematic variations of given parameter.
/// @param name the name of the parameter.
std::vector<TString> SystTool::GetListOfVariations(const TString &name)
{
    auto findresult = SystListMap.find(name);
    if (findresult != SystListMap.end())
    {
        return SystListMap.find(name)->second;
    }
    else
    {
        MSGUser()->StartTitle("SystTool::GetListOfVariations");
        MSGUser()->MSG(MSGLevel::WARNING, "Variable ", name, " does not exist, return NULL vector");
        MSGUser()->EndTitle();
        return std::vector<TString>();
    }
}

/// @brief set the current variation to the given one.
/// @param name the name of the systematic variation.
void SystTool::SetToVariation(const TString &name)
{
    if (isSetVar)
    {
        // if SystMapFull has been made
        auto findresult = SystMapFull.find(name);
        if (findresult != SystMapFull.end())
        {
            for (auto &var : findresult->second)
            {
                *(var.Current) = *(var.Variation);
            }
            currentvar = name;
        }
        else
        {
            MSGUser()->StartTitle("SystTool::SetVariation");
            MSGUser()->MSG(MSGLevel::WARNING, "Variation ", name, " does not exist, no variation will be set");
            MSGUser()->EndTitle();
            return;
        }
    }
    else
    {
        // make SystMapFull
        SystMapFull.clear();
        for (auto &systname : SystList)
        {
            std::vector<AuxVar> tempvec;
            for (auto &t : VarMapFull)
            {
                AuxVar tempvar;
                tempvar.Current = &(t.second.CurrentVar);
                auto findresult = t.second.VarMap.find(systname);
                if (findresult != t.second.VarMap.end())
                {
                    tempvar.Variation = &(findresult->second);
                }
                else if (t.second.NominalVar.has_value())
                {
                    tempvar.Variation = &(t.second.NominalVar);
                }
                else
                {
                    MSGUser()->StartTitle("SystTool::SetVariation");
                    MSGUser()->MSG(MSGLevel::WARNING, "Variable ", t.first, " has not Nominal variation, will set to the first variation ", t.second.VarMap.begin()->first);
                    MSGUser()->EndTitle();
                    tempvar.Variation = &(t.second.VarMap.begin()->second);
                }
                tempvec.push_back(tempvar);
            }
            SystMapFull.insert(pair<TString, std::vector<AuxVar>>(systname, tempvec));
        }
        isSetVar = true;
        SetToVariation(name);
    }
}

/// @brief get the number of systematic variations.
int SystTool::GetNVariations()
{
    return SystList.size();
}

/// @brief get the number of systematic variations of given parameter.
/// @param name the name of the parameter.
int SystTool::GetNVariations(const TString &name)
{
    auto findresult = VarMapFull.find(name);
    if (findresult != VarMapFull.end())
    {
        return findresult->second.VarMap.size();
    }
    else
    {
        MSGUser()->StartTitle("SystTool::SetVariation");
        MSGUser()->MSG(MSGLevel::WARNING, "Variable ", name, " does not exist, returnint zero");
        MSGUser()->EndTitle();
        return 0;
    }
}
