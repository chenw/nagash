//***************************************************************************************
/// @file MSGTool.h
/// @author Chen Wamg, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"

namespace NAGASH
{
    /// @brief class to define different message level
    enum class MSGLevel
    {
        SILENT = 7,
        FATAL = 6,
        ERROR = 5,
        WARNING = 4,
        INFO = 3,
        FOCUS = 2,
        DEBUG = 1
    };

    /// @class NAGASH::MSGTool MSGTool.h "NAGASH/MSGTool.h"
    /// @ingroup AnalysisClasses
    /// @brief Maniplulate all messages of %NAGASH
    class MSGTool : public std::enable_shared_from_this<MSGTool>
    {
    private:
        friend class LoopEvent;
#ifdef NAGASH_HEPMC
	friend class LoopHepMC;
#endif
        friend class ConfigTool;
        friend class Analysis;

        MSGLevel OutputLevel = MSGLevel::INFO;
        MSGLevel DefinedLevel = MSGLevel::INFO;
        TString FocusName = "UNKNOWN";
        std::vector<TString> FuncTitle;
        std::ofstream outfile;
        bool tolog = false;
        bool isCreateLog = false;
        bool isOpenLog = false;
        TString LogFileName;

        mutable std::shared_mutex MSGMutex; // for using MSGTool concurrently

        void PrintTitle();
        void PrintSpace();
        void PrintEnd();
        void PrintLevel(MSGLevel level);

        template <typename... Args>
        void Print(Args &&...args);

    public:
        MSGTool(MSGLevel level, const TString &name = "UNKNOWN");
        MSGTool(const MSGTool &msg, int ThreadNumber);
        MSGTool() = default;
        MSGTool(const MSGTool &msg) = delete;
        MSGTool(MSGTool &&msg) = delete;
        MSGTool &operator=(const MSGTool &msg) = delete;
        MSGTool &operator=(MSGTool &&msg) = delete;
        ~MSGTool();

        template <typename... Args>
        void MSG(MSGLevel level, Args &&...args);
        template <typename... Args>
        void MSG_ERROR(Args &&...args);
        template <typename... Args>
        void MSG_WARNING(Args &&...args);
        template <typename... Args>
        void MSG_INFO(Args &&...args);
        template <typename... Args>
        void MSG_DEBUG(Args &&...args);

        MSGLevel SetOutputLevel(MSGLevel level);
        MSGLevel Level() const;
        void CreateLog(const TString &name);
        void OpenLog(const TString &name);
        void CloseLog();
        void StartFocusRegion(const TString &name);
        void EndFocusRegion(const TString &name);
        void StartTitle(const TString &name);
        void EndTitle();

        /** @brief Used to call MSGTool::EndTitle() automatically.

        When you must call MSGTool::StartTitleWithGuard like

        @code{c++}
        auto titleguard = MSGUser()->StartTitleWithGuard();
        @endcode

        the EndTitle() will be called automatically when titleguard destructed.
        */
        class MSGTitleGuard
        {
            friend class MSGTool;

        private:
            MSGTitleGuard(std::shared_ptr<MSGTool> tempmsg) : msg(tempmsg) {}
            std::shared_ptr<MSGTool> msg;

        public:
            MSGTitleGuard() = delete;
            MSGTitleGuard(const MSGTitleGuard &) = delete;
            MSGTitleGuard(MSGTitleGuard &&) = delete;
            MSGTitleGuard &operator=(const MSGTitleGuard &) = delete;
            MSGTitleGuard &operator=(MSGTitleGuard &&) = delete;

            ~MSGTitleGuard()
            {
                msg->EndTitle();
            }
        };

        std::unique_ptr<MSGTitleGuard> StartTitleWithGuard(const TString &title);
    };

    /// @brief Output with given message level. Messages below the level in MSGTool will be ignored.
    template <typename... Args>
    inline void MSGTool::MSG(MSGLevel level, Args &&...args)
    {
        if (level >= OutputLevel)
        {
            PrintTitle();
            PrintSpace();
            PrintLevel(level);
            PrintSpace();
            Print(args...);
            PrintEnd();
        }
    }

    /// @brief Output with ERROR level. Messages below the level in MSGTool will be ignored.
    template <typename... Args>
    inline void MSGTool::MSG_ERROR(Args &&...args)
    {
        MSG(MSGLevel::ERROR, std::forward<Args>(args)...);
    }

    /// @brief Output with WARNING level. Messages below the level in MSGTool will be ignored.
    template <typename... Args>
    inline void MSGTool::MSG_WARNING(Args &&...args)
    {
        MSG(MSGLevel::WARNING, std::forward<Args>(args)...);
    }

    /// @brief Output with INFO level. Messages below the level in MSGTool will be ignored.
    template <typename... Args>
    inline void MSGTool::MSG_INFO(Args &&...args)
    {
        MSG(MSGLevel::INFO, std::forward<Args>(args)...);
    }

    /// @brief Output with DEBUG level. Messages below the level in MSGTool will be ignored.
    template <typename... Args>
    inline void MSGTool::MSG_DEBUG(Args &&...args)
    {
        MSG(MSGLevel::DEBUG, std::forward<Args>(args)...);
    }

    template <typename... Args>
    inline void MSGTool::Print(Args &&...args)
    {
        if (tolog)
            (outfile << ... << args);
        else
            (std::cout << ... << args);
    }

    inline void MSGTool::PrintSpace()
    {
        Print("     ");
    }

    inline void MSGTool::PrintEnd()
    {
        if (tolog)
            outfile << std::endl;
        else
            std::cout << std::endl;
    }

} // namespace NAGASH
