//***************************************************************************************
/// @file Job.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/ConfigTool.h"
#include "NAGASH/MSGTool.h"
#include "NAGASH/Toolkit.h"
#include "NAGASH/Timer.h"
#include "NAGASH/ResultGroup.h"

namespace NAGASH
{
    /** @class NAGASH::Job Job.h "NAGASH/Job.h"
        @ingroup AnalysisClasses
        @brief Virtual base class for all kinds of jobs inside NAGASH, handled by Analysis.

        Note that each Job class defined by the user should contain the following static
        function which can be called by Analysis:
        @code {.cxx}
        NAGASH::StatusCode UserJob::Process(const NAGASH::ConfigTool &config, std::shared_ptr<NAGASH::ResultGroup> result, ....some other parameters....)
        {
            // the first argument is the ConfigTool that you should pass to constructor
            // the second argument is the ResultGroup you want to save. Returned by Analysis::SubmitJob().

            UserJob ujob(config, ....some other parameters....);
            // ... do something ...

            // merge the result so that it can be saved
            result->Merge(ujob.ResultGroupUser())

            return NAGASH::StatusCode::SUCCESS;
        }
        @endcode
        Details can be seen in topic @ref AnalysisClasses.
    */
    class Job
    {
    public:
        Job(const ConfigTool &c);
        Job() = delete;
        Job(const Job &) = delete;
        Job &operator=(const Job &) = delete;
        Job &operator=(Job &&) = delete;
        Job(Job &&) = delete;
        virtual ~Job() = default;
        std::shared_ptr<ResultGroup> ResultGroupUser();
        friend class Analysis;

    protected:
        std::shared_ptr<ConfigTool> ConfigUser();
        std::shared_ptr<Toolkit> ToolkitUser();
        std::shared_ptr<Timer> TimerUser();
        std::shared_ptr<MSGTool> MSGUser();
        int JobNumber();
        inline static std::recursive_mutex DefaultMutex = std::recursive_mutex(); ///< Default mutex, for multi-thread usage.

    private:
        std::shared_ptr<ConfigTool> config;
        std::shared_ptr<MSGTool> msg;
        std::shared_ptr<Toolkit> toolkit;
        std::shared_ptr<Timer> timer;
        std::shared_ptr<ResultGroup> result;
        int jobnumber;
    };

    /// @brief Constructor.
    /// @param c input ConfigTool.
    inline Job::Job(const ConfigTool &c)
    {
        config = std::make_shared<ConfigTool>(c);
        msg = ConfigUser()->MSGUser();
        jobnumber = ConfigUser()->GetPar<int>("JobNumber");
        if (jobnumber >= 0)
        {
            msg = std::make_shared<MSGTool>(*(ConfigUser()->MSGUser().get()), jobnumber);
        }
        ConfigUser()->SetMSG(MSGUser());
        result = std::make_shared<ResultGroup>(msg, config);
        toolkit = std::make_shared<Toolkit>(msg);
        timer = toolkit->GetTool<Timer>();
    }

    inline std::shared_ptr<ConfigTool> Job::ConfigUser() { return config; }       ///< get the ConfigTool.
    inline std::shared_ptr<Toolkit> Job::ToolkitUser() { return toolkit; }        ///< get the Toolkit.
    inline std::shared_ptr<MSGTool> Job::MSGUser() { return msg; }                ///< get the MSGTool.
    inline std::shared_ptr<Timer> Job::TimerUser() { return timer; }              ///< get the Timer.
    inline std::shared_ptr<ResultGroup> Job::ResultGroupUser() { return result; } ///< get the ResultGroup.
    inline int Job::JobNumber() { return jobnumber; }                             ///< get the number of job inside Analysis.
} // namespace NAGASH
