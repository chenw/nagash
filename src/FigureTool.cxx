#include "NAGASH/FigureTool.h"
#include "NAGASH/HistTool.h"

using namespace NAGASH;

/** @class NAGASH::FigureTool FigureTool.h "NAGASH/FigureTool.h"
    @ingroup ToolClasses
    @brief Class for plotting variaous types of figures.

    See the following example:
    @code{.cxx}
    // demonstrate the use of FigureTool
    #include "NAGASH/FigureTool.h"
    #include "NAGASH/Global.h"
    #include "TDatime.h"

    using namespace NAGASH;

    int main(int argc, char **argv)
    {
        // prepare histograms
        TRandom3 myrandom(0);
        TH1D *Gaus0 = new TH1D("Gaus0", "Gaus0", 100, -5, 5);
        Gaus0->Sumw2();
        TH1D *Gaus1 = new TH1D("Gaus1", "Gaus1", 100, -5, 5);
        Gaus1->Sumw2();
        TH1D *Gaus2 = new TH1D("Gaus2", "Gaus2", 100, -5, 5);
        Gaus2->Sumw2();
        TH1D *Gaus3 = new TH1D("Gaus3", "Gaus3", 100, -5, 5);
        Gaus3->Sumw2();
        TH1D *Gaus4 = new TH1D("Gaus4", "Gaus4", 100, -5, 5);
        Gaus4->Sumw2();
        TH1D *Gaus5 = new TH1D("Gaus5", "Gaus5", 100, -5, 5);
        Gaus5->Sumw2();

        for (int i = 0; i < 10000; i++)
        {
            Gaus0->Fill(myrandom.Gaus(0, 1));
            Gaus1->Fill(myrandom.Gaus(2, 1));
            Gaus2->Fill(myrandom.Gaus(-2, 1));
            Gaus3->Fill(myrandom.Gaus(0, 4));
            Gaus4->Fill(myrandom.Gaus(0, 0.25));
            Gaus5->Fill(myrandom.Gaus(0, 1.01));
        }

        std::shared_ptr<MSGTool> MSG = std::make_shared<MSGTool>();
        std::shared_ptr<FigureTool> MyFigureTool = std::make_shared<FigureTool>(MSG);
        MyFigureTool->SetFigureDirectory(".");

        // first plot, 4 pads
        auto MyCanvas = MyFigureTool->BookFigure("Test1.pdf", 5000, 5000);

        auto MyPad = MyCanvas->BookPad("Pad1", 0, 0, 2500, 2500);
        MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
        MyPad->SetAxisTitle("Value", "#frac{G}{G}");

        MyPad = MyCanvas->BookPad("Pad2", 2500, 0, 5000, 2500);
        MyPad->SetInputHist1D("Gaus1", Gaus1, "E0");
        MyPad->SetAxisTitle("Value");

        MyPad = MyCanvas->BookPad("Pad3", 0, 2500, 2500, 5000);
        MyPad->SetInputHist1D("Gaus2", Gaus2, "E0");
        // UNKNOWN set the pad left no space for the label and title of this axis
        MyPad->SetAxisTitle("UNKNOWN", "Number");
        MyPad->SetAxisTitleOffset(0, 5);

        MyPad = MyCanvas->BookPad("Pad4", 2500, 2500, 5000, 5000);
        MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
        MyPad->SetInputHist1D("Gaus3", Gaus3, "E0");
        MyPad->SetInputHist1D("Gaus4", Gaus4, "E0");

        // second plot, draw a pad inside another pad
        MyCanvas = MyFigureTool->BookFigure("Test2.pdf", 500, 500);
        MyPad = MyCanvas->BookPad("Pad0", 0, 0, 500, 500);
        MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
        MyPad->SetInputHist1D("Gaus1", Gaus1, "E0");
        MyPad->SetInputHist1D("Gaus2", Gaus2, "E0");
        MyPad->SetAxisTitle("Value", "Number");
        MyPad->SetAxisTitleOffset(1.2, 5);

        MyPad = MyCanvas->BookPad("Pad1", 250, 250, 400, 400);
        MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
        MyPad->SetInputHist1D("Gaus3", Gaus3, "E0");
        MyPad->SetInputHist1D("Gaus4", Gaus4, "E0");
        MyPad->SetAxisTitle("Value", "Number");

        // third plot, usage of hstack and histogram links, also draw in atlas style
        TF1 *f1 = new TF1(
            "gaus", [](double *x, double *par)
            { return 400 * exp(-x[0] * x[0] / 2); },
            -5, 5);

        MyCanvas = MyFigureTool->BookATLASSquare("Test3.pdf");
        MyPad = MyCanvas->BookPad("MainPad", 0, 200, 600, 600);
        {
            auto hs = MyPad->SetHStack("Gaus1+2", {"Gaus1", "Gaus2"}, "hist");
            hs->DrawTotalHist("E2", [](TH1D *h)
                            {
                                h->SetFillStyle(3354);
                                h->SetFillColor(1);
                                h->SetMarkerColorAlpha(1, 0);
                                h->SetLineColor(1); });
        }
        MyPad->SetInputHist1DGraph("Gaus0", Gaus0, "ZP",
                                [](TGraph *g)
                                {
                                    g->SetMarkerSize(0.6);
                                    g->SetMarkerColor(1);
                                    g->SetMarkerStyle(20);
                                    g->SetLineWidth(1);
                                    g->SetLineColor(1);
                                });
        MyPad->SetInputHist1D("Gaus1", Gaus1, "E0");
        MyPad->SetInputHist1D("Gaus2", Gaus2, "E0");
        MyPad->SetInputHist1D("Gaus3", Gaus3, "E0");
        MyPad->SetInputHist1D("Gaus5", Gaus5, "E0");
        MyPad->SetInputFunc1D("GausFunc", f1, "");
        MyPad->SetAxisTitle("UNKNOWN", "Number");
        // MyPad->SetGridx();
        // MyPad->SetGridy();
        // set the legend, user can either the position manually or the tool will
        // auto adjust the range of y axis to place the legend

        // MyPad->SetYaxisRange(0, 800);
        // MyPad->SetLegend("ATLAS Internal", "", 0.2, 0.6, 1, 0.9);
        // MyPad->SetLegend("ATLAS Internal    #sqrt{s}=13TeV, 140 fb^{-1}");
        MyPad->SetLegend();
        // draw ATLAS plus the string you given
        MyPad->DrawATLASLabel("Internal", 0.19, 0.84, 1);
        // draw extra text
        MyPad->SetLatex("Test", 0.19, 0.79);
        MyPad->SetLatex("Test2", 0.19, 0.74, kRed);
        MyPad->SetLatex("Test3", 0.19, 0.69, kBlue);
        MyPad->SetLegendColumn(2);

        MyPad = MyCanvas->BookPad("SubPad", 0, 0, 600, 200);
        MyPad->SetYaxisRange(0.57, 1.43);
        MyPad->SetAxisTitleOffset(1.4, 1.7);
        // define a hist using the predefined hists
        // the first the type, currently support RATIO PULL DELTA CHI2
        // then the linked list of hists, divided by '/'
        // the last is the drawing option:
        // VAL means drawing the value
        // ERR means drawing the error
        // TOPERR means using the first hist error only
        // BOTTEMERR means using the second hist error only
        MyPad->SetInputHist1D("Ratio", "RATIO/Gaus0/Gaus0/VAL", "E0");
        MyPad->SetInputHist1DGraph("Ratio2", "RATIO/Gaus0/Gaus5/VALERR", "ZP",
                                [](TGraph *g)
                                {
                                    g->SetMarkerSize(0.6);
                                    g->SetMarkerColor(1);
                                    g->SetMarkerStyle(20);
                                    g->SetLineWidth(1);
                                    g->SetLineColor(1); });
        MyPad->SetAxisTitle("Value", "#frac{Gaus0}{Gaus5}");

        // the fourth plot, for 2D hists
        gStyle->SetTimeOffset(0);
        TDatime dateBegin(2010, 1, 1, 0, 0, 0);
        TDatime dateEnd(2011, 1, 1, 0, 0, 0);

        auto h1 = new TH2D("h1", "Machine A + B", 12, dateBegin.Convert(), dateEnd.Convert(), 1000, 0, 1000);
        auto h2 = new TH2D("h2", "Machine B", 12, dateBegin.Convert(), dateEnd.Convert(), 1000, 0, 1000);

        float Rand;
        for (int i = dateBegin.Convert(); i < dateEnd.Convert(); i += 86400 * 30)
        {
            for (int j = 0; j < 1000; j++)
            {
                Rand = gRandom->Gaus(500 + sin(i / 10000000.) * 100, 50);
                h1->Fill(i, Rand);
                Rand = gRandom->Gaus(500 + sin(i / 11000000.) * 100, 70);
                h2->Fill(i, Rand);
            }
        }

        MyCanvas = MyFigureTool->BookFigure("Test4.pdf", 5000, 5000);
        MyPad = MyCanvas->BookPad("MainPad", 0, 0, 5000, 5000);
        MyPad->SetAxisTitle("Date [month/year]", "Candle");
        // user own setting of axis style
        MyPad->SetAxisStyle([](TAxis *Xaxis, TAxis *Yaxis, TAxis *Zaxis)
                            { Xaxis->SetNdivisions(510); });

        MyPad->SetInputHist2D("Machine A+B", h1, "candle2", [](TH2D *h)
                            {
                                h->SetBarWidth(0.4);
                                h->SetBarOffset(-0.25);
                                h->SetLineColor(kBlue);
                                h->SetFillColor(kYellow);
                                h->SetFillStyle(1001);
                                h->GetXaxis()->SetTimeDisplay(1);
                                h->GetXaxis()->SetTimeFormat("%m/%y"); });

        // the last argument is the user's own setting of histogram style
        MyPad->SetInputHist2D("Machine B", h2, "candle3", [](TH2D *h)
                            {
                                h->SetBarWidth(0.4);
                                h->SetBarOffset(0.25);
                                h->SetLineColor(kRed);
                                h->SetFillColor(kGreen); });

        // the fifth plot, test for colz option
        auto hcol1 = new TH2D("hcol1", "Option COLor example ", 40, -4, 4, 40, -20, 20);
        float px, py;
        for (Int_t i = 0; i < 25000; i++)
        {
            gRandom->Rannor(px, py);
            hcol1->Fill(px, 5 * py);
        }

        MyCanvas = MyFigureTool->BookFigure("Test5.pdf", 5000, 5000);
        MyPad = MyCanvas->BookPad("MainPad", 0, 0, 5000, 5000);
        MyPad->SetInputHist2D("col", hcol1, "COLZ");
        MyPad->SetAxisTitle("x", "y", "Entries");

        // the sixth plot, test for TGraph
        double x[100], y1[100], y2[100];
        int n = 20;
        for (int i = 0; i < n; i++)
        {
            x[i] = i * 0.1;
            y1[i] = 10 * sin(x[i] + 0.2);
            y2[i] = 10 * sin(x[i] + 0.4);
        }
        auto g1 = new TGraph(n, x, y1);
        auto g2 = new TGraph(n, x, y2);

        MyCanvas = MyFigureTool->BookFigure("Test6.pdf", 5000, 5000);
        MyPad = MyCanvas->BookPad("MainPad", 0, 0, 5000, 5000);
        MyPad->SetInputGraph1D("sin1", g1, "AP"); // you must set "A" option for the first element, if no other elements(THStack, TH1D, TF1) exist
        MyPad->SetInputGraph1D("sin2", g2, "P");  // you should only set "A" option once, or it will overwrite the previous plots
        MyPad->SetAxisTitle("x", "y");

        // draw the figure in one time
        MyFigureTool->DrawFigures();
    }
    @endcode
*/

// Functions for FigureStyleHelper

FigureStyleHelper::FigureStyleHelper(std::shared_ptr<MSGTool> MSG)
{
    msg = MSG;

    gStyle->SetLineStyleString(2, "[12 12]"); // postscript dashes
    gStyle->SetFrameBorderMode(0);
    gStyle->SetFrameFillColor(0);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetCanvasColor(0);
    gStyle->SetPadBorderMode(0);
    gStyle->SetPadColor(0);
    gStyle->SetStatColor(0);
    gStyle->SetPaperSize(20, 26);

    /*
     const double CL = 0.0;
     const double CH = 1.0;

     double nstops[7] = {0, 0.17, 0.33, 0.50, 0.67, 0.83, 1.0};
     double Red[7] = {CH, CH, CL, CL, CL, CH, CH};
     double Green[7] = {CL, CH, CH, CH, CL, CL, CL};
     double Blue[7] = {CL, CL, CL, CH, CH, CH, CL};

     colorstart = TColor::CreateGradientColorTable(7, nstops, Red, Green, Blue, 200);
     */

    /*
    for (int index = 0; index < 200; index++)
        MSGUser()->MSG_INFO(index, "   ", gROOT->GetColor(colorstart + index)->GetRed(), "   ", gROOT->GetColor(colorstart + index)->GetGreen(), "   ", gROOT->GetColor(colorstart + index)->GetBlue());
        */

    // set the palette for 2D plots
    // gStyle->SetPalette(0);
}

void FigureStyleHelper::SetPadStyle(FigurePad *pad)
{
    FigureElement *root = pad->GetRootElement();
    FigureSquareRange canvasrange = root->Range_Total;
    FigureSquareRange padrange = pad->Range_Total;

    // set ticks above and right
    pad->GetPad()->SetTicks();

    // get the axis from the first element
    TAxis *Xaxis = nullptr;
    TAxis *Yaxis = nullptr;
    TAxis *Zaxis = nullptr;

    bool is_for_hstack = false;

    std::shared_ptr<FigureElement> firstelement = pad->GetLinkElement(0);

    if (firstelement->IsType("FigureHist1D"))
    {
        Xaxis = (std::dynamic_pointer_cast<FigureHist1D>(firstelement))->GetHist()->GetXaxis();
        Yaxis = (std::dynamic_pointer_cast<FigureHist1D>(firstelement))->GetHist()->GetYaxis();
    }
    else if (firstelement->IsType("FigureHStack"))
    {
        Xaxis = (std::dynamic_pointer_cast<FigureHStack>(firstelement))->empty_hist->GetXaxis();
        Yaxis = (std::dynamic_pointer_cast<FigureHStack>(firstelement))->empty_hist->GetYaxis();
        is_for_hstack = true;
    }
    else if (firstelement->IsType("FigureGraph1D"))
    {
        Xaxis = (std::dynamic_pointer_cast<FigureGraph1D>(firstelement))->GetGraph()->GetXaxis();
        Yaxis = (std::dynamic_pointer_cast<FigureGraph1D>(firstelement))->GetGraph()->GetYaxis();
    }
    else if (firstelement->IsType("FigureFunc1D"))
    {
        Xaxis = (std::dynamic_pointer_cast<FigureFunc1D>(firstelement))->GetFunc()->GetXaxis();
        Yaxis = (std::dynamic_pointer_cast<FigureFunc1D>(firstelement))->GetFunc()->GetYaxis();
    }
    else if (firstelement->IsType("FigureHist2D"))
    {
        Xaxis = (std::dynamic_pointer_cast<FigureHist2D>(firstelement))->GetHist()->GetXaxis();
        Yaxis = (std::dynamic_pointer_cast<FigureHist2D>(firstelement))->GetHist()->GetYaxis();
        Zaxis = (std::dynamic_pointer_cast<FigureHist2D>(firstelement))->GetHist()->GetZaxis();
    }

    // Set Margin
    int n_bottom = canvasrange.RT_Y * TITLE_MARGIN_PERCENT;
    if (pad->NeedBottomMargin())
        pad->GetPad()->SetBottomMargin(((double)n_bottom) / (padrange.RT_Y - padrange.LB_Y));
    else
    {
        pad->GetPad()->SetBottomMargin(0);
        n_bottom = 0;
    }

    int n_left = canvasrange.RT_X * TITLE_MARGIN_PERCENT;
    if (pad->NeedLeftMargin())
        pad->GetPad()->SetLeftMargin(((double)n_left) / (padrange.RT_X - padrange.LB_X));
    else
    {
        pad->GetPad()->SetLeftMargin(0);
        n_left = 0;
    }

    int n_top = canvasrange.RT_Y * TOP_BLANK_MARGIN_PERCENT;
    if (padrange.RT_Y > canvasrange.RT_Y - n_top)
    {
        pad->GetPad()->SetTopMargin(((double)padrange.RT_Y - canvasrange.RT_Y + n_top) / (padrange.RT_Y - padrange.LB_Y));
        n_top = padrange.RT_Y - canvasrange.RT_Y + n_top;
    }
    else
    {
        pad->GetPad()->SetTopMargin(0);
        n_top = 0;
    }

    int n_right = canvasrange.RT_X * (TITLE_MARGIN_PERCENT + COLZ_MARGIN_PERCENT);
    int n_right_blank = canvasrange.RT_X * RIGHT_BLANK_MARGIN_PERCENT;
    if (padrange.RT_X > canvasrange.RT_X - n_right_blank)
        n_right_blank = padrange.RT_X - canvasrange.RT_X + n_right_blank;
    else
        n_right_blank = 0;
    if (pad->NeedRightMargin() == false)
        n_right = 0;
    if (n_right < n_right_blank)
        n_right = n_right_blank;
    pad->GetPad()->SetRightMargin(((double)n_right) / (padrange.RT_X - padrange.LB_X));

    // Set Ticks, Title, Label and Division
    int n_axis = TICK_LENGTH_PERCENT * canvasrange.RT_X;
    int n_label = TEXT_SIZE_PERCENT * canvasrange.RT_X;
    int div_n1_x = (double)DIVISION_N1 * (padrange.RT_X - padrange.LB_X - n_left - n_right) / canvasrange.RT_X;
    int div_n1_y = (double)DIVISION_N1 * (padrange.RT_Y - padrange.LB_Y - n_top - n_bottom) / canvasrange.RT_X;
    int div_n1_z = (double)DIVISION_N1 * (padrange.RT_Y - padrange.LB_Y - n_top - n_bottom) / canvasrange.RT_X;

    if (2 < div_n1_x < 5)
        div_n1_x = 5;
    if (2 < div_n1_y < 5)
        div_n1_y = 5;
    if (2 < div_n1_z < 5)
        div_n1_z = 5;

    int div_n_x = div_n1_x + 100 * DIVISION_N2 + 10000 * DIVISION_N3;
    int div_n_y = div_n1_y + 100 * DIVISION_N2 + 10000 * DIVISION_N3;
    int div_n_z = div_n1_z + 100 * DIVISION_N2 + 10000 * DIVISION_N3;

    if (Xaxis)
    {
        /*
        if (((double)n_axis) / (padrange.RT_Y - padrange.LB_Y) > 0.05)
            MSGUser()->MSG_WARNING("TICK_LENGTH_PERCENT = ", TICK_LENGTH_PERCENT, " is too large for Pad ", pad->GetName().TString::Data(), " X axis!");
            */
        Xaxis->SetTickLength(((double)n_axis) / (padrange.RT_Y - padrange.LB_Y));
        Xaxis->SetNdivisions(div_n_x);
        Xaxis->SetLabelFont(TEXT_FONT * 10 + 3);
        Xaxis->SetLabelSize(n_label);
        Xaxis->SetTitleFont(TEXT_FONT * 10 + 3);
        Xaxis->SetTitleSize(n_label);
        Xaxis->SetMaxDigits(4);

        if (!pad->user_set_title_offset)
        {
            double offset = -0.1 + 6.13 * ((double)n_bottom) / (padrange.RT_Y - padrange.LB_Y);
            if (offset < 1)
                offset = 1;
            Xaxis->SetTitleOffset(offset);
        }
        else
        {
            Xaxis->SetTitleOffset(pad->GetXTitleOffset());
        }
        if (pad->NeedBottomMargin())
            Xaxis->SetTitle(pad->GetXTitle().TString::Data());
    }
    if (Yaxis)
    {
        /*
        if (((double)n_axis) / (padrange.RT_X - padrange.LB_X) > 0.05)
            MSGUser()->MSG_WARNING("TICK_LENGTH_PERCENT = ", TICK_LENGTH_PERCENT, " is too large for Pad ", pad->GetName().TString::Data(), " Y axis!");
            */
        Yaxis->SetTickLength(((double)n_axis) / (padrange.RT_X - padrange.LB_X));
        Yaxis->SetNdivisions(div_n_y);
        // Yaxis->SetNdivisions(505);
        Yaxis->SetLabelFont(TEXT_FONT * 10 + 3);
        Yaxis->SetLabelSize(n_label);
        Yaxis->SetTitleFont(TEXT_FONT * 10 + 3);
        Yaxis->SetTitleSize(n_label);
        Yaxis->SetMaxDigits(3);

        if (!pad->user_set_title_offset)
        {
            double offset = -0.25 + 12.666 * ((double)n_left) / (padrange.RT_X - padrange.LB_X);
            if (offset < 1)
                offset = 1;
            Yaxis->SetTitleOffset(offset);
        }
        else
        {
            Yaxis->SetTitleOffset(pad->GetYTitleOffset());
        }

        if (pad->NeedLeftMargin())
            Yaxis->SetTitle(pad->GetYTitle().TString::Data());
    }
    if (Zaxis)
    {
        /*
        if (((double)n_axis) / (padrange.RT_X - padrange.LB_X) > 0.05)
            MSGUser()->MSG_WARNING("TICK_LENGTH_PERCENT = ", TICK_LENGTH_PERCENT, " is too large for Pad ", pad->GetName().TString::Data(), " Y axis!");
            */
        Zaxis->SetTickLength(((double)n_axis) / (padrange.RT_X - padrange.LB_X));
        Zaxis->SetNdivisions(div_n_z);
        Zaxis->SetLabelFont(TEXT_FONT * 10 + 3);
        Zaxis->SetLabelSize(n_label);
        Zaxis->SetTitleFont(TEXT_FONT * 10 + 3);
        Zaxis->SetTitleSize(n_label);
        Zaxis->SetMaxDigits(4);

        if (!pad->user_set_title_offset)
        {
            double offset = -0.25 + 12.666 * ((double)n_left) / (padrange.RT_X - padrange.LB_X);
            if (offset < 1)
                offset = 1;
            Zaxis->SetTitleOffset(offset);
        }
        else
        {
            Zaxis->SetTitleOffset(pad->GetZTitleOffset());
        }

        if (pad->NeedRightMargin())
            Zaxis->SetTitle(pad->GetZTitle().TString::Data());
    }

    // set the axis range, if user not set y axis range and legend position, auto adjust the range of y axis
    if (pad->user_set_rangex && Xaxis)
        Xaxis->SetRangeUser(pad->min_xaxis, pad->max_xaxis);
    if (pad->user_set_rangey && Yaxis)
        Yaxis->SetRangeUser(pad->min_yaxis, pad->max_yaxis);
    if (pad->user_set_rangez && Zaxis)
        Zaxis->SetRangeUser(pad->min_zaxis, pad->max_zaxis);

    if (Xaxis)
    {
        pad->max_xaxis = Xaxis->GetXmax();
        pad->min_xaxis = Xaxis->GetXmin();
    }

    if (Yaxis)
    {
        pad->max_yaxis = Yaxis->GetXmax();
        pad->min_yaxis = Yaxis->GetXmin();
    }

    if (Zaxis)
    {
        pad->max_zaxis = Zaxis->GetXmax();
        pad->min_zaxis = Zaxis->GetXmin();
    }

    // auto adjust y range to plot the legend
    if (!pad->user_set_rangey && pad->plot_legend && Yaxis)
    {
        double max_yaxis_adjust = 0;
        double min_yaxis_adjust = 0;
        for (int i = 0; i < pad->GetNLinkElement(); i++)
        {
            auto elem = pad->GetLinkElement(i);
            double ymin, ymax;
            if (elem->IsType("FigureHist1D"))
                std::tie(ymin, ymax) = (std::dynamic_pointer_cast<FigureHist1D>(elem))->GetMinAndMax(pad->min_xaxis, pad->max_xaxis);
            if (elem->IsType("FigureHStack"))
                std::tie(ymin, ymax) = (std::dynamic_pointer_cast<FigureHStack>(elem))->GetMinAndMax(pad->min_xaxis, pad->max_xaxis);
            if (elem->IsType("FigureGraph1D"))
                std::tie(ymin, ymax) = (std::dynamic_pointer_cast<FigureGraph1D>(elem))->GetMinAndMax(pad->min_xaxis, pad->max_xaxis);
            if (elem->IsType("FigureFunc1D"))
                std::tie(ymin, ymax) = (std::dynamic_pointer_cast<FigureFunc1D>(elem))->GetMinAndMax(pad->min_xaxis, pad->max_xaxis);

            if (i == 0)
            {
                min_yaxis_adjust = ymin;
                max_yaxis_adjust = ymax;
            }
            else
            {
                if (ymin < min_yaxis_adjust)
                    min_yaxis_adjust = ymin;
                if (ymax > max_yaxis_adjust)
                    max_yaxis_adjust = ymax;
            }
        }

        if (is_for_hstack)
            min_yaxis_adjust = 0;

        if (!pad->user_set_legend_position)
        {
            double length_pad;
            int nrows = pad->n_colored_objects / pad->legend_column;
            if (pad->n_colored_objects % pad->legend_column != 0)
                nrows += 1;

            if ((1 + nrows) * LeftPercentage + 0.04 < 0.5)
                length_pad = (max_yaxis_adjust - min_yaxis_adjust) / ((1 - (1 + nrows) * LeftPercentage - 0.04));
            else
                length_pad = (max_yaxis_adjust - min_yaxis_adjust) / 0.5;

            max_yaxis_adjust += (0.04 + (1 + nrows) * LeftPercentage) * length_pad * 1.2;
            min_yaxis_adjust += -LeftPercentage * length_pad;

            if (is_for_hstack)
                min_yaxis_adjust = 0;
        }
        else
        {
            double top_percentage = 1 - pad->GetPad()->GetTopMargin() - pad->legend_ymin;
            double length_pad = (max_yaxis_adjust - min_yaxis_adjust) / (1 - top_percentage);

            max_yaxis_adjust += top_percentage * length_pad * 1.2;
            min_yaxis_adjust += -LeftPercentage * length_pad;

            if (is_for_hstack)
                min_yaxis_adjust = 0;
        }

        Yaxis->SetRangeUser(min_yaxis_adjust, max_yaxis_adjust);
    }

    // set the legend position and style
    if (pad->plot_legend)
    {
        pad->legend->SetFillColor(0);
        pad->legend->SetFillStyle(0);
        pad->legend->SetLineColor(0);
        pad->legend->SetLineWidth(0);
        pad->legend->SetTextSize(LEGEND_TEXT_SIZE_PERCENT);
        pad->legend->SetTextFont(TEXT_FONT * 10 + 2);

        if (pad->user_set_legend_position)
        {
            pad->legend->SetX1(pad->legend_xmin);
            pad->legend->SetX2(pad->legend_xmax);
            pad->legend->SetY1(pad->legend_ymin);
            pad->legend->SetY2(pad->legend_ymax);
        }
        else
        {
            if (pad->user_set_rangey)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("FigureStyleHelper::SetPadStyle");
                MSGUser()->MSG_WARNING("pad y axis range set but legend position not set, user should manually set the position of legend");
            }

            double percentage;

            if (pad->n_colored_objects % pad->legend_column == 0)
                percentage = (1 + (int)(pad->n_colored_objects / pad->legend_column)) * LeftPercentage;
            else
                percentage = (2 + (int)(pad->n_colored_objects / pad->legend_column)) * LeftPercentage;

            pad->legend->SetX1(1 - pad->GetPad()->GetRightMargin() - 0.04 - LEGEND_COLUMN_WIDTH * pad->legend_column);
            pad->legend->SetX2(1 - pad->GetPad()->GetRightMargin() - 0.04);
            pad->legend->SetY1(1 - pad->GetPad()->GetTopMargin() - percentage);
            pad->legend->SetY2(1 - pad->GetPad()->GetTopMargin() - 0.04);

            // MSGUser()->MSG_INFO(pad->GetPad()->GetLeftMargin() + 0.04, " ", 1 - pad->GetPad()->GetLeftMargin() - 0.04, " ", 1 - pad->GetPad()->GetTopMargin() - percentage, " ", 1 - pad->GetPad()->GetTopMargin() - 0.04);
        }
    }

    if (pad->AxisOptionFunc)
    {
        pad->AxisOptionFunc(Xaxis, Yaxis, Zaxis);
    }
}

void FigureStyleHelper::SetHist1DStyle(FigureHist1D *hist)
{
    hist->GetHist()->SetTitle("");
    hist->GetHist()->SetStats(0);

    // calculate the color
    /*
    int index = hist->index_in_pad;
    int nelements = ((FigurePad *)(hist->GetMotherElement()))->GetNColoredObjects();
    int step = 199 / nelements;
    if (step > 66)
        step = 66;

    if (index % 2 == 0)
        index = index / 2;
    else
        index = nelements - ((index + 1) / 2);

    int ncolor = index * step + colorstart;
    */

    // color scheme in NGFigure
    int index = hist->index_in_pad % 12;
    int round = (hist->index_in_pad - index) / 12 * 2 + 1;
    Color_t mypalette[12] = {kRed, kBlue, kGreen, kPink, kAzure, kOrange, kMagenta, kCyan, kYellow, kViolet, kTeal, kSpring + 1};
    Color_t ncolor = mypalette[index] + round;

    hist->GetHist()->SetLineColor(ncolor);
    hist->GetHist()->SetMarkerColor(ncolor);
    hist->GetHist()->SetLineWidth(2);
    hist->GetHist()->SetMarkerSize(0);
    hist->GetHist()->SetFillStyle(0);
    if (hist->ForHStack)
    {
        hist->GetHist()->SetFillStyle(1001);
        hist->GetHist()->SetFillColor(ncolor);
        hist->GetHist()->SetLineWidth(0);
    }

    // MSGUser()->MSG_INFO(index, " ", ncolor, "   ", gROOT->GetColor(ncolor)->GetRed(), "   ", gROOT->GetColor(ncolor)->GetGreen(), "   ", gROOT->GetColor(ncolor)->GetBlue());
}

void FigureStyleHelper::SetGraph1DStyle(FigureGraph1D *graph)
{
    graph->GetGraph()->SetTitle("");

    // color scheme in NGFigure
    int index = graph->index_in_pad % 12;
    int round = (graph->index_in_pad - index) / 12 * 2 + 1;
    Color_t mypalette[12] = {kRed, kBlue, kGreen, kPink, kAzure, kOrange, kMagenta, kCyan, kYellow, kViolet, kTeal, kSpring + 1};
    Color_t ncolor = mypalette[index] + round;

    int Marker_Style[6] = {20, 21, 22, 23, 29, 33};

    graph->GetGraph()->SetLineWidth(2);
    graph->GetGraph()->SetLineColor(ncolor);
    graph->GetGraph()->SetLineStyle(1);

    FigureElement *root = graph->GetRootElement();
    FigureSquareRange canvasrange = root->Range_Total;

    graph->GetGraph()->SetMarkerSize(0.002 * (canvasrange.RT_Y - canvasrange.LB_Y));
    graph->GetGraph()->SetMarkerColor(ncolor);
    graph->GetGraph()->SetMarkerStyle(Marker_Style[graph->index_in_pad % 6]);
    graph->GetGraph()->SetFillStyle(0);
}

void FigureStyleHelper::SetHist1DGraphStyle(FigureHist1D *hist)
{
    hist->mygraph->SetTitle("");

    // color scheme in NGFigure
    int index = hist->index_in_pad % 12;
    int round = (hist->index_in_pad - index) / 12 * 2 + 1;
    Color_t mypalette[12] = {kRed, kBlue, kGreen, kPink, kAzure, kOrange, kMagenta, kCyan, kYellow, kViolet, kTeal, kSpring + 1};
    Color_t ncolor = mypalette[index] + round;

    int Marker_Style[6] = {20, 21, 22, 23, 29, 33};

    hist->mygraph->SetLineWidth(2);
    hist->mygraph->SetLineColor(ncolor);
    hist->mygraph->SetLineStyle(1);

    FigureElement *root = hist->GetRootElement();
    FigureSquareRange canvasrange = root->Range_Total;

    hist->mygraph->SetMarkerSize(0.002 * (canvasrange.RT_Y - canvasrange.LB_Y));
    hist->mygraph->SetMarkerColor(ncolor);
    hist->mygraph->SetMarkerStyle(Marker_Style[hist->index_in_pad % 6]);
    hist->mygraph->SetFillStyle(0);
}

void FigureStyleHelper::SetFunc1DStyle(FigureFunc1D *func)
{
    func->GetFunc()->SetTitle("");
    // color scheme in NGFigure
    int index = func->index_in_pad % 12;
    int round = (func->index_in_pad - index) / 12 * 2 + 1;
    Color_t mypalette[12] = {kRed, kBlue, kGreen, kPink, kAzure, kOrange, kMagenta, kCyan, kYellow, kViolet, kTeal, kSpring + 1};
    Color_t ncolor = mypalette[index] + round;

    func->GetFunc()->SetLineWidth(2);
    func->GetFunc()->SetLineColor(ncolor);
    func->GetFunc()->SetFillColor(ncolor);
    func->GetFunc()->SetMarkerColor(ncolor);
    func->GetFunc()->SetFillStyle(0);
}

void FigureStyleHelper::SetHist2DStyle(FigureHist2D *hist)
{
    hist->GetHist()->SetTitle("");
    hist->GetHist()->SetStats(0);
    // color scheme in NGFigure
    int index = hist->index_in_pad % 12;
    int round = (hist->index_in_pad - index) / 12 * 2 + 1;
    Color_t mypalette[12] = {kRed, kBlue, kGreen, kPink, kAzure, kOrange, kMagenta, kCyan, kYellow, kViolet, kTeal, kSpring + 1};
    Color_t ncolor = mypalette[index] + round;

    hist->GetHist()->SetLineColor(ncolor);
    hist->GetHist()->SetFillColor(ncolor);
}

void FigureStyleHelper::SetLatexStyle(FigureLatex *latex)
{
    latex->mylatex->SetTextFont(TEXT_FONT * 10 + 2);
    latex->mylatex->SetTextSize(LEGEND_TEXT_SIZE_PERCENT);
}

FigureStyleHelper::~FigureStyleHelper()
{
}

// Functions for FigureElement

FigureElement::FigureElement(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const TString &tyname, FigureElement *mother)
{
    msg = MSG;
    stylehelper = shelper;
    name = elementname;
    castname = tyname;
    MotherElement = mother;
}

void FigureElement::DrawElement()
{
    MSGUser()->MSG_ERROR("Function FigureElement::DrawElement Should be override!!!");
}

void FigureElement::CD()
{
    // This is usually empty override if necessary
}

void FigureElement::SetStyle()
{
    // This is usually empty override if necessary
}

void FigureElement::Draw()
{
    DrawElement();
    for (auto &iter : LinkedElement)
    {
        CD();
        iter->Draw();
    }
    SetStyle();
}

bool FigureElement::Contain(std::shared_ptr<FigureElement> subelement)
{
    bool contain = false;
    for (int i = 0; i < Range_vec.size(); i++)
    {
        for (int j = 0; j < subelement->Range_vec.size(); j++)
        {
            if (Contain(Range_vec[i], (subelement->Range_vec)[j]) == true)
                contain = true;
        }
    }

    return contain;
}

FigureElement *FigureElement::GetRootElement()
{
    FigureElement *root = this;

    while (root->MotherElement != nullptr)
        root = root->MotherElement;

    return root;
}

std::shared_ptr<FigureElement> FigureElement::FindLinkElement(const TString &name)
{
    if (auto findresult = MapLinkedElement.find(name); findresult != MapLinkedElement.end())
        return LinkedElement.at(findresult->second);

    for (auto &iter : LinkedElement)
    {
        auto foundelement = iter->FindLinkElement(name);
        if (foundelement != nullptr)
            return foundelement;
    }

    return nullptr;
}

std::shared_ptr<FigureElement> FigureElement::TraceElement(const TString &name)
{
    FigureElement *root = GetRootElement();
    return root->FindLinkElement(name);
}

bool FigureElement::CheckDuplicate(const TString &elementname)
{
    if (MapLinkedElement.find(elementname) != MapLinkedElement.end())
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("FigureElement::CheckDuplicate");
        MSGUser()->MSG_ERROR("can not book element with same name ", elementname);
        return false;
    }

    return true;
}

bool FigureElement::Contain(FigureSquareRange A, FigureSquareRange SubA)
{
    bool contain = true;
    if (A.LB_X > SubA.LB_X)
        contain = false;
    if (A.LB_Y > SubA.LB_Y)
        contain = false;
    if (A.RT_X < SubA.RT_X)
        contain = false;
    if (A.RT_Y < SubA.RT_Y)
        contain = false;

    return contain;
}

int FigureElement::GetLinkIndex(const TString &name)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("FigureElement::GetLinkIndex");
    if (auto findresult = MapLinkedElement.find(name); findresult != MapLinkedElement.end())
        return findresult->second;
    else
    {
        MSGUser()->MSG_ERROR("does not find element ", name, " return -1");
        return -1;
    }
}

std::shared_ptr<FigureElement> FigureElement::GetLinkElement(int index)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("FigureElement::GetLinkElement");
    if (index < 0 || index >= LinkedElement.size())
    {
        MSGUser()->MSG_ERROR("index out of range, return nullptr");
        return nullptr;
    }
    else
        return LinkedElement[index];
}

// Functions for FigureHist1D

FigureHist1D::FigureHist1D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const TString &linkinfo, const TString &option, FigureElement *mother, std::function<void(TH1D *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureHist1D", mother)
{
    LinkInfo = linkinfo;
    DrawOption = option;
    OptionFunc = optionfunc;
    FromHistLink = true;
}

FigureHist1D::FigureHist1D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TH1D *hist, const TString &option, FigureElement *mother, std::function<void(TH1D *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureHist1D", mother)
{
    myhist = (TH1D *)hist->Clone(TString::Format("Temp_%s", elementname.TString::Data()).TString::Data());
    myhist->SetDirectory(0);
    DrawOption = option;
    OptionFunc = optionfunc;
    FromHistLink = false;
}

// Draw histogram as TGraph rather than TH1D
FigureHist1D::FigureHist1D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TH1D *hist, const TString &option, FigureElement *mother, std::function<void(TGraph *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureHist1D", mother)
{
    myhist = (TH1D *)hist->Clone(TString::Format("Temp_%s", elementname.TString::Data()).TString::Data());
    myhist->SetDirectory(0);
    DrawOption = option;
    TGraphOptionFunc = optionfunc;
    FromHistLink = false;
    DrawAsTGraph = true;
}

FigureHist1D::FigureHist1D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const TString &linkinfo, const TString &option, FigureElement *mother, std::function<void(TGraph *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureHist1D", mother)
{
    LinkInfo = linkinfo;
    DrawOption = option;
    TGraphOptionFunc = optionfunc;
    FromHistLink = true;
    DrawAsTGraph = true;
}

void FigureHist1D::DrawElement()
{
    if (FromHistLink == true && myhist == nullptr)
        CreateHistFromHistLink();
    if (myhist == nullptr)
        return;
    if (ForHStack)
        return;

    if (!DrawAsTGraph)
        myhist->Draw(DrawOption);
    else
    {
        // convert the histogram to TGraph
        mygraph = new TGraphErrors((TH1 *)myhist);
        mygraph->GetXaxis()->SetRangeUser(myhist->GetXaxis()->GetXmin(), myhist->GetXaxis()->GetXmax());

        // set ex to zero
        for (int i = 0; i < mygraph->GetN(); i++)
            mygraph->SetPointError(i, 0, mygraph->GetErrorY(i));

        mygraph->Draw(DrawOption);
    }
}

void FigureHist1D::SetStyle()
{
    if (!DrawAsTGraph)
    {
        StyleUser()->SetHist1DStyle(this);
        OptionFunc(myhist);
    }
    else
    {
        StyleUser()->SetHist1DGraphStyle(this);
        TGraphOptionFunc((TGraph *)mygraph);
    }
}

FigureHist1D::~FigureHist1D()
{
    delete myhist;
    if (mygraph)
        delete mygraph;
}

TString FigureHist1D::GetLinkMode()
{
    if (LinkInfo.TString::BeginsWith("RATIO"))
        return "RATIO";
    if (LinkInfo.TString::BeginsWith("PULL"))
        return "PULL";
    if (LinkInfo.TString::BeginsWith("DELTA"))
        return "DELTA";
    if (LinkInfo.TString::BeginsWith("CHI2"))
        return "CHI2";
    return "UNKNOWN";
}

std::vector<TString> FigureHist1D::GetLinkObject()
{
    std::vector<TString> LinkObjectName;
    TString name = "";
    bool start = false;
    for (int i = 0; i < LinkInfo.Length(); i++)
    {
        if (LinkInfo(i) == '/' && start == true)
        {
            LinkObjectName.push_back(name);
            name = "";
        }

        if (start == true && LinkInfo(i) != '/')
            name = name + LinkInfo(i);

        if (LinkInfo(i) == '/' && start == false)
        {
            start = true;
            name = "";
        } // Start
    }

    return LinkObjectName;
}

void FigureHist1D::CreateHistFromHistLink()
{
    TString LinkMode = GetLinkMode();
    if (LinkMode.TString::EqualTo("UNKNOWN"))
    {
        MSGUser()->MSG_ERROR("FigureHist1D ", GetName().TString::Data(), " cannot be generated from ", LinkMode.TString::Data());
        return;
    }

    auto LinkObjectName = GetLinkObject();
    std::vector<std::shared_ptr<FigureElement>> LinkObject;
    for (int i = 0; i < LinkObjectName.size(); i++)
    {
        auto obj = TraceElement(LinkObjectName[i]);
        if (obj != nullptr)
            LinkObject.push_back(obj);
        else
            MSGUser()->MSG_ERROR("can not find linked element ", LinkObjectName[i]);
    }

    std::vector<TH1D *> LinkHists;

    auto CheckLinkObjectType = [&](int num) -> bool
    {
        for (int i = 0; i < num; i++)
        {
            if (LinkObject[i]->IsType("FigureHist1D"))
            {
                if ((std::dynamic_pointer_cast<FigureHist1D>(LinkObject[i]))->GetHist() == nullptr)
                    (std::dynamic_pointer_cast<FigureHist1D>(LinkObject[i]))->CreateHistFromHistLink();

                LinkHists.emplace_back(std::dynamic_pointer_cast<FigureHist1D>(LinkObject[i])->GetHist());
            }
            else if (LinkObject[i]->IsType("FigureHStack"))
            {
                LinkHists.emplace_back(std::dynamic_pointer_cast<FigureHStack>(LinkObject[i])->GetTotalHist());
            }
            else
            {
                MSGUser()->MSG_ERROR("Link Object must be FigureHist1D or FigureHStack");
                return false;
            }
        }

        return true;
    };

    if (LinkMode.TString::EqualTo("RATIO"))
    {
        // This needs two object
        if (LinkObject.size() != 2)
        {
            MSGUser()->MSG_ERROR("RATIO Link Needs two objects!!! FigureHist1D:", GetName().TString::Data(), " has ", LinkObject.size());
            return;
        }

        if (!CheckLinkObjectType(2))
            return;

        std::shared_ptr<HistTool> histtool = std::make_shared<HistTool>(MSGUser());
        TH1D *hist0 = nullptr;
        TH1D *hist1 = nullptr;
        hist0 = (TH1D *)(LinkHists[0]->Clone("TempRatio0_" + GetName()));
        if (LinkObject[0] == LinkObject[1])
            hist1 = hist0;
        else
            hist1 = (TH1D *)(LinkHists[1]->Clone("TempRatio1_" + GetName()));

        myhist = (TH1D *)hist0->Clone(GetName().TString::Data());
        histtool->Ratio(hist0, hist1, myhist);

        if (LinkInfo.TString::EndsWith("/") == false)
        {
            TString specialterm = LinkInfo(LinkInfo.Last('/') + 1, LinkInfo.Length() - LinkInfo.Last('/') - 1);
            if (specialterm.Contains("VAL") == false && specialterm.Contains("ERR") == false)
                specialterm = "VALERR";

            if (specialterm.Contains("VAL") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinContent(i + 1, 1);
            }

            if (specialterm.Contains("ERR") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinError(i + 1, 0);
            }

            if (specialterm.Contains("TOPERR"))
            {
                for (int i = 0; i < hist1->GetXaxis()->GetNbins(); i++)
                    hist1->SetBinError(i + 1, 0);
                histtool->Ratio(hist0, hist1, myhist);
            }

            if (specialterm.Contains("BOTTOMERR"))
            {
                for (int i = 0; i < hist0->GetXaxis()->GetNbins(); i++)
                    hist0->SetBinError(i + 1, 0);
                histtool->Ratio(hist0, hist1, myhist);
            }
        }

        delete hist0;
        if (hist0 != hist1)
            delete hist1;
    }

    if (LinkMode.TString::EqualTo("PULL"))
    {
        // This needs two object
        if (LinkObject.size() != 2)
        {
            MSGUser()->MSG_ERROR("PULL Link Needs two objects!!! FigureHist1D:", GetName().TString::Data(), " has ", LinkObject.size());
            return;
        }

        if (!CheckLinkObjectType(2))
            return;

        std::shared_ptr<HistTool> histtool = std::make_shared<HistTool>(MSGUser());
        auto hist0 = (TH1D *)(LinkHists[0]->Clone("TempPull0_" + GetName()));
        auto hist1 = (TH1D *)(LinkHists[1]->Clone("TempPull1_" + GetName()));
        myhist = (TH1D *)hist0->Clone(GetName().TString::Data());
        histtool->Pull(hist0, hist1, myhist);

        if (LinkInfo.TString::EndsWith("/") == false)
        {
            TString specialterm = LinkInfo(LinkInfo.Last('/') + 1, LinkInfo.Length() - LinkInfo.Last('/') - 1);
            if (specialterm.Contains("VAL") == false && specialterm.Contains("ERR") == false)
                specialterm = "VALERR";

            if (specialterm.Contains("VAL") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinContent(i + 1, 1);
            }

            if (specialterm.Contains("ERR") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinError(i + 1, 0);
            }

            if (specialterm.Contains("TOPERR"))
            {
                for (int i = 0; i < hist1->GetXaxis()->GetNbins(); i++)
                    hist1->SetBinError(i + 1, 0);
                histtool->Pull(hist0, hist1, myhist);
            }

            if (specialterm.Contains("BOTTOMERR"))
            {
                for (int i = 0; i < hist0->GetXaxis()->GetNbins(); i++)
                    hist0->SetBinError(i + 1, 0);
                histtool->Pull(hist0, hist1, myhist);
            }
        }

        delete hist0;
        delete hist1;
    }

    if (LinkMode.TString::EqualTo("DELTA"))
    {
        // This needs two object
        if (LinkObject.size() != 2)
        {
            MSGUser()->MSG_ERROR("DELTA Link Needs two objects!!! FigureHist1D:", GetName().TString::Data(), " has ", LinkObject.size());
            return;
        }

        if (!CheckLinkObjectType(2))
            return;

        std::shared_ptr<HistTool> histtool = std::make_shared<HistTool>(MSGUser());
        auto hist0 = (TH1D *)(LinkHists[0]->Clone("TempDelta0_" + GetName()));
        auto hist1 = (TH1D *)(LinkHists[1]->Clone("TempDelta1_" + GetName()));
        myhist = (TH1D *)hist0->Clone(GetName().TString::Data());
        histtool->Delta(hist0, hist1, myhist);

        if (LinkInfo.TString::EndsWith("/") == false)
        {
            TString specialterm = LinkInfo(LinkInfo.Last('/') + 1, LinkInfo.Length() - LinkInfo.Last('/') - 1);
            if (specialterm.Contains("VAL") == false && specialterm.Contains("ERR") == false)
                specialterm = "VALERR";

            if (specialterm.Contains("VAL") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinContent(i + 1, 1);
            }

            if (specialterm.Contains("ERR") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinError(i + 1, 0);
            }

            if (specialterm.Contains("TOPERR"))
            {
                for (int i = 0; i < hist1->GetXaxis()->GetNbins(); i++)
                    hist1->SetBinError(i + 1, 0);
                histtool->Delta(hist0, hist1, myhist);
            }

            if (specialterm.Contains("BOTTOMERR"))
            {
                for (int i = 0; i < hist0->GetXaxis()->GetNbins(); i++)
                    hist0->SetBinError(i + 1, 0);
                histtool->Delta(hist0, hist1, myhist);
            }
        }

        delete hist0;
        delete hist1;
    }

    if (LinkMode.TString::EqualTo("CHI2"))
    {
        // This needs two object
        if (LinkObject.size() != 2)
        {
            MSGUser()->MSG_ERROR("CHI2 Link Needs two objects!!! FigureHist1D:", GetName().TString::Data(), " has ", LinkObject.size());
            return;
        }

        if (!CheckLinkObjectType(2))
            return;

        std::shared_ptr<HistTool> histtool = std::make_shared<HistTool>(MSGUser());
        auto hist0 = (TH1D *)(LinkHists[0]->Clone("TempChi20_" + GetName()));
        auto hist1 = (TH1D *)(LinkHists[1]->Clone("TempChi21_" + GetName()));
        myhist = (TH1D *)hist0->Clone(GetName().TString::Data());
        histtool->Chi2(hist0, hist1, myhist);

        if (LinkInfo.TString::EndsWith("/") == false)
        {
            TString specialterm = LinkInfo(LinkInfo.Last('/') + 1, LinkInfo.Length() - LinkInfo.Last('/') - 1);
            if (specialterm.Contains("VAL") == false && specialterm.Contains("ERR") == false)
                specialterm = "VALERR";

            if (specialterm.Contains("VAL") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinContent(i + 1, 1);
            }

            if (specialterm.Contains("ERR") == false)
            {
                for (int i = 0; i < myhist->GetXaxis()->GetNbins(); i++)
                    myhist->SetBinError(i + 1, 0);
            }

            if (specialterm.Contains("TOPERR"))
            {
                for (int i = 0; i < hist1->GetXaxis()->GetNbins(); i++)
                    hist1->SetBinError(i + 1, 0);
                histtool->Chi2(hist0, hist1, myhist);
            }

            if (specialterm.Contains("BOTTOMERR"))
            {
                for (int i = 0; i < hist0->GetXaxis()->GetNbins(); i++)
                    hist0->SetBinError(i + 1, 0);
                histtool->Chi2(hist0, hist1, myhist);
            }
        }

        delete hist0;
        delete hist1;
    }
}

std::pair<double, double> FigureHist1D::GetMinAndMax(double xmin, double xmax)
{
    if (FromHistLink == true && myhist == nullptr)
        CreateHistFromHistLink();

    double ymin, ymax;
    int count = 0;

    for (int i = 1; i <= myhist->GetNbinsX(); i++)
    {
        if (myhist->GetBinLowEdge(i) < xmin)
            continue;
        if (myhist->GetBinLowEdge(i) + myhist->GetBinWidth(i) > xmax)
            continue;

        if (count == 0)
        {
            ymin = myhist->GetBinContent(i) - myhist->GetBinError(i);
            ymax = myhist->GetBinContent(i) + myhist->GetBinError(i);
        }
        else
        {
            if (ymin > myhist->GetBinContent(i) - myhist->GetBinError(i))
                ymin = myhist->GetBinContent(i) - myhist->GetBinError(i);
            if (ymax < myhist->GetBinContent(i) + myhist->GetBinError(i))
                ymax = myhist->GetBinContent(i) + myhist->GetBinError(i);
        }

        ++count;
    }

    return std::pair<double, double>(ymin, ymax);
}

void FigureHist1D::Scale(double factor)
{
    if (FromHistLink && myhist == nullptr)
        CreateHistFromHistLink();

    myhist->Scale(factor);
}

void FigureHist1D::Rebin(int num)
{
    if (FromHistLink && myhist == nullptr)
        CreateHistFromHistLink();

    myhist->Rebin(num);
}

// Functions for FigureHStack
FigureHStack::FigureHStack(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const std::vector<TString> &linkhists, const TString &option, FigureElement *mother)
    : FigureElement(MSG, shelper, elementname, "FigureHStack", mother)
{
    this->LinkedHists = linkhists;
    this->DrawOption = option;
    myhstack = new THStack(elementname, "");

    if (linkhists.size() == 0)
        MSGUser()->MSG_ERROR("you should link at least one hist to hstack");
}

void FigureHStack::SetStyle()
{
    auto titleguard = MSGUser()->StartTitleWithGuard("FigureHStack::SetStyle");

    for (auto &name : LinkedHists)
    {
        auto obj = TraceElement(name);
        if (obj == nullptr)
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not exist, will be ignored");
            continue;
        }

        if (obj->GetTypeName() != "FigureHist1D")
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not belong to FigureHist1D, will be ignored");
            continue;
        }

        std::shared_ptr<FigureHist1D> hist = std::dynamic_pointer_cast<FigureHist1D>(obj);
        if (hist->FromHistLink == true && hist->myhist == nullptr)
            hist->CreateHistFromHistLink();

        myhstack->Add(hist->GetHist());
        hist->ForHStack = true;
    }
}

void FigureHStack::DrawElement()
{
    auto titleguard = MSGUser()->StartTitleWithGuard("FigureHStack::DrawElement");
    int count = 0;
    for (auto &name : LinkedHists)
    {
        auto obj = TraceElement(name);
        if (obj == nullptr)
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not exist, will be ignored");
            continue;
        }

        if (obj->GetTypeName() != "FigureHist1D")
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not belong to FigureHist1D, will be ignored");
            continue;
        }

        std::shared_ptr<FigureHist1D> hist = std::dynamic_pointer_cast<FigureHist1D>(obj);
        if (hist->FromHistLink == true && hist->myhist == nullptr)
            hist->CreateHistFromHistLink();

        if (count == 0)
        {
            empty_hist = (TH1D *)(hist->GetHist()->Clone("empty_hist"));
            empty_hist->SetDirectory(0);
            empty_hist->SetStats(0);
            empty_hist->SetTitle("");
            empty_hist->Reset("ICESM");

            empty_hist->Draw();
        }

        ++count;
    }

    myhstack->Draw(DrawOption + " same");

    if (draw_total_hist)
    {
        GetTotalHist();
        total_hist_optionfunc(total_hist);
        total_hist->SetTitle("");
        total_hist->SetStats(0);
        total_hist->Draw(total_hist_drawoption);
    }
}

/// @brief Whether draw the total hist of the hstack.
/// @param option option for drawing the total hist.
/// @param optionfunc user-defined function for adjusting the style of the total hist, takes the pointer of TH1D as input.
void FigureHStack::DrawTotalHist(const TString &option, std::function<void(TH1D *)> optionfunc)
{
    draw_total_hist = true;
    total_hist_drawoption = option + " same";
    total_hist_optionfunc = optionfunc;
}

TH1D *FigureHStack::GetTotalHist()
{
    CalculateTotalHist();
    return total_hist;
}

void FigureHStack::CalculateTotalHist()
{
    if (total_hist)
        return;

    int count = 0;
    for (auto &name : LinkedHists)
    {
        auto obj = TraceElement(name);
        if (obj == nullptr)
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not exist, will be ignored");
            continue;
        }

        if (obj->GetTypeName() != "FigureHist1D")
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not belong to FigureHist1D, will be ignored");
            continue;
        }

        std::shared_ptr<FigureHist1D> hist = std::dynamic_pointer_cast<FigureHist1D>(obj);
        if (hist->FromHistLink == true && hist->myhist == nullptr)
            hist->CreateHistFromHistLink();

        if (count == 0)
        {
            total_hist = (TH1D *)(hist->GetHist()->Clone("total_hist"));
            total_hist->SetDirectory(0);
            // total_hist->Sumw2();
        }
        else
        {
            total_hist->Add(hist->GetHist());
        }

        ++count;
    }
}

std::pair<double, double> FigureHStack::GetMinAndMax(double xmin, double xmax)
{
    CalculateTotalHist();
    double ymin, ymax;
    int count = 0;

    for (int i = 1; i <= total_hist->GetNbinsX(); i++)
    {
        if (total_hist->GetBinLowEdge(i) < xmin)
            continue;
        if (total_hist->GetBinLowEdge(i) + total_hist->GetBinWidth(i) > xmax)
            continue;

        if (count == 0)
        {
            ymin = total_hist->GetBinContent(i) - total_hist->GetBinError(i);
            ymax = total_hist->GetBinContent(i) + total_hist->GetBinError(i);
        }
        else
        {
            if (ymin > total_hist->GetBinContent(i) - total_hist->GetBinError(i))
                ymin = total_hist->GetBinContent(i) - total_hist->GetBinError(i);
            if (ymax < total_hist->GetBinContent(i) + total_hist->GetBinError(i))
                ymax = total_hist->GetBinContent(i) + total_hist->GetBinError(i);
        }

        ++count;
    }

    return std::pair<double, double>(ymin, ymax);
}

void FigureHStack::Scale(double factor)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("FigureHStack::Scale");
    for (auto &name : LinkedHists)
    {
        auto obj = TraceElement(name);
        if (obj == nullptr)
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not exist, will be ignored");
            continue;
        }

        if (obj->GetTypeName() != "FigureHist1D")
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not belong to FigureHist1D, will be ignored");
            continue;
        }

        std::shared_ptr<FigureHist1D> hist = std::dynamic_pointer_cast<FigureHist1D>(obj);
        hist->Scale(factor);
    }

    if (total_hist)
        total_hist->Scale(factor);
}

void FigureHStack::Rebin(int num)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("FigureHStack::Rebin");
    for (auto &name : LinkedHists)
    {
        auto obj = TraceElement(name);
        if (obj == nullptr)
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not exist, will be ignored");
            continue;
        }

        if (obj->GetTypeName() != "FigureHist1D")
        {
            MSGUser()->MSG_WARNING("input element ", name, " does not belong to FigureHist1D, will be ignored");
            continue;
        }

        std::shared_ptr<FigureHist1D> hist = std::dynamic_pointer_cast<FigureHist1D>(obj);
        hist->Rebin(num);
    }

    if (total_hist)
        total_hist->Rebin(num);
    if (empty_hist)
        empty_hist->Rebin(num);
}

FigureHStack::~FigureHStack()
{
    delete myhstack;
    delete empty_hist;
    delete total_hist;
    delete total_hist_graph;
}

// Functions for FigureGraph1D

FigureGraph1D::FigureGraph1D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TGraph *graph, const TString &option, FigureElement *mother, std::function<void(TGraph *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureGraph1D", mother)
{
    mygraph = (TGraph *)graph->Clone(TString::Format("Temp_%s", elementname.Data()));
    DrawOption = option;
    OptionFunc = optionfunc;
}

void FigureGraph1D::DrawElement()
{
    mygraph->Draw(DrawOption.TString::Data());
}

void FigureGraph1D::SetStyle()
{
    StyleUser()->SetGraph1DStyle(this);
    OptionFunc(mygraph);
}

std::pair<double, double> FigureGraph1D::GetMinAndMax(double xmin, double xmax)
{
    double ymin, ymax;
    int count = 0;

    for (int i = 0; i < mygraph->GetN(); i++)
    {
        auto xvalue = mygraph->GetPointX(i);
        auto xerrorup = mygraph->GetErrorXhigh(i);
        auto xerrordown = mygraph->GetErrorXlow(i);

        auto yvalue = mygraph->GetPointY(i);
        auto yerrorup = mygraph->GetErrorYhigh(i);
        auto yerrordown = mygraph->GetErrorYlow(i);

        // in case the graph has no error on points
        if (xerrorup == -1)
        {
            xerrorup = 0;
            xerrordown = 0;
            yerrorup = 0;
            yerrordown = 0;
        }

        if (xvalue - xerrordown < xmin || xvalue + xerrorup > xmax)
            continue;

        if (count == 0)
        {
            ymin = yvalue - yerrordown;
            ymax = yvalue + yerrorup;
        }
        else
        {
            if (ymin > yvalue - yerrordown)
                ymin = yvalue - yerrordown;
            if (ymax < yvalue + yerrorup)
                ymax = yvalue + yerrorup;
        }

        ++count;
    }

    return std::pair<double, double>(ymin, ymax);
}

FigureGraph1D::~FigureGraph1D()
{
    delete mygraph;
}

// Functions for FigureFunc1D

FigureFunc1D::FigureFunc1D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TF1 *func, const TString &option, FigureElement *mother, std::function<void(TF1 *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureFunc1D", mother)
{
    myfunc = (TF1 *)func->Clone(TString::Format("Temp_%s", elementname.Data()));
    DrawOption = option;
    OptionFunc = optionfunc;
}

void FigureFunc1D::DrawElement()
{
    myfunc->Draw(DrawOption.TString::Data());
}

void FigureFunc1D::SetStyle()
{
    StyleUser()->SetFunc1DStyle(this);
    OptionFunc(myfunc);
}

std::pair<double, double> FigureFunc1D::GetMinAndMax(double xmin, double xmax)
{
    return std::pair<double, double>(myfunc->GetMinimum(xmin, xmax), myfunc->GetMaximum(xmin, xmax));
}

FigureFunc1D::~FigureFunc1D()
{
    delete myfunc;
}

// Functions for FigureHist2D
FigureHist2D::FigureHist2D(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TH2D *hist, const TString &option, FigureElement *mother, std::function<void(TH2D *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigureHist2D", mother)
{
    myhist = (TH2D *)hist->Clone(TString::Format("Temp_%s", elementname.Data()));
    DrawOption = option;
    OptionFunc = optionfunc;
}

FigureHist2D::~FigureHist2D()
{
    delete myhist;
}

void FigureHist2D::Rebin(int rebinnumx, int rebinnumy)
{
    myhist->RebinX(rebinnumx);
    myhist->RebinY(rebinnumy);
}

void FigureHist2D::Scale(double factor)
{
    myhist->Scale(factor);
}

void FigureHist2D::DrawElement()
{
    myhist->Draw(DrawOption);
}

void FigureHist2D::SetStyle()
{
    StyleUser()->SetHist2DStyle(this);
    OptionFunc(myhist);
}

// Functions for FigureLatex

FigureLatex::FigureLatex(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper,
                         const TString &content, double posx, double poxy, Color_t color, FigureElement *mother, std::function<void(TLatex *)> optionfunc)
    : FigureElement(MSG, shelper, content, "FigureLatex", mother)
{
    mylatex = new TLatex(posx, poxy, content.TString::Data());
    mylatex->SetTextColor(color);
    mylatex->SetNDC();
    OptionFunc = optionfunc;
}

FigureLatex::~FigureLatex()
{
    delete mylatex;
}

void FigureLatex::DrawElement()
{
    mylatex->AppendPad();
}

void FigureLatex::SetStyle()
{
    StyleUser()->SetLatexStyle(this);
    OptionFunc(mylatex);
}

// Functions for FigurePad

FigurePad::FigurePad(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, int lbx, int lby, int rtx, int rty, FigureElement *mother, std::function<void(TPad *)> optionfunc)
    : FigureElement(MSG, shelper, elementname, "FigurePad", mother)
{
    mypad = new TPad(elementname.TString::Data(), elementname.TString::Data(),
                     (double)(lbx - mother->Range_Total.LB_X) / (mother->Range_Total.RT_X - mother->Range_Total.LB_X),
                     (double)(lby - mother->Range_Total.LB_Y) / (mother->Range_Total.RT_Y - mother->Range_Total.LB_Y),
                     (double)(rtx - mother->Range_Total.LB_X) / (mother->Range_Total.RT_X - mother->Range_Total.LB_X),
                     (double)(rty - mother->Range_Total.LB_Y) / (mother->Range_Total.RT_Y - mother->Range_Total.LB_Y));
    OptionFunc = optionfunc;

    Range_Total.LB_X = lbx;
    Range_Total.LB_Y = lby;
    Range_Total.RT_X = rtx;
    Range_Total.RT_Y = rty;

    Range_vec.push_back(Range_Total);
}

void FigurePad::DrawElement()
{
    mypad->Draw();
    // Make sure the linked objects are drew under this pad
    mypad->cd();
}

void FigurePad::SetStyle()
{
    StyleUser()->SetPadStyle(this);
    OptionFunc(this->GetPad());

    // draw the legend
    if (plot_legend)
    {
        // add entries
        std::deque<std::shared_ptr<FigureElement>> v_legend_elements;
        if (legend_entries.size() == 0)
        {
            v_legend_elements = LinkedElement;
            legend_options = std::vector<TString>(v_legend_elements.size(), "lpfe");
        }
        else
        {
            int i = 0;
            for (auto &entry : legend_entries)
            {
                for (auto &elem : LinkedElement)
                    if (elem->GetName() == entry)
                        v_legend_elements.emplace_back(elem);

                ++i;

                if (i != v_legend_elements.size())
                    MSGUser()->MSG_WARNING("Given legend entry ", entry, " does not exist in the linked objects");
            }
        }

        for (int i = 0; i < v_legend_elements.size(); ++i)
        {
            auto elem = v_legend_elements.at(i);
            auto elem_option = legend_options.at(i);
            if (elem->IsType("FigureHist1D"))
            {
                auto hist = std::dynamic_pointer_cast<FigureHist1D>(elem);
                if (!hist->DrawAsTGraph)
                    legend->AddEntry(hist->GetHist(), elem->GetName(), elem_option);
                else
                    legend->AddEntry(hist->mygraph, elem->GetName(), elem_option);
            }
            if (elem->IsType("FigureGraph1D"))
                legend->AddEntry((std::dynamic_pointer_cast<FigureGraph1D>(elem))->GetGraph(), elem->GetName(), elem_option);
            if (elem->IsType("FigureFunc1D"))
                legend->AddEntry((std::dynamic_pointer_cast<FigureFunc1D>(elem))->GetFunc(), elem->GetName(), elem_option);
            if (elem->IsType("FigureHist2D"))
                legend->AddEntry((std::dynamic_pointer_cast<FigureHist2D>(elem))->GetHist(), elem->GetName(), elem_option);
            if (elem->IsType("FigureHStack"))
            {
                auto hstack = std::dynamic_pointer_cast<FigureHStack>(elem);
                if (hstack->draw_total_hist)
                    legend->AddEntry((std::dynamic_pointer_cast<FigureHStack>(elem))->GetTotalHist(), elem->GetName(), elem_option);
            }
        }

        if (legend_header.BeginsWith("ATLAS"))
        {
            // draw "ATLAS" slanted
            TLatex *atlas_text = new TLatex(legend->GetX1(), legend->GetY2() - 0.03, "ATLAS");
            atlas_text->SetNDC();
            atlas_text->SetTextFont(72);
            atlas_text->SetTextSize(StyleUser()->LEGEND_TEXT_SIZE_PERCENT);
            atlas_text->SetTextColor(1);
            atlas_text->AppendPad();

            TString rest_info = legend_header(5, legend_header.Length());
            TLatex rest_text;
            rest_text.SetNDC();
            rest_text.SetTextFont(42);
            rest_text.SetTextSize(StyleUser()->LEGEND_TEXT_SIZE_PERCENT);
            rest_text.SetTextColor(1);
            rest_text.DrawLatex(legend->GetX1() + 1.6 * StyleUser()->LEGEND_TEXT_SIZE_PERCENT * 696 * GetPad()->GetWh() / (472 * GetPad()->GetWw()), legend->GetY2() - 0.03, rest_info.Data());

            legend->SetHeader("", "");
        }
        else if (legend_header != "")
        {
            legend->SetHeader(legend_header, legend_header_option);
        }
        legend->SetNColumns(legend_column);

        legend->Draw("same");
    }

    mypad->Update();
    mypad->RedrawAxis();
}

void FigurePad::CD()
{
    mypad->cd();
}

FigurePad::~FigurePad()
{
    // delete mypad;
}

/// @brief Book a new pad in this pad.
/// @param padname name of the new pad.
/// @param lbx x value of the left bottom of the new pad.
/// @param lby y value of the left bottom of the new pad.
/// @param rtx x value of the right top of the new pad.
/// @param rty y value of the right top of the new pad.
/// @param optionfunc user-defined function for adjusting the style of the new pad.
/// @return
std::shared_ptr<FigurePad> FigurePad::BookPad(const TString &padname, int lbx, int lby, int rtx, int rty, std::function<void(TPad *)> optionfunc)
{
    if (!CheckDuplicate(padname))
        return nullptr;

    auto newpad = std::shared_ptr<FigurePad>(new FigurePad(MSGUser(), StyleUser(), padname, lbx, lby, rtx, rty, this, optionfunc));
    if (Contain(newpad) == false)
        MSGUser()->MSG_ERROR("New Pad ", padname.TString::Data(), " is not within the Canvas ", GetName().TString::Data());
    MapLinkedElement.emplace(padname, LinkedElement.size());
    LinkedElement.emplace_back(newpad);
    return newpad;
}

/// @brief Book a FigureHist1D on this pad.
/// @param elementname name of the TH1D to be drawn, used in the links and legend.
/// @param hist pointer to the TH1D.
/// @param option draw options, passed to TH1D::Draw().
/// @param optionfunc user-defined function for adjusting the style of the TH1D, can override the default style.
/// @return the pointer to the FigureHist1D.
std::shared_ptr<FigureHist1D> FigurePad::SetInputHist1D(const TString &elementname, TH1D *hist, const TString &option, std::function<void(TH1D *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newhist = std::shared_ptr<FigureHist1D>(new FigureHist1D(MSGUser(), StyleUser(), elementname, hist, newoption, this, optionfunc));
    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newhist);
    // waitforfirstlink = false;
    newhist->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newhist;
}

/** @brief Book a FigureHist1D on this pad, the histogram is generated from other FigureHist1D you passed to the FigureTool.
    @param elementname name of the TH1D to be drawn.
    @param linkinfo input string for controling the generation of the new histogram. The syntax is "(link type)/(linked histogram one)/(linked histogram two)/(plot type)".
    possible options for the link type are: `RATIO`, `PULL`, `DELTA` and `CHI2`.
    possible options for the value type are: `VAL`(value), `ERR`(error), `TOPERR`(only use the error of the first one), `BOTTOMERR`(only use the error of the second one). You can combine them such as "VALERR".
    @param option Draw options, passed to TH1D::Draw().
    @param optionfunc user-defined function for adjusting the style of the TH1D, can override the default style.
    @return the pointer to the FigureHist1D.
*/
std::shared_ptr<FigureHist1D> FigurePad::SetInputHist1D(const TString &elementname, const TString &linkinfo, const TString &option, std::function<void(TH1D *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newhist = std::shared_ptr<FigureHist1D>(new FigureHist1D(MSGUser(), StyleUser(), elementname, linkinfo, newoption, this, optionfunc));
    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newhist);
    // waitforfirstlink = false;
    newhist->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newhist;
}

/// @brief Book a FigureHist1D on this pad, the histogram will be converted to a TGraphErrors object when drawing.
/// @param elementname name of the element.
/// @param hist pointer to the TH1D.
/// @param option draw options, passed to TGraph::Draw().
/// @param optionfunc user-defined function for adjusting the style of the TGraph, can override the default style.
/// @return the pointer to the FigureHist1D.
std::shared_ptr<FigureHist1D> FigurePad::SetInputHist1DGraph(const TString &elementname, TH1D *hist, const TString &option, std::function<void(TGraph *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newhist = std::shared_ptr<FigureHist1D>(new FigureHist1D(MSGUser(), StyleUser(), elementname, hist, newoption, this, optionfunc));
    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newhist);
    // waitforfirstlink = false;
    newhist->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newhist;
}

/** @brief Book a FigureHist1D on this pad, the histogram is generated from other FigureHist1D you passed to the FigureTool. The new histogram will be converted to a TGraphErrors object when drawing.
    @param elementname name of the TH1D to be drawn.
    @param linkinfo input string for controling the generation of the new histogram. The syntax is "(link type)/(linked histogram one)/(linked histogram two)/(plot type)".
    possible options for the link type are: `RATIO`, `PULL`, `DELTA` and `CHI2`.
    possible options for the value type are: `VAL`(value), `ERR`(error), `TOPERR`(only use the error of the first one), `BOTTOMERR`(only use the error of the second one). You can combine them such as "VALERR".
    @param option Draw options, passed to TGraph::Draw().
    @param optionfunc user-defined function for adjusting the style of the TGraph, can override the default style.
    @return the pointer to the FigureHist1D.
*/
std::shared_ptr<FigureHist1D> FigurePad::SetInputHist1DGraph(const TString &elementname, const TString &linkinfo, const TString &option, std::function<void(TGraph *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newhist = std::shared_ptr<FigureHist1D>(new FigureHist1D(MSGUser(), StyleUser(), elementname, linkinfo, newoption, this, optionfunc));
    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newhist);
    // waitforfirstlink = false;
    newhist->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newhist;
}

/// @brief Book a new THStack on this pad, note that only one NAGASH::FigureHStack can be booked in a pad.
/// @param elementname name of the THStack to be drawn, can be used in the links and legend.
/// @param linkinfo a vector of TString containing all names of histograms you want to use constucting the new THStack.
/// @param option Draw options, passed to THStack::Draw().
/// @return the pointer to the FigureHStack.
std::shared_ptr<FigureHStack> FigurePad::SetHStack(const TString &elementname, const std::vector<TString> &linkinfo, const TString &option)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    auto titleguard = MSGUser()->StartTitleWithGuard("FigurePad::SetHStack");
    if (hashstack)
    {
        MSGUser()->MSG_WARNING("THStack for this pad has already existed");
        return nullptr;
    }
    else
    {
        auto newhstack = std::shared_ptr<FigureHStack>(new FigureHStack(MSGUser(), StyleUser(), elementname, linkinfo, option, this));
        LinkedElement.emplace_front(newhstack);
        for (auto &iter : MapLinkedElement)
            iter.second += 1;
        MapLinkedElement.emplace(elementname, 0);

        return newhstack;
    }
}

/// @brief Book a new FigureGraph1D on this pad.
/// @param elementname name of the graph to be drawn, can be used in the legend.
/// @param graph pointer to the TGraph.
/// @param option draw options, passed to TGraph::Draw().
/// @param optionfunc user-defined function for adjusting the style of the TGraph, can override the default style.
/// @return pointer to the FigureGraph1D.
std::shared_ptr<FigureGraph1D> FigurePad::SetInputGraph1D(const TString &elementname, TGraph *graph, const TString &option, std::function<void(TGraph *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newgraph = std::shared_ptr<FigureGraph1D>(new FigureGraph1D(MSGUser(), StyleUser(), elementname, graph, newoption, this, optionfunc));

    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newgraph);
    // waitforfirstlink = false;
    newgraph->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newgraph;
}

/// @brief Book a new FigureFunc1D on this pad.
/// @param elementname name of the function to be drawn, can be used in the legend.
/// @param func pointer to the TF1.
/// @param option draw options, passed to TF1::Draw().
/// @param optionfunc user-defined function for adjusting the style of the TF1, can override the default style.
/// @return pointer to the FigureFunc1D.
std::shared_ptr<FigureFunc1D> FigurePad::SetInputFunc1D(const TString &elementname, TF1 *func, const TString &option, std::function<void(TF1 *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newfunc = std::shared_ptr<FigureFunc1D>(new FigureFunc1D(MSGUser(), StyleUser(), elementname, func, newoption, this, optionfunc));

    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newfunc);
    // waitforfirstlink = false;
    newfunc->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newfunc;
}

/// @brief Book a new FigureHist2D on this pad.
/// @param elementname name of the histogram to be drawn, can be used in the legend.
/// @param hist pointer to the TH2D.
/// @param option draw options, passed to TH2D::Draw().
/// @param optionfunc user-defined function for adjusting the style of the TH2D, can override the default style.
/// @return pointer to the FigureHist2D.
std::shared_ptr<FigureHist2D> FigurePad::SetInputHist2D(const TString &elementname, TH2D *hist, const TString &option, std::function<void(TH2D *)> optionfunc)
{
    if (!CheckDuplicate(elementname))
        return nullptr;

    TString newoption = option;
    // if (waitforfirstlink == false)
    newoption = option + " same";
    auto newhist = std::shared_ptr<FigureHist2D>(new FigureHist2D(MSGUser(), StyleUser(), elementname, hist, newoption, this, optionfunc));
    MapLinkedElement.emplace(elementname, LinkedElement.size());
    LinkedElement.emplace_back(newhist);
    // waitforfirstlink = false;
    newhist->index_in_pad = n_colored_objects;
    ++n_colored_objects;

    return newhist;
}

/// @brief Book a new FigureLatex on this pad.
/// @param content content of the text you want to draw.
/// @param posx relative x value of the text(0 to 1).
/// @param poxy relative y value of the text(0 to 1).
/// @param color color of the text.
/// @param optionfunc user-defined function for adjusting the style of the TLatex, can override the default style.
void FigurePad::SetLatex(const TString &content, double posx, double poxy, Color_t color, std::function<void(TLatex *)> optionfunc)
{
    if (!CheckDuplicate(content))
        return;

    auto newlatex = std::shared_ptr<FigureLatex>(new FigureLatex(MSGUser(), StyleUser(), content, posx, poxy, color, this, optionfunc));
    MapLinkedElement.emplace(content, LinkedElement.size());
    LinkedElement.emplace_back(newlatex);
}

/// @brief Draw a "ATLAS" plus the given content like *ATLAS* Internal, the ATLAS text will be drawn slanted.
/// @param content extra content after the "ATLAS" text
/// @param posx relative x positon of the label(0 to 1).
/// @param posy relative y positon of the label(0 to 1).
/// @param color color of the extra text.
/// @param space space between "ATLAS" and the other content
void FigurePad::DrawATLASLabel(const TString &content, double posx, double posy, Color_t color, double space)
{
    SetLatex("ATLAS", posx, posy, color, [](TLatex *latex)
             { latex->SetTextFont(72); });

    if (space <= 0)
        SetLatex(content, posx + 1.6 * StyleUser()->LEGEND_TEXT_SIZE_PERCENT * 696 * GetPad()->GetWh() / (472 * GetPad()->GetWw()), posy, color);
    else
        SetLatex(content, posx + space, posy, color);
}

/// @brief Normalize one element to another.
/// @param elemname1 name of the element to be scaled.
/// @param elemname2 name of the element to be scaled to.
/// @param xmin start point of calculating the normalized value.
/// @param xmax end point of calculating the normalized value.
void FigurePad::Norm(const TString &elemname1, const TString &elemname2, double xmin, double xmax)
{
    // norm the integral of elem1 to elem2
    auto elem1 = FindLinkElement(elemname1);
    auto elem2 = FindLinkElement(elemname2);

    TH1D *h1 = nullptr;
    TH1D *h2 = nullptr;

    if (elem1->IsType("FigureHist1D") && elem2->IsType("FigureHist1D"))
    {
        h1 = (std::dynamic_pointer_cast<FigureHist1D>(elem1))->GetHist();
        h2 = (std::dynamic_pointer_cast<FigureHist1D>(elem2))->GetHist();
    }
    else if (elem1->IsType("FigureHStack") && elem2->IsType("FigureHist1D"))
    {
        h1 = (std::dynamic_pointer_cast<FigureHStack>(elem1))->GetTotalHist();
        h2 = (std::dynamic_pointer_cast<FigureHist1D>(elem2))->GetHist();
    }
    else if (elem1->IsType("FigureHist1D") && elem2->IsType("FigureHStack"))
    {
        h1 = (std::dynamic_pointer_cast<FigureHist1D>(elem1))->GetHist();
        h2 = (std::dynamic_pointer_cast<FigureHStack>(elem2))->GetTotalHist();
    }
    else
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("FigurePad::Norm");
        MSGUser()->MSG_WARNING("can only norm element with type FigureHist1D or FigureHStack");
        return;
    }

    double factor;
    if (xmin == xmax && xmin == 0)
    {
        factor = h2->Integral() / h1->Integral();
    }
    else
    {
        int bin1 = h1->FindBin(xmin);
        int bin2 = h1->FindBin(xmax);

        factor = h2->Integral(bin1, bin2) / h1->Integral(bin1, bin2);
    }

    if (elem1->IsType("FigureHist1D"))
        std::dynamic_pointer_cast<FigureHist1D>(elem1)->Scale(factor);
    if (elem1->IsType("FigureHStack"))
        std::dynamic_pointer_cast<FigureHStack>(elem1)->Scale(factor);
}

/// @brief Set the legend of this pad.
/// @param header header of the legend, if given an empty string, no header will be drawn.
/// @param option options to draw the legend, passed to TLegend::Draw().
void FigurePad::SetLegend(const TString &header, const TString &option)
{
    plot_legend = true;
    legend = new TLegend();
    legend_header = header;
    legend_header_option = option;
}

/// @brief Set the legend of this pad with manually set position.
/// @param header header of the legend, if given an empty string, no header will be drawn.
/// @param option option to draw the legend, passed to TLegend::Draw().
/// @param xmin left-bottom x position of the legend.
/// @param ymin left-bottom y position of the legend.
/// @param xmax right-top x position of the legend.
/// @param ymax right-top y position of the legend.
void FigurePad::SetLegend(const TString &header, const TString &option, double xmin, double ymin, double xmax, double ymax)
{
    plot_legend = true;
    legend = new TLegend();
    legend_header = header;
    legend_header_option = option;
    user_set_legend_position = true;
    legend_xmin = xmin;
    legend_xmax = xmax;
    legend_ymin = ymin;
    legend_ymax = ymax;
}

/// @brief Set the column number of the legend.
/// @param column number of the columns.
void FigurePad::SetLegendColumn(int column)
{
    legend_column = column;
}

/// @brief Set the entries passed to the legend, if this function is not called, all the elements in the pad will be drawn in the legend.
/// @param entries vector of names of the elements to be drawn in the legend.
/// @param options vector of options for each element to be drawn to the legend, the default one is "lpfe".
void FigurePad::SetLegendEntries(const std::vector<TString> &entries, const std::vector<TString> &options)
{
    if (entries.size() != options.size())
    {
        MSGUser()->MSG_WARNING("entries and options must have the same size");
        return;
    }
    legend_entries = entries;
    legend_options = options;
}

/// @brief Draw grids on x axis.
void FigurePad::SetGridx(bool grid)
{
    if (grid)
        mypad->SetGridx(1);
    else
        mypad->SetGridx(0);
}

/// @brief Draw grids on y axis.
void FigurePad::SetGridy(bool grid)
{
    if (grid)
        mypad->SetGridy(1);
    else
        mypad->SetGridy(0);
}

/// @brief Set x-axis to be drawn in logarithmic scale.
void FigurePad::SetLogx(int value)
{
    mypad->SetLogx(value);
}

/// @brief Set y-axis to be drawn in logarithmic scale.
void FigurePad::SetLogy(int value)
{
    mypad->SetLogy(value);
}

/// @brief Set z-axis to be drawn in logarithmic scale.
void FigurePad::SetLogz(int value)
{
    mypad->SetLogz(value);
}

/// @brief Set the range of the x-axis to be drawn.
/// @param min minimum value of the x-axis.
/// @param max maximum value of the x-axis.
void FigurePad::SetXaxisRange(double min, double max)
{
    user_set_rangex = true;
    max_xaxis = max;
    min_xaxis = min;
}

/// @brief Set the range of the y-axis to be drawn.
/// @param min minimum value of the y-axis.
/// @param max maximum value of the y-axis.
void FigurePad::SetYaxisRange(double min, double max)
{
    user_set_rangey = true;
    max_yaxis = max;
    min_yaxis = min;
}

/// @brief Set the range of the z-axis to be drawn.
/// @param min minimum value of the z-axis.
/// @param max maximum value of the z-axis.
void FigurePad::SetZaxisRange(double min, double max)
{
    user_set_rangez = true;
    max_zaxis = max;
    min_zaxis = min;
}

// Functions for FigureCanvas

FigureCanvas::FigureCanvas(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, int npx, int npy)
    : FigureElement(MSG, shelper, elementname, "FigureCanvas", nullptr)
{
    mycanvas = new TCanvas(elementname.TString::Data(), elementname.TString::Data(), npx, npy);

    Range_Total.LB_X = 0;
    Range_Total.LB_Y = 0;
    Range_Total.RT_X = npx;
    Range_Total.RT_Y = npy;

    Range_vec.push_back(Range_Total);
}

FigureCanvas::~FigureCanvas()
{
    delete mycanvas;
}

void FigureCanvas::DrawElement()
{
    mycanvas->Draw();
    mycanvas->cd();
    // Make sure thew linked objects are drew under this canvas
}

void FigureCanvas::CD()
{
    mycanvas->cd();
}

/// @brief Book a FigurePad in this canvas.
/// @param padname name of the pad.
/// @param lbx left-bottom x position of the pad.
/// @param lby left-bottom y position of the pad.
/// @param rtx right-top x position of the pad.
/// @param rty right-top y position of the pad.
/// @param optionfunc user-defined function to draw the pad, passed to TPad::Draw().
/// @return the pointer to the FigurePad object.
std::shared_ptr<FigurePad> FigureCanvas::BookPad(const TString &padname, int lbx, int lby, int rtx, int rty, std::function<void(TPad *)> optionfunc)
{
    if (!CheckDuplicate(padname))
        return nullptr;

    auto newpad = std::shared_ptr<FigurePad>(new FigurePad(MSGUser(), StyleUser(), padname, lbx, lby, rtx, rty, this, optionfunc));
    if (Contain(newpad) == false)
        MSGUser()->MSG_ERROR("New Pad ", padname.TString::Data(), " is not within the Canvas ", GetName().TString::Data());
    MapLinkedElement.emplace(padname, LinkedElement.size());
    LinkedElement.emplace_back(newpad);
    return newpad;
}

/// @brief Book a FigurePad in this canvas. The position is given in relative coordinates.
/// @param padname name of the pad.
/// @param lbx left-bottom x position of the pad in relative coordinates(0 to 1).
/// @param lby left-bottom y position of the pad in relative coordinates(0 to 1).
/// @param rtx right-top x position of the pad in relative coordinates (0 to 1).
/// @param rty right-top y position of the pad in relative coordinates (0 to 1).
/// @param optionfunc user-defined function to draw the pad, passed to TPad::Draw().
/// @return the pointer to the FigurePad object.
std::shared_ptr<FigurePad> FigureCanvas::BookPadNDC(const TString &padname, double lbx, double lby, double rtx, double rty, std::function<void(TPad *)> optionfunc)
{
    if (!CheckDuplicate(padname))
        return nullptr;

    int newlbx = lbx * (this->Range_Total.RT_X - this->Range_Total.LB_X) + this->Range_Total.LB_X;
    int newlby = lby * (this->Range_Total.RT_Y - this->Range_Total.LB_Y) + this->Range_Total.LB_Y;
    int newrtx = rtx * (this->Range_Total.RT_X - this->Range_Total.LB_X) + this->Range_Total.LB_X;
    int newrty = rty * (this->Range_Total.RT_Y - this->Range_Total.LB_Y) + this->Range_Total.LB_Y;

    auto newpad = std::shared_ptr<FigurePad>(new FigurePad(MSGUser(), StyleUser(), padname, newlbx, newlby, newrtx, newrty, this, optionfunc));
    if (Contain(newpad) == false)
        MSGUser()->MSG_ERROR("New Pad ", padname.TString::Data(), " is not within the Canvas ", GetName().TString::Data());
    MapLinkedElement.emplace(padname, LinkedElement.size());
    LinkedElement.emplace_back(newpad);
    return newpad;
}

// Functions for FigureTool

/// @brief Constructor for FigureTool.
/// @param MSG input MSGTool.
FigureTool::FigureTool(std::shared_ptr<MSGTool> MSG) : Tool(MSG)
{
    stylehelper = std::make_shared<FigureStyleHelper>(MSG);
}

/// @brief Constructor for FigureTool.
/// @param MSG input MSGTool.
/// @param shelper user-defined FigureStyleHelper.
FigureTool::FigureTool(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper) : Tool(MSG)
{
    stylehelper = shelper;
}

/// @brief Book a new FigureCanvas as a new figure.
/// @param figurename name of the new figure.
/// @param size_x x-size of the new canvas.
/// @param size_y y-size of the new canvas.
/// @return the pointer to the new FigureCanvas object.
std::shared_ptr<FigureCanvas> FigureTool::BookFigure(const TString &figurename, int size_x, int size_y)
{
    auto newfigure = std::shared_ptr<FigureCanvas>(new FigureCanvas(MSGUser(), stylehelper, figurename, size_x, size_y));
    BookedFigure.emplace(figurename, newfigure);
    return newfigure;
}

/// @brief Book a new FigureCanvas as a new figure. The size is 600*600 as recommended by ATLAS.
/// @param figurename name of the new figure.
/// @return the pointer to the new FigureCanvas object.
std::shared_ptr<FigureCanvas> FigureTool::BookATLASSquare(const TString &figurename)
{
    return BookFigure(figurename, 600, 600);
}

/// @brief Book a new FigureCanvas as a new figure. The size is 800*600 as recommended by ATLAS.
/// @param figurename name of the new figure.
/// @return the pointer to the new FigureCanvas object.
std::shared_ptr<FigureCanvas> FigureTool::BookATLASRectangular(const TString &figurename)
{
    return BookFigure(figurename, 800, 600);
}

/// @brief Draw all the figures booked in this tool.
void FigureTool::DrawFigures()
{
    for (auto &iter : BookedFigure)
    {
        MSGUser()->MSG_INFO("Draw ", iter.first.TString::Data());
        iter.second->Draw();
        iter.second->SaveAs(TString::Format("%s/%s", figuredir.TString::Data(), iter.first.TString::Data()));
    }
}
