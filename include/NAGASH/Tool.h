//***************************************************************************************
/// @file Tool.h
/// @author Wenhao Ma
//***************************************************************************************

/** @defgroup ToolClasses Tool Classes
    @brief Classes that served as tools in your analysis.

### Create your own tool

In NAGASH, tool is just literal meaning, you can get it by

@code {.cxx}
std::shared_ptr<USERTOOL> mytool = ToolkitUser()->GetTool<USERTOOL>(some config parameter);
@endcode

in the framework. (More specifically, in classes NAGASH::LoopEvent, NAGASH::Tool and NAGASH::Result)

If you want to create your own tool, you should organize your header file `USERTOOL.h` as follows :

@code {.cxx}
#pragma once

#include "NAGASH.h"

class USERTOOL : public NAGASH::Tool
{
public:
   // you can override the construction fucntion
   // but the first argument must be msgtool
   USERTOOL(std::shared_ptr<NAGASH::MSGTool> MSG, other arguments);
   // other member function you want to define
   void foo(); // example
};
@endcode

The corresponding source file `USERTOOL.cxx` should goes like :

@code {.cxx}
#include "USERTOOL.h"

USERTOOL(std::shared_ptr<NAGASH::MSGTool> MSG, other arguments) : Tool(MSG)
{
    // do what you want
}

void USERTOOL::foo()
{
    // your tool function
    // this will give a output like
    // ...->USERTOOL::fool    INFO   foo
    auto titleguard = MSGUser()->StartTitleWithGuard("USERTOOL::foo");
    MSGUser()->MSG(MSGLevel::INFO,"foo");
}
@endcode

The base class Tool has protected member function NAGASH::Tool::MSGUser() which maintains the MSGTool during the analysis.
Use NAGASH::MSGTool::StartTitleWithGuard() to let you know where the output message comes from.
 */

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/MSGTool.h"

namespace NAGASH
{
    /// @class NAGASH::Tool Tool.h "NAGASH/Tool.h"
    /// @ingroup ToolClasses
    /// @brief Provide interface for all tools in NAGASH.
    class Tool
    {
    protected:
        /// @brief Constructor.
        /// @param MSG the input MSGTool.
        Tool(std::shared_ptr<MSGTool> MSG) : msg(MSG) {}
        Tool() = delete;
        Tool(const Tool &tool) = delete;
        Tool(Tool &&tool) = delete;
        Tool &operator=(const Tool &tool) = delete;
        Tool &operator=(Tool &&tool) = delete;
        virtual ~Tool() = default;

        /// @brief return the MSGTool inside.
        std::shared_ptr<MSGTool> MSGUser();

    private:
        std::shared_ptr<MSGTool> msg;
    };

    inline std::shared_ptr<MSGTool> Tool::MSGUser() { return msg; }
} // namespace NAGASH
