#pragma once

#include "NAGASH/Tool.h"

namespace NAGASH
{
    class UnfoldTool : public Tool
    {
    private:
        std::vector<double> Data_Reco;            // Data Reco
        std::vector<double> Data_Reco_Correct;    // Data Reco * Fiducial
        std::vector<double> Theory_Truth;         // Theory Truth
        std::vector<double> Theory_Truth_Correct; // Theory Truth * Efficiency
        std::vector<double> Data_Truth;           // Data Truth
        std::vector<double> Data_Truth_Correct;   // Data Truth / Efficiency
        std::vector<double> Data_Reco_Estimated;  // Sigmaj Rij Theory Truthj
        std::vector<double> Data_Truth_Estimated;

        std::vector<bool> IsMeasured;
        std::vector<bool> IsSet;

        std::vector<std::vector<double>> R_Matrix;
        std::vector<std::vector<double>> M_Matrix;
        std::vector<std::vector<bool>> MatrixIsSet;

        std::vector<double> FiducialCorrection;
        std::vector<double> EfficiencyCorrection;
        std::vector<bool> FCorrectionIsSet;
        std::vector<bool> ECorrectionIsSet;

        std::vector<double> ReweightFactor;

        int binnum;

        std::vector<std::vector<double>> Data_Truth_Correct_EachItr;

    public:
        UnfoldTool(std::shared_ptr<MSGTool> msg, int num);
        void SetData(int index, double value);
        void SetTheory(int index, double value);
        void SetMatrix(int indexi, int indexj, double value);
        void SetFiducialCorrection(int index, double value);
        void SetEfficiencyCorrection(int index, double value);

        bool Check();
        void Clear();

        void BayesMatrix();
        void EstimateDataTruth();
        void EstimateDataReco();
        void RenewTheory();
        void PrintResult();
        void BayesUnfold(int iteration);
        void DisplayEachItr();

        void ReweightUnfold(int &iteration);
        void EstimateReweightFactor();
        void ApplyReweightFactor();

        void GetResult(int itration, TH1D *result);
        virtual ~UnfoldTool() = default;
    };
}
