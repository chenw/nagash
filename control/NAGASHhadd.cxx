#include "NAGASH/Global.h"
#include "NAGASH/NAGASHFile.h"

using namespace NAGASH;

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        std::cout << "Usage : NAGASHhadd TARGET SOURCES" << std::endl;
        return 0;
    }

    std::string outfile(argv[1]);
    std::vector<std::string> infiles;
    for (int i = 2; i < argc; i++)
        infiles.emplace_back(argv[i]);

    NAGASHFile::hadd(outfile, infiles);

    return 0;
}