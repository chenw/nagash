//***************************************************************************************
/// @file MapTool.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    /// @class NAGASH::MapTool MapTool.h "NAGASH/MapTool.h"
    /// @brief Helper functions for std::map.
    /// @ingroup ToolClasses
    class MapTool : public Tool
    {
    public:
        /// @brief Constructor.
        MapTool(std::shared_ptr<MSGTool> MSG) : Tool(MSG) {}
        unsigned int EditDistance(const TString &s1, const TString &s2) const;
        unsigned int EditDistance(const std::string &s1, const std::string &s2) const;

        template <typename T>
        typename std::map<TString, T>::iterator FindMostSimilar(std::map<TString, T> &mymap, const TString &key);

        template <typename T>
        typename std::map<std::string, T>::iterator FindMostSimilar(std::map<std::string, T> &mymap, const std::string &key);

        template <typename T1, typename T2>
        std::vector<T1> GetListOfKeys(const std::map<T1, T2> &inputmap) const;

        template <typename T1, typename T2>
        std::vector<T2> GetListOfValues(const std::map<T1, T2> &inputmap) const;
    };

    /// @brief Find value to the most similar key in a map.
    /// @tparam T type of the value of the map.
    /// @param mymap the input map.
    /// @param key the key to be searched.
    /// @return if the key exists in the map, return the iterator pointing to the key, otherwise return value to the most similar key in the map. The similarity is calculated by the EditDistance function.
    template <typename T>
    inline typename std::map<TString, T>::iterator MapTool::FindMostSimilar(std::map<TString, T> &mymap, const TString &key)
    {
        if (mymap.size() == 0)
        {
            MSGUser()->StartTitle("MapTool::FindMostSimilar");
            MSGUser()->MSG_WARNING("The given map is empty, returning its end");
            MSGUser()->EndTitle();
            return std::end(mymap);
        }

        auto findresult = mymap.find(key);
        if (findresult != mymap.end())
            return findresult;
        else
        {
            MSGUser()->StartTitle("MapTool::FindMostSimilar");
            // find the most similiar key
            typename std::map<TString, T>::iterator keypos;
            int mindist = -1;
            for (typename std::map<TString, T>::iterator it = mymap.begin(); it != mymap.end(); ++it)
            {
                if (mindist == -1)
                {
                    mindist = EditDistance(it->first, key);
                    keypos = it;
                }
                else
                {
                    int temp = EditDistance(it->first, key);
                    if (temp < mindist)
                    {
                        mindist = temp;
                        keypos = it;
                    }
                }
            }
            MSGUser()->MSG(MSGLevel::WARNING, "No exact match of input string, turn to fuzzy search(the best match in the key). Key ", keypos->first, " is find (key ", key, " is the input)");
            MSGUser()->EndTitle();
            return keypos;
        }
    }

    /// @brief Find value to the most similar key in a map.
    /// @tparam T type of the value of the map.
    /// @param mymap the input map.
    /// @param key the key to be searched.
    /// @return if the key exists in the map, return the iterator pointing to the key, otherwise return value to the most similar key in the map. The similarity is calculated by the EditDistance function.
    template <typename T>
    inline typename std::map<std::string, T>::iterator MapTool::FindMostSimilar(std::map<std::string, T> &mymap, const std::string &key)
    {
        if (mymap.size() == 0)
        {
            MSGUser()->StartTitle("MapTool::FindMostSimilar");
            MSGUser()->MSG_WARNING("The given map is empty, returning its end");
            MSGUser()->EndTitle();
            return std::end(mymap);
        }

        auto findresult = mymap.find(key);
        if (findresult != mymap.end())
            return findresult;
        else
        {
            MSGUser()->StartTitle("MapTool::FindMostSimilar");
            // find the most similiar key
            typename std::map<std::string, T>::iterator keypos;
            int mindist = -1;
            for (typename std::map<std::string, T>::iterator it = mymap.begin(); it != mymap.end(); ++it)
            {
                if (mindist == -1)
                {
                    mindist = EditDistance(it->first, key);
                    keypos = it;
                }
                else
                {
                    int temp = EditDistance(it->first, key);
                    if (temp < mindist)
                    {
                        mindist = temp;
                        keypos = it;
                    }
                }
            }
            MSGUser()->MSG(MSGLevel::WARNING, "No exact match of input string, turn to fuzzy search(the best match in the key). Key ", keypos->first, " is find (key ", key, " is the input)");
            MSGUser()->EndTitle();
            return keypos;
        }
    }

    /// @brief Get the vector of keys in a map.
    template <typename T1, typename T2>
    inline std::vector<T1> MapTool::GetListOfKeys(const std::map<T1, T2> &inputmap) const
    {
        std::vector<T1> tempv;
        for (auto &t : inputmap)
            tempv.push_back(t.first);
        return tempv;
    }

    /// @brief Get the vector of values in a map.
    template <typename T1, typename T2>
    inline std::vector<T2> MapTool::GetListOfValues(const std::map<T1, T2> &inputmap) const
    {
        std::vector<T2> tempv;
        for (auto &t : inputmap)
            tempv.push_back(t.second);
        return tempv;
    }

} // namespace NAGASH
