#include "NAGASH/NAGASH.h"

using namespace NAGASH;

int main(int argc, char *argv[])
{
    // this is a demo to show how to use NAGASH::ProfileFitter
    // here is a two signal model with data, mc simulation signal, bkg and systematic variation
    // the goal is to fit the signal strength for the two signals.
    std::vector<double> DataContent{66794, 214244, 145599, 62531, 18430};
    std::vector<double> DataError{258.445, 462.865, 381.574, 250.062, 135.757};
    std::vector<double> SignalOneContent{12252.9, 47435.4, 45553.5, 21592.8, 5953.28};
    std::vector<double> SignalOneError{383.36, 728.494, 648.582, 430.871, 213.435};
    std::vector<double> SignalTwoContent{50357, 129318, 40142.3, 7079.96, 1415.54};
    std::vector<double> SignalTwoError{2727.81, 4637.69, 2361.29, 903.572, 339.72};
    std::vector<double> BkgContent{10429.1, 43201.2, 47754.4, 24940.8, 7688.81};
    std::vector<double> BkgError{234.322, 447.727, 328.898, 189.348, 102.708};

    std::vector<double> SignalOneVarUp{12963.3, 48982.9, 45909.3, 21290.4, 5756.26};
    std::vector<double> SignalOneVarDown{11930.7, 46005.3, 44797.4, 21799.6, 6084.36};
    std::vector<double> SignalTwoVarUp{52079.1, 134586, 40431.5, 7105.3, 1322.69};
    std::vector<double> SignalTwoVarDown{49602.8, 126769, 39258.1, 7002.01, 1317.11};
    std::vector<double> BkgVarUp{10394.3, 42322.9, 46049.8, 24141.7, 7412.09};
    std::vector<double> BkgVarDown{10583.3, 44078.9, 48915.4, 25655.8, 7976.2};

    // construct tool
    auto msg = std::make_shared<MSGTool>();
    auto guard = msg->StartTitleWithGuard("Profile Fitter Test");
    auto PLHFitter = std::make_shared<ProfileFitter>(msg);

    // signal strength for two signals
    PLHFitter->BookParameterOfInterest("sf1", 1, 0.5, 2);
    PLHFitter->BookParameterOfInterest("sf2", 1, 0.5, 2);

    // nominal values
    PLHFitter->BookObservableData("dist", DataContent, DataError);
    PLHFitter->BookObservableNominal("dist", "SignalOne", SignalOneContent, SignalOneError);
    PLHFitter->BookObservableNominal("dist", "SignalTwo", SignalTwoContent, SignalTwoError);
    PLHFitter->BookObservableNominal("dist", "Bkg", BkgContent, BkgError);

    // book poi variations
    std::vector<double> SignalOneSFUp(5);
    std::vector<double> SignalOneSFDown(5);
    std::transform(SignalOneContent.begin(), SignalOneContent.end(), SignalOneSFUp.begin(), [](double x)
                   { return x * 1.1; });
    std::transform(SignalOneContent.begin(), SignalOneContent.end(), SignalOneSFDown.begin(), [](double x)
                   { return x * 0.9; });

    PLHFitter->BookObservableVariation("dist", "SignalOne", SignalOneSFUp, "sf1", 1.1);
    PLHFitter->BookObservableVariation("dist", "SignalOne", SignalOneSFDown, "sf1", 0.9);

    std::vector<double> SignalTwoSFUp(5);
    std::vector<double> SignalTwoSFDown(5);
    std::transform(SignalTwoContent.begin(), SignalTwoContent.end(), SignalTwoSFUp.begin(), [](double x)
                   { return x * 1.1; });
    std::transform(SignalTwoContent.begin(), SignalTwoContent.end(), SignalTwoSFDown.begin(), [](double x)
                   { return x * 0.9; });

    PLHFitter->BookObservableVariation("dist", "SignalTwo", SignalTwoSFUp, "sf2", 1.1);
    PLHFitter->BookObservableVariation("dist", "SignalTwo", SignalTwoSFDown, "sf2", 0.9);

    // book systematic variation
    PLHFitter->BookNuisanceParameter("var");
    PLHFitter->BookObservableVariationNominal("dist", "SignalOne", SignalOneContent, "var");
    PLHFitter->BookObservableVariationNominal("dist", "SignalTwo", SignalTwoContent, "var");
    PLHFitter->BookObservableVariationNominal("dist", "Bkg", BkgContent, "var");

    PLHFitter->BookObservableVariationUp("dist", "SignalOne", SignalOneVarUp, "var");
    PLHFitter->BookObservableVariationUp("dist", "SignalTwo", SignalTwoVarUp, "var");
    PLHFitter->BookObservableVariationUp("dist", "Bkg", BkgVarUp, "var");

    PLHFitter->BookObservableVariationDown("dist", "SignalOne", SignalOneVarDown, "var");
    PLHFitter->BookObservableVariationDown("dist", "SignalTwo", SignalTwoVarDown, "var");
    PLHFitter->BookObservableVariationDown("dist", "Bkg", BkgVarDown, "var");

    // start to fit
    PLHFitter->SetObservableRange("dist", 0, 4);
    PLHFitter->SetGammaPrefix("SignalOne", "SignalOne");
    PLHFitter->SetGammaPrefix("SignalTwo", "SignalTwo");
    PLHFitter->SetGammaPrefix("Bkg", "Bkg");

    PLHFitter->Fit(ProfileFitter::FitMethod::ProfileLikelihood);

    msg->MSG_INFO(PLHFitter->GetParameter("sf1")->Value, " ", PLHFitter->GetParameter("sf1")->Error);
    msg->MSG_INFO(PLHFitter->GetParameter("sf2")->Value, " ", PLHFitter->GetParameter("sf2")->Error);

    // get systematic uncertainty
    auto uncmap = PLHFitter->EstimateUnc(std::vector<TString>{"var"}, ProfileFitter::FitMethod::AnalyticalChiSquare, 100);
    msg->MSG_INFO(uncmap.find("sf1")->second, " ", uncmap.find("sf2")->second);

    return -1;
}