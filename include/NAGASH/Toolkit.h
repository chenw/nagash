//***************************************************************************************
/// @file Toolkit.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    /// @class NAGASH::Toolkit Toolkit.h "NAGASH/Toolkit.h"
    /// @ingroup AnalysisClasses ToolClasses
    /// @brief Manipulate the Tool classes, give them correct MSGTool inside NAGASH::Job
    class Toolkit
    {
    public:
        /// @brief Constructor.
        /// @param MSG the input MSGTool.
        Toolkit(std::shared_ptr<MSGTool> MSG) : msg(MSG) {}
        Toolkit() = delete;

        template <typename T, typename... Args>
        std::shared_ptr<T> GetTool(Args &&...args);

    private:
        std::shared_ptr<MSGTool> msg;
    };

    /// @brief Create a new tool with the MSGTool inside.
    /// @tparam T type of the tool.
    /// @tparam ...Args type of other arguments passed to the constructor of T.
    /// @param ...args arguments passed to the constructor of T.
    /// @return the created tool.
    template <typename T, typename... Args>
    inline std::shared_ptr<T> Toolkit::GetTool(Args &&...args)
    {
        static_assert(!std::is_pointer<T>::value, "class Toolkit: Can not book tool in pointer type");
        static_assert(std::is_base_of<Tool, T>::value, "class Toolkit: Input type must be or be inherited from Tool class");
        return std::make_shared<T>(msg, std::forward<Args>(args)...);
    }

} // namespace NAGASH
