#include "NAGASH/FigureX.h"

#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnSimplex.h"
#include "Minuit2/MnHesse.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnScan.h"
#include "Minuit2/MnContours.h"
#include "Minuit2/MnPlot.h"



using namespace NAGASH;
using namespace std;
using namespace DevelFeature;

FigureX::FigureX(std::shared_ptr<MSGTool> msg, const TString &style_config) : Tool(msg)
{
    Style = std::make_shared<ConfigTool>(style_config.TString::Data());
}

void FigureX::ConfigFigures()
{
    for (auto &t : Figure_Map)
    {
        t.second->ConfigFigure();
    }
}

void FigureX::DrawFigures()
{
    MSGUser()->MSG_INFO("FigureX::DrawFigures");
    for (auto &t : Figure_Map)
    {
        t.second->DrawFigure();
    }
}

void FigureProfile::DrawFigure()
{
    TuneFreeParameters();
    Canvas->UpdatePosition();
    Canvas->Draw();
    Canvas->m_canvas->SaveAs(Name.TString::Data());
}

void FigureProfile::TuneFreeParameters()
{
    std::vector<std::shared_ptr<FigureParameter>> FreeParameter_Vec;
    for (auto &t : Par_Map)
    {
        if (t.second->Type == FigureParameterType::FLOAT)
        {
            FreeParameter_Vec.push_back(t.second);
            if (FreeParameter_Vec.size() == 1)
                MSGUser()->MSG_INFO("        Name      Init    Step    Min    Max");
            MSGUser()->MSG_INFO("FLOAT   ", t.first.TString::Data(),
                                "   ", t.second->Init,
                                "   ", t.second->Step,
                                "   ", t.second->Min,
                                "   ", t.second->Max);
        }
    }
    MSGUser()->MSG_INFO("Collect ", FreeParameter_Vec.size(), " FLOAT Parameters");
    FCN fcn(Canvas, FreeParameter_Vec);

    int ndiv = 2;
    int index_imp_last = -1;
    for (int iround = 0; iround < 1000; iround++)
    {

        bool findsmaller = false;
        for (int i = 0; i < FreeParameter_Vec.size(); i++)
        {

            if (index_imp_last >= 0 && i == index_imp_last)
                continue;
            MSGUser()->MSG_INFO("Try to improve ", FreeParameter_Vec[i]->Name.TString::Data());

            ROOT::Minuit2::MnUserParameters upar;
            for (int k = 0; k < FreeParameter_Vec.size(); k++)
            {
                upar.Add(FreeParameter_Vec[k]->Name.TString::Data(),
                         FreeParameter_Vec[k]->Init,
                         FreeParameter_Vec[k]->Step,
                         FreeParameter_Vec[k]->Min,
                         FreeParameter_Vec[k]->Max);
            }
            ROOT::Minuit2::MnScan scan(fcn, upar, 2);
            auto res = scan.Scan(i, ndiv, FreeParameter_Vec[i]->Min, FreeParameter_Vec[i]->Max);
            double smaller_par = 0;
            double smaller_val = -999;

            for (int j = 0; j < res.size(); j++)
            {
                // if (i == 2)
                //     MSGUser()->MSG_INFO(FreeParameter_Vec[i]->Name.TString::Data(), "   ", res[j].first, "   ", res[j].second);
                if (fabs(res[j].second + 999) < 1e-6)
                    continue;

                if (fabs(smaller_val + 999) < 1e-6)
                {
                    smaller_val = res[j].second;
                    smaller_par = res[j].first;
                    if (j != 0)
                        findsmaller = true;
                }
                else if (smaller_val > res[j].second)
                {
                    smaller_val = res[j].second;
                    smaller_par = res[j].first;
                    findsmaller = true;
                }
            }

            if (findsmaller)
            {
                MSGUser()->MSG_INFO("Round ", iround,
                                    " Move Parameter: ", FreeParameter_Vec[i]->Name.TString::Data(),
                                    " From ", FreeParameter_Vec[i]->Init,
                                    " To ", smaller_par,
                                    " Val = ", smaller_val);
                FreeParameter_Vec[i]->Init = smaller_par;
                index_imp_last = i;
                break;
            }
        }
        if (findsmaller == false)
        {
            MSGUser()->MSG_INFO("Round ", iround, " Get No Improvement");
            if (ndiv > 1e6)
                break;
            else
                ndiv = ndiv * 2;
            MSGUser()->MSG_INFO("NDiv = ", ndiv);
            index_imp_last = -1;
        }
    }

    MSGUser()->MSG_INFO("Fitted Result: ");
    for (int i = 0; i < FreeParameter_Vec.size(); i++)
    {
        FreeParameter_Vec[i]->Val = FreeParameter_Vec[i]->Init;
        MSGUser()->MSG_INFO(FreeParameter_Vec[i]->Name.TString::Data(), "   ", FreeParameter_Vec[i]->Val);
    }
}

double FigureProfile::FCN::operator()(const std::vector<double> &vpara) const
{
    for (int i = 0; i < vpara.size(); i++)
    {
        Par_Vec[i]->Val = vpara[i];
    }
    Canvas->UpdatePosition();
    bool isok = Canvas->Check();
    if (!isok)
        return -999;
    double CanvasScore = Canvas->Score();
    return CanvasScore;
}

bool FigureElement::Check()
{
    bool isok = OwnCheck();
    if (!isok)
        return false;
    for (int i = 0; i < LinkedElement.size(); i++)
    {
        isok = LinkedElement[i]->Check();
        if (!isok)
            return false;
    }
    return true;
}

void FigureElement::Draw()
{
    DrawElement();
    for (int i = 0; i < LinkedElement.size(); i++)
        LinkedElement[i]->Draw();
}

double FigureElement::OwnScore()
{
    return 0;
}
double FigureElement::Score()
{
    double score = OwnScore();
    for (int i = 0; i < LinkedElement.size(); i++)
        score = score + LinkedElement[i]->Score();
    return score;
}

void FigureElement::UpdatePosition()
{
    UpdateOwnPosition();
    for (int i = 0; i < LinkedElement.size(); i++)
        LinkedElement[i]->UpdatePosition();
}

FigureProfile::FigureProfile(std::shared_ptr<MSGTool> msg, const TString &name, const TString &modefile)
{
    m_msg = msg;
    Name = name;
    if (modefile.TString::EqualTo("") == false)
        Mode = std::make_shared<ConfigTool>(modefile.TString::Data());
    else
        Mode = nullptr;
}

std::shared_ptr<FigureParameter> FigureProfile::BookParameter(const TString &name)
{
    std::shared_ptr<FigureParameter> par = nullptr;
    if (Par_Map.find(name) == Par_Map.end())
    {
        par = std::make_shared<FigureParameter>(name, FigureParameterType::FIXED);
        Par_Map.emplace(std::pair<TString, std::shared_ptr<FigureParameter>>(name, par));
        return par;
    }
    return nullptr;
}

bool FigureProfile::InitParameter(std::shared_ptr<FigureParameter> par)
{
    if (Mode == nullptr)
        return false;

    bool doexist = false;
    TString type = Mode->GetPar<TString>(TString::Format("%s_TYPE", par->Name.TString::Data()), &doexist);
    if (!doexist)
        return false;

    if (type.TString::EqualTo("FIXED"))
        par->Type = FigureParameterType::FIXED;
    else if (type.TString::EqualTo("FLOAT"))
        par->Type = FigureParameterType::FLOAT;
    else
        return false;

    double temp = Mode->GetPar<double>(TString::Format("%s_VAL", par->Name.TString::Data()), &doexist);
    if (!doexist)
        return false;
    par->Val = temp;

    if (type.TString::EqualTo("FIXED"))
    {
        temp = Mode->GetPar<double>(TString::Format("%s_INIT", par->Name.TString::Data()), &doexist);
        if (!doexist)
            return false;
        par->Init = temp;
        temp = Mode->GetPar<double>(TString::Format("%s_MAX", par->Name.TString::Data()), &doexist);
        if (!doexist)
            return false;
        par->Max = temp;
        temp = Mode->GetPar<double>(TString::Format("%s_MIN", par->Name.TString::Data()), &doexist);
        if (!doexist)
            return false;
        par->Min = temp;
        temp = Mode->GetPar<double>(TString::Format("%s_STEP", par->Name.TString::Data()), &doexist);
        if (!doexist)
            return false;
        par->Step = temp;
    }

    return true;
}

void Figure_MultiCompare::BookHist(const TString &name, TH1D *hist)
{
    if (Hist_Map.find(name) == Hist_Map.end())
    {
        Hist_Map.emplace(std::pair<TString, TH1D *>(name, hist));
    }
}

void Figure_MultiCompare::ConfigFigure()
{
    MSGUser()->MSG_INFO("Config ", Name.TString::Data(), " with Profile: MultiCompare");
    std::shared_ptr<FigureParameter> Size_X = BookParameter("Size_X");
    std::shared_ptr<FigureParameter> Size_Y = BookParameter("Size_Y");
    std::shared_ptr<FigureParameter> Min_X = BookParameter("Min_X");
    std::shared_ptr<FigureParameter> Max_X = BookParameter("Max_X");
    std::shared_ptr<FigureParameter> Min_Y = BookParameter("Min_Y");
    std::shared_ptr<FigureParameter> Max_Y = BookParameter("Max_Y");
    std::shared_ptr<FigureParameter> Main_Top_Margin = BookParameter("Main_Top_Margin");
    std::shared_ptr<FigureParameter> Main_Left_Margin = BookParameter("Main_Left_Margin");
    std::shared_ptr<FigureParameter> Main_Right_Margin = BookParameter("Main_Right_Margin");
    std::shared_ptr<FigureParameter> Main_Bottom_Margin = BookParameter("Main_Bottom_Margin");
    std::shared_ptr<FigureParameter> Main_Legend_X = BookParameter("Main_Legend_X");
    std::shared_ptr<FigureParameter> Main_Legend_Y = BookParameter("Main_Legend_Y");

    std::shared_ptr<FigureParameter> Zero = std::make_shared<FigureParameter>("ZERO", FigureParameterType::FIXED);
    Zero->Val = 0;
    // Book All the parameters needed

    Canvas = std::make_shared<FigureCanvas>("Main_Casnvas", nullptr, Size_X, Size_Y);
    std::shared_ptr<FigurePad> Pad = Canvas->Book<FigurePad>("Main_Pad", Zero, Zero, Size_X, Size_Y);
    Pad->SetAxis(Min_X, Min_Y, Max_X, Max_Y);
    Pad->SetMargin(Main_Top_Margin, Main_Bottom_Margin, Main_Left_Margin, Main_Right_Margin);
    Pad->SetTitle(X_Axis_Title, Y_Axis_Title);

    std::vector<std::shared_ptr<FigureHist1D>> hist_vec;
    for (auto &t : Hist_Map)
    {
        auto Hist = Pad->Book<FigureHist1D>(t.first, t.second);
        hist_vec.push_back(Hist);
    }

    std::shared_ptr<FigureLegend> Legend = Pad->Book<FigureLegend>("Main_Legend", Main_Legend_Header, Main_Legend_X, Main_Legend_Y);
    for (int i = 0; i < hist_vec.size(); i++)
        Legend->SetEntry(hist_vec[i]);

    // Book Tree Structure
    if (!InitParameter(Main_Legend_X))
    {
        Main_Legend_X->Val = 2500;
        Main_Legend_X->Init = 2500;
        Main_Legend_X->Max = 5000;
        Main_Legend_X->Min = 0;
        Main_Legend_X->Step = 100;
        Main_Legend_X->Type = FigureParameterType::FLOAT;
    }
    if (!InitParameter(Main_Legend_Y))
    {
        Main_Legend_Y->Val = 2500;
        Main_Legend_Y->Init = 2500;
        Main_Legend_Y->Max = 5000;
        Main_Legend_Y->Min = 0;
        Main_Legend_Y->Step = 100;
        Main_Legend_Y->Type = FigureParameterType::FLOAT;
    }
    if (!InitParameter(Main_Top_Margin))
    {
        Main_Top_Margin->Val = 0.04;
        Main_Top_Margin->Type = FigureParameterType::FIXED;
    }
    if (!InitParameter(Main_Left_Margin))
    {
        Main_Left_Margin->Val = 0.13;
        Main_Left_Margin->Type = FigureParameterType::FIXED;
    }
    if (!InitParameter(Main_Right_Margin))
    {
        Main_Right_Margin->Val = 0.02;
        Main_Right_Margin->Type = FigureParameterType::FIXED;
    }
    if (!InitParameter(Main_Bottom_Margin))
    {
        Main_Bottom_Margin->Val = 0.11;
        Main_Bottom_Margin->Type = FigureParameterType::FIXED;
    }
    if (!InitParameter(Size_X))
    {
        Size_X->Val = 5000;
        Size_X->Type = FigureParameterType::FIXED;
    }
    if (!InitParameter(Size_Y))
    {
        Size_Y->Val = 5000;
        Size_Y->Type = FigureParameterType::FIXED;
    }
    if ((!InitParameter(Min_X)) ||
        (!InitParameter(Max_X)))
    {
        Min_X->Type = FigureParameterType::FLOAT;
        Min_X->Val = 2e8;
        for (auto &t : Hist_Map)
        {
            double val = t.second->GetXaxis()->GetBinLowEdge(1);
            if (Min_X->Val > val)
                Min_X->Val = val;
        }
        Max_X->Type = FigureParameterType::FLOAT;
        Max_X->Val = -2e8;
        for (auto &t : Hist_Map)
        {
            int index = t.second->GetXaxis()->GetNbins();
            double val = t.second->GetXaxis()->GetBinLowEdge(index) +
                         t.second->GetXaxis()->GetBinWidth(index);
            if (Max_X->Val < val)
                Max_X->Val = val;
        }

        Min_X->Init = Min_X->Val;
        Max_X->Init = Max_X->Val;
        Min_X->Step = (Max_X->Val - Min_X->Val) / 3;
        Max_X->Step = (Max_X->Val - Min_X->Val) / 3;
        Min_X->Min = Min_X->Val;
        Min_X->Max = Max_X->Val;
        Max_X->Min = Min_X->Val;
        Max_X->Max = Max_X->Val;
    }

    if ((!InitParameter(Min_Y)) ||
        (!InitParameter(Max_Y)))
    {
        Min_Y->Type = FigureParameterType::FLOAT;
        Min_Y->Val = 2e8;
        for (auto &t : Hist_Map)
        {
            int index = t.second->GetXaxis()->GetNbins();
            for (int i = 0; i < index; i++)
            {
                if (Min_Y->Val > t.second->GetBinContent(i + 1))
                    Min_Y->Val = t.second->GetBinContent(i + 1);
            }
        }
        Max_Y->Type = FigureParameterType::FLOAT;
        Max_Y->Val = -2e8;
        for (auto &t : Hist_Map)
        {
            int index = t.second->GetXaxis()->GetNbins();
            for (int i = 0; i < index; i++)
            {
                if (Max_Y->Val < t.second->GetBinContent(i + 1))
                    Max_Y->Val = t.second->GetBinContent(i + 1);
            }
        }

        Min_Y->Init = Min_Y->Val;
        Max_Y->Init = Max_Y->Val;
        Min_Y->Step = (Max_Y->Val - Min_Y->Val) / 3;
        Max_Y->Step = (Max_Y->Val - Min_Y->Val) / 3;
        Min_Y->Min = Min_Y->Val - 3 * (Max_Y->Val - Min_Y->Val);
        Min_Y->Max = Max_Y->Val;
        Max_Y->Min = Min_Y->Val;
        Max_Y->Max = Max_Y->Val + 3 * (Max_Y->Val - Min_Y->Val);
    }
}

double FigureCanvas::OwnScore()
{
    return 0;
}

void FigureCanvas::DrawElement()
{
    m_canvas = new TCanvas(Name.TString::Data(), Name.TString::Data(), Size_X->Val, Size_Y->Val);
    m_canvas->Draw();
}

void FigureCanvas::UpdateOwnPosition()
{
    ClearPosition();
    Left_Down_X.push_back(0);
    Left_Down_Y.push_back(0);
    Right_Up_X.push_back(Size_X->Val);
    Right_Up_Y.push_back(Size_Y->Val);
}

void FigurePad::DrawElement()
{
    m_pad = new TPad(Name.TString::Data(), Name.TString::Data(),
                     ((double)(Left_Down_X[0] - MotherElement->Left_Down_X[0])) / (MotherElement->Right_Up_X[0] - MotherElement->Left_Down_X[0]),
                     ((double)(Left_Down_Y[0] - MotherElement->Left_Down_Y[0])) / (MotherElement->Right_Up_Y[0] - MotherElement->Left_Down_Y[0]),
                     ((double)(Right_Up_X[0] - MotherElement->Left_Down_X[0])) / (MotherElement->Right_Up_X[0] - MotherElement->Left_Down_X[0]),
                     ((double)(Right_Up_Y[0] - MotherElement->Left_Down_Y[0])) / (MotherElement->Right_Up_Y[0] - MotherElement->Left_Down_Y[0]));
    MotherElement->CD();
    m_pad->Draw();
    m_pad->SetMargin(Left_Margin_Rate->Val, Right_Margin_Rate->Val, Bottom_Margin_Rate->Val, Top_Margin_Rate->Val);
}

bool FigurePad::OwnCheck()
{
    if (HaveAxis)
    {
        double axis_ld_x;
        double axis_ld_y;
        double axis_ru_x;
        double axis_ru_y;
        GetAxisRange(axis_ld_x, axis_ld_y, axis_ru_x, axis_ru_y);

        if (X_Axis_Max->Val < X_Axis_Min->Val)
            return false;
        if (Y_Axis_Max->Val < Y_Axis_Min->Val)
            return false;
        for (int i = 0; i < LinkedElement.size(); i++)
        {
            for (int j = 0; j < LinkedElement[i]->Left_Down_X.size(); j++)
            {
                if (LinkedElement[i]->Left_Down_X[j] < axis_ld_x ||
                    LinkedElement[i]->Left_Down_X[j] > axis_ru_x)
                    return false;
                if (LinkedElement[i]->Left_Down_Y[j] < axis_ld_y ||
                    LinkedElement[i]->Left_Down_Y[j] > axis_ru_y)
                    return false;
                if (LinkedElement[i]->Right_Up_X[j] < axis_ld_x ||
                    LinkedElement[i]->Right_Up_X[j] > axis_ru_x)
                    return false;
                if (LinkedElement[i]->Right_Up_Y[j] < axis_ld_y ||
                    LinkedElement[i]->Right_Up_Y[j] > axis_ru_y)
                    return false;
            }
        }

        return CheckCollision();
    }

    return true;
}

bool FigureElement::CheckCollision()
{
    bool isok = true;
    for (int i = 0; i < LinkedElement.size(); i++)
    {
    }

    return true;
}

double FigurePad::OwnScore()
{
    double score = 0;
    // (RU_X->Val - LD_X->Val) * (RU_Y->Val - LD_Y->Val);
    if (HaveAxis)
    {
        for (int i = 0; i < LinkedElement.size(); i++)
        {
            for (int j = 0; j < LinkedElement[i]->Left_Down_X.size(); j++)
            {
                score = score +
                        (LinkedElement[i]->Right_Up_X[j] - LinkedElement[i]->Left_Down_X[j]) *
                            (LinkedElement[i]->Right_Up_Y[j] - LinkedElement[i]->Left_Down_Y[j]);
            }
        }
        return (RU_X->Val - LD_X->Val) * (RU_Y->Val - LD_Y->Val) * LinkedElement.size() - score;
    }

    return 0;
}

void FigurePad::GetAxisRange(double &draw_ld_x, double &draw_ld_y, double &draw_ru_x, double &draw_ru_y)
{
    double width = RU_X->Val - LD_X->Val;
    double height = RU_Y->Val - LD_Y->Val;

    draw_ld_x = LD_X->Val + Left_Margin_Rate->Val * width;
    draw_ld_y = LD_Y->Val + Bottom_Margin_Rate->Val * height;
    draw_ru_x = RU_X->Val - Right_Margin_Rate->Val * width;
    draw_ru_y = RU_Y->Val - Top_Margin_Rate->Val * height;
}

void FigurePad::UpdateOwnPosition()
{
    ClearPosition();
    Left_Down_X.push_back(LD_X->Val);
    Left_Down_Y.push_back(LD_Y->Val);
    Right_Up_X.push_back(RU_X->Val);
    Right_Up_Y.push_back(RU_Y->Val);
}

void FigureHist1D::UpdateOwnPosition()
{
    ClearPosition();
    double ld_x;
    double ld_y;
    double ru_x;
    double ru_y;
    ((FigurePad *)MotherElement)->GetAxisRange(ld_x, ld_y, ru_x, ru_y);

    double min_x = ((FigurePad *)MotherElement)->X_Axis_Min->Val;
    double max_x = ((FigurePad *)MotherElement)->X_Axis_Max->Val;
    double min_y = ((FigurePad *)MotherElement)->Y_Axis_Min->Val;
    double max_y = ((FigurePad *)MotherElement)->Y_Axis_Max->Val;

    for (int i = 0; i < Hist->GetXaxis()->GetNbins(); i++)
    {
        double val = Hist->GetBinContent(i + 1);
        double pos = Hist->GetBinCenter(i + 1);
        double bin_width = Hist->GetBinWidth(i + 1);
        double err = Hist->GetBinError(i + 1);

        if (err < 1e-8 && fabs(val) < 1e-8)
            continue;

        Left_Down_X.push_back(ld_x + (pos - min_x - bin_width / 2) / (max_x - min_x) * (ru_x - ld_x));
        Left_Down_Y.push_back(ld_y + (val - min_y - err) / (max_y - min_y) * (ru_y - ld_y));
        Right_Up_X.push_back(ld_x + (pos - min_x + bin_width / 2) / (max_x - min_x) * (ru_x - ld_x));
        Right_Up_Y.push_back(ld_y + (val - min_y + err) / (max_y - min_y) * (ru_y - ld_y));
    }
}

double FigureHist1D::OwnScore()
{
    double score = 0;
    // for (int i = 0; i < Left_Down_X.size(); i++)
    // {
    //     score = score + (Right_Up_X[i] - Left_Down_X[i]) * (Right_Up_Y[i] - Left_Down_Y[i]);
    // }
    // cout << Name.TString::Data() << "   " << score << endl;
    return score;
}

void FigureHist1D::DrawElement()
{
    MotherElement->CD();
    int index = -1;
    for (int i = 0; i < MotherElement->LinkedElement.size(); i++)
    {
        if (MotherElement->LinkedElement[i]->NeedAxis == true)
            index++;
        if (Name.TString::EqualTo(MotherElement->LinkedElement[i]->Name.TString::Data()))
            break;
    }
    if (index == 0)
    {
        Hist->Draw("E0");
        int index = Hist->GetXaxis()->FindBin(((FigurePad *)MotherElement)->X_Axis_Min->Val);
        if (Hist->GetXaxis()->GetBinLowEdge(index + 1) -
                ((FigurePad *)MotherElement)->X_Axis_Min->Val <
            (Hist->GetXaxis()->GetBinWidth(index)) / 100)
            Hist->GetXaxis()->SetRangeUser(Hist->GetXaxis()->GetBinLowEdge(index + 1) + (Hist->GetXaxis()->GetBinWidth(index + 1)) * 1e-4,
                                           ((FigurePad *)MotherElement)->X_Axis_Max->Val);
        else
            Hist->GetXaxis()->SetRangeUser(((FigurePad *)MotherElement)->X_Axis_Min->Val,
                                           ((FigurePad *)MotherElement)->X_Axis_Max->Val);

        Hist->GetYaxis()->SetRangeUser(((FigurePad *)MotherElement)->Y_Axis_Min->Val,
                                       ((FigurePad *)MotherElement)->Y_Axis_Max->Val);

        Hist->SetStats(false);
        Hist->SetTitle("");
        Hist->GetYaxis()->SetMaxDigits(3);
        Hist->GetXaxis()->SetTitleFont(32);
        Hist->GetYaxis()->SetTitleFont(32);

        double X_Size = (MotherElement->Right_Up_X[0] - MotherElement->Left_Down_X[0]);
        double Y_Size = (MotherElement->Right_Up_Y[0] - MotherElement->Left_Down_Y[0]);

        if (X_Size < Y_Size)
        {
            Hist->GetXaxis()->SetTitleSize((double)200 / X_Size);
            Hist->GetYaxis()->SetTitleSize((double)200 / X_Size);
        }
        else
        {
            Hist->GetXaxis()->SetTitleSize((double)200 / Y_Size);
            Hist->GetYaxis()->SetTitleSize((double)200 / Y_Size);
        }

        if (((FigurePad *)MotherElement)->X_Axis_Title.TString::EqualTo("") == false)
            Hist->GetXaxis()->SetTitle(((FigurePad *)MotherElement)->X_Axis_Title);
        if (((FigurePad *)MotherElement)->Y_Axis_Title.TString::EqualTo("") == false)
            Hist->GetYaxis()->SetTitle(((FigurePad *)MotherElement)->Y_Axis_Title);
    }
    else
    {
        Hist->Draw("E0 same");
    }
}
void FigureLegend::UpdateOwnPosition()
{
    ClearPosition();
    double width = GetWidth();
    double height = GetHeight();

    Left_Down_X.push_back(Pos_X->Val - width / 2);
    Left_Down_Y.push_back(Pos_Y->Val - height / 2);
    Right_Up_X.push_back(Pos_X->Val + width / 2);
    Right_Up_Y.push_back(Pos_Y->Val + height / 2);
}

void FigureLegend::DrawElement()
{
    MotherElement->CD();
    Legend = new TLegend(((double)(Left_Down_X[0] - MotherElement->Left_Down_X[0])) / (MotherElement->Right_Up_X[0] - MotherElement->Left_Down_X[0]),
                         ((double)(Left_Down_Y[0] - MotherElement->Left_Down_Y[0])) / (MotherElement->Right_Up_Y[0] - MotherElement->Left_Down_Y[0]),
                         ((double)(Right_Up_X[0] - MotherElement->Left_Down_X[0])) / (MotherElement->Right_Up_X[0] - MotherElement->Left_Down_X[0]),
                         ((double)(Right_Up_Y[0] - MotherElement->Left_Down_Y[0])) / (MotherElement->Right_Up_Y[0] - MotherElement->Left_Down_Y[0]));

    Legend->SetTextFont(32);

    double X_Size = (MotherElement->Right_Up_X[0] - MotherElement->Left_Down_X[0]);
    double Y_Size = (MotherElement->Right_Up_Y[0] - MotherElement->Left_Down_Y[0]);

    if (X_Size < Y_Size)
        Legend->SetTextSize((double)200 / X_Size);
    else
        Legend->SetTextSize((double)200 / Y_Size);

    Legend->SetHeader(Header.TString::Data());
    for (int i = 0; i < Entry_Vec.size(); i++)
    {
        Legend->AddEntry(Entry_Vec[i]->GetObject(), Entry_Vec[i]->Name.TString::Data(), "lpfe");
    }
    Legend->Draw();
}

double FigureLegend::GetHeight()
{
    int nrow = Entry_Vec.size();
    if (Header.TString::EqualTo("") == false)
        nrow = nrow + 1;
    return nrow * 200 * 1.50;
}

double FigureLegend::GetWidth()
{
    int length_max = -999;
    for (int i = 0; i < Entry_Vec.size(); i++)
    {
        if (Entry_Vec[i]->Name.TString::Length() > length_max)
            length_max = Entry_Vec[i]->Name.TString::Length();
    }

    return (length_max * 200 * 0.5) / (1 - 0.25);
}

double FigureLegend::OwnScore()
{
    return pow(Pos_X->Val, 2) - pow(Pos_Y->Val, 2);
}
