//***************************************************************************************
/// @file MSGTool.cxx
/// @author Chen Wamg, Wenhao Ma
//***************************************************************************************

#include "NAGASH/MSGTool.h"

using namespace NAGASH;
using namespace std;

/// @brief Construct MSGTool with given level, message below this level will not be printed.
/// @param level the message level.
/// @param name the focus region name.
MSGTool::MSGTool(MSGLevel level, const TString &name)
{
    DefinedLevel = level;
    FocusName = name;

    if (DefinedLevel == MSGLevel::FOCUS)
        OutputLevel = MSGLevel::INFO;
    else
        OutputLevel = level;
}

/// @brief Construct sub-MSGTool for multi-thread usage.
/// @param msg the mother MSGTool.
/// @param ThreadNumber the thread number.
MSGTool::MSGTool(const MSGTool &msg, int ThreadNumber)
{
    TString num;
    num.Form("_JobNum%d", ThreadNumber);
    OutputLevel = msg.OutputLevel;
    DefinedLevel = msg.DefinedLevel;
    FocusName = msg.FocusName;
    isCreateLog = msg.isCreateLog;
    isOpenLog = msg.isOpenLog;
    LogFileName = msg.LogFileName + num;
    FuncTitle = msg.FuncTitle;

    if (isOpenLog)
        OpenLog(LogFileName);
    if (isCreateLog)
        CreateLog(LogFileName);
}

MSGTool::~MSGTool()
{
    CloseLog();
}

/// @brief Create log file with given file name.
/// @param name file name of the log.
void MSGTool::CreateLog(const TString &name)
{
    if (tolog == true)
        CloseLog();

    outfile = ofstream(name.TString::Data(), ios::out);
    LogFileName = name;
    tolog = true;
    isCreateLog = true;
    isOpenLog = false;
}

/// @brief Open log file with given file name.
/// @param name file name of the log.
void MSGTool::OpenLog(const TString &name)
{
    if (tolog == true)
        CloseLog();

    outfile = ofstream(name.TString::Data(), ios::app);
    LogFileName = name;
    tolog = true;
    isOpenLog = true;
    isCreateLog = false;
}

/// @brief Close the log file.
void MSGTool::CloseLog()
{
    if (tolog == false)
        return;
    else
    {
        tolog = false;
        isCreateLog = false;
        isOpenLog = false;
        outfile.close();
    }
}

/// @brief Start focus region, inside this region the message level would be set to debug
/// @param name name of the focus region.
void MSGTool::StartFocusRegion(const TString &name)
{
    if ((DefinedLevel == MSGLevel::FOCUS) && (FocusName.TString::EqualTo(name) == true))
        OutputLevel = MSGLevel::DEBUG;
}

/// @brief End focus region.
/// @param name name of the focus region.
void MSGTool::EndFocusRegion(const TString &name)
{
    if ((DefinedLevel == MSGLevel::FOCUS) && (FocusName.TString::EqualTo(name) == true))
        OutputLevel = MSGLevel::INFO;
}

/** @brief Start a title with given name.

    The output should be like
    ~~~
    ...->(title name)->(messages)
    ~~~

    @param name title name.
*/
void MSGTool::StartTitle(const TString &name)
{
    std::unique_lock msglock(MSGMutex);
    FuncTitle.push_back(name);
}

/// @brief Start a title with given name.
/// @param name the title name.
/// @return the pointer of MSGTitleGuard. Destruction of this pointer will end the title.
std::unique_ptr<MSGTool::MSGTitleGuard> MSGTool::StartTitleWithGuard(const TString &name)
{
    std::unique_lock msglock(MSGMutex);
    FuncTitle.push_back(name);
    return std::unique_ptr<MSGTitleGuard>(new MSGTitleGuard(shared_from_this()));
}

/// @brief End the current title.
void MSGTool::EndTitle()
{
    std::unique_lock msglock(MSGMutex);
    if (FuncTitle.size() > 0)
        FuncTitle.pop_back();
}

void MSGTool::PrintTitle()
{
    std::shared_lock msglock(MSGMutex);
    if (FuncTitle.size() == 0)
        Print("Title Track Lost");
    else
    {
        for (size_t i = 0; i < FuncTitle.size(); i++)
        {
            if (i == FuncTitle.size() - 1)
                Print(FuncTitle[i]);
            else
                Print(FuncTitle[i], "->");
        }
    }
}

void MSGTool::PrintLevel(MSGLevel level)
{
    if (level == MSGLevel::FATAL)
        Print("FATAL");
    if (level == MSGLevel::ERROR)
        Print("ERROR");
    if (level == MSGLevel::WARNING)
        Print("WARNING");
    if (level == MSGLevel::INFO)
        Print("INFO");
    if (level == MSGLevel::DEBUG)
        Print("DEBUG");
}

/// @brief Set the output message level.
/// @param level message level.
/// @return the given message level.
MSGLevel MSGTool::SetOutputLevel(MSGLevel level)
{
    auto prev = OutputLevel;
    OutputLevel = level;
    return prev;
}

/// @brief Get the current message level.
MSGLevel MSGTool::Level() const
{
    return OutputLevel;
}
