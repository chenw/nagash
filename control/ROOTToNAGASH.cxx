#include "NAGASH/NAGASH.h"
#include "NAGASH/NGCount.h"
#include "NAGASH/NNGHist.h"
#include "NAGASH/NAGASHFile.h"

using namespace NAGASH;

bool isFixedBinSize(TAxis *axis)
{
    return axis->GetXbins()->GetSize() == 0 ? true : false;
}

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Usage: " << argv[0] << " (input ROOT file) (output NAGASH file)" << std::endl;
        return 0;
    }

    TFile *f = new TFile(argv[1], "READ");
    if (f->IsZombie())
    {
        std::cout << "Input Root file is broken!" << std::endl;
        return 0;
    }

    TIter next(f->GetListOfKeys());
    TKey *key;
    int nhist = 0;

    NAGASHFile outfile(std::make_shared<MSGTool>(), argv[2], NAGASHFile::Mode::WRITE);

    while ((key = (TKey *)next()))
    {
        nhist++;
        if (TString(key->GetClassName()) == TString("TH1D"))
        {
            TH1D *h = (TH1D *)(key->ReadObj());
            if (isFixedBinSize(h->GetXaxis()))
            {
                NNGHist<Axis::Regular> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
            else
            {
                NNGHist<Axis::Variable> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
        }

        if (TString(key->GetClassName()) == TString("TH2D"))
        {
            TH2D *h = (TH2D *)(key->ReadObj());
            if (isFixedBinSize(h->GetXaxis()) && isFixedBinSize(h->GetYaxis()))
            {
                NNGHist<Axis::Regular, Axis::Regular> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
            else if (isFixedBinSize(h->GetXaxis()) && !isFixedBinSize(h->GetYaxis()))
            {
                NNGHist<Axis::Regular, Axis::Variable> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
            else if (!isFixedBinSize(h->GetXaxis()) && isFixedBinSize(h->GetYaxis()))
            {
                NNGHist<Axis::Variable, Axis::Regular> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
            else
            {
                NNGHist<Axis::Variable, Axis::Variable> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
        }

        if (TString(key->GetClassName()) == TString("TH3D"))
        {
            TH3D *h = (TH3D *)(key->ReadObj());
            if (isFixedBinSize(h->GetXaxis()))
            {
                NNGHist<Axis::Regular, Axis::Regular, Axis::Regular> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
            else
            {
                NNGHist<Axis::Variable, Axis::Variable, Axis::Variable> nnghist(h);
                outfile.SaveHist(h->GetName(), nnghist);
            }
        }

        //		if (nhist % 100 == 0)
        //			std::cout << nhist << std::endl;
    }

    outfile.Close();

    return 0;
}
