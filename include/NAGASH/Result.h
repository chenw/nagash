//***************************************************************************************
/// @file Result.h
/// @author Wenhao Ma
//***************************************************************************************

/** @defgroup ResultClasses Result Classes
    @brief Classes for storing and manipulating the results of your analysis.

    ### Create your own Result

    In NAGASH, Result is the result of your job. And you can get it by

    @code{.cxx}
    auto myresult = ResultGroupUser()->BookResult<ResultType>(name, filename, ...);
    @endcode

    inside any class inherite from NAGASH::Job. The first argument is necessary, and the second argument is optional if no other arguments are needed.

    If you want to define your own result, you should organize your code as :

    @code{.cxx}
    #pragma once

    class UserResult : public NAGASH::Result
    {
        UserResult(std::shared_ptr<NAGASH::MSGTool> msg, std::shared_ptr<NAGASH::ConfigTool> config, const TString &name, const TString &filename, other arguments you want)
            : Result(msg, config, name, filename)
        {
            // do something you want
            // Then you can get this result by
            // ResultGroupUser()->BookResult<UserResult>(name, filename, other arguments);
            // inside class inherite from NAGASH::Job
        }

        // here are two functions you should override
        void Combine(std::shared_ptr<Result> result) override
        {
            // combine with other result
            auto subresult = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
            if (subresult != nullptr)
            {
                // do something you want
            }
        }

        // this function is optional
        void WriteToFile() override
        {
        }
    }
    @endcode
 */

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/MSGTool.h"
#include "NAGASH/Toolkit.h"
#include "NAGASH/ConfigTool.h"
#include "NAGASH/SystTool.h"

namespace NAGASH
{
    /// @class NAGASH::Result Result.h "NAGASH/Result.h"
    /// @ingroup ResultClasses
    /// @brief Provide virtual interface to manipulate all the results inside %NAGASH.
    class Result
    {
    public:
        const TString &GetResultName() { return Name; };         ///< Return the name of the result.
        const TString &GetOutputFileName() { return FileName; }; ///< Return the name of the output file.

        virtual void Combine(std::shared_ptr<Result> result) {} ///< virtual interface to combine two results.
        virtual void WriteToFile() {}                           /// virtual interface to write the result to a file.

    protected:
        /// @brief Contructor.
        /// @param MSG input MSGTool.
        /// @param c input ConfigTool.
        /// @param rname the name of the result.
        /// @param fname the output file name of the result.
        Result(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "")
            : msg(MSG), config(c), toolkit(MSG), Name(rname), FileName(fname) {}

        Result() = delete;
        Result(const Result &result) = delete;
        Result &operator=(const Result &result) = delete;
        Result(Result &&result) = delete;
        Result &operator=(Result &&result) = delete;
        virtual ~Result() = default;

        std::shared_ptr<MSGTool> MSGUser();       ///< Return the internal MSGTool.
        std::shared_ptr<ConfigTool> ConfigUser(); ///< Return the internal ConfigTool.>
        Toolkit *ToolkitUser();                   ///< Return the internal Toolkit.

        void SetOutputFileName(const TString &fileName) { FileName = fileName; } ///< Set the output file name of the result.

    private:
        std::shared_ptr<MSGTool> msg;
        std::shared_ptr<ConfigTool> config;
        Toolkit toolkit;
        TString Name;
        TString FileName;
    };

    inline std::shared_ptr<MSGTool> Result::MSGUser() { return msg; }
    inline Toolkit *Result::ToolkitUser() { return &toolkit; }
    inline std::shared_ptr<ConfigTool> Result::ConfigUser() { return config; }
} // namespace NAGASH
