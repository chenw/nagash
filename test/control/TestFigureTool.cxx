#include "NAGASH/FigureTool.h"
#include "NAGASH/Global.h"
#include "TDatime.h"

using namespace NAGASH;

int main(int argc, char **argv)
{
    // prepare histograms
    TRandom3 myrandom(0);
    TH1D *Gaus0 = new TH1D("Gaus0", "Gaus0", 100, -5, 5);
    Gaus0->Sumw2();
    TH1D *Gaus1 = new TH1D("Gaus1", "Gaus1", 100, -5, 5);
    Gaus1->Sumw2();
    TH1D *Gaus2 = new TH1D("Gaus2", "Gaus2", 100, -5, 5);
    Gaus2->Sumw2();
    TH1D *Gaus3 = new TH1D("Gaus3", "Gaus3", 100, -5, 5);
    Gaus3->Sumw2();
    TH1D *Gaus4 = new TH1D("Gaus4", "Gaus4", 100, -5, 5);
    Gaus4->Sumw2();
    TH1D *Gaus5 = new TH1D("Gaus5", "Gaus5", 100, -5, 5);
    Gaus5->Sumw2();

    for (int i = 0; i < 10000; i++)
    {
        Gaus0->Fill(myrandom.Gaus(0, 1));
        Gaus1->Fill(myrandom.Gaus(2, 1));
        Gaus2->Fill(myrandom.Gaus(-2, 1));
        Gaus3->Fill(myrandom.Gaus(0, 4));
        Gaus4->Fill(myrandom.Gaus(0, 0.25));
        Gaus5->Fill(myrandom.Gaus(0, 1.01));
    }

    std::shared_ptr<MSGTool> MSG = std::make_shared<MSGTool>();
    std::shared_ptr<FigureTool> MyFigureTool = std::make_shared<FigureTool>(MSG);
    MyFigureTool->SetFigureDirectory(".");

    // first plot, 4 pads
    auto MyCanvas = MyFigureTool->BookFigure("Test1.pdf", 5000, 5000);

    auto MyPad = MyCanvas->BookPad("Pad1", 0, 0, 2500, 2500);
    MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
    MyPad->SetAxisTitle("Value", "#frac{G}{G}");

    MyPad = MyCanvas->BookPad("Pad2", 2500, 0, 5000, 2500);
    MyPad->SetInputHist1D("Gaus1", Gaus1, "E0");
    MyPad->SetAxisTitle("Value");

    MyPad = MyCanvas->BookPad("Pad3", 0, 2500, 2500, 5000);
    MyPad->SetInputHist1D("Gaus2", Gaus2, "E0");
    // UNKNOWN set the pad left no space for the label and title of this axis
    MyPad->SetAxisTitle("UNKNOWN", "Number");
    MyPad->SetAxisTitleOffset(0, 5);

    MyPad = MyCanvas->BookPad("Pad4", 2500, 2500, 5000, 5000);
    MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
    MyPad->SetInputHist1D("Gaus3", Gaus3, "E0");
    MyPad->SetInputHist1D("Gaus4", Gaus4, "E0");

    // second plot, draw a pad inside another pad
    MyCanvas = MyFigureTool->BookFigure("Test2.pdf", 500, 500);
    MyPad = MyCanvas->BookPad("Pad0", 0, 0, 500, 500);
    MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
    MyPad->SetInputHist1D("Gaus1", Gaus1, "E0");
    MyPad->SetInputHist1D("Gaus2", Gaus2, "E0");
    MyPad->SetAxisTitle("Value", "Number");
    MyPad->SetAxisTitleOffset(1.2, 5);

    MyPad = MyCanvas->BookPad("Pad1", 250, 250, 400, 400);
    MyPad->SetInputHist1D("Gaus0", Gaus0, "E0");
    MyPad->SetInputHist1D("Gaus3", Gaus3, "E0");
    MyPad->SetInputHist1D("Gaus4", Gaus4, "E0");
    MyPad->SetAxisTitle("Value", "Number");

    // third plot, usage of hstack and histogram links, also draw in atlas style
    TF1 *f1 = new TF1(
        "gaus", [](double *x, double *par)
        { return 400 * exp(-x[0] * x[0] / 2); },
        -5, 5);

    MyCanvas = MyFigureTool->BookATLASSquare("Test3.pdf");
    MyPad = MyCanvas->BookPad("MainPad", 0, 200, 600, 600);
    {
        auto hs = MyPad->SetHStack("Gaus1+2", {"Gaus1", "Gaus2"}, "hist");
        hs->DrawTotalHist("E2",
                          [](TH1D *h)
                          {
                              h->SetFillStyle(3354);
                              h->SetFillColor(1);
                              h->SetMarkerColorAlpha(1, 0);
                              h->SetLineColor(1);
                          });
    }
    MyPad->SetInputHist1DGraph("Gaus0", Gaus0, "ZP",
                               [](TGraph *g)
                               {
                                   g->SetMarkerSize(0.6);
                                   g->SetMarkerColor(1);
                                   g->SetMarkerStyle(20);
                                   g->SetLineWidth(1);
                                   g->SetLineColor(1);
                               });
    MyPad->SetInputHist1D("Gaus1", Gaus1, "E0");
    MyPad->SetInputHist1D("Gaus2", Gaus2, "E0");
    MyPad->SetInputHist1D("Gaus3", Gaus3, "E0");
    MyPad->SetInputHist1D("Gaus5", Gaus5, "E0");
    MyPad->SetInputFunc1D("GausFunc", f1, "");
    MyPad->SetAxisTitle("UNKNOWN", "Number");
    // MyPad->SetGridx();
    // MyPad->SetGridy();
    // set the legend, user can either the position manually or the tool will
    // auto adjust the range of y axis to place the legend

    // MyPad->SetYaxisRange(0, 800);
    // MyPad->SetLegend("ATLAS Internal", "", 0.2, 0.6, 1, 0.9);
    // MyPad->SetLegend("ATLAS Internal    #sqrt{s}=13TeV, 140 fb^{-1}");
    // MyPad->SetLegend("", "", 0.5, 0.3, 0.9, 0.9);
    MyPad->SetLegend();
    MyPad->DrawATLASLabel("Internal", 0.19, 0.84, 1);
    MyPad->SetLatex("Test", 0.19, 0.79);
    MyPad->SetLatex("Test2", 0.19, 0.74, kRed);
    MyPad->SetLatex("Test3", 0.19, 0.69, kBlue, [](TLatex *l)
                    { l->SetTextFont(72); l->SetTextSize(0.03); });
    MyPad->SetLegendColumn(2);

    MyPad = MyCanvas->BookPad("SubPad", 0, 0, 600, 200);
    MyPad->SetYaxisRange(0.57, 1.43);
    MyPad->SetAxisTitleOffset(1.4, 1.7);
    // define a hist using the predefined hists
    // the first the type, currently support RATIO PULL DELTA CHI2
    // then the linked list of hists, divided by '/'
    // the last is the drawing option:
    // VAL means drawing the value
    // ERR means drawing the error
    // TOPERR means using the first hist error only
    // BOTTEMERR means using the second hist error only
    MyPad->SetInputHist1D("Ratio", "RATIO/Gaus0/Gaus0/VAL", "E0");
    MyPad->SetInputHist1DGraph("Ratio2", "RATIO/Gaus0/Gaus5/VALERR", "ZP",
                               [](TGraph *g)
                               {
                                   g->SetMarkerSize(0.6);
                                   g->SetMarkerColor(1);
                                   g->SetMarkerStyle(20);
                                   g->SetLineWidth(1);
                                   g->SetLineColor(1);
                               });
    MyPad->SetAxisTitle("Value", "#frac{Gaus0}{Gaus5}");

    // the fourth plot, for 2D hists
    gStyle->SetTimeOffset(0);
    TDatime dateBegin(2010, 1, 1, 0, 0, 0);
    TDatime dateEnd(2011, 1, 1, 0, 0, 0);

    auto h1 = new TH2D("h1", "Machine A + B", 12, dateBegin.Convert(), dateEnd.Convert(), 1000, 0, 1000);
    auto h2 = new TH2D("h2", "Machine B", 12, dateBegin.Convert(), dateEnd.Convert(), 1000, 0, 1000);

    float Rand;
    for (int i = dateBegin.Convert(); i < dateEnd.Convert(); i += 86400 * 30)
    {
        for (int j = 0; j < 1000; j++)
        {
            Rand = gRandom->Gaus(500 + sin(i / 10000000.) * 100, 50);
            h1->Fill(i, Rand);
            Rand = gRandom->Gaus(500 + sin(i / 11000000.) * 100, 70);
            h2->Fill(i, Rand);
        }
    }

    MyCanvas = MyFigureTool->BookFigure("Test4.pdf", 5000, 5000);
    MyPad = MyCanvas->BookPad("MainPad", 0, 0, 5000, 5000);
    MyPad->SetAxisTitle("Date [month/year]", "Candle");
    // user own setting of axis style
    MyPad->SetAxisStyle([](TAxis *Xaxis, TAxis *Yaxis, TAxis *Zaxis)
                        { Xaxis->SetNdivisions(510); });

    MyPad->SetInputHist2D("Machine A+B", h1, "candle2", [](TH2D *h)
                          {
                              h->SetBarWidth(0.4);
                              h->SetBarOffset(-0.25);
                              h->SetLineColor(kBlue);
                              h->SetFillColor(kYellow);
                              h->SetFillStyle(1001);
                              h->GetXaxis()->SetTimeDisplay(1);
                              h->GetXaxis()->SetTimeFormat("%m/%y"); });

    // the last argument is the user's own setting of histogram style
    MyPad->SetInputHist2D("Machine B", h2, "candle3", [](TH2D *h)
                          {
                              h->SetBarWidth(0.4);
                              h->SetBarOffset(0.25);
                              h->SetLineColor(kRed);
                              h->SetFillColor(kGreen); });

    // the fifth plot, test for colz option
    auto hcol1 = new TH2D("hcol1", "Option COLor example ", 40, -4, 4, 40, -20, 20);
    float px, py;
    for (Int_t i = 0; i < 25000; i++)
    {
        gRandom->Rannor(px, py);
        hcol1->Fill(px, 5 * py);
    }

    MyCanvas = MyFigureTool->BookFigure("Test5.pdf", 5000, 5000);
    MyPad = MyCanvas->BookPad("MainPad", 0, 0, 5000, 5000);
    MyPad->SetInputHist2D("col", hcol1, "COLZ");
    MyPad->SetAxisTitle("x", "y", "Entries");

    // the sixth plot, test for TGraph
    double x[100], y1[100], y2[100];
    int n = 20;
    for (int i = 0; i < n; i++)
    {
        x[i] = i * 0.1;
        y1[i] = 10 * sin(x[i] + 0.2);
        y2[i] = 10 * sin(x[i] + 0.4);
    }
    auto g1 = new TGraph(n, x, y1);
    auto g2 = new TGraph(n, x, y2);

    MyCanvas = MyFigureTool->BookFigure("Test6.pdf", 5000, 5000);
    MyPad = MyCanvas->BookPad("MainPad", 0, 0, 5000, 5000);
    MyPad->SetInputGraph1D("sin1", g1, "AP"); // you must set "A" option for the first element, if no other elements(THStack, TH1D, TF1) exist
    MyPad->SetInputGraph1D("sin2", g2, "P");  // you should only set "A" option once, or it will overwrite the previous plots
    MyPad->SetAxisTitle("x", "y");

    // draw the figure in one time
    MyFigureTool->DrawFigures();
}
