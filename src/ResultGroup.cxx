//***************************************************************************************
/// @file ResultGroup.cxx
/// @author Wenhao Ma
//***************************************************************************************

#include "NAGASH/ResultGroup.h"

using namespace NAGASH;
using namespace std;

void ResultGroup::AddResult(std::shared_ptr<Result> result)
{
    auto titlegurad = msg->StartTitleWithGuard("ResultGroup::AddResult");
    if (result == nullptr)
    {
        msg->MSG_ERROR("Input result is NULL");
        return;
    }

    auto name = result->GetResultName();
    auto findresult = this->ResultMap.find(name);
    if (findresult != this->ResultMap.end())
        msg->MSG_WARNING("Result ", name, " already exists, this book will be ignored");
    else
        this->ResultMap.emplace(name, result);
}

void ResultGroup::Merge(std::shared_ptr<ResultGroup> subGroup)
{
    auto titlegurad = msg->StartTitleWithGuard("ResultGroup::Merge");
    if (subGroup == nullptr)
    {
        msg->MSG_ERROR("Input sub ResultGroup is NULL");
        return;
    }

    // if this ResultGroup is empty, just copy its info
    if (this->ResultMap.size() == 0)
    {
        this->ResultMap = subGroup->ResultMap;
        return;
    }

    for (auto &t : subGroup->ResultMap)
    {
        auto findresult = this->ResultMap.find(t.first);
        if (findresult != this->ResultMap.end())
        {
            findresult->second->Combine(t.second);
        }
        else
        {
            this->ResultMap.emplace(std::pair<TString, std::shared_ptr<Result>>(t.first, t.second));
        }
    }
}

void ResultGroup::Write()
{
    for (auto &t : ResultMap)
        t.second->WriteToFile();
}
