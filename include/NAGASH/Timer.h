//***************************************************************************************
/// @file Timer.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    class Timer : public Tool
    {
    public:
        /// @brief Constructor
        /// @param msg input MSGTool.
        /// @param name name of the timer.
        Timer(std::shared_ptr<MSGTool> msg, const TString &name = "Timer") : Tool(msg), TimerName(name) {}
        void PrintCurrent(const TString &prefix = "");
        void Record(const TString &Name);
        void Duration(const TString &Name1, const TString &Name2);
        void Clear();

    private:
        std::vector<std::chrono::time_point<std::chrono::system_clock>> TimeRecord;
        std::map<TString, int> TimeIndexMap;
        std::vector<TString> TimeRecordName;
        TString TimerName;
    };

} // namespace NAGASH
