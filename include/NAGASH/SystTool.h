//***************************************************************************************
/// @file HistBase.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    class SystTool : public Tool
    {
    public:
        SystTool(std::shared_ptr<MSGTool> MSG) : Tool(MSG) {}
        template <typename T>
        void BookVariation(const TString &name, const TString &systname, T *var);
        template <typename T>
        T Get(const TString &name);
        void SetVariationCorrelation(const TString &name, double correlation); // if not set, correlation is assumed to be zero
        double GetVariationCorrelation(const TString &name);
        void SetToVariation(const TString &name);
        const TString &CurrentVariation();
        const std::vector<TString> &GetListOfVariations();
        std::vector<TString> GetListOfVariations(const TString &name);
        int GetNVariations();
        int GetNVariations(const TString &name);

    private:
        struct VarWithSyst
        {
            std::map<TString, std::any> VarMap;
            std::any CurrentVar;
            std::any NominalVar;
        };

        struct AuxVar
        {
            std::any *Current;
            std::any *Variation;
        };

        bool isSetVar = false;
        bool isSystListSorted = false;
        TString currentvar;
        std::map<TString, VarWithSyst> VarMapFull;
        std::map<TString, std::vector<AuxVar>> SystMapFull;
        std::map<TString, double> CorrMap;
        std::map<TString, std::vector<TString>> SystListMap;
        std::vector<TString> SystList;
    };

    /// @brief Book a systematic variation of a parameter.
    /// @tparam T the type of the parameter.
    /// @param name the name of the parameter.
    /// @param systname the name of the systematic variation.
    /// @param var the pointer to the variation.
    template <typename T>
    void SystTool::BookVariation(const TString &name, const TString &systname, T *var)
    {
        auto findresult = VarMapFull.find(name);
        if (findresult != VarMapFull.end())
        {
            if (findresult->second.VarMap.find(systname) == findresult->second.VarMap.end())
            {
                SystListMap.find(name)->second.emplace_back(systname);
                findresult->second.VarMap.emplace(std::pair<TString, std::any>(systname, std::any(var)));
                isSetVar = false;
            }
            else
            {
                MSGUser()->StartTitle("SystTool::BookVariation");
                MSGUser()->MSG(MSGLevel::WARNING, "Variable ", name, " already has variation ", systname, ", this call will be ignored");
                MSGUser()->EndTitle();
                return;
            }
        }
        else
        {
            VarWithSyst tempVarWithSyst;
            tempVarWithSyst.VarMap.emplace(std::pair<TString, std::any>(systname, std::any(var)));
            tempVarWithSyst.CurrentVar = std::any(var);
            VarMapFull.emplace(std::pair<TString, VarWithSyst>(name, tempVarWithSyst));
            SystListMap.emplace(std::pair<TString, std::vector<TString>>(name, std::vector<TString>{systname}));
            isSetVar = false;
        }

        if (systname == "Nominal")
        {
            VarMapFull.find(name)->second.NominalVar = std::any(var);
            auto listfindresult = SystListMap.find(name);
            std::swap(listfindresult->second[0], listfindresult->second.back());
        }

        if (std::find(std::begin(SystList), std::end(SystList), systname) == std::end(SystList))
        {
            SystList.emplace_back(systname);
            CorrMap.insert(std::pair<TString, double>(systname, 0));
        }
    }

    /// @brief Get the value of the parameter with the current systematic variation.
    /// @tparam T the type of the parameter.
    /// @param name the name of the parameter.
    /// @return current variation value of the parameter.
    template <typename T>
    T SystTool::Get(const TString &name)
    {
        auto findresult = VarMapFull.find(name);
        if (findresult != VarMapFull.end())
        {
            try
            {
                auto par = *(std::any_cast<T *>(findresult->second.CurrentVar));
                return par;
            }
            catch (const std::bad_any_cast &e)
            {
                MSGUser()->StartTitle("SystTool::Get");
                MSGUser()->MSG(MSGLevel::ERROR, "Given type of variable ", name, " is wrong, return NULL value");
                MSGUser()->EndTitle();
                return T();
            }
        }
        else
        {
            MSGUser()->StartTitle("SystTool::Get");
            MSGUser()->MSG(MSGLevel::ERROR, "Variable ", name, " does not exist, return NULL value");
            MSGUser()->EndTitle();
            return T();
        }
    }

} // namespace NAGASH
