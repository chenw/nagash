// Count for next generation of NAGASH
// Compile-time multi dimensional counter

#pragma once

#include "NAGASH/Global.h"

namespace NAGASH
{
    class NAGASHFile;

    template <size_t Dimension>
    class NGCount
    {
        template <size_t D>
        friend class NGCount;

        template <typename FirstAxis, typename... MoreAxis>
        friend class NNGHist; // to access to private member functions

        friend class NAGASH::NAGASHFile;

    public:
        NGCount() : SumX(Dimension, 0), SumX2(Dimension, std::vector<double>(Dimension, 0)){};

        template <typename... FillPar>
        inline void Add(FillPar &&...par)
        {
            constexpr std::size_t npar = sizeof...(FillPar);
            static_assert(npar == Dimension || npar == Dimension + 1, "must be match");
            ++Entries;
            if constexpr (npar == Dimension)
            {
                ++SumW;
                ++SumW2;
                Add_impl(std::forward_as_tuple(par...));
            }
            if constexpr (npar == Dimension + 1)
                AddWithWeight_impl(std::forward_as_tuple(par...));
        }

        inline int GetEntries() const
        {
            return Entries;
        }

        inline double GetSum(int index) const
        {
            if (index >= 0 && index < SumX.size())
                return SumX[index];
            else
                return 0;
        }

        inline double GetSum2(int index) const
        {
            if (index >= 0 && index < SumX.size())
                return SumX2[index][index];
            else
                return 0;
        }

        inline double GetSumW() const { return SumW; }
        inline double GetSumW2() const { return SumW2; }

        inline double GetMean(int index) const
        {
            if (index >= 0 && index < SumX.size())
                return SumW != 0 ? SumX[index] / SumW : 0;
            else
                return 0;
        }

        inline double GetSumStatUnc(int index) const
        {
            if (index >= 0 && index < SumX.size())
                return GetMean(index) * sqrt(SumW2);
            else
                return 0;
        }

        inline double GetMeanStatUnc(int index) const
        {
            return SumW != 0 ? GetStdVar(index) * sqrt(SumW2) / SumW : 0;
        }

        inline double GetVar(int index) const
        {
            if (index >= 0 && index < SumX.size())
                return SumW != 0 ? (SumX2[index][index] / SumW - (SumX[index] / SumW) * (SumX[index] / SumW)) : 0;
            else
                return 0;
        }

        inline double GetStdVar(int index) const
        {
            return sqrt(fabs(GetVar(index)));
        }

        inline double GetCovariance(int i1, int i2) const
        {
            if (i1 > i2)
                std::swap(i1, i2);
            if (i1 >= 0 && i1 < SumX.size() && i2 >= 0 && i2 < SumX.size())
                return SumW != 0 ? (SumX2[i1][i2] / SumW - (SumX[i1] / SumW) * (SumX[i2] / SumW)) : 0;
            else
                return 0;
        }

        inline double GetCorrelation(int i1, int i2) const
        {
            double stdvar1 = GetStdVar(i1);
            double stdvar2 = GetStdVar(i2);
            if (stdvar1 != 0 && stdvar2 != 0)
                return GetCovariance(i1, i2) / (stdvar1 * stdvar2);
            else
                return 0;
        }

        void Reset()
        {
            SumW = 0;
            SumW2 = 0;
            for (int i = 0; i < SumX.size(); i++)
            {
                SumX[i] = 0;
                for (int j = 0; j < SumX.size(); j++)
                    SumX2[i][j] = 0;
            }
        }

        void Scale(double factor)
        {
            factor = fabs(factor);
            if (factor != 0)
            {
                SumW /= factor;
                SumW2 /= factor * factor;

                for (int i = 0; i < SumX.size(); i++)
                {
                    SumX[i] /= factor;
                    for (int j = 0; j < SumX.size(); j++)
                        SumX2[i][j] /= factor;
                }
            }
        }

        template <size_t Index>
        auto IntegrateOver() const
        {
            static_assert(Dimension > 1, "Can not be integrated");
            static_assert(Index < Dimension, "must be lower than dimension");
            NGCount<Dimension - 1> newcounter;

            newcounter.SumW = this->SumW;
            newcounter.SumW2 = this->SumW2;
            newcounter.SumX = this->SumX;
            newcounter.SumX2 = this->SumX2;
            newcounter.SumX.erase(newcounter.SumX.begin() + Index);
            newcounter.SumX2.erase(newcounter.SumX2.begin() + Index);
            for (int i = 0; i < newcounter.SumX2.size(); i++)
                newcounter.SumX2[i].erase(newcounter.SumX2[i].begin() + Index);
        }

        void Combine(const NGCount<Dimension> &inC, double factor = 1)
        {
            this->SumW += inC.SumW * factor;
            this->SumW2 += inC.SumW2 * factor * factor;
            this->Entries += inC.Entries;

            for (int i = 0; i < SumX.size(); i++)
            {
                this->SumX[i] += inC.SumX[i] * factor;
                for (int j = 0; j < SumX.size(); j++)
                    this->SumX2[i][j] += inC.SumX2[i][j] * factor;
            }
        }

    private:
        template <typename TTuple>
        inline void Add_tuple(TTuple &&tuple, double weight = 1)
        {
            constexpr std::size_t npar = std::tuple_size<std::remove_reference_t<TTuple>>::value;
            static_assert(npar == Dimension, "must be match");
            AddWithWeight_impl(std::tuple_cat(std::forward<TTuple>(tuple), std::tuple<double>(weight)));
        }

        template <typename TTuple,
                  size_t Index = 0,
                  size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value>
        inline void Add_impl(TTuple &&value, double weight = 1)
        {
            if constexpr (Index < Size)
            {
                SumX[Index] += std::get<Index>(value) * weight;
                SumX2[Index][Index] += std::get<Index>(value) * std::get<Index>(value) * weight;
                if constexpr (Index > 0)
                    AddX2_impl<TTuple, Index, Index - 1>(std::forward<TTuple>(value), weight);
            }

            if constexpr (Index < Size - 1)
                Add_impl<TTuple, Index + 1, Size>(std::forward<TTuple>(value), weight);
        }

        template <typename TTuple,
                  size_t Index = 0,
                  size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value>
        inline void AddWithWeight_impl(TTuple &&value)
        {
            SumW += std::get<Size - 1>(value);
            SumW2 += std::get<Size - 1>(value) * std::get<Size - 1>(value);
            Add_impl<TTuple, Index, Size - 1>(std::forward<TTuple>(value), std::get<Size - 1>(value));
        }

        template <typename TTuple, size_t Index1, size_t Index2>
        inline void AddX2_impl(TTuple &&value, double weight = 1)
        {
            if constexpr (Index2 >= 0)
                SumX2[Index2][Index1] += std::get<Index1>(value) * std::get<Index2>(value) * weight;
            if constexpr (Index2 > 0)
                AddX2_impl<TTuple, Index1, Index2 - 1>(std::forward<TTuple>(value));
        }

        double SumW = 0;
        double SumW2 = 0;
        int Entries = 0;
        std::vector<double> SumX;
        std::vector<std::vector<double>> SumX2;
    };
}
