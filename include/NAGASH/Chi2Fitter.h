//***************************************************************************************
/// @file Chi2Fitter.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Tool.h"
#include "NAGASH/HistTool.h"
#include "NAGASH/Global.h"

#include "Minuit2/FCNGradientBase.h"

namespace NAGASH
{
    class Chi2Fitter : public Tool
    {
    public:
        /// Fit method
        enum class FitMethod
        {
            Eigen, ///< use matrix calculation to get the fitted value, Eigen library is used.
            Minuit ///< use minuit library to fit the value.
        };
        Chi2Fitter(std::shared_ptr<MSGTool>, uint64_t, TH1D *, TH2D *_cov_ = nullptr);
        Chi2Fitter(std::shared_ptr<MSGTool>, uint64_t);
        void SetModel(const std::vector<double> &, TH1D *, TH2D *cov = nullptr);
        void SetModel(const std::vector<double> &, double);
        void SetRange(int min, int max);
        void Fit(FitMethod fm = FitMethod::Eigen);
        double GetChi2(uint64_t);
        double FitChi2();
        double Value(uint64_t);
        double Error(uint64_t);
        double OffSet();
        const std::vector<double> &Value() { return value; }
        const TMatrixD &Cov() { return cov; }

    private:
        /// @brief fcn function to calculate the \f$\chi^2\f$
        class Chi2FitterFCN : public ROOT::Minuit2::FCNGradientBase
        {
        public:
            Chi2FitterFCN(int _dim_, std::vector<std::vector<double>> *_vv_, std::vector<double> *_vc_) : vc(_vc_), vv(_vv_), dim(_dim_) {}
            // function value
            double operator()(const std::vector<double> &) const override;
            // gradient
            std::vector<double> Gradient(const std::vector<double> &) const override;
            // error defination
            double Up() const { return 1.0; }
            // whether minuit check the gradient is correct, but its validility has been checked.
            bool CheckGradient() const override { return false; }

        private:
            double QuadraticChi2(const std::vector<double> &, const std::vector<double> &) const;
            std::vector<double> GradientQuadraticChi2(const std::vector<double> &, const std::vector<double> &) const;
            std::vector<double> *vc;
            std::vector<std::vector<double>> *vv;
            size_t dim;
        };

        bool isvalid = false;
        uint64_t dimension = 1;
        int RangeMin = 1;
        int RangeMax = 1;
        TH1D *target = nullptr;
        TH2D *targetcov = nullptr;
        std::vector<double> vchi2;
        std::vector<std::vector<double>> vvalue;
        std::vector<double> vvaluemax;
        std::vector<double> vvaluemin;
        std::vector<double> value;
        TMatrixD cov;
        double chi2 = 0;
        double offset = 0;

        HistTool histtool;
    };
}
