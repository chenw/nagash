#include "NAGASH/Global.h"
#include "NAGASH/FigureX.h"

using namespace NAGASH;
using namespace std;
using namespace DevelFeature;

int main(int argc, char **argv)
{
    // prepare histograms
    TRandom3 myrandom(0);
    TH1D *Gaus0 = new TH1D("Gaus0", "Gaus0", 100, -5, 5);
    Gaus0->Sumw2();
    TH1D *Gaus1 = new TH1D("Gaus1", "Gaus1", 100, -5, 5);
    Gaus1->Sumw2();
    TH1D *Gaus2 = new TH1D("Gaus2", "Gaus2", 100, -5, 5);
    Gaus2->Sumw2();
    TH1D *Gaus3 = new TH1D("Gaus3", "Gaus3", 100, -5, 5);
    Gaus3->Sumw2();
    TH1D *Gaus4 = new TH1D("Gaus4", "Gaus4", 100, -5, 5);
    Gaus4->Sumw2();
    TH1D *Gaus5 = new TH1D("Gaus5", "Gaus5", 100, -5, 5);
    Gaus5->Sumw2();
    TH1D *Uniform01 = new TH1D("Uniform01", "Uniform01", 100, -5, 5);
    Uniform01->Sumw2();

    for (int i = 0; i < 10000; i++)
    {
        Gaus0->Fill(myrandom.Gaus(0, 1));
        Gaus1->Fill(myrandom.Gaus(2, 1));
        Gaus2->Fill(myrandom.Gaus(-2, 1));
        Gaus3->Fill(myrandom.Gaus(0, 4));
        Gaus4->Fill(myrandom.Gaus(0, 0.25));
        Gaus5->Fill(myrandom.Gaus(0, 1.01));
        Uniform01->Fill(myrandom.Uniform(0, 1));
    }

    std::shared_ptr<MSGTool> MSG = std::make_shared<MSGTool>();
    std::shared_ptr<FigureX> MyFigureTool = std::make_shared<FigureX>(MSG, "config/TestStyle.conf");

    auto MyFigure = MyFigureTool->BookFigure<Figure_MultiCompare>("Test_1.eps", "");
    MyFigure->BookHist("Uniform(0,1)", Uniform01);
    MyFigure->SetTitle("Title X", "Title Y");
    MyFigure->SetHeader("Test Header");
    MyFigure->ConfigFigure();

    MyFigure = MyFigureTool->BookFigure<Figure_MultiCompare>("Test_2.eps", "");
    MyFigure->BookHist("Gaus(0,1)", Gaus0);
    MyFigure->BookHist("Gaus(0,0.25)", Gaus4);
    MyFigure->SetTitle("Title X", "Title Y");
    MyFigure->SetHeader("Test Header");
    MyFigure->ConfigFigure();

    MyFigureTool->DrawFigures();

    return 0;
}
