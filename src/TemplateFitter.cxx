//***************************************************************************************
/// @file TemplateFitter.cxx
/// @author Wenhao Ma
//***************************************************************************************

#include "NAGASH/TemplateFitter.h"

#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnSimplex.h"
#include "Minuit2/MnHesse.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnContours.h"
#include "Minuit2/MnPlot.h"


using namespace NAGASH;

/** @class NAGASH::TemplateFitter TemplateFitter.h "NAGASH/TemplateFitter.h"
    @ingroup ToolClasses
    @brief Fit the contributions use template histograms to a target histogram.

For example, if you know the target distribution is composed of two sub distributions like:
\f[
f(x) = \alpha\cdot\frac{\exp\left(-(x-1)^2/2\right)}{\sqrt{2\pi}}+\beta\cdot\frac{2\exp\left(-2(x-2)^2\right)}{{\sqrt{2\pi}}}
\f]
Of course \f$\alpha = 1-\beta\f$, but in real world we are more interested in the ratio \f$\alpha/\beta\f$.
And now you have a histogram filled with distribution \f$(x)\f$, then you can get \f$\alpha/\beta\f$ using this tool like:

@code {.cxx}
// Demonstration of using NAGASH::TemplatFitter
#include "NAGASH.h"

using namespace NAGASH;

int main()
{
    // prepare for target histogram and sub histograms
    TRandom3 myrnd;
    TH1D *htarget = new TH1D("htarget", "htarget", 50, -3, 5);
    TH1D *hsub1 = new TH1D("hsub1", "hsub1", 50, -3, 5);
    TH1D *hsub2 = new TH1D("hsub2", "hsub2", 50, -3, 5);

    for (int i = 0; i < 1e6; i++)
    {
        hsub1->Fill(myrnd.Gaus(1, 1));
        hsub2->Fill(myrnd.Gaus(2, 0.5));

        // the contribution is 2:1
        htarget->Fill(myrnd.Gaus(1, 1));
        if (i % 2 == 0)
            htarget->Fill(myrnd.Gaus(2, 0.5));
    }

    // start fitting
    TemplateFitter fitter(std::make_shared<MSGTool>());
    fitter.SetTarget(htarget);
    fitter.SetTemplate(hsub1);
    fitter.SetTemplate(hsub2);
    fitter.Fit();
    std::cout << "The ratio is : " << fitter.GetSF(0) / fitter.GetSF(1) << std::endl;
    std::cout << "Fit chi2 is " << fitter.GetChi2() << std::endl;
}
@endcode

The output will be like:

@code
The ratio is : 2.01191
Fit chi2 is 39.7431
@endcode
 */

TemplateFitter::TemplateFitterFCN::TemplateFitterFCN(TH1D *_htarget, const std::vector<TH1D *> &vh, int _min_, int _max_)
{
    h_target = _htarget;
    h_template = vh;
    min = _min_;
    max = _max_;
}

void TemplateFitter::TemplateFitterFCN::SetRelation(std::function<double(const std::vector<double> &, const std::vector<double> &)> cf,
                                                    std::function<double(const std::vector<double> &, const std::vector<double> &, const std::vector<double> &)> ef)
{
    contentfunction = cf;
    errorfunction = ef;
}

double TemplateFitter::TemplateFitterFCN::operator()(const std::vector<double> &vpara) const
{
    double chi2 = 0;

    std::vector<double> contents(h_template.size());
    std::vector<double> errors(h_template.size());

    bool use_user_defined_function = bool(contentfunction) && bool(contentfunction);

    for (int i = min; i <= max; i++)
    {
        double BinContent = h_target->GetBinContent(i);
        double BinError = pow(h_target->GetBinError(i), 2);

        for (size_t j = 0; j < h_template.size(); j++)
        {
            contents[j] = h_template[j]->GetBinContent(i);
            errors[j] = h_template[j]->GetBinError(i);
        }

        if (use_user_defined_function)
        {
            BinContent -= contentfunction(vpara, contents);
            BinError += errorfunction(vpara, contents, errors);
        }
        else
        {
            for (size_t j = 0; j < h_template.size(); j++)
            {
                BinContent -= vpara[j] * h_template[j]->GetBinContent(i);
                BinError += pow(vpara[j] * h_template[j]->GetBinError(i), 2);
            }
        }

        if (BinError != 0) // avoid nan passed to chi2
            chi2 += BinContent * BinContent / BinError;
    }

    return chi2;
}

/// @brief Clear this tool.
void TemplateFitter::Clear()
{
    ht = nullptr;
    vh.clear();
    sf.clear();
    sferr.clear();
    isfixed.clear();
    chi2 = 0;
}

/// @brief Set the target histogram.
void TemplateFitter::SetTarget(TH1D *h)
{
    if (!h)
    {
        MSGUser()->StartTitle("TemplateFitter::SetTarget");
        MSGUser()->MSG(MSGLevel::ERROR, "Target hist is NULL");
        MSGUser()->EndTitle();
        return;
    }

    ht = h;
    FitRangeMin = 1;
    FitRangeMax = ht->GetNbinsX();
}

/// @brief Set the template histogram.
/// @param h template histogram.
/// @param SF initial value of the scale factor.
/// @param fix whether to fix the scale factor during the fit.
void TemplateFitter::SetTemplate(TH1D *h, double SF, bool fix)
{
    if (!h)
    {
        MSGUser()->StartTitle("TemplateFitter::SetTemplate");
        MSGUser()->MSG(MSGLevel::ERROR, "Template hist is NULL");
        MSGUser()->EndTitle();
        return;
    }

    vh.push_back(h);
    sf.push_back(SF);
    sfmin.push_back(-999);
    sfmax.push_back(-1000);
    sferr.push_back(0);
    isfixed.push_back(fix);
}

/// @brief Set the template histogram.
/// @param h template histogram.
/// @param SF initial value of the scale factor.
/// @param SFMIN minimum value of the scale factor during the fit.
/// @param SFMAX maximum value of the scale factor during the fit.
void TemplateFitter::SetTemplate(TH1D *h, double SF, double SFMIN, double SFMAX)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("TemplateFitter::SetTemplate");
    if (!h)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Template hist is NULL");
        return;
    }

    vh.push_back(h);
    sf.push_back(SF);
    if (SFMIN < SFMAX && SF < SFMAX && SF > SFMIN)
    {
        sfmin.push_back(SFMIN);
        sfmax.push_back(SFMAX);
    }
    else
    {
        MSGUser()->MSG_WARNING("Input scale factor range is wrong, brought back to default");
        sfmin.push_back(-999);
        sfmax.push_back(-1000);
    }
    sferr.push_back(0);
    isfixed.push_back(false);
}

/// @brief Set the relation between the target histogram and the template histograms.
/// if not called, the default relation is: \f$ t=\sum s_i \cdot h_i\f$, here \f$ t\f$ is the target histogram, \f$ s_i\f$ is the scale factor and \f$ h_i\f$ is the corresponding template histogram.
/// @param cf the function to calculate the content, first argument is the vector of the scale factors, second argument is the vector of the contents of template histograms.
/// @param ef the function to calculate the error, first argument is the vector of the scale factors, second argument is the vector of the contents of template histograms, third argument is the vector of the errors of template histograms.
void TemplateFitter::SetRelation(std::function<double(const std::vector<double> &, const std::vector<double> &)> cf,
                                 std::function<double(const std::vector<double> &, const std::vector<double> &, const std::vector<double> &)> ef)
{
    this->contentfunction = cf;
    this->errorfunction = ef;
}

/// @brief Set the fit range.
/// @param min minimum bin index.
/// @param max maximum bin index.
void TemplateFitter::SetFitRange(int min, int max)
{
    if (min <= max && min > 0)
    {
        FitRangeMin = min;
        FitRangeMax = max;
    }
    else
    {
        MSGUser()->StartTitle("TemplateFitter::SetFitRange");
        MSGUser()->MSG(MSGLevel::ERROR, "Wrong fit range, brought back to default");
        MSGUser()->EndTitle();
    }
}

bool TemplateFitter::Check()
{
    auto titleguard = MSGUser()->StartTitleWithGuard("TemplateFitter::Check");
    if (!ht)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Target hist not set");
        return false;
    }

    if (sf.size() < 1)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "No template hist");
        return false;
    }

    for (size_t i = 0; i < vh.size(); i++)
    {
        if (vh[i]->GetNbinsX() < FitRangeMax)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Wrong fit range or wrong input template hist");
            return false;
        }
    }

    return true;
}

/// @brief Run the fit.
void TemplateFitter::Fit()
{
    auto titleguard = MSGUser()->StartTitleWithGuard("TemplateFitter::Fit");
    if (!Check())
    {
        MSGUser()->MSG(MSGLevel::WARNING, "Fit won't proceed");
        return;
    }

    TemplateFitterFCN fcn(ht, vh, FitRangeMin, FitRangeMax);
    if (contentfunction && errorfunction)
        fcn.SetRelation(contentfunction, errorfunction);

    ROOT::Minuit2::MnUserParameters upar;
    for (size_t i = 0; i < sf.size(); i++)
    {
        TString name;
        name.Form("sf_%lu", i);
        if (sfmin[i] > sfmax[i])
        {
            upar.Add(name.Data(), sf[i], fabs(sf[i] / 5));
            // upar.SetLowerLimit(i, 0);
        }
        else
        {
            upar.Add(name.Data(), sf[i], (sfmax[i] - sfmin[i]) / 5, sfmin[i], sfmax[i]);
        }
        if (isfixed[i])
            upar.Fix(i);
    }

    // use simplex first
    ROOT::Minuit2::MnSimplex simplex(fcn, upar, 2);
    auto minsimplex = simplex(1e6);
    // then use migrad
    ROOT::Minuit2::MnMigrad migrad(fcn, minsimplex.UserState(), ROOT::Minuit2::MnStrategy(2));
    auto minmigrad = migrad(1e6);

    for (size_t i = 0; i < sf.size(); i++)
    {
        sf[i] = minmigrad.UserState().Value(i);
        sferr[i] = minmigrad.UserState().Error(i);
    }
    chi2 = minmigrad.Fval();
}

/// @brief Get the scale factors for ith template.
/// @param i template index
double TemplateFitter::GetSF(uint64_t i)
{
    if (i < sf.size() && i >= 0)
    {
        return sf[i];
    }
    else
    {
        MSGUser()->StartTitle("TemplateFitter::GetSF");
        MSGUser()->MSG(MSGLevel::ERROR, "No SF correspond to input number, return -1");
        MSGUser()->EndTitle();
        return -1;
    }
}

/// @brief Get the uncertainty of the scale factors for ith template.
/// @param i template index
double TemplateFitter::GetSFError(uint64_t i)
{
    if (i < sf.size() && i >= 0)
    {
        return sferr[i];
    }
    else
    {
        MSGUser()->StartTitle("TemplateFitter::GetSFError");
        MSGUser()->MSG(MSGLevel::ERROR, "No SF correspond to input number, return -1");
        MSGUser()->EndTitle();
        return -1;
    }
}

/// @brief Get the fit \f$\chi^2\f$
double TemplateFitter::GetChi2()
{
    return chi2;
}