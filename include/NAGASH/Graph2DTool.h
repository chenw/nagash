#pragma once

#include "Tool.h"

namespace NAGASH
{
    class Triangle;
    class Circle;

    class Point
    {
    private:
        double x;
        double y;

    public:
        Point(double _x, double _y)
        {
            x = _x;
            y = _y;
        }

        Point()
        {
            x = 0;
            y = 0;
        }

        double X() { return x; }
        double Y() { return y; }
        double Distance(Point a) { return sqrt(pow(a.X() - x, 2) + pow(a.Y() - y, 2)); }
        bool IsInside(Circle c);
        bool IsInside(Triangle t);
    };

    class Edge
    {
    private:
        Point s;
        Point e;

    public:
        Edge(Point start, Point end)
        {
            s = start;
            e = end;
        }

        Point Start() { return s; }
        Point End() { return e; }
        double Length() { return s.Distance(e); }
    };

    class Circle
    {
    private:
        Point c;
        double r;

    public:
        Circle(Point center, double rad)
        {
            c = center;
            r = rad;
        }
        Point Center() { return c; }
        double Radius() { return r; }
    };

    class Triangle
    {
    private:
        Point P[3];

    public:
        Triangle(Point a, Point b, Point c)
        {
            P[0] = a;
            P[1] = b;
            P[2] = c;
        }

        Triangle(Edge line, Point p)
        {
            P[0] = p;
            P[1] = line.Start();
            P[2] = line.End();
        }

        Point Vertex(int index) { return P[index]; }
        Circle CircumscribedCircle();
        Point Circumcenter() { return CircumscribedCircle().Center(); }
    };

    class Graph2DTool : public Tool
    {
    public:
        Graph2DTool(std::shared_ptr<MSGTool> msg, double prec = 1e-5);
        std::vector<Triangle> DelaunayTriangulation(std::vector<Point> p_vec);

    private:
        double PRECISION;
    };
}
