//***************************************************************************************
/// @file HistBase.h
/// @author Wenhao Ma
//***************************************************************************************

/** @defgroup Histograms Histogram Classes
    @brief Classes for storing and manipulating histograms.
 */

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/HistTool.h"
#include "NAGASH/NAGASHFile.h"
#include "NAGASH/Result.h"
#include "NAGASH/TFileHelper.h"

namespace NAGASH
{
    class PlotGroup;

    /** @ingroup Histograms ResultClasses
        @class NAGASH::HistBase HistBase.h "NAGASH/HistBase.h"
        @brief Virtual base class for histograms.
    */
    class HistBase : public Result
    {
        friend class PlotGroup;

    public:
        /// @brief  Policies of dealing with systematic uncertainties.
        enum class SystPolicy
        {
            Average,  ///< take the average difference \f$\bar{O_{\text{var}}-O_{\text{nominal}}}\f$ in the same group.
            Maximum,  ///< take the maximum difference \f$\max(O_{\text{var}}-O_{\text{nominal}})\f$ in the same group.
            Variance, ///< the variance of the all systematic variations in the same group.
            CTEQ,     ///< up - down, \f$(O_\text{up}-O_\text{down})/2\f$
            Backup,   ///< not add to total systematic uncertainties, just for future use.
        };

    protected:
        HistBase(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename) : Result(MSG, c, name, filename) {}
        virtual std::shared_ptr<HistBase> CloneVirtual(const TString &name) { return nullptr; }

    public:
        virtual bool BookSystematicVariation(const TString &name, double corr_factor = 0, SystPolicy policy = SystPolicy::Maximum) { return false; }
        virtual bool BookSystematicVariation(const TString &name1, const TString &name2, double corr_factor = 0, SystPolicy policy = SystPolicy::Maximum) { return false; }
        virtual bool BookSystematicVariation(const std::vector<TString> &names, double corr_factor = 0, SystPolicy policy = SystPolicy::Maximum) { return false; }
        virtual void ClearLinkPlot() {}
        virtual TH1 *GetNominalHistVirtual() { return nullptr; }
        virtual size_t GetNVariations() { return 0; }
        virtual double GetVariationCorrelationFactor(const TString &) { return 0; }
        virtual TString GetVariationName(uint64_t) { return ""; }
        virtual std::vector<std::tuple<std::vector<TString>, double, SystPolicy>> GetVariationTypes() { return std::vector<std::tuple<std::vector<TString>, double, SystPolicy>>(); }
        virtual TH1 *GetVariationVirtual(uint64_t) { return nullptr; }
        virtual TH1 *GetVariationVirtual(const TString &) { return nullptr; }
        virtual bool IsBackupVariation(uint64_t) { return false; };
        virtual bool IsBackupVariation(const TString &) { return false; };
        virtual bool Process() { return false; }
        virtual bool Recover() { return false; }
        virtual bool Recover(TFile *file) { return false; }
        virtual bool Recover(const TString &filename) { return false; }
        virtual bool Recover(std::shared_ptr<TFileHelper>) { return false; }
        virtual bool RegroupSystematicVariation(const std::vector<TString> &names, SystPolicy policy) { return false; }
        virtual bool RemoveSystematicVariation(const TString &name) { return false; }
        virtual bool RenameSystematicVariation(const TString &name_old, const TString &name_new) { return false; }
        virtual void Reset() {}
        virtual void Scale(double) {}
        virtual void SetLinkPlot(uint64_t index, std::shared_ptr<HistBase> subplot) {}
        virtual void SetLinkType(const TString &) {}
        virtual void SetLinkType(std::function<double(const std::vector<double> &)>, std::function<double(const std::vector<double> &, const std::vector<double> &)>) {}
        virtual void SetLinkType(std::function<void(const std::vector<TH1 *> &, TH1 *)>) {}
        virtual bool SetSystematicVariation(uint64_t index) { return false; }
        virtual bool SetSystematicVariation(const TString &name = "Nominal") { return false; }
        virtual void SetUnprocessed() {}
        virtual void Write() {}
        virtual ~HistBase() = default;
    };

}
