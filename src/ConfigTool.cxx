//***************************************************************************************
/// @file ConfigTool.cxx
/// @author Wenhao Ma
//***************************************************************************************

#include "NAGASH/ConfigTool.h"

using namespace NAGASH;
using namespace std;

// new version
// use std::regex to match token
// also enable using # as a start of comment

/** @class NAGASH::ConfigTool ConfigTool.h "NAGASH/ConfigTool.h"
    @ingroup AnalysisClasses
    @brief provide interface to config objects in %NAGASH.

This class is intended to book and get parameters as well as read parameters from outside config file.
Also, this class maintains a MSGTool so that after reading the input config file, correct MSGTool can be constructed (output level, log file name, etc).

This tool can read config info from given config file. The config file should be organized like following:

~~~
# This is a comment
# any parameter should be booked in one line as :
# type name value1 value2 value3 ....
# there are totally 10 types supported :
# int long bool double string intvec longvec boolvec doublevec stringvec
# the following parameters are reserved parameters which can be read by NAGASH framework internally
include PathToOtherConfig # include other config file if you want
int Nthread 5 # number of threads for Analysis::ThreadPool
long EvtMax 1000 # number of entry you want to read in
bool CreateLog 0 # create log file or print to screen
bool OpenLog 0 # open log file, if it does not exist, create a new one
string LogFileName MyAnaOut.log # log file name
string MSGLevel INFO # message level
intvec FileSelection 1 # the file index in the file list that you want to loop over, start at 1
string FileSelectionPattern pattern # the file with name matched the pattern will be used to loop over
~~~

After construction, parameters can be get by GetPar() method.
 */

/// @brief Constructor.
/// @param inputfilename name of the config file.
ConfigTool::ConfigTool(const char *inputfilename)
{
    msg = std::make_shared<MSGTool>();

    msg->StartTitle("ConfigTool::ReadConfigFromFile");
    ReadConfigFromFile(inputfilename);
    msg->EndTitle();

    // Config MSGTool
    bool doexistmsg = false;
    bool doexistfocus = false;
    bool doexistlog = false;
    msg->OutputLevel = MSGLevel::SILENT;
    auto msglevel = GetPar<TString>("MSGLevel", &doexistmsg);
    auto FocusName = GetPar<TString>("FocusRegionName", &doexistfocus);
    auto LogName = GetPar<TString>("LogFileName", &doexistlog);
    bool docreatelog = GetPar<bool>("CreateLog");
    bool doopenlog = GetPar<bool>("OpenLog");
    msg->OutputLevel = MSGLevel::INFO;
    MSGLevel level = MSGLevel::INFO;
    if (doexistmsg)
    {
        if (msglevel == "SILENT")
            level = MSGLevel::SILENT;
        if (msglevel == "FATAL")
            level = MSGLevel::FATAL;
        if (msglevel == "ERROR")
            level = MSGLevel::ERROR;
        if (msglevel == "WARNING")
            level = MSGLevel::WARNING;
        if (msglevel == "INFO")
            level = MSGLevel::INFO;
        if (msglevel == "DEBUG")
            level = MSGLevel::DEBUG;
        if (msglevel == "FOCUS")
            level = MSGLevel::FOCUS;

        if (doexistfocus)
            msg.reset(new MSGTool(level, FocusName));
        else
            msg.reset(new MSGTool(level));
    }

    if (doexistlog)
    {
        if (docreatelog)
            msg->CreateLog(LogName);
        if (doopenlog)
            msg->OpenLog(LogName);
    }
    else if (docreatelog || doopenlog)
    {
        MSGUser()->StartTitle("ConfigTool::SetMSG");
        MSGUser()->MSG(MSGLevel::WARNING, "LogFileName not set, will print message to console");
        MSGUser()->EndTitle();
    }
}

/// @brief Read config info from given file
/// @param filename input file name
void ConfigTool::ReadConfigFromFile(const TString &filename)
{
    std::ifstream infile;
    infile.open(filename, std::ios::in);
    if (!infile.is_open())
    {
        msg->MSG_ERROR("config file ", filename, " does not exist");
        return;
    }

    auto abspath = std::filesystem::absolute(std::filesystem::path(filename.Data()));
    if (std::ranges::find(includedFiles, abspath) != includedFiles.end())
    {
        msg->MSG_WARNING("input file ", filename, " has already been read");
        return;
    }
    else
        includedFiles.emplace_back(abspath);

    std::string line;
    while (std::getline(infile, line))
    {
        // remove comment
        auto commentstart = line.find("#");
        if (commentstart != std::string::npos)
        {
            line.erase(commentstart, line.size());
        }

        // use regex to match token
        // std::regex word_regex("([\\+-/A-Za-z0-9_\\.]+)");
        std::regex word_regex("([^\\s]+)");
        auto words_begin = std::sregex_iterator(line.begin(), line.end(), word_regex);
        auto words_end = std::sregex_iterator();
        std::vector<std::string> matched_words;
        for (auto i = words_begin; i != words_end; ++i)
        {
            matched_words.push_back((*i).str());
        }

        if (matched_words.size() >= 2)
        {
            if (matched_words[0] == "include")
            {
                for (uint32_t i = 1; i < matched_words.size(); i++)
                    ReadConfigFromFile(matched_words[i]);

                continue;
            }
        }

        if (matched_words.size() >= 3)
        {
            std::string type = matched_words[0];
            std::string name = matched_words[1];

            if (type == "int")
            {
                std::istringstream record(matched_words[2]);
                int temp_int;
                record >> temp_int;
                BookParFile<int>(name, temp_int);
            }
            else if (type == "intvec")
            {
                std::vector<int> temp_intvec;
                for (uint32_t i = 2; i < matched_words.size(); i++)
                {
                    std::istringstream record(matched_words[i]);
                    int temp_int;
                    record >> temp_int;
                    temp_intvec.push_back(temp_int);
                }
                BookParFile<std::vector<int>>(name, temp_intvec);
            }
            else if (type == "double")
            {
                std::istringstream record(matched_words[2]);
                double temp_double;
                record >> temp_double;
                BookParFile<double>(name, temp_double);
            }
            else if (type == "doublevec")
            {
                std::vector<double> temp_doublevec;
                for (uint64_t i = 2; i < matched_words.size(); i++)
                {
                    std::istringstream record(matched_words[i]);
                    double temp_double;
                    record >> temp_double;
                    temp_doublevec.push_back(temp_double);
                }
                BookParFile<std::vector<double>>(name, temp_doublevec);
            }
            else if (type == "long")
            {
                std::istringstream record(matched_words[2]);
                long temp_long;
                record >> temp_long;
                BookParFile<long>(name, temp_long);
            }
            else if (type == "longvec")
            {
                std::vector<long> temp_longvec;
                for (uint64_t i = 2; i < matched_words.size(); i++)
                {
                    std::istringstream record(matched_words[i]);
                    long temp_long;
                    record >> temp_long;
                    temp_longvec.push_back(temp_long);
                }
                BookParFile<std::vector<long>>(name, temp_longvec);
            }
            else if (type == "bool")
            {
                std::istringstream record(matched_words[2]);
                bool temp_bool;
                record >> temp_bool;
                BookParFile<bool>(name, temp_bool);
            }
            else if (type == "boolvec")
            {
                std::vector<bool> temp_boolvec;
                for (uint64_t i = 2; i < matched_words.size(); i++)
                {
                    std::istringstream record(matched_words[i]);
                    bool temp_bool;
                    record >> temp_bool;
                    temp_boolvec.push_back(temp_bool);
                }
                BookParFile<std::vector<bool>>(name, temp_boolvec);
            }
            else if (type == "string")
            {
                std::istringstream record(matched_words[2]);
                TString temp_string;
                record >> temp_string;
                BookParFile<TString>(name, temp_string);
            }
            else if (type == "stringvec")
            {
                std::vector<TString> temp_stringvec;
                for (uint64_t i = 2; i < matched_words.size(); i++)
                {
                    std::istringstream record(matched_words[i]);
                    TString temp_string;
                    record >> temp_string;
                    temp_stringvec.push_back(temp_string);
                }
                BookParFile<std::vector<TString>>(name, temp_stringvec);
            }
            else
            {
                MSGUser()->MSG_WARNING("parameter ", name, " type ", type, " is not allowed");
            }
        }
    }

    infile.close();
}