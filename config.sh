#!/bin/bash
echo "# Generating setupNAGASH.sh"
cat > setupNAGASH.sh << EOF
# NAGASH
export NAGASH_INSTALL_DIR=`pwd`
export PATH=`pwd`/bin:\$PATH
export LD_LIBRARY_PATH=`pwd`/lib:\$LD_LIBRARY_PATH
export LIBRARY_PATH=`pwd`/lib:\$LIBRARY_PATH
export C_INCLUDE_PATH=`pwd`/include:\$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=`pwd`/include:\$CPLUS_INCLUDE_PATH
EOF
echo "# setupNAGASH.sh generated"
echo "# please run:"
echo "# source setupNAGASH.sh"
