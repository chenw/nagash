///***************************************************************************************
/// @file NGFigure.cxx
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#include "NAGASH/NGFigure.h"

using namespace std;
using namespace NAGASH;

/** @class NAGASH::NGFigure NGFigure.h NAGASH/NGFigure.h
    @ingroup ToolClasses
    @brief Next Generation Figure Package, now replaced by NAGASH::FigureTool.

    Finished in 2019.Jun.4th Version 1.0
    Modified to adjust NAGASH in 2021.3.4
 */

/// @brief Constructor
/// @param msg input MSGTool.
/// @param FigureName name of the figure you want to save.
/// @param TitleX x-axis title.
/// @param TitleY y-axis title.
NGFigure::NGFigure(std::shared_ptr<MSGTool> msg, const char *FigureName, const char *TitleX, const char *TitleY)
    : Tool(msg), SaveFileName(FigureName), Y_Title_Main_Pad(TitleY), X_Title_Main_Pad(TitleX), X_Title_Sub_Pad(TitleX), unc(msg), histtool(msg)
{
}

/// @brief The mode of the figure.
/// @param main_mode_tag mode of the main pad, possible values are: `SINGLE` (single histogram), `MULTI` (multiple histograms), `HSTACK` (hstack histogram) and `HIST2D`(2D histogram).
/// @param sub_mode_tag (optional) mode of the sub pad, possible values are: `RATIO`, `PULL`, `DELTA` and `PERCENTAGE`(hstack only).
/// @param SubTitleY sub pad y-axis title.
void NGFigure::SetMode(const char *main_mode_tag, const char *sub_mode_tag, const char *SubTitleY)
{
    Mode_Tag_Main = (TString)main_mode_tag;
    Mode_Tag_Sub = (TString)sub_mode_tag;

    Y_Title_Sub_Pad = (TString)SubTitleY;
    if (Mode_Tag_Main == "HIST2D")
        isFor_Hist2D = true;

    isSet_Mode = true;
}

/// @brief Draw range of the xaxis
void NGFigure::SetXRange(double min, double max)
{
    if (min >= max)
    {
        MSGUser()->StartTitle("NGFigure::SetXRange");
        MSGUser()->MSG(MSGLevel::ERROR, "Min value is larger than Max value!");
        MSGUser()->EndTitle();
        return;
    }
    isSet_X_Range = true;
    Max_X_Range = max;
    Min_X_Range = min;
}

/// @brief Set input histogram.
/// @param filename ROOT file name that stores the histogram.
/// @param histname name of the histogram.
/// @param legendname name of the legend of this histogram.
/// @param rebinnum rebin number of the histogram if you want to rebin it.
/// @param scale scale factor of the histogram if you want to scale it.
void NGFigure::SetInputHist(const char *filename, const char *histname, const char *legendname, int rebinnum, double scale)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::SetInputHist");
    TFile *file = nullptr;
    file = new TFile(filename);
    if (file == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "File ", filename, " Cannot be opened!");
        return;
    }

    TH1D *hist = nullptr;
    hist = (TH1D *)file->Get(histname);
    if (hist == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Hist ", histname, " Cannot be found!");
        file->Close();
        return;
    }

    hist->SetDirectory(0);
    file->Close();
    Main_Hist.push_back(hist);
    if (scale > 0)
        hist->Scale(scale);
    else
        MSGUser()->MSG_WARNING("scale factor is negative, hist will not be scaled!");
    LegendName.push_back((TString)legendname);
    RebinNum.push_back(rebinnum);
}

/// @brief Set input histogram.
/// @param infile pointer to the TFile.
/// @param histname name of the histogram.
/// @param legendname name of the legend of this histogram.
/// @param rebinnum rebin number of the histogram if you want to rebin it.
/// @param scale scale factor of the histogram if you want to scale it.
void NGFigure::SetInputHist(TFile *infile, const char *histname, const char *legendname, int rebinnum, double scale)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::SetInputHist");
    TFile *file = nullptr;
    file = infile;
    if (file == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Input File Does Not Exist!");
        return;
    }

    TH1D *hist = nullptr;
    hist = (TH1D *)file->Get(histname);
    if (hist == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Hist ", histname, " Cannot be found!");
        return;
    }

    hist->SetDirectory(0);
    Main_Hist.push_back(hist);

    if (scale > 0)
        hist->Scale(scale);
    else
        MSGUser()->MSG_WARNING("scale factor is negative, hist will not be scaled!");

    LegendName.push_back((TString)legendname);
    RebinNum.push_back(rebinnum);
}

/// @brief Set input histogram.
/// @param hist pointer to the histogram.
/// @param legendname name of the legend of this histogram.
/// @param rebinnum rebin number of the histogram if you want to rebin it.
/// @param scale scale factor of the histogram if you want to scale it.
void NGFigure::SetInputHist(TH1D *hist, const char *legendname, int rebinnum, double scale)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::SetInputHist");
    if (hist == nullptr)
    {
        MSGUser()->MSG_ERROR("Input Hist Cannot be found!");
        return;
    }

    auto histcopy = (TH1D *)hist->Clone((TString)hist->GetName() + "_copy");
    histcopy->SetDirectory(0);

    Main_Hist.push_back(histcopy);

    if (scale > 0)
        histcopy->Scale(scale);
    else
        MSGUser()->MSG_WARNING("scale factor is negative, hist will not be scaled!");

    LegendName.push_back((TString)legendname);
    RebinNum.push_back(rebinnum);
}

/// @brief Set input 2D histogram.
/// @param filename ROOT file name that stores the 2D histogram.
/// @param histname name of the 2D histogram.
/// @param legendname name of the legend of this 2D histogram.
/// @param rebinnumx rebin number of the xaxis.
/// @param rebinnumy rebin number of the yaxis.
void NGFigure::SetInputHist2D(const char *filename, const char *histname, const char *legendname, int rebinnumx, int rebinnumy)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::SetInputHist2D");
    TFile *file = nullptr;
    file = new TFile(filename);
    if (file == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "File ", filename, " Cannot be opened!");
        return;
    }

    TH2D *hist = nullptr;
    hist = (TH2D *)file->Get(histname);
    if (hist == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Hist ", histname, " Cannot be found!");
        file->Close();
        return;
    }
    hist->SetDirectory(0);
    file->Close();
    Hist2D = hist;
    LegendName.push_back((TString)legendname);
    RebinNumX = rebinnumx;
    RebinNumY = rebinnumy;
}

/// @brief Set input 2D histogram.
/// @param infile pointer to the TFile.
/// @param histname name of the 2D histogram.
/// @param legendname name of the legend of this 2D histogram.
/// @param rebinnumx rebin number of the xaxis.
/// @param rebinnumy rebin number of the yaxis.
void NGFigure::SetInputHist2D(TFile *infile, const char *histname, const char *legendname, int rebinnumx, int rebinnumy)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("NGFigure::SetInputHist2D");

    TFile *file = nullptr;
    file = infile;
    if (file == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Input File Does Not Exist!");
        return;
    }

    TH2D *hist = nullptr;
    hist = (TH2D *)file->Get(histname);
    if (hist == nullptr)
    {
        MSGUser()->MSG(MSGLevel::ERROR, "Hist ", histname, " Cannot be found!");
        return;
    }
    hist->SetDirectory(0);
    Hist2D = hist;
    LegendName.push_back((TString)legendname);
    RebinNumX = rebinnumx;
    RebinNumY = rebinnumy;
}

/// @brief Set input 2D histogram.
/// @param hist pointer to the 2D histogram.
/// @param legendname name of the legend of this 2D histogram.
/// @param rebinnumx rebin number of the xaxis.
/// @param rebinnumy rebin number of the yaxis.
void NGFigure::SetInputHist2D(TH2D *hist, const char *legendname, int rebinnumx, int rebinnumy)
{
    if (hist == nullptr)
    {
        MSGUser()->StartTitle("NGFigure::SetInputHist2D");
        MSGUser()->MSG(MSGLevel::ERROR, "Input Hist Cannot be found!");
        MSGUser()->EndTitle();
        return;
    }

    Hist2D = (TH2D *)hist->Clone((TString)hist->GetName() + "_copy");
    Hist2D->SetDirectory(0);
    LegendName.push_back((TString)legendname);
    RebinNumX = rebinnumx;
    RebinNumY = rebinnumy;
}

/// @brief Set input graph.
/// @param graph pointer to the TGraph.
/// @param legendname name of the legend of this graph.
/// @param drawoption draw option of this graph, same as options in TGraph::Draw().
void NGFigure::SetInputGraph(TGraph *graph, const char *legendname, const char *drawoption)
{
    if (graph == nullptr)
    {
        MSGUser()->StartTitle("NGFigure::SetInputGraph");
        MSGUser()->MSG(MSGLevel::ERROR, "Input Graph Cannot be found!");
        MSGUser()->EndTitle();
        return;
    }
    Main_Graph.push_back((TGraph *)graph->Clone((TString)graph->GetName() + "_copy"));
    LegendName_Graph.push_back((TString)legendname);
    DrawOption_Graph.push_back((TString)drawoption);

    if (Main_Graph.size() == 1)
    {
        Graph_Marker_Style.push_back(20);
        Graph_Marker_Size.push_back(8);
        Graph_Marker_Color.push_back(2);

        Graph_Line_Style.push_back(1);
        Graph_Line_Width.push_back(2);
        Graph_Line_Color.push_back(2);
    }
    else if (Main_Graph.size() == 2)
    {
        Graph_Marker_Style.push_back(21);
        Graph_Marker_Size.push_back(8);
        Graph_Marker_Color.push_back(3);

        Graph_Line_Style.push_back(1);
        Graph_Line_Width.push_back(2);
        Graph_Line_Color.push_back(3);
    }
    else if (Main_Graph.size() == 3)
    {
        Graph_Marker_Style.push_back(22);
        Graph_Marker_Size.push_back(8);
        Graph_Marker_Color.push_back(4);

        Graph_Line_Style.push_back(1);
        Graph_Line_Width.push_back(2);
        Graph_Line_Color.push_back(4);
    }
    else if (Main_Graph.size() == 4)
    {
        Graph_Marker_Style.push_back(23);
        Graph_Marker_Size.push_back(8);
        Graph_Marker_Color.push_back(5);

        Graph_Line_Style.push_back(1);
        Graph_Line_Width.push_back(2);
        Graph_Line_Color.push_back(5);
    }
    else if (Main_Graph.size() == 5)
    {
        Graph_Marker_Style.push_back(29);
        Graph_Marker_Size.push_back(8);
        Graph_Marker_Color.push_back(6);

        Graph_Line_Style.push_back(1);
        Graph_Line_Width.push_back(2);
        Graph_Line_Color.push_back(6);
    }
    else
    {
        Graph_Marker_Style.push_back(33);
        Graph_Marker_Size.push_back(8);
        Graph_Marker_Color.push_back(7);

        Graph_Line_Style.push_back(1);
        Graph_Line_Width.push_back(2);
        Graph_Line_Color.push_back(7);
    }
}

void NGFigure::SetGraphMarker(int marker_style, int marker_size, int marker_color)
{
    if (Graph_Marker_Style.size() > 0)
    {
        Graph_Marker_Style.back() = marker_style;
        Graph_Marker_Size.back() = marker_size;
        Graph_Marker_Color.back() = marker_color;
    }
    else
    {
        MSGUser()->StartTitle("NGFigure::SetGraphMarker");
        MSGUser()->MSG(MSGLevel::ERROR, "Graph Not Set!");
        MSGUser()->EndTitle();
    }
}

void NGFigure::SetGraphLine(int line_style, int line_width, int line_color)
{
    if (Graph_Line_Style.size() > 0)
    {
        Graph_Line_Style.back() = line_style;
        Graph_Line_Width.back() = line_width;
        Graph_Line_Color.back() = line_color;
    }
    else
    {
        MSGUser()->StartTitle("NGFigure::SetGraphMarker");
        MSGUser()->MSG(MSGLevel::ERROR, "Graph Not Set!");
        MSGUser()->EndTitle();
    }
}

bool NGFigure::RebinHist()
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::RebinHist");
    if (isFor_Hist2D)
    {
        if (Hist2D == nullptr)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "No Input Hist!");
            return false;
        }
        binnumx = Hist2D->GetXaxis()->GetNbins();
        binnumy = Hist2D->GetYaxis()->GetNbins();
        if (RebinNumX > 0)
        {
            if ((binnumx % (RebinNumX)) != 0)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Input Hist2D X axis can not rebin ", RebinNumX, "!");
                return false;
            }
            Hist2D->RebinX(RebinNumX);
        }
        if (RebinNumY > 0)
        {
            if ((binnumy % (RebinNumY)) != 0)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Input Hist2D Y axis can not rebin ", RebinNumY, "!");
                return false;
            }

            Hist2D->RebinY(RebinNumY);
        }
    }
    else
    {
        if (Main_Hist.size() == 0)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "No Input Hist!");
            return false;
        }

        Num_Main_Hist = Main_Hist.size();
        MSGUser()->MSG(MSGLevel::DEBUG, "Total Input Hist Number: ", Num_Main_Hist);

        for (size_t i = 0; i < Num_Main_Hist; i++)
        {
            int binnum = Main_Hist[i]->GetXaxis()->GetNbins();
            if (RebinNum[i] > 0)
            {
                if ((binnum % (RebinNum[i])) != 0)
                {
                    MSGUser()->MSG(MSGLevel::ERROR, "Input Hist ", i, " Can not rebin ", RebinNum[i], " !");
                    return false;
                }
                Main_Hist[i]->Rebin(RebinNum[i]);
            }

            if (isSet_X_Range)
                Main_Hist[i]->GetXaxis()->SetRangeUser(Min_X_Range, Max_X_Range);
        }
    }

    return true;
}

bool NGFigure::Check()
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::Check");
    Num_X_Bin = Main_Hist[0]->GetXaxis()->GetNbins();
    MSGUser()->MSG(MSGLevel::DEBUG, "X Axis Bin Number : ", Num_X_Bin);
    X_Binning = new double[Num_X_Bin + 1];
    for (int i = 0; i < Num_X_Bin; i++)
        X_Binning[i] = Main_Hist[0]->GetBinLowEdge(i + 1);
    MSGUser()->MSG(MSGLevel::DEBUG, "Binning Checked!");
    X_Binning[Num_X_Bin] = Main_Hist[0]->GetBinLowEdge(Num_X_Bin) + Main_Hist[0]->GetBinWidth(Num_X_Bin);

    for (size_t i = 0; i < Num_Main_Hist; i++)
    {
        if (Main_Hist[i]->GetXaxis()->GetNbins() != Num_X_Bin)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Input Hist ", i, " Bin Number Incorrect!");
            return false;
        }

        for (int j = 0; j < Main_Hist[i]->GetXaxis()->GetNbins(); j++)
        {
            if (fabs(Main_Hist[i]->GetBinLowEdge(j + 1) - X_Binning[j]) > 1e-5)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Input Hist ", i, " Bin ", j, " low edge incorrect");
                return false;
            }
        }

        if (fabs(Main_Hist[i]->GetBinLowEdge(Num_X_Bin) + Main_Hist[i]->GetBinWidth(Num_X_Bin) - X_Binning[Num_X_Bin]) > 1e-5)
        {
            MSGUser()->MSG(MSGLevel::ERROR, "Input Hist ", i, " Last Bin High Edge Incorrect");
            return false;
        }
    }
    MSGUser()->MSG(MSGLevel::DEBUG, "Check finished!");
    return true;
}

bool NGFigure::Normalize()
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("NGFigure::Normalize");
    if (!isSet_Norm_Range)
    {
        if (isSet_X_Range)
        {
            Max_Norm_Range = Max_X_Range;
            Min_Norm_Range = Min_X_Range;
        }
        else
        {
            Max_Norm_Range = X_Binning[Num_X_Bin];
            Min_Norm_Range = X_Binning[0];
        }
    }

    if (Mode_Tag_Main == "HSTACK")
    {
        Total_Hist = (TH1D *)Main_Hist[0]->Clone("Total_Hist");
        Total_Hist->Reset();

        Total_Hist_With_Error = (TH1D *)Main_Hist[0]->Clone("Total_Hist_With_Error");
        Total_Hist_With_Error->Reset();

        if (Mode_Tag_Sub == "PERCENTAGE" || Mode_Tag_Sub == "UNKNOWN")
        {
            if (Num_Main_Hist < 2)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Only 1 Hist Defined for HSTACK please use SINGLE");
                return false;
            }
        }
        else
        {
            if (Num_Main_Hist < 3)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Only 2 Hist Defined for HSTACK please use MULTI");
                return false;
            }
        }

        for (size_t i = 1; i < Num_Main_Hist; i++)
        {
            Total_Hist_With_Error->Add(Main_Hist[i]);
            for (int j = 0; j < Main_Hist[i]->GetXaxis()->GetNbins(); j++)
                Main_Hist[i]->SetBinError(j + 1, 0);

            Total_Hist->Add(Main_Hist[i]);
        }

        if (Mode_Tag_Sub == "PERCENTAGE" || Mode_Tag_Sub == "UNKNOWN")
        {
            Total_Hist_With_Error->Add(Main_Hist[0]);
            for (int j = 0; j < Main_Hist[0]->GetXaxis()->GetNbins(); j++)
                Main_Hist[0]->SetBinError(j + 1, 0);

            Total_Hist->Add(Main_Hist[0]);
        }
    }

    if (Num_Main_Hist < 2)
        return true;

    if (isNeed_Norm)
    {
        double inte = 1;
        if (Mode_Tag_Main == "HSTACK" && Mode_Tag_Sub != "UNKNOWN" && Mode_Tag_Sub != "PERCENTAGE")
        {
            for (size_t i = 0; i < Num_Main_Hist; i++)
            {
                int StartBin = Main_Hist[i]->FindBin(Min_Norm_Range);
                int EndBin = Main_Hist[i]->FindBin(Max_Norm_Range);

                if (Max_Norm_Range == Main_Hist[i]->GetBinLowEdge(EndBin))
                    EndBin = EndBin - 1;

                if (i == 0)
                    inte = Main_Hist[i]->Integral(StartBin, EndBin);

                if (!isExtern_Norm && i != 0)
                    Norm_Factor = inte / (Total_Hist->Integral(StartBin, EndBin));

                if (i != 0)
                    Main_Hist[i]->Scale(Norm_Factor);
            }

            Total_Hist->Scale(Norm_Factor);
            Total_Hist_With_Error->Scale(Norm_Factor);
        }
        if (Mode_Tag_Main == "MULTI")
        {
            if (isFixed_Norm && isExtern_Norm && isUnity_Norm)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Unclear Normalization Setting!");
                return false;
            }

            for (size_t i = 0; i < Num_Main_Hist; i++)
            {
                int StartBin = Main_Hist[i]->FindBin(Min_Norm_Range);
                int EndBin = Main_Hist[i]->FindBin(Max_Norm_Range);

                if (Max_Norm_Range == Main_Hist[i]->GetBinLowEdge(EndBin))
                    EndBin = EndBin - 1;

                if (i == 0)
                    inte = Main_Hist[i]->Integral(StartBin, EndBin);

                if (isFixed_Norm && (!isExtern_Norm) && i == 1)
                    Norm_Factor = inte / (Main_Hist[i]->Integral(StartBin, EndBin));

                if (isFixed_Norm && i != 0)
                    Main_Hist[i]->Scale(Norm_Factor);

                /*
                if (!isFixed_Norm && isExtern_Norm && isNeed_ExternNorm[i])
                    Main_Hist[i]->Scale(ExternNorm_Factor[i]);
                    */

                if (isUnity_Norm)
                    Main_Hist[i]->Scale(1.0 / (Main_Hist[i]->Integral(StartBin, EndBin)));

                if (!isFixed_Norm && !isExtern_Norm && !isUnity_Norm && i != 0)
                    Main_Hist[i]->Scale(inte / (Main_Hist[i]->Integral(StartBin, EndBin)));
            }
        }
    }

    return true;
}

bool NGFigure::ConfigParameters()
{
    auto msgguard = MSGUser()->StartTitleWithGuard("NGFigure::ConfigParameters");
    if (Mode_Tag_Main == "SINGLE")
    {
        Main_Pad_isNeeded = true;
        Sub_Pad_isNeeded = false;
    }
    else if (Mode_Tag_Main == "MULTI")
    {
        Main_Pad_isNeeded = true;
        if (Mode_Tag_Sub == "UNKNOWN")
            Sub_Pad_isNeeded = false;
        else
            Sub_Pad_isNeeded = true;
    }
    else if (Mode_Tag_Main == "HIST2D")
    {
        Main_Pad_isNeeded = true;
        Sub_Pad_isNeeded = false;
    }
    else if (Mode_Tag_Main == "HSTACK")
    {
        Main_Pad_isNeeded = true;
        if (Mode_Tag_Sub == "UNKNOWN")
            Sub_Pad_isNeeded = false;
        else
            Sub_Pad_isNeeded = true;
    }
    if (isFor_Hist2D)
    {
        if (DoMatrix)
        {
            TH2D *matrix = new TH2D("_matrix_", "_matrix_", binnumx, 0, binnumx, binnumy, 0, binnumy);
            matrix->Sumw2();
            Matrix_Save = Hist2D;
            Hist2D = matrix;

            for (int i = 0; i < binnumx; i++)
            {
                for (int j = 0; j < binnumy; j++)
                {
                    Hist2D->SetBinContent(i + 1, j + 1, Matrix_Save->GetBinContent(i + 1, j + 1));
                    Hist2D->SetBinError(i + 1, j + 1, Matrix_Save->GetBinError(i + 1, j + 1));
                }
            }
        }

        Hist2D->SetTitle("");
        Hist2D->SetStats(0);

        Text2D = (TH2D *)Hist2D->Clone("_Text2D_");
        for (int i = 0; i < binnumx; i++)
        {
            for (int j = 0; j < binnumy; j++)
            {
                Text2D->SetBinContent(i + 1, j + 1, (int)((Text2D->GetBinContent(i + 1, j + 1)) * pow(10, asmb)) / pow(10, asmb));
                Text2D->SetBinError(i + 1, j + 1, (int)((Text2D->GetBinError(i + 1, j + 1)) * pow(10, asmb)) / pow(10, asmb));
            }
        }
        Text2D->SetTitle("");
        Text2D->SetStats(0);

        if (!isSet_X_Range)
        {
            Max_X_Range = Hist2D->GetXaxis()->GetBinLowEdge(binnumx) + 0.9 * Hist2D->GetXaxis()->GetBinWidth(binnumx);
            Min_X_Range = Hist2D->GetXaxis()->GetBinLowEdge(1) + 0.1 * Hist2D->GetXaxis()->GetBinWidth(1);
        }

        if (!isSet_Y_Range_Main)
        {
            Max_Y_Range_Main = Hist2D->GetYaxis()->GetBinLowEdge(binnumy) + 0.9 * Hist2D->GetYaxis()->GetBinWidth(binnumy);
            Min_Y_Range_Main = Hist2D->GetYaxis()->GetBinLowEdge(1) + 0.1 * Hist2D->GetYaxis()->GetBinWidth(1);
        }
    }
    else
    {
        if (DoSeparate)
        {
            if (Mode_Tag_Main == "MULTI")
            {
                if (Num_Main_Hist < 2)
                {
                    MSGUser()->MSG(MSGLevel::ERROR, "Only One Hist is Set! Please use SINGLE");
                    return false;
                }
                for (int i = 0; i < Num_X_Bin; i++)
                {
                    double sum_err2 = 0;
                    for (int j = 0; j < 2; j++)
                    {
                        sum_err2 = sum_err2 + Main_Hist[j]->GetBinError(i + 1) * Main_Hist[j]->GetBinError(i + 1);
                    }
                    for (size_t j = 0; j < Num_Main_Hist; j++)
                    {
                        Main_Hist[j]->SetBinError(i + 1, 0.0001 * Main_Hist[j]->GetBinContent(i + 1));
                    }
                    Main_Hist[0]->SetBinError(i + 1, sqrt(sum_err2));
                }
            }
        }
        // Calculate Sub Hist
        if (Mode_Tag_Main == "MULTI" && Mode_Tag_Sub != "UNKNOWN")
        {
            if (Num_Main_Hist < 2)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Only One Hist is Set! Please use SINGLE");
                return false;
            }

            for (size_t i = 0; i < Num_Main_Hist; i++)
            {
                sprintf(name, "SubHist_%lu", i);
                TH1D *sub_hist = new TH1D(name, name, Num_X_Bin, X_Binning);
                sub_hist->Sumw2();

                if (Mode_Tag_Sub == "RATIO")
                    histtool.Ratio(Main_Hist[0], Main_Hist[i], sub_hist, DoSeparate);
                else if (Mode_Tag_Sub == "PULL")
                    histtool.Pull(Main_Hist[0], Main_Hist[i], sub_hist);
                else if (Mode_Tag_Sub == "DELTA")
                    histtool.Delta(Main_Hist[0], Main_Hist[i], sub_hist, DoSeparate);
                else
                {
                    MSGUser()->MSG(MSGLevel::ERROR, "Unknown Sub Tag");
                    return false;
                }

                Sub_Hist.push_back(sub_hist);
            }

            if (ShowChi2)
            {
                for (size_t i = 1; i < Num_Main_Hist; i++)
                {
                    double chi2 = 0;
                    int ndof = 0;

                    for (int j = 0; j < Num_X_Bin; j++)
                    {
                        if (isSet_X_Range)
                        {
                            int StartBin = Main_Hist[0]->FindBin(Min_X_Range);
                            int EndBin = Main_Hist[0]->FindBin(Max_X_Range);

                            if (Max_X_Range == Main_Hist[0]->GetBinLowEdge(EndBin))
                                EndBin = EndBin - 1;

                            if (j + 1 < StartBin || j + 1 > EndBin)
                                continue;
                        }

                        double val = Main_Hist[0]->GetBinContent(j + 1) - Main_Hist[i]->GetBinContent(j + 1);
                        double val_err = sqrt(Main_Hist[0]->GetBinError(j + 1) * Main_Hist[0]->GetBinError(j + 1) +
                                              Main_Hist[i]->GetBinError(j + 1) * Main_Hist[i]->GetBinError(j + 1));
                        if (val_err == 0)
                            continue;

                        ndof = ndof + 1;
                        chi2 = chi2 + val * val / val_err / val_err;
                    }
                    MSGUser()->MSG(MSGLevel::INFO, "Chi Square Value: ", chi2);
                    sprintf(name, "#chi^{2}/ndf=%.1f/%d", chi2, ndof);
                    for (int j = 0; j < 3; j++)
                        LegendName[i] = LegendName[i] + " ";
                    int overlimit = 0;
                    if (((TString)name).Length() > 22)
                        overlimit = ((TString)name).Length() - 22;
                    while (LegendName[i].Length() < 16 - overlimit)
                        LegendName[i] = LegendName[i] + " ";
                    LegendName[i] = LegendName[i] + (TString)name;
                }
            }
        }

        if (Mode_Tag_Main == "HSTACK" && Mode_Tag_Sub != "UNKNOWN")
        {
            if (Num_Main_Hist < 3)
            {
                MSGUser()->MSG(MSGLevel::ERROR, "Only 2 Hist is Set! Please use MULTI");
                return false;
            }

            if (Mode_Tag_Sub == "PERCENTAGE")
            {
                for (size_t i = 0; i < Num_Main_Hist; i++)
                {
                    sprintf(name, "SubHist_%lu", i);
                    TH1D *sub_hist = new TH1D(name, name, Num_X_Bin, X_Binning);
                    sub_hist->Sumw2();

                    histtool.Ratio(Main_Hist[i], Total_Hist, sub_hist);
                    Sub_Hist.push_back(sub_hist);
                }
            }
            else
            {
                sprintf(name, "SubHist_%d", 0);
                TH1D *sub_hist = new TH1D(name, name, Num_X_Bin, X_Binning);
                sub_hist->Sumw2();

                if (Mode_Tag_Sub == "RATIO")
                    histtool.Ratio(Main_Hist[0], Main_Hist[0], sub_hist, DoSeparate);
                else if (Mode_Tag_Sub == "PULL")
                    histtool.Pull(Main_Hist[0], Main_Hist[0], sub_hist);
                else if (Mode_Tag_Sub == "DELTA")
                    histtool.Delta(Main_Hist[0], Main_Hist[0], sub_hist, DoSeparate);
                else
                {
                    MSGUser()->MSG_ERROR("Unknown Sub Tag");
                    return false;
                }
                Sub_Hist.push_back(sub_hist);

                sprintf(name, "SubHist_%d", 1);
                sub_hist = new TH1D(name, name, Num_X_Bin, X_Binning);
                sub_hist->Sumw2();

                if (Mode_Tag_Sub == "RATIO")
                    histtool.Ratio(Main_Hist[0], Total_Hist_With_Error, sub_hist, DoSeparate);
                else if (Mode_Tag_Sub == "PULL")
                    histtool.Pull(Main_Hist[0], Total_Hist_With_Error, sub_hist);
                else if (Mode_Tag_Sub == "DELTA")
                    histtool.Delta(Main_Hist[0], Total_Hist_With_Error, sub_hist, DoSeparate);
                else
                {
                    MSGUser()->MSG_ERROR("Unknown Sub Tag");
                    return false;
                }

                Sub_Hist.push_back(sub_hist);

                if (DoSeparate)
                {
                    for (int i = 0; i < Num_X_Bin; i++)
                    {
                        double temp_err1 = Sub_Hist[1]->GetBinError(i + 1);
                        double temp_err0 = Sub_Hist[0]->GetBinError(i + 1);

                        Sub_Hist[1]->SetBinError(i + 1, temp_err0);
                        Sub_Hist[0]->SetBinError(i + 1, temp_err1);
                    }
                }

                if (ShowChi2)
                {
                    double chi2 = 0;
                    int ndof = 0;

                    for (int i = 0; i < Num_X_Bin; i++)
                    {
                        if (isSet_X_Range)
                        {
                            int StartBin = Main_Hist[0]->FindBin(Min_X_Range);
                            int EndBin = Main_Hist[0]->FindBin(Max_X_Range);

                            if (Max_X_Range == Main_Hist[0]->GetBinLowEdge(EndBin))
                                EndBin = EndBin - 1;

                            if (i + 1 < StartBin || i + 1 > EndBin)
                                continue;
                        }

                        double val = Main_Hist[0]->GetBinContent(i + 1) - Total_Hist_With_Error->GetBinContent(i + 1);
                        double val_err = sqrt(Main_Hist[0]->GetBinError(i + 1) * Main_Hist[0]->GetBinError(i + 1) +
                                              Total_Hist_With_Error->GetBinError(i + 1) * Total_Hist_With_Error->GetBinError(i + 1));
                        if (val_err == 0)
                            continue;

                        ndof = ndof + 1;
                        chi2 = chi2 + val * val / val_err / val_err;
                    }
                    MSGUser()->MSG_INFO("Chi Square Value: ", chi2);
                    sprintf(name, "#chi^{2}/ndf=%.1f/%d", chi2, ndof);
                    for (int i = 0; i < 2; i++)
                        LegendName[0] = LegendName[0] + " ";
                    int overlimit = 0;
                    if (((TString)name).Length() > 20)
                        overlimit = ((TString)name).Length() - 20;
                    while (LegendName[0].Length() < 14 - overlimit)
                        LegendName[0] = LegendName[0] + " ";
                    LegendName[0] = LegendName[0] + (TString)name;
                }
            }
        }

        if (Mode_Tag_Main == "HSTACK")
        {
            Main_Stack = new THStack("Main_Stack", "");
            if (Mode_Tag_Sub == "PERCENTAGE")
                Sub_Stack = new THStack("Sub_Stack", "");

            for (int i = Num_Main_Hist - 1; i >= 1; i--)
            {
                Main_Stack->Add(Main_Hist[i]);
                if (Mode_Tag_Sub == "PERCENTAGE")
                    Sub_Stack->Add(Sub_Hist[i]);
            }
            if (Mode_Tag_Sub == "PERCENTAGE")
                Sub_Stack->Add(Sub_Hist[0]);

            if (Mode_Tag_Sub == "PERCENTAGE" || Mode_Tag_Sub == "UNKNOWN")
            {
                Main_Stack->Add(Main_Hist[0]);
                Empty_Hist = new TH1D("Empty_Hist", "Empty_Hist", Num_X_Bin, X_Binning);
                Empty_Hist->Sumw2();
                Empty_Hist->SetTitle("");
                Empty_Hist->SetStats(0);
                if (Mode_Tag_Sub == "PERCENTAGE")
                {
                    Empty_Hist_Sub = new TH1D("Empty_Hist_Sub", "Empty_Hist_Sub", Num_X_Bin, X_Binning);
                    Empty_Hist_Sub->Sumw2();
                    Empty_Hist_Sub->SetTitle("");
                    Empty_Hist_Sub->SetStats(0);
                }
            }
        }

        double max_main = 0;
        double min_main = 0;
        double max_sub = 0;
        double min_sub = 0;

        for (size_t i = 0; i < Num_Main_Hist; i++)
        {
            for (int j = 0; j < Num_X_Bin; j++)
            {
                double val_up = Main_Hist[i]->GetBinContent(j + 1) + Main_Hist[i]->GetBinError(j + 1);
                double val_down = Main_Hist[i]->GetBinContent(j + 1) - Main_Hist[i]->GetBinError(j + 1);

                if (isSet_X_Range)
                {
                    int StartBin = Main_Hist[i]->FindBin(Min_X_Range);
                    int EndBin = Main_Hist[i]->FindBin(Max_X_Range);

                    if (Max_X_Range == Main_Hist[i]->GetBinLowEdge(EndBin))
                        EndBin = EndBin - 1;

                    if (j + 1 < StartBin || j + 1 > EndBin)
                        continue;
                }

                if (i == 0 && j == 0 && !isSet_X_Range)
                {
                    max_main = val_up;
                    min_main = val_down;
                }

                if (i == 0 && j + 1 == Main_Hist[i]->FindBin(Min_X_Range) && isSet_X_Range)
                {
                    max_main = val_up;
                    min_main = val_down;
                }

                if (val_up > max_main)
                    max_main = val_up;
                if (val_down < min_main)
                    min_main = val_down;
            }
        }

        if (Mode_Tag_Main == "HSTACK")
        {
            for (int j = 0; j < Num_X_Bin; j++)
            {
                double val_up = Total_Hist->GetBinContent(j + 1) + Total_Hist->GetBinError(j + 1);
                double val_down = Total_Hist->GetBinContent(j + 1) - Total_Hist->GetBinError(j + 1);

                if (isSet_X_Range)
                {
                    int StartBin = Total_Hist->FindBin(Min_X_Range);
                    int EndBin = Total_Hist->FindBin(Max_X_Range);

                    if (Max_X_Range == Total_Hist->GetBinLowEdge(EndBin))
                        EndBin = EndBin - 1;

                    if (j + 1 < StartBin || j + 1 > EndBin)
                        continue;
                }

                if (val_up > max_main)
                    max_main = val_up;
                if (val_down < min_main)
                    min_main = val_down;
            }
        }

        if (Sub_Hist.size() > 0)
        {
            for (size_t i = 0; i < Sub_Hist.size(); i++)
            {
                for (int j = 0; j < Num_X_Bin; j++)
                {
                    double val_up = Sub_Hist[i]->GetBinContent(j + 1) + Sub_Hist[i]->GetBinError(j + 1);
                    double val_down = Sub_Hist[i]->GetBinContent(j + 1) - Sub_Hist[i]->GetBinError(j + 1);

                    if (isSet_X_Range)
                    {
                        int StartBin = Sub_Hist[i]->FindBin(Min_X_Range);
                        int EndBin = Sub_Hist[i]->FindBin(Max_X_Range);

                        if (Max_X_Range == Sub_Hist[i]->GetBinLowEdge(EndBin))
                            EndBin = EndBin - 1;

                        if (j + 1 < StartBin || j + 1 > EndBin)
                            continue;
                    }

                    if (i == 0 && j == 0 && !isSet_X_Range)
                    {
                        max_sub = val_up;
                        min_sub = val_down;
                    }

                    if (i == 0 && j + 1 == Sub_Hist[i]->FindBin(Min_X_Range) && isSet_X_Range)
                    {
                        max_sub = val_up;
                        min_sub = val_down;
                    }

                    if (val_up > max_sub)
                        max_sub = val_up;
                    if (val_down < min_sub)
                        min_sub = val_down;
                }
            }
        }

        double length_main;
        if ((Num_Main_Hist + 3) * LeftPercentage + 0.04 < 0.5)
            length_main = (max_main - min_main) / (1 - (Num_Main_Hist + 3) * LeftPercentage - 0.04);
        else
            length_main = (max_main - min_main) / 0.5;

        double length_sub;
        if (2 * LeftPercentage + 0.08 < 0.5)
            length_sub = (max_sub - min_sub) / (1 - 2 * LeftPercentage - 0.08);
        else
            length_sub = (max_sub - min_sub) / 0.5;

        if (!isSet_X_Range)
        {
            Max_X_Range = X_Binning[Num_X_Bin];
            Min_X_Range = X_Binning[0];
        }
        else
        {
            // X Range Check
            if (Max_X_Range < Min_X_Range)
            {
                MSGUser()->MSG_ERROR("Max is Smaller Than Min!");
                return false;
            }
            if (Max_X_Range < X_Binning[0])
            {
                MSGUser()->MSG_ERROR("Hist is not defined in the ordered range!");
                return false;
            }
            if (Min_X_Range > X_Binning[Num_X_Bin])
            {
                MSGUser()->MSG_ERROR("Hist is not defined in the ordered range!");
                return false;
            }
            if (Min_X_Range < X_Binning[0])
            {
                MSGUser()->MSG_ERROR("Min value too small!");
                Min_X_Range = X_Binning[0];
            }
            if (Max_X_Range > X_Binning[Num_X_Bin])
            {
                MSGUser()->MSG_ERROR("Max value too large!");
                Max_X_Range = X_Binning[Num_X_Bin];
            }
        }

        if (!isSet_Y_Range_Main)
        {
            Max_Y_Range_Main = max_main + (0.04 + (Num_Main_Hist + 2) * LeftPercentage) * length_main;
            Min_Y_Range_Main = min_main - LeftPercentage * length_main;
        }

        if (!isSet_Y_Range_Sub && Sub_Pad_isNeeded)
        {
            Max_Y_Range_Sub = max_sub + (LeftPercentage + 0.04) * length_sub;
            Min_Y_Range_Sub = min_sub - (LeftPercentage + 0.04) * length_sub;
        }

        // For Some Mode Fix Range
        if (Mode_Tag_Main == "HSTACK")
        {
            Min_Y_Range_Main = 2e-5;
            if (Mode_Tag_Sub == "PERCENTAGE")
            {
                Max_Y_Range_Sub = 1.07;
                Min_Y_Range_Sub = 0;
            }
        }
    }

    // Load Parameters for Style
    RightMargin_Main = 0.05;
    LeftMargin_Main = 0.15;
    TopMargin_Main = 0.07;

    RightMargin_Sub = 0.05;
    LeftMargin_Sub = 0.15;
    TopMargin_Sub = 0;
    BottomMargin_Sub = 0.35;

    if (Sub_Pad_isNeeded)
        BottomMargin_Main = 0;
    else
        BottomMargin_Main = 0.19;

    if (isFor_Hist2D)
    {
        RightMargin_Main = 0.16;
        LeftMargin_Main = 0.15;
        TopMargin_Main = 0.13;
        BottomMargin_Main = 0.10;

        if (isNeed_SetBinNameX || isNeed_SetBinNameY)
        {
            LeftMargin_Main = 0.19;
            BottomMargin_Main = 0.19;
        }

        if (DoTextOnly)
            RightMargin_Main = 0.05;
    }

    X_Label_Size_Main = 175;
    X_Label_Font_Main = 73;
    X_Title_Size_Main = 175;
    X_Title_Font_Main = 73;
    X_Title_Offset_Main = 1.1;

    Y_Label_Size_Main = 175;
    Y_Label_Font_Main = 73;
    Y_Title_Size_Main = 175;
    Y_Title_Font_Main = 73;
    if (Sub_Pad_isNeeded)
        Y_Title_Offset_Main = 2.2;
    else
        Y_Title_Offset_Main = 1.1;

    if (isFor_Hist2D)
    {
        Y_Title_Offset_Main = 2.2;
    }

    Z_Label_Size_Main = 175;
    Z_Label_Font_Main = 73;
    Z_Label_Offset_Main = 0.01;

    X_Label_Size_Sub = 175;
    X_Label_Font_Sub = 73;
    X_Title_Size_Sub = 175;
    X_Title_Font_Sub = 73;
    X_Title_Offset_Sub = 4;

    Y_Label_Size_Sub = 175;
    Y_Label_Font_Sub = 73;
    Y_Title_Size_Sub = 175;
    Y_Title_Font_Sub = 73;
    Y_Title_Offset_Sub = 2;

    Hist_Line_Width = 2;
    Hist_Marker_Size = 0;
    Sub_Fill_Style = 0;
    Hist_Fill_Style = 0;
    Sub_Fill_Style_First = 0;
    Hist_Fill_Style_First = 0;
    if (Mode_Tag_Main == "HSTACK")
    {
        Hist_Fill_Style = 1001; // Solid Filling
        if (Mode_Tag_Sub == "UNKNOWN")
            Hist_Fill_Style_First = 1001;

        if (Mode_Tag_Sub == "PERCENTAGE")
        {
            Hist_Fill_Style_First = 1001;
            Sub_Fill_Style = 1001;
            Sub_Fill_Style_First = 1001;
        }
    }
    if (DoSeparate)
    {
        Hist_Fill_Style_First = 3544;
        Sub_Fill_Style_First = 3544;
        Hist_Line_Width = 2;

        if (Mode_Tag_Main == "HSTACK")
        {
            Hist_Fill_Style_First = 0;
            Hist_Line_Width = 2;
        }
    }

    Legend_Text_Size = 0.05;
    Legend_Text_Font = 72;

    if (Style_Index == 1)
    {
        X_Label_Size_Main = 0.05;
        X_Label_Font_Main = 42;
        X_Title_Size_Main = 0.07;
        X_Title_Font_Main = 42;
        X_Title_Offset_Main = 1.1;

        Y_Label_Size_Main = 0.05;
        Y_Label_Font_Main = 42;
        Y_Title_Size_Main = 0.07;
        Y_Title_Font_Main = 42;
        if (Sub_Pad_isNeeded)
            Y_Title_Offset_Main = 1.1;
        else
            Y_Title_Offset_Main = 1.1;

        Z_Label_Size_Main = 0.03;
        Z_Label_Font_Main = 42;
        Z_Label_Offset_Main = 0.01;

        X_Label_Size_Sub = 0.13;
        X_Label_Font_Sub = 42;
        X_Title_Size_Sub = 0.15;
        X_Title_Font_Sub = 42;
        X_Title_Offset_Sub = 1.;

        Y_Label_Size_Sub = 0.13;
        Y_Label_Font_Sub = 42;
        Y_Title_Size_Sub = 0.15;
        Y_Title_Font_Sub = 42;
        Y_Title_Offset_Sub = 0.43;

        Legend_Text_Size = 0.05;
        Legend_Text_Font = 42;
    }

    return true;
}

bool NGFigure::DefinePad()
{
    // Define the Canvas
    if (Sub_Pad_isNeeded)
    {
        figure = new TCanvas("canvas", "canvas", 5000, 5000);
        Main_Pad = new TPad("Main_Pad", "Main_Pad", 0, 0.3, 1, 1.0);
        figure->cd();
        Main_Pad->Draw();
        Sub_Pad = new TPad("Sub_Pad", "Sub_Pad", 0, 0, 1, 0.3);
        figure->cd();
        Sub_Pad->Draw();
    }
    else
    {
        if (isFor_Hist2D)
            figure = new TCanvas("canvas", "canvas", 5000, 5000);
        else
            figure = new TCanvas("canvas", "canvas", 5000, 3500); // 3500
        Main_Pad = new TPad("Main_Pad", "Main_Pad", 0, 0, 1, 1.0);

        figure->cd();
        Main_Pad->Draw();
    }

    return true;
}

bool NGFigure::DrawHist()
{
    figure->cd();
    Main_Pad->cd();
    // Go to Main Pad

    if (Num_Main_Hist % Legend_Column == 0)
        Main_Legend = new TLegend(LeftMargin_Main + 0.04, 1 - TopMargin_Main - 0.04 - (1 + (int)(Num_Main_Hist / Legend_Column)) * LeftPercentage,
                                  1 - RightMargin_Main - 0.04, 1 - TopMargin_Main - 0.04);
    else
        Main_Legend = new TLegend(LeftMargin_Main + 0.04, 1 - TopMargin_Main - 0.04 - (2 + (int)(Num_Main_Hist / Legend_Column)) * LeftPercentage,
                                  1 - RightMargin_Main - 0.04, 1 - TopMargin_Main - 0.04);

    Main_Legend->SetFillColor(0);
    Main_Legend->SetFillStyle(0);
    Main_Legend->SetLineColor(0);
    Main_Legend->SetLineWidth(0);
    Main_Legend->SetTextSize(Legend_Text_Size);
    Main_Legend->SetTextFont(Legend_Text_Font);
    if (Legend_Column != 1)
        Main_Legend->SetNColumns(Legend_Column);

    if (Mode_Tag_Main == "HSTACK")
    {
        if (Mode_Tag_Sub == "UNKNOWN" || Mode_Tag_Sub == "PERCENTAGE")
        {
            Empty_Hist->Draw("E");
            Main_Pad_Y_Axis = Empty_Hist->GetYaxis();
            Main_Pad_X_Axis = Empty_Hist->GetXaxis();
        }
        else
        {
            Main_Hist[0]->Draw("E");
            Main_Pad_Y_Axis = Main_Hist[0]->GetYaxis();
            Main_Pad_X_Axis = Main_Hist[0]->GetXaxis();
        }

        for (size_t i = 0; i < Num_Main_Hist; i++)
            Main_Legend->AddEntry(Main_Hist[i], LegendName[i], "lpfe");

        if (DoSeparate && Mode_Tag_Sub != "UNKNOWN" && Mode_Tag_Sub != "PERCENTAGE")
            Main_Legend->AddEntry(Sub_Hist[0], "Syst. Unc.", "lpfe");
    }
    else
    {
        for (size_t i = 0; i < Num_Main_Hist; i++)
        {
            if (i == 0)
            {
                if (DoSeparate)
                    Main_Hist[i]->Draw("E2");
                else
                    Main_Hist[i]->Draw("E0");
            }
            else
            {
                Main_Pad->cd();
                Main_Hist[i]->Draw("E0 same");
                Main_Pad->Update();
            }
            if (Num_Main_Hist != 1)
                Main_Legend->AddEntry(Main_Hist[i], LegendName[i], "lpfe");
        }
        Main_Pad_Y_Axis = Main_Hist[0]->GetYaxis();
        Main_Pad_X_Axis = Main_Hist[0]->GetXaxis();
    }

    if (Main_Graph.size() > 0)
    {
        for (size_t i = 0; i < Main_Graph.size(); i++)
        {
            Main_Graph[i]->Draw(DrawOption_Graph[i]);
            Main_Legend->AddEntry(Main_Graph[i], LegendName_Graph[i], "lpfe");
        }
    }

    Main_Legend->SetHeader(HeaderName);
    if (DoLegend)
        Main_Legend->Draw("same");

    figure->cd();
    // Return to Canvas

    if (Sub_Pad_isNeeded)
    {
        Sub_Pad->cd();
        // Go to Sub Pad

        if (Mode_Tag_Main == "HSTACK" && Mode_Tag_Sub == "PERCENTAGE")
        {
            Empty_Hist_Sub->Draw("E");
            Sub_Pad_Y_Axis = Empty_Hist_Sub->GetYaxis();
            Sub_Pad_X_Axis = Empty_Hist_Sub->GetXaxis();
        }
        else
        {
            for (size_t i = 0; i < Sub_Hist.size(); i++)
            {
                if (i == 0)
                {
                    if (DoSeparate)
                        Sub_Hist[i]->Draw("E2");
                    else
                        Sub_Hist[i]->Draw("E0");
                }
                else
                    Sub_Hist[i]->Draw("E same");
            }
            Sub_Pad_Y_Axis = Sub_Hist[0]->GetYaxis();
            Sub_Pad_X_Axis = Sub_Hist[0]->GetXaxis();
        }

        figure->cd();
    }

    return true;
}

bool NGFigure::SaveHist()
{
    figure->SaveAs(SaveFileName);
    return true;
}

void NGFigure::SetMargin()
{
    Main_Pad->SetTopMargin(TopMargin_Main);
    Main_Pad->SetBottomMargin(BottomMargin_Main);
    Main_Pad->SetLeftMargin(LeftMargin_Main);
    Main_Pad->SetRightMargin(RightMargin_Main);
    Main_Pad->SetTicks();
    Main_Pad->SetGridx(NeedGridX_Main);
    Main_Pad->SetGridy(NeedGridY_Main);
    if (DoMainLogY)
    {
        Main_Pad->SetLogy();
    }

    if (Sub_Pad_isNeeded)
    {
        Sub_Pad->SetTopMargin(TopMargin_Sub);
        Sub_Pad->SetBottomMargin(BottomMargin_Sub);
        Sub_Pad->SetLeftMargin(LeftMargin_Sub);
        Sub_Pad->SetRightMargin(RightMargin_Sub);
        Sub_Pad->SetTicks();
        Sub_Pad->SetGridx(NeedGridX_Sub);
        Sub_Pad->SetGridy(NeedGridY_Sub);
    }
}

void NGFigure::SetMainLogY(bool dosetlogY)
{
    DoMainLogY = dosetlogY;
}

void NGFigure::SetAxis()
{
    Main_Pad_Y_Axis->SetLabelFont(Y_Label_Font_Main);
    Main_Pad_Y_Axis->SetLabelSize(Y_Label_Size_Main);
    Main_Pad_Y_Axis->SetTitleFont(Y_Title_Font_Main);
    Main_Pad_Y_Axis->SetTitleSize(Y_Title_Size_Main);
    Main_Pad_Y_Axis->SetTitleOffset(Y_Title_Offset_Main);
    Main_Pad_Y_Axis->SetMaxDigits(4);
    Main_Pad_Y_Axis->SetTitle(Y_Title_Main_Pad);
    Main_Pad_Y_Axis->SetRangeUser(Min_Y_Range_Main, Max_Y_Range_Main);

    Main_Pad_X_Axis->SetLabelFont(X_Label_Font_Main);
    Main_Pad_X_Axis->SetLabelSize(X_Label_Size_Main);
    Main_Pad_X_Axis->SetTitleFont(X_Title_Font_Main);
    Main_Pad_X_Axis->SetTitleSize(X_Title_Size_Main);
    Main_Pad_X_Axis->SetTitleOffset(X_Title_Offset_Main);
    Main_Pad_X_Axis->SetMaxDigits(4);
    Main_Pad_X_Axis->SetRangeUser(Min_X_Range + 1e-9, Max_X_Range - 1e-9);

    if (isFor_Hist2D)
    {
        Main_Pad_Z_Axis->SetLabelFont(Z_Label_Font_Main);
        Main_Pad_Z_Axis->SetLabelSize(Z_Label_Size_Main);
        Main_Pad_Z_Axis->SetLabelOffset(Z_Label_Offset_Main);
        Main_Pad_Z_Axis->SetMaxDigits(3);

        if (isNeed_SetBinNameX)
        {
            for (size_t i = 0; i < Name_X_Bin.size(); i++)
                Main_Pad_X_Axis->SetBinLabel(i + 1, Name_X_Bin[i]);
            Hist2D->LabelsOption("v");
            Main_Pad_X_Axis->SetTitle("");
        }
        if (isNeed_SetBinNameY)
        {
            for (size_t i = 0; i < Name_Y_Bin.size(); i++)
                Main_Pad_Y_Axis->SetBinLabel(i + 1, Name_Y_Bin[i]);
            Hist2D->LabelsOption("v");
            Main_Pad_Y_Axis->SetTitle("");
        }

        if (isSet_Z_Range_Main)
            Main_Pad_Z_Axis->SetRangeUser(Min_Z_Range_Main, Max_Z_Range_Main);
    }

    if (Sub_Pad_isNeeded)
    {
        Sub_Pad_Y_Axis->SetLabelFont(Y_Label_Font_Sub);
        Sub_Pad_Y_Axis->SetLabelSize(Y_Label_Size_Sub);
        Sub_Pad_Y_Axis->SetTitleFont(Y_Title_Font_Sub);
        Sub_Pad_Y_Axis->SetTitleSize(Y_Title_Size_Sub);
        Sub_Pad_Y_Axis->SetTitleOffset(Y_Title_Offset_Sub);
        Sub_Pad_Y_Axis->SetMaxDigits(3);
        Sub_Pad_Y_Axis->SetNdivisions(505);
        Sub_Pad_Y_Axis->SetTitle(Y_Title_Sub_Pad);
        Sub_Pad_Y_Axis->SetRangeUser(Min_Y_Range_Sub, Max_Y_Range_Sub);

        Sub_Pad_X_Axis->SetLabelFont(X_Label_Font_Sub);
        Sub_Pad_X_Axis->SetLabelSize(X_Label_Size_Sub);
        Sub_Pad_X_Axis->SetTitleFont(X_Title_Font_Sub);
        Sub_Pad_X_Axis->SetTitleSize(X_Title_Size_Sub);
        Sub_Pad_X_Axis->SetTitleOffset(X_Title_Offset_Sub);
        Sub_Pad_X_Axis->SetMaxDigits(4);
        Sub_Pad_X_Axis->SetTitle(X_Title_Sub_Pad);
        Sub_Pad_X_Axis->SetRangeUser(Min_X_Range + 1e-9, Max_X_Range - 1e-9);
    }
    else
        Main_Pad_X_Axis->SetTitle(X_Title_Main_Pad);

    if (isFor_Hist2D && isNeed_SetBinNameX)
        Main_Pad_X_Axis->SetTitle("");

    MSGUser()->StartTitle("NGFigure::SetAxis");
    MSGUser()->MSG_DEBUG("X Range Max:  ", Max_X_Range, "   Min:  ", Min_X_Range);
    MSGUser()->MSG_DEBUG("Y Range Main Max:  ", Max_Y_Range_Main, "   Min:  ", Min_Y_Range_Main);
    MSGUser()->MSG_DEBUG("Y Range Sub  Max:  ", Max_Y_Range_Sub, "   Min:  ", Min_Y_Range_Sub);
    MSGUser()->EndTitle();
}

void NGFigure::SetHist()
{
    for (size_t i = 0; i < Num_Main_Hist; i++)
    {
        Main_Hist[i]->SetLineWidth(Hist_Line_Width);
        Main_Hist[i]->SetMarkerSize(Hist_Marker_Size);
        if (i == 0)
            Main_Hist[i]->SetFillStyle(Hist_Fill_Style_First);
        else
            Main_Hist[i]->SetFillStyle(Hist_Fill_Style);

        Main_Hist[i]->SetTitle("");
        Main_Hist[i]->SetStats(0);

        int index = i % 12;
        int round = (i - index) / 12 * 2 + 1;

        if (index == 0)
        {
            Main_Hist[i]->SetLineColor(kRed + round);
            Main_Hist[i]->SetMarkerColor(kRed + round);
            if (Mode_Tag_Main == "HSTACK" && (Mode_Tag_Sub == "UNKNOWN" || Mode_Tag_Sub == "PERCENTAGE"))
                Main_Hist[i]->SetFillColor(kRed + round);
            if (Mode_Tag_Main == "MULTI" && DoSeparate)
                Main_Hist[i]->SetFillColor(kRed + round);

            if (Style_Index == 1)
            {
                if (Mode_Tag_Main == "MULTI" && DoSeparate)
                    Main_Hist[i]->SetFillColor(kGray + 2);
                // Main_Hist[i]->SetFillColor(0); // Hacked

                Main_Hist[i]->SetMarkerStyle(20);
                Main_Hist[i]->SetMarkerSize(12);
                Main_Hist[i]->SetLineColor(kGray + 2);
                Main_Hist[i]->SetMarkerColor(kGray + 2);
            }
        }
        if (index == 1)
        {
            Main_Hist[i]->SetLineColor(kBlue + round);
            Main_Hist[i]->SetMarkerColor(kBlue + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kBlue + round);

            if (Style_Index == 1)
            {
                Main_Hist[i]->SetMarkerStyle(21);
                Main_Hist[i]->SetMarkerSize(12);
                Main_Hist[i]->SetLineColor(kAzure + 1);
                Main_Hist[i]->SetMarkerColor(kAzure + 1);
                if (Mode_Tag_Main == "HSTACK")
                    Main_Hist[i]->SetFillColor(kAzure + 1);
            }
        }
        if (index == 2)
        {
            Main_Hist[i]->SetLineColor(kGreen + round);
            Main_Hist[i]->SetMarkerColor(kGreen + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kGreen + round);

            if (Style_Index == 1)
            {
                Main_Hist[i]->SetMarkerStyle(22);
                Main_Hist[i]->SetMarkerSize(12);
                Main_Hist[i]->SetLineColor(kOrange + 1);
                Main_Hist[i]->SetMarkerColor(kOrange + 1);
                if (Mode_Tag_Main == "HSTACK")
                    Main_Hist[i]->SetFillColor(kOrange + 1);
            }
        }
        if (index == 3)
        {
            Main_Hist[i]->SetLineColor(kPink + round);
            Main_Hist[i]->SetMarkerColor(kPink + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kPink + round);

            if (Style_Index == 1)
            {
                Main_Hist[i]->SetMarkerStyle(23);
                Main_Hist[i]->SetMarkerSize(12);
                Main_Hist[i]->SetLineColor(kRed + 1);
                Main_Hist[i]->SetMarkerColor(kRed + 1);
            }
        }
        if (index == 4)
        {
            Main_Hist[i]->SetLineColor(kAzure + round);
            Main_Hist[i]->SetMarkerColor(kAzure + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kAzure + round);

            if (Style_Index == 1)
            {
                Main_Hist[i]->SetMarkerStyle(33);
                Main_Hist[i]->SetMarkerSize(12);
                Main_Hist[i]->SetLineColor(kGreen + 1);
                Main_Hist[i]->SetMarkerColor(kGreen + 1);
            }
        }
        if (index == 5)
        {
            Main_Hist[i]->SetLineColor(kSpring + round);
            Main_Hist[i]->SetMarkerColor(kSpring + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kSpring + round);
        }
        if (index == 6)
        {
            Main_Hist[i]->SetLineColor(kMagenta + round);
            Main_Hist[i]->SetMarkerColor(kMagenta + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kMagenta + round);
        }
        if (index == 7)
        {
            Main_Hist[i]->SetLineColor(kCyan + round);
            Main_Hist[i]->SetMarkerColor(kCyan + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kCyan + round);
        }
        if (index == 8)
        {
            Main_Hist[i]->SetLineColor(kYellow + round);
            Main_Hist[i]->SetMarkerColor(kYellow + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kYellow + round);
        }
        if (index == 9)
        {
            Main_Hist[i]->SetLineColor(kViolet + round);
            Main_Hist[i]->SetMarkerColor(kViolet + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kViolet + round);
        }
        if (index == 10)
        {
            Main_Hist[i]->SetLineColor(kTeal + round);
            Main_Hist[i]->SetMarkerColor(kTeal + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kTeal + round);
        }
        if (index == 11)
        {
            Main_Hist[i]->SetLineColor(kOrange + round);
            Main_Hist[i]->SetMarkerColor(kOrange + round);
            if (Mode_Tag_Main == "HSTACK")
                Main_Hist[i]->SetFillColor(kOrange + round);
        }
        Main_Pad->Update();
    }
    if (Main_Graph.size() > 0)
    {
        for (size_t i = 0; i < Main_Graph.size(); i++)
        {
            Main_Graph[i]->SetMarkerSize(Graph_Marker_Size[i]);
            Main_Graph[i]->SetMarkerStyle(Graph_Marker_Style[i]);
            Main_Graph[i]->SetMarkerColor(Graph_Marker_Color[i]);

            Main_Graph[i]->SetLineWidth(Graph_Line_Width[i]);
            Main_Graph[i]->SetLineStyle(Graph_Line_Style[i]);
            Main_Graph[i]->SetLineColor(Graph_Line_Color[i]);

            Main_Graph[i]->SetFillStyle(0);
        }

        Main_Pad->Update();
    }
    if (Sub_Pad_isNeeded)
    {
        if (Sub_Hist.size() == Num_Main_Hist)
        {
            for (size_t i = 0; i < Sub_Hist.size(); i++)
            {
                Sub_Hist[i]->SetLineWidth(Hist_Line_Width);
                Sub_Hist[i]->SetMarkerSize(Hist_Marker_Size);
                if (i == 0)
                    Sub_Hist[i]->SetFillStyle(Sub_Fill_Style_First);
                else
                    Sub_Hist[i]->SetFillStyle(Sub_Fill_Style);

                if (DoSeparate && i == 0)
                    Sub_Hist[i]->SetLineWidth(0);

                Sub_Hist[i]->SetTitle("");
                Sub_Hist[i]->SetStats(0);

                int index = i % 12;
                int round = (i - index) / 12 * 2 + 1;

                if (index == 0)
                {
                    Sub_Hist[i]->SetLineColor(kRed + round);
                    Sub_Hist[i]->SetMarkerColor(kRed + round);
                    if (Mode_Tag_Main == "HSTACK")
                        Sub_Hist[i]->SetFillColor(kRed + round);
                    if (DoSeparate)
                    {
                        Sub_Hist[i]->SetFillColor(kGray + round);
                        Sub_Hist[i]->SetLineColor(kGray + 2);
                        Sub_Hist[i]->SetMarkerColor(kGray + 2);
                    }

                    if (Style_Index == 1)
                    {
                        Sub_Hist[i]->SetLineColor(kGray + 2);
                        Sub_Hist[i]->SetMarkerColor(kGray + 2);
                    }
                }
                if (index == 1)
                {
                    Sub_Hist[i]->SetLineColor(kBlue + round);
                    Sub_Hist[i]->SetMarkerColor(kBlue + round);
                    if (Mode_Tag_Main == "HSTACK")
                        Sub_Hist[i]->SetFillColor(kBlue + round);

                    if (Style_Index == 1)
                    {
                        Sub_Hist[i]->SetMarkerStyle(21);
                        Sub_Hist[i]->SetMarkerSize(12);
                        Sub_Hist[i]->SetLineColor(kAzure + 1);
                        Sub_Hist[i]->SetMarkerColor(kAzure + 1);
                    }
                }
                if (index == 2)
                {
                    Sub_Hist[i]->SetLineColor(kGreen + round);
                    Sub_Hist[i]->SetMarkerColor(kGreen + round);
                    if (Mode_Tag_Main == "HSTACK")
                        Sub_Hist[i]->SetFillColor(kGreen + round);

                    if (Style_Index == 1)
                    {
                        Sub_Hist[i]->SetMarkerStyle(22);
                        Sub_Hist[i]->SetMarkerSize(12);
                        Sub_Hist[i]->SetLineColor(kOrange + 1);
                        Sub_Hist[i]->SetMarkerColor(kOrange + 1);
                    }
                }
                if (index == 3)
                {
                    Sub_Hist[i]->SetLineColor(kPink + round);
                    Sub_Hist[i]->SetMarkerColor(kPink + round);
                    if (Style_Index == 1)
                    {
                        Sub_Hist[i]->SetMarkerStyle(23);
                        Sub_Hist[i]->SetMarkerSize(12);
                        Sub_Hist[i]->SetLineColor(kRed + 1);
                        Sub_Hist[i]->SetMarkerColor(kRed + 1);
                    }
                }
                if (index == 4)
                {
                    Sub_Hist[i]->SetLineColor(kAzure + round);
                    Sub_Hist[i]->SetMarkerColor(kAzure + round);

                    if (Style_Index == 1)
                    {
                        Sub_Hist[i]->SetMarkerStyle(33);
                        Sub_Hist[i]->SetMarkerSize(12);
                        Sub_Hist[i]->SetLineColor(kGreen + 1);
                        Sub_Hist[i]->SetMarkerColor(kGreen + 1);
                    }
                }
                if (index == 5)
                {
                    Sub_Hist[i]->SetLineColor(kSpring + round);
                    Sub_Hist[i]->SetMarkerColor(kSpring + round);
                }
                if (index == 6)
                {
                    Sub_Hist[i]->SetLineColor(kMagenta + round);
                    Sub_Hist[i]->SetMarkerColor(kMagenta + round);
                }
                if (index == 7)
                {
                    Sub_Hist[i]->SetLineColor(kCyan + round);
                    Sub_Hist[i]->SetMarkerColor(kCyan + round);
                }
                if (index == 8)
                {
                    Sub_Hist[i]->SetLineColor(kYellow + round);
                    Sub_Hist[i]->SetMarkerColor(kYellow + round);
                }
                if (index == 9)
                {
                    Sub_Hist[i]->SetLineColor(kViolet + round);
                    Sub_Hist[i]->SetMarkerColor(kViolet + round);
                }
                if (index == 10)
                {
                    Sub_Hist[i]->SetLineColor(kTeal + round);
                    Sub_Hist[i]->SetMarkerColor(kTeal + round);
                }
                if (index == 11)
                {
                    Sub_Hist[i]->SetLineColor(kOrange + round);
                    Sub_Hist[i]->SetMarkerColor(kOrange + round);
                }
            }
        }
        else
        {
            for (size_t i = 0; i < Sub_Hist.size(); i++)
            {
                Sub_Hist[i]->SetLineWidth(Hist_Line_Width);
                Sub_Hist[i]->SetMarkerSize(Hist_Marker_Size);
                if (i == 0)
                    Sub_Hist[i]->SetFillStyle(Sub_Fill_Style_First);
                else
                    Sub_Hist[i]->SetFillStyle(Sub_Fill_Style);

                if (DoSeparate && i == 0)
                    Sub_Hist[i]->SetLineWidth(0);

                Sub_Hist[i]->SetTitle("");
                Sub_Hist[i]->SetStats(0);

                int index = i % 12;
                int round = (i - index) / 12 * 2 + 1;

                if (index == 0)
                {
                    Sub_Hist[i]->SetLineColor(kRed + round);
                    Sub_Hist[i]->SetMarkerColor(kRed + round);
                    if (Mode_Tag_Main == "HSTACK")
                        Sub_Hist[i]->SetFillColor(kRed + round);
                    if (DoSeparate)
                        Sub_Hist[i]->SetFillColor(kRed + round);
                }

                if (index == 1)
                {
                    Sub_Hist[i]->SetLineColor(kBlue + round);
                    Sub_Hist[i]->SetMarkerColor(kBlue + round);
                    if (Mode_Tag_Main == "HSTACK")
                        Sub_Hist[i]->SetFillColor(kBlue + round);

                    if (Style_Index == 1)
                    {
                        Sub_Hist[i]->SetMarkerStyle(20);
                        Sub_Hist[i]->SetMarkerSize(8);
                        Sub_Hist[i]->SetLineColor(kGray + 2);
                        Sub_Hist[i]->SetMarkerColor(kGray + 2);
                    }
                }
            }
        }

        Sub_Pad->Update();
    }
}

bool NGFigure::DrawHStack()
{
    if (Mode_Tag_Main == "HSTACK")
    {
        figure->cd();
        Main_Pad->cd();

        Main_Stack->Draw("same");
        if (Mode_Tag_Sub != "PERCENTAGE" && Mode_Tag_Sub != "UNKNOWN")
            Main_Hist[0]->Draw("E same");

        figure->cd();

        if (Mode_Tag_Sub == "PERCENTAGE")
        {
            figure->cd();
            Sub_Pad->cd();

            Sub_Stack->Draw("same");

            figure->cd();
        }
    }

    return true;
}

bool NGFigure::DrawHist2D()
{
    if (Mode_Tag_Main == "HIST2D")
    {
        figure->cd();
        Main_Pad->cd();

        Main_Legend = new TLegend(LeftMargin_Main + 0.02, 1 - TopMargin_Main - 0.02,
                                  1 - RightMargin_Main - 0.02, 1 - 0.02);

        Main_Legend->SetFillColor(0);
        Main_Legend->SetFillStyle(0);
        Main_Legend->SetLineColor(0);
        Main_Legend->SetLineWidth(0);
        Main_Legend->SetTextSize(Legend_Text_Size);
        Main_Legend->SetTextFont(Legend_Text_Font);

        if (DoTextOnly)
        {
            Text2D->Draw("TEXT,ERROR");
            Main_Pad_X_Axis = Text2D->GetXaxis();
            Main_Pad_Y_Axis = Text2D->GetYaxis();
            Main_Pad_Z_Axis = Text2D->GetZaxis();

            TH2D *temp = Text2D;
            Text2D = Hist2D;
            Hist2D = temp;
        }
        else
        {
            Hist2D->Draw("COLZ");

            Main_Pad_X_Axis = Hist2D->GetXaxis();
            Main_Pad_Y_Axis = Hist2D->GetYaxis();
            Main_Pad_Z_Axis = Hist2D->GetZaxis();

            if (!DoColorOnly)
                Text2D->Draw("TEXT,ERROR same");
        }

        Main_Legend->SetHeader(LegendName[0]);
        if (DoLegend)
            Main_Legend->Draw("same");

        figure->cd();
    }

    return true;
}

bool NGFigure::HackStagger()
{
    if (DoStagger && Mode_Tag_Main == "MULTI")
    {
        if (Num_Main_Hist > 2)
        {
            Stagger_Num_X_Bin = (Num_Main_Hist + 1) * Num_X_Bin;
            Stagger_X_Binning = new double[Stagger_Num_X_Bin + 1];
            for (int i = 0; i < Num_X_Bin; i++)
            {
                for (size_t j = 0; j < Num_Main_Hist + 1; j++)
                    Stagger_X_Binning[i * (Num_Main_Hist + 1) + j] = X_Binning[i] + (X_Binning[i + 1] - X_Binning[i]) / (Num_Main_Hist + 1) * j;
            }
            Stagger_X_Binning[Stagger_Num_X_Bin] = X_Binning[Num_X_Bin];

            for (size_t i = 1; i < Num_Main_Hist; i++)
            {
                Stagger_Save_Main_Hist.push_back(Main_Hist[i]);
                sprintf(name, "Stagger_Main_Hist_%lu", i);
                Main_Hist[i] = new TH1D(name, name, Stagger_Num_X_Bin, Stagger_X_Binning);
                Main_Hist[i]->Sumw2();

                for (int j = 0; j < Num_X_Bin; j++)
                {
                    double val = Stagger_Save_Main_Hist[i - 1]->GetBinContent(j + 1);
                    double val_err = Stagger_Save_Main_Hist[i - 1]->GetBinError(j + 1);

                    Main_Hist[i]->SetBinContent(j * (Num_Main_Hist + 1) + i + 1, val);
                    Main_Hist[i]->SetBinError(j * (Num_Main_Hist + 1) + i + 1, val_err);
                }
            }
        }
        if (Sub_Hist.size() > 2)
        {
            for (size_t i = 1; i < Sub_Hist.size(); i++)
            {
                Stagger_Save_Sub_Hist.push_back(Sub_Hist[i]);
                sprintf(name, "Stagger_Sub_Hist_%lu", i);
                Sub_Hist[i] = new TH1D(name, name, Stagger_Num_X_Bin, Stagger_X_Binning);
                Sub_Hist[i]->Sumw2();

                for (int j = 0; j < Num_X_Bin; j++)
                {
                    double val = Stagger_Save_Sub_Hist[i - 1]->GetBinContent(j + 1);
                    double val_err = Stagger_Save_Sub_Hist[i - 1]->GetBinError(j + 1);

                    Sub_Hist[i]->SetBinContent(j * (Num_Main_Hist + 1) + i + 1, val);
                    Sub_Hist[i]->SetBinError(j * (Num_Main_Hist + 1) + i + 1, val_err);
                }
            }
        }
    }

    return true;
}

void NGFigure::DrawFigure()
{
    auto msgguard = MSGUser()->StartTitleWithGuard("NGFigure::DrawFigure");
    if (isSet_Mode == false)
    {
        MSGUser()->MSG_ERROR("Mode not Set!");
        return;
    }

    if (isFor_Hist2D)
    {
        RebinHist();
        ConfigParameters(); // Set Every Numbers Needed
        DefinePad();
        DrawHist2D();
        SetMargin();
    }
    else
    {
        RebinHist();
        Check();            // Check If Everything OK
        Normalize();        // Do Changes On Hist
        ConfigParameters(); // Set Every Numbers Needed
        HackStagger();
        DefinePad();
        DrawHist(); // Basic hist on canvas
        SetMargin();
        SetHist();
    }

    SetAxis();
    if (Mode_Tag_Main == "HSTACK")
        DrawHStack();

    // Set All kinds of style
    SaveHist();
}

NGFigure::~NGFigure()
{
    if (figure != nullptr)
        delete figure;

    for (size_t i = 0; i < Sub_Hist.size(); i++)
        delete Sub_Hist[i];
    for (size_t i = 0; i < Stagger_Save_Sub_Hist.size(); i++)
        delete Stagger_Save_Sub_Hist[i];
    if (Stagger_Save_Main_Hist.size() > 0)
    {
        for (size_t i = 1; i < Main_Hist.size(); i++)
        {
            if (Main_Hist[i])
                delete Main_Hist[i];
        }
    }

    if (Total_Hist_With_Error != nullptr)
        delete Total_Hist_With_Error;
    if (Total_Hist != nullptr)
        delete Total_Hist;
    if (Empty_Hist != nullptr)
        delete Empty_Hist;
    if (Empty_Hist_Sub != nullptr)
        delete Empty_Hist_Sub;

    if (Main_Stack != nullptr)
        delete Main_Stack;
    if (Sub_Stack != nullptr)
        delete Sub_Stack;
    if (Stagger_X_Binning != nullptr)
        delete[] Stagger_X_Binning;
    if (X_Binning != nullptr)
        delete[] X_Binning;

    if (Matrix_Save != nullptr)
        delete Hist2D;
    if (Text2D != nullptr)
        delete Text2D;
}

void NGFigure::SetStagger(bool dostagger)
{
    DoStagger = dostagger;
}

void NGFigure::SetMainYRange(double min, double max)
{
    if (min > max)
    {
        MSGUser()->StartTitle("NGFigure::SetMainYRange");
        MSGUser()->MSG_ERROR("Max value is smaller than Min value!");
        MSGUser()->EndTitle();
        return;
    }
    isSet_Y_Range_Main = true;
    Max_Y_Range_Main = max;
    Min_Y_Range_Main = min;
}

void NGFigure::SetMainZRange(double min, double max)
{
    if (min > max)
    {
        MSGUser()->StartTitle("NGFigure::SetMainZRange");
        MSGUser()->MSG_ERROR("Max value is smaller than Min value!");
        MSGUser()->EndTitle();
        return;
    }
    isSet_Z_Range_Main = true;
    Max_Z_Range_Main = max;
    Min_Z_Range_Main = min;
}

void NGFigure::SetSubYRange(double min, double max)
{
    if (min > max)
    {
        MSGUser()->StartTitle("NGFigure::SetSubYRange");
        MSGUser()->MSG_ERROR("Max value is smaller than Min value!");
        MSGUser()->EndTitle();
        return;
    }
    isSet_Y_Range_Sub = true;
    Max_Y_Range_Sub = max;
    Min_Y_Range_Sub = min;
}

void NGFigure::SetNormRange(double min, double max)
{
    if (min > max)
    {
        MSGUser()->StartTitle("NGFigure::SetNormRange");
        MSGUser()->MSG_ERROR("Max value is smaller than Min value!");
        MSGUser()->EndTitle();
        return;
    }
    isSet_Norm_Range = true;
    Max_Norm_Range = max;
    Min_Norm_Range = min;
}

void NGFigure::SetSubGridX(bool grid)
{
    NeedGridX_Sub = grid;
}

void NGFigure::SetSubGridY(bool grid)
{
    NeedGridY_Sub = grid;
}

void NGFigure::SetMainGridX(bool grid)
{
    NeedGridX_Main = grid;
}

void NGFigure::SetMainGridY(bool grid)
{
    NeedGridY_Main = grid;
}

void NGFigure::SetGridX(bool grid)
{
    SetSubGridX(grid);
    SetMainGridX(grid);
}

void NGFigure::SetGridY(bool grid)
{
    SetSubGridY(grid);
    SetMainGridY(grid);
}

void NGFigure::SetGrid(bool grid)
{
    SetGridX(grid);
    SetGridY(grid);
}

void NGFigure::SetAsmble(int index)
{
    asmb = index;
}

void NGFigure::SetStyleIndex(int index)
{
    Style_Index = index;
}

void NGFigure::SetLeftPercentage(double percent)
{
    LeftPercentage = percent;
}

void NGFigure::SetMatrixStyle(bool domatrix)
{
    DoMatrix = domatrix;
}

void NGFigure::SetFixedNormFactor(bool fix, double scale)
{
    isFixed_Norm = fix;
    Norm_Factor = scale;
}

void NGFigure::SetHeaderName(const char *header)
{
    HeaderName = (TString)header;
}

void NGFigure::SetShowChi2(bool show)
{
    ShowChi2 = show;
}

void NGFigure::SetXBinName(const char *binname)
{
    Name_X_Bin.push_back((TString)binname);
    isNeed_SetBinNameX = true;
}

void NGFigure::SetYBinName(const char *binname)
{
    Name_Y_Bin.push_back((TString)binname);
    isNeed_SetBinNameY = true;
}

void NGFigure::SeparateUncertainty(bool doseparate)
{
    DoSeparate = doseparate;
}

void NGFigure::Set2DTextOnly(bool dotextonly)
{
    DoTextOnly = dotextonly;
}

void NGFigure::Set2DColorOnly(bool docoloronly)
{
    DoColorOnly = docoloronly;
}

void NGFigure::SetNotNorm()
{
    isNeed_Norm = false;
}

void NGFigure::SetLegendColumn(int num)
{
    Legend_Column = num;
}

void NGFigure::SetLegend(bool dolegend)
{
    DoLegend = dolegend;
}
