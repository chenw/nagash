#include "NAGASH/NAGASHFile.h"

namespace NAGASH
{
    NAGASHFile::NAGASHFile(std::shared_ptr<MSGTool> msg, const std::string &filename, Mode option)
        : Tool(msg), file_name(filename), file_open_mode(option)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NAGASHFile::OpenFile");
        if (option == Mode::READ)
        {
            nagashfile.open(filename, std::ios::in | std::ios::binary);
            this->is_open = ReadFile();
            return;
        }

        if (option == Mode::WRITE)
        {
            int isexist = access(filename.c_str(), F_OK);
            if (isexist == 0)
                MSGUser()->MSG_WARNING("file ", filename, " already exists, will be overwritten");

            nagashfile.open(filename, std::ios::in | std::ios::trunc | std::ios::out | std::ios::binary);
            if (!nagashfile.is_open())
            {
                MSGUser()->MSG_ERROR("file ", filename, " can not be opened to write");
                return;
            }
            this->is_open = true;
            WriteString(magic_number);
            nagashfile.write(reinterpret_cast<char *>(&main_version), sizeof(size_t));
            nagashfile.write(reinterpret_cast<char *>(&sub_version), sizeof(size_t));
            nagashfile.write(reinterpret_cast<char *>(&list_position), sizeof(size_t));
        }

        if (option == Mode::UPDATE)
        {
            nagashfile.open(filename, std::ios::in | std::ios::out | std::ios::binary);
            this->is_open = ReadFile();
        }
    }

    NAGASHFile::~NAGASHFile()
    {
        Close();
    }

    void NAGASHFile::Close()
    {
        if (!IsOpen())
            return;

        if (this->file_open_mode != Mode::READ)
        {
            // write the list
            nagashfile.seekp(list_position, std::ios::beg);
            list_size = name_position_map.size();
            nagashfile.write(reinterpret_cast<char *>(&list_size), sizeof(size_t));
            for (auto &t : name_position_map)
            {
                WriteString(t.first);
                WriteVector<size_t>(t.second);
            }

            // write the list position
            nagashfile.seekp(28, std::ios::beg);
            nagashfile.write(reinterpret_cast<char *>(&list_position), sizeof(size_t));
            nagashfile.flush();
        }

        nagashfile.close();
        this->is_open = false;
    }

    void NAGASHFile::ReadString(std::string &str)
    {
        size_t stringsize;
        nagashfile.read(reinterpret_cast<char *>(&stringsize), sizeof(size_t));
        str.resize(stringsize);
        nagashfile.read(reinterpret_cast<char *>(&str[0]), str.size());
    }

    void NAGASHFile::WriteString(const std::string &str)
    {
        size_t stringsize = str.size();
        nagashfile.write(reinterpret_cast<const char *>(&stringsize), sizeof(size_t));
        nagashfile.write(reinterpret_cast<const char *>(&str[0]), str.size());
    }

    bool NAGASHFile::ReadFile()
    {
        if (!nagashfile.is_open())
        {
            MSGUser()->MSG_ERROR("file ", file_name, " can not be opened to read");
            return false;
        }
        this->is_open = true;
        // verify if this is a nagash file
        ReadString(magic_number);
        if (magic_number != "whma")
        {
            MSGUser()->MSG_ERROR("file ", file_name, " is not a nagash file");
            return false;
        }

        // read version number
        nagashfile.read(reinterpret_cast<char *>(&main_version), sizeof(size_t));
        nagashfile.read(reinterpret_cast<char *>(&sub_version), sizeof(size_t));

        // read list position
        nagashfile.read(reinterpret_cast<char *>(&list_position), sizeof(size_t));

        // read hist list
        nagashfile.seekg(list_position, std::ios::beg);
        nagashfile.read(reinterpret_cast<char *>(&list_size), sizeof(size_t));

        for (size_t i = 0; i < list_size; i++)
        {
            std::string name;
            std::vector<size_t> vpos;
            ReadString(name);
            ReadVector<size_t>(vpos);
            name_position_map.insert(std::pair<std::string, std::vector<size_t>>(name, vpos));
        }

        MSGUser()->MSG_DEBUG("open file ", this->file_name, " version : ", this->main_version, ".", this->sub_version);
        return true;
    }

    std::tuple<bool, bool, size_t> NAGASHFile::FindHistPosition(const std::string &name)
    {
        size_t position = 0;
        bool histexist = false;
        bool isoutrange = false;
        auto findresult = name_position_map.find(name);
        if (findresult == name_position_map.end())
        {
            std::regex word_regex("(;[0-9]+)$");
            auto words_begin = std::sregex_iterator(name.begin(), name.end(), word_regex);
            if (words_begin != std::sregex_iterator())
            {
                std::string matched = ((*words_begin).str());
                std::string newname = std::regex_replace(name, word_regex, "");
                auto newfindresult = name_position_map.find(newname);
                if (newfindresult != name_position_map.end())
                {
                    matched.erase(0, 1);
                    std::stringstream sstream(matched);
                    size_t histindex;
                    sstream >> histindex;
                    if (newfindresult->second.size() > histindex)
                    {
                        histexist = true;
                        position = newfindresult->second[histindex];
                    }
                    else
                        isoutrange = true;
                }
            }
        }
        else
        {
            histexist = true;
            position = findresult->second.back();
        }

        return std::tuple<bool, bool, size_t>(histexist, isoutrange, position);
    }

    void NAGASHFile::PrintListOfHists(std::ostream &outs)
    {
        if (!IsOpen())
        {
            MSGUser()->StartTitle("NAGASHFile::PrintListOfHists");
            MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened");
            MSGUser()->EndTitle();
            return;
        }

        outs << "List of histograms stored in file " << this->file_name << " :" << std::endl;

        // print out all the histograms along with their position and size
        for (auto &t : name_position_map)
        {
            for (size_t i = 0; i < t.second.size(); i++)
            {
                // print out size
                outs << "    NNGHist<";
                nagashfile.seekg(t.second[i], std::ios::beg);
                size_t dim;
                nagashfile.read(reinterpret_cast<char *>(&dim), sizeof(size_t));
                size_t axesinfosize;
                nagashfile.read(reinterpret_cast<char *>(&axesinfosize), sizeof(size_t));

                for (size_t i = 0; i < dim; i++)
                {
                    std::string axisname;
                    ReadString(axisname);
                    if (i < dim - 1)
                        outs << axisname << ", ";
                    else
                        outs << axisname << ">  ";

                    size_t controlblocksize;
                    nagashfile.read(reinterpret_cast<char *>(&controlblocksize), sizeof(size_t));
                    nagashfile.seekg(controlblocksize, std::ios::cur);
                }

                outs << t.first << ";" << i << std::endl;
            }
        }
    }

    NAGASHFile::AuxHist::AuxHist(size_t _dim_) : dim(_dim_), SumX(_dim_, 0), SumX2(_dim_, std::vector<double>(_dim_, 0))
    {
    }

    std::pair<bool, bool> NAGASHFile::AuxHist::Add(const AuxHist &inh)
    {
        if (this->axesinfo != inh.axesinfo)
            return std::pair<bool, bool>(false, false);

        this->SumW += inh.SumW;
        this->SumW2 += inh.SumW2;

        for (size_t i = 0; i < this->SumX.size(); ++i)
        {
            this->SumX[i] += inh.SumX[i];
            for (size_t j = 0; j < this->SumX.size(); ++j)
                this->SumX2[i][j] += inh.SumX2[i][j];
        }

        for (size_t i = 0; i < this->content.size(); ++i)
        {
            this->content[i] += inh.content[i];
            this->error[i] += inh.error[i];
        }

        return std::pair<bool, bool>(true, !(this->overflow_option == inh.overflow_option));
    }

    NAGASHFile::AuxHist NAGASHFile::LoadAuxHist(size_t pos)
    {
        nagashfile.seekg(pos);
        size_t dim;
        nagashfile.read(reinterpret_cast<char *>(&dim), sizeof(size_t));

        AuxHist outhist(dim);

        ReadVector<char>(outhist.axesinfo);
        nagashfile.read(reinterpret_cast<char *>(&outhist.nvar), sizeof(size_t));
        nagashfile.read(reinterpret_cast<char *>(&(outhist.overflow_option)), sizeof(OverflowOption));
        nagashfile.read(reinterpret_cast<char *>(&(outhist.SumW)), sizeof(double));
        nagashfile.read(reinterpret_cast<char *>(&(outhist.SumW2)), sizeof(double));
        for (size_t i = 0; i < dim; i++)
            nagashfile.read(reinterpret_cast<char *>(&(outhist.SumX[i])), sizeof(double));
        for (size_t i = 0; i < dim; i++)
            for (size_t j = 0; j < dim; j++)
                nagashfile.read(reinterpret_cast<char *>(&(outhist.SumX2[i][j])), sizeof(double));

        ReadVector<double>(outhist.content);
        ReadVector<double>(outhist.error);

        return outhist;
    }

    void NAGASHFile::SaveAuxHist(const std::string &hname, AuxHist &outhist)
    {
        std::vector<size_t> hpos{list_position};

        name_position_map.insert(std::pair<std::string, std::vector<size_t>>(hname, hpos));
        nagashfile.write(reinterpret_cast<char *>(&(outhist.dim)), sizeof(size_t));
        WriteVector<char>(outhist.axesinfo);
        nagashfile.write(reinterpret_cast<char *>(&outhist.nvar), sizeof(size_t));
        nagashfile.write(reinterpret_cast<char *>(&(outhist.overflow_option)), sizeof(OverflowOption));
        nagashfile.write(reinterpret_cast<char *>(&(outhist.SumW)), sizeof(double));
        nagashfile.write(reinterpret_cast<char *>(&(outhist.SumW2)), sizeof(double));
        for (size_t i = 0; i < outhist.dim; i++)
            nagashfile.write(reinterpret_cast<char *>(&(outhist.SumX[i])), sizeof(double));
        for (size_t i = 0; i < outhist.dim; i++)
            for (size_t j = 0; j < outhist.dim; j++)
                nagashfile.write(reinterpret_cast<char *>(&(outhist.SumX2[i][j])), sizeof(double));
        WriteVector<double>(outhist.content);
        WriteVector<double>(outhist.error);

        list_position = nagashfile.tellp();
    }

    // for adding histograms from different files
    void NAGASHFile::hadd(const std::string &outfilename, const std::vector<std::string> &infilenames)
    {
        auto msg = std::make_shared<MSGTool>();
        auto titleguard = msg->StartTitleWithGuard("NAGASHFile::hadd");

        int isexist = access(outfilename.c_str(), F_OK);
        if (isexist == 0)
        {
            msg->MSG_ERROR("target file already exists, hadd won't proceed. PLEASE CHECK.");
            return;
        }

        NAGASHFile outfile(msg, outfilename, Mode::WRITE);
        if (!outfile.IsOpen())
            return;

        msg->MSG_INFO("Target file ", outfilename);

        if (infilenames.size() == 0)
        {
            msg->MSG_ERROR("No source file given, abort");
            return;
        }

        std::vector<std::map<std::string, AuxHist>> name_hist_map_vector;
        std::vector<std::string> valid_file_names;

        for (size_t i = 0; i < infilenames.size(); i++)
        {
            NAGASHFile infile(msg, infilenames[i], Mode::READ);
            if (!infile.IsOpen())
                continue;

            name_hist_map_vector.emplace_back(std::map<std::string, AuxHist>());
            valid_file_names.emplace_back(infilenames[i]);
            // only the last written histogram will be merged if there are multi histograms with the same name
            for (auto &t : infile.name_position_map)
                name_hist_map_vector.back().insert(std::pair<std::string, AuxHist>(t.first, infile.LoadAuxHist(t.second.back())));

            msg->MSG_INFO("Source file ", i, " ", infilenames[i]);
        }

        if (infilenames.size() == 0)
        {
            msg->MSG_ERROR("No valid source file given, abort");
            return;
        }

        std::map<std::string, AuxHist> final_name_hist_map = name_hist_map_vector[0];

        // loop over other files
        for (size_t i = 1; i < name_hist_map_vector.size(); i++)
        {
            // loop over map
            for (auto &t : name_hist_map_vector[i])
            {
                auto findresult = final_name_hist_map.find(t.first);
                if (findresult != final_name_hist_map.end())
                {
                    // try to add to the target hist
                    auto [isAddSuccess, isWarningNeeded] = findresult->second.Add(t.second);
                    if (!isAddSuccess)
                    {
                        msg->MSG_WARNING("failed to add histogram with different binning (in adding hist ", t.first, " from file ", valid_file_names[i], " to hist from file", valid_file_names[0], ")");
                        continue;
                    }

                    if (isWarningNeeded)
                        msg->MSG_WARNING("adding histogram with different statistics options (in adding hist ", t.first, " from file ", valid_file_names[i], " to hist from file", valid_file_names[0], ")");
                }
                else
                    final_name_hist_map.insert(t);
            }
        }

        for (auto &t : final_name_hist_map)
            outfile.SaveAuxHist(t.first, t.second);

        outfile.Close();
    }

}