//***************************************************************************************
/// @file Uncertainty.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    /// @class NAGASH::Uncertainty Uncertainty.h "NAGASH/Uncertainty.h"
    /// @ingroup ToolClasses
    /// @brief Provide static functions for calculating uncertainties.
    class Uncertainty : public Tool
    {
    public:
        /// @brief Constructor.
        /// @param MSG input MSGTool.
        Uncertainty(std::shared_ptr<MSGTool> MSG) : Tool(MSG) {}
        static double AplusB(double A_err, double B_err);
        static double AminusB(double A_err, double B_err);
        static double AoverAplusB(double A, double AplusB, double A_err, double AplusB_err);
        static double AoverB(double A, double B, double A_err, double B_err);
        static double AminusBoverAplusB(double A, double B, double A_err, double B_err);
    };
} // namespace NAGASH
