//***************************************************************************************
/// @file ConfigTool.h
/// @author Wenhao Ma, Luxin Zhang
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/MSGTool.h"

namespace NAGASH
{
    class ConfigTool
    {
    public:
        ConfigTool() { msg = std::make_shared<MSGTool>(); };
        ConfigTool(const char *inputfilename);
        ConfigTool(const ConfigTool &c) = default;
        ConfigTool &operator=(const ConfigTool &c) = default;
        ConfigTool(ConfigTool &&c) = default;
        ConfigTool &operator=(ConfigTool &&c) = default;

        template <typename T>
        void BookPar(const TString &name, const T &value);
        template <class T>
        T GetPar(const TString &name, bool *doexist = nullptr) const;
        std::shared_ptr<MSGTool> MSGUser() const;
        void SetMSG(std::shared_ptr<MSGTool> MSG);
        void RemovePar(const TString &name);

    private:
        std::map<TString, std::any> ParMap;
        std::map<TString, std::any> ParMapFile;
        std::vector<std::filesystem::path> includedFiles;
        mutable std::shared_ptr<MSGTool> msg = nullptr;
        template <typename T>
        void BookParFile(const TString &name, const T &value);
        void ReadConfigFromFile(const TString &filename);
    };

    /// @brief Get the MSGTool pointer.
    /// @return  the pointer of MSGTool.
    inline std::shared_ptr<MSGTool> ConfigTool::MSGUser() const { return msg; }

    /// @brief Set the MSGTool.
    /// @param MSG input pointer of MSGTool.
    inline void ConfigTool::SetMSG(std::shared_ptr<MSGTool> MSG) { msg = MSG; }

    /// @brief Remove the parameter with given name.
    /// @param name the name of the parameter to be removed.
    inline void ConfigTool::RemovePar(const TString &name)
    {
        ParMap.erase(name);
        ParMapFile.erase(name);
    }

    /// @brief Book parameter with given name and value.
    /// @tparam T type of the parameter.
    /// @param name name of the parameter.
    /// @param value value of the parameter.
    template <typename T>
    inline void ConfigTool::BookPar(const TString &name, const T &value)
    {
        static_assert(!std::is_pointer<T>::value, "class ConfigTool : Can not book parameter in pointer type");
        static_assert(std::is_default_constructible<T>::value, "class ConfigTool : Can not book parameter in not_default_constructible type");

        std::any temp = std::make_any<T>(value);
        if (ParMap.find(name) != ParMap.end())
        {
            MSGUser()->StartTitle("ConfigTool::BookPar");
            MSGUser()->MSG(MSGLevel::WARNING, "parameter ", name, " has already existed, will be overwritten");
            MSGUser()->EndTitle();
            ParMap[name] = temp;
        }
        else
        {
            ParMap.insert(std::pair<TString, std::any>(name, temp));
        }
    }

    /// @brief Book parameter from input file.
    /// @tparam T type of the parameter.
    /// @param name name of the parameter.
    /// @param value value of the parameter.
    template <typename T>
    inline void ConfigTool::BookParFile(const TString &name, const T &value)
    {
        static_assert(!std::is_pointer<T>::value, "class ConfigTool : Can not book parameter in pointer type");
        static_assert(std::is_default_constructible<T>::value, "class ConfigTool : Can not book parameter in not_default_constructible type");

        std::any temp = std::make_any<T>(value);
        if (ParMapFile.find(name) != ParMapFile.end())
        {
            MSGUser()->StartTitle("ConfigTool::BookPar");
            MSGUser()->MSG(MSGLevel::WARNING, "parameter ", name, " has already existed, will be overwritten");
            MSGUser()->EndTitle();
            ParMapFile[name] = temp;
        }
        else
        {
            ParMapFile.insert(std::pair<TString, std::any>(name, temp));
        }
    }

    /// @brief Get the parameter with given name and type.
    /**
      For parameters read from config file, you should get them like :

      ~~~
      ConfigTool config("config.dat");
      auto intType = config.GetPar<int>("intType");
      auto longType = config.GetPar<long>("longType");
      auto boolType = config.GetPar<bool>("boolType");
      auto doubleType = config.GetPar<double>("doubleType");
      auto stringType = config.GetPar<TString>("stringType");
      auto intvecType = config.GetPar<std::vector<int>>("intvecType");
      auto longvecType = config.GetPar<std::vector<long>>("longvecType");
      auto boolvecType = config.GetPar<std::vector<bool>>("boolvecType");
      auto doublevecType = config.GetPar<std::vector<double>>("doublevecType");
      auto stringvecType = config.GetPar<std::vector<TString>>("stringvecType");
       ~~~
    */
    /// @tparam T the type of the parameter.
    /// @param[in] name the name of the parameter.
    /// @param[out] doexist the parameter exist or not.
    /// @return the parameter you want to get, or null value if the parameter does not exist.
    template <class T>
    inline T ConfigTool::GetPar(const TString &name, bool *doexist) const
    {
        static_assert(std::is_default_constructible<T>::value, "class ConfigTool : Can not get parameter in no_default_constructible type");
        auto re = ParMap.find(name);
        auto reF = ParMapFile.find(name);
        if (reF != ParMapFile.end())
        {
            try
            {
                auto par = std::any_cast<T>(reF->second);
                if (doexist != nullptr)
                    *doexist = true;
                return par;
            }
            catch (const std::bad_any_cast &e)
            {
                MSGUser()->StartTitle("ConfigTool::GetPar");
                MSGUser()->MSG(MSGLevel::ERROR, "parameter ", name, " type is not the given type, will return null");
                MSGUser()->EndTitle();
                return T();
            }
        }
        else if (re != ParMap.end())
        {
            try
            {
                auto par = std::any_cast<T>(re->second);
                if (doexist != nullptr)
                    *doexist = true;
                return par;
            }
            catch (const std::bad_any_cast &e)
            {
                MSGUser()->StartTitle("ConfigTool::GetPar");
                MSGUser()->MSG(MSGLevel::ERROR, "parameter ", name, " type is not the given type, will return null");
                MSGUser()->EndTitle();
                return T();
            }
        }
        else
        {
            MSGUser()->StartTitle("ConfigTool::GetPar");
            MSGUser()->MSG(MSGLevel::ERROR, "parameter ", name, " does not exist, will return null");
            MSGUser()->EndTitle();
            if (doexist != nullptr)
                *doexist = false;
            return T();
        }
    };

} // namespace NAGASH
