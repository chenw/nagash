//***************************************************************************************
/// @file PlotGroup.cxx
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#include "NAGASH/PlotGroup.h"

using namespace NAGASH;

/** @class NAGASH::PlotGroup PlotGroup.h "NAGASH/PlotGroup.h"
    @ingroup Histograms ResultClasses
    @brief Provide a base class for manipulating a group of histograms at the same time.

    Here is a simple demonstration of how to use PlotGroup:
    @anchor PlotGroup_example
    @code{.cpp}
    // demonstration of creating user's own plotgroup
    #pragma once

    #include "NAGASH.h"

    using namespace NAGASH;

    class WEvent_Skim
    {
    public:
        TLorentzVector Lepton;
        TLorentzVector MET;
        TLorentzVector Recoil;
        double WMT;
    };

    // you can call :
    // ResultGroupUser()->BookResult<WCommonPlot>("GroupName", "OutputFileName");
    // inside NAGASH::LoopEvent::InitialUser

    class WCommonPlot : public PlotGroup
    {
    public:
        WCommonPlot(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "");
        void FillPlots(const WEvent &);
        virtual ~WCommonPlot();

        std::shared_ptr<Plot1D> LeptonPt;
        std::shared_ptr<Plot1D> LeptonEta;
        std::shared_ptr<Plot1D> LeptonPhi;
        std::shared_ptr<Plot1D> MT;
        std::shared_ptr<Plot1D> MET;
        std::shared_ptr<Plot1D> WPT;
    };

    inline WCommonPlot::WCommonPlot(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname) : PlotGroup(MSG, c, rname, fname)
    {
        LeptonPt = BookPlot1D("LeptonPt", 150, 25, 100);
        LeptonEta = BookPlot1D("LeptonEta", 200, -2.5, 2.5);
        LeptonPhi = BookPlot1D("LeptonPhi", 200, -3.1415926, 3.1415926);
        MT = BookPlot1D("MT", 300, 0, 150);
        MET = BookPlot1D("MET", 300, 0, 150);
        WPT = BookPlot1D("WPT", 200, 0, 100);
    }

    inline WCommonPlot::FillPlots(const WEvent &event)
    {
        LeptonPt->Fill(event.Lepton.Pt());
        LeptonEta->Fill(event.Lepton.Eta());
        LeptonPhi->Fill(event.Lepton.Phi());
        MT->Fill(event.WMT);
        MET->Fill(event.MET.Pt());
        WPT->Fill(event.Result.Pt());
    }
    @endcode
 */

/// @brief Constructor, same as that of Result.
PlotGroup::PlotGroup(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname) : Result(MSG, c, rname, fname), maptool(MSG)
{
}

/// @brief Combine two PlotGroups.
void PlotGroup::Combine(std::shared_ptr<Result> result)
{
    MSGUser()->StartTitle("PlotGroup::Combine");
    auto subPlotGroup = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
    if (subPlotGroup != nullptr)
    {
        for (auto &sub : subPlotGroup->PlotMap)
        {
            auto findresult = this->PlotMap.find(sub.first);
            if (findresult != this->PlotMap.end())
            {
                findresult->second->Combine(sub.second);
            }
            else
            {
                auto subname = sub.first;
                auto newPlot = sub.second->CloneVirtual(this->GetResultName() + " " + subname);
                this->PlotMap.emplace(std::pair<TString, std::shared_ptr<HistBase>>(subname, newPlot));
            }
        }
    }
    MSGUser()->EndTitle();
}

/// @brief Call NGHist::Process() for each histogram inside this PlotGroup.
void PlotGroup::Process()
{
    for (auto &t : PlotVector)
        t->Process();
}

/// @brief Apply the given function to each histogram inside this PlotGroup.
/// @param func the function to be applied to each histogram.
void PlotGroup::ForEachPlot(std::function<void(std::shared_ptr<HistBase>)> func)
{
    for (auto &t : PlotVector)
        func(t);
}

/// @brief Call NGHist::SetUnprocessed() for each histogram inside this PlotGroup.
void PlotGroup::SetUnprocessed()
{
    for (auto &t : PlotVector)
        t->SetUnprocessed();
}

/// @brief Call NGHist::Scale() for each histogram inside this PlotGroup.
/// @param sf the scale factor.
void PlotGroup::Scale(double sf)
{
    for (auto &t : PlotVector)
        t->Scale(sf);
}

/// @brief Scale a variation of each histogram inside this PlotGroup.
/// @param varname the name of the variation.
/// @param sf the scale factor.
void PlotGroup::ScaleVariation(const TString &varname, double sf)
{
    auto titleguard = MSGUser()->StartTitleWithGuard("PlotGroup::ScaleVariation");
    for (auto &t : PlotVector)
    {
        if (auto h = t->GetVariationVirtual(varname); h)
            h->Scale(sf);
        else
            MSGUser()->MSG_WARNING("plot ", t->GetResultName(), " does not have variation ", varname, ", will not scale this variation");
    }
}

/// @brief Set link function for each histogram inside this PlotGroup.
void PlotGroup::SetLinkType(std::function<double(const std::vector<double> &)> cf, std::function<double(const std::vector<double> &, const std::vector<double> &)> ef)
{
    for (auto &t : PlotVector)
        t->SetLinkType(cf, ef);
}

/// @brief  Set the type of the link for each histogram inside this PlotGroup.
void PlotGroup::SetLinkType(const TString &type)
{
    for (auto &t : PlotVector)
        t->SetLinkType(type);
}

/// @brief  Set the type of the link for each histogram inside this PlotGroup.
void PlotGroup::SetLinkType(std::function<void(const std::vector<TH1 *> &, TH1 *)> hf)
{
    for (auto &t : PlotVector)
        t->SetLinkType(hf);
}

/// @brief  Set the linked plotgroup, should be the same structure of this one.
void PlotGroup::SetLinkPlotGroup(uint64_t index, std::shared_ptr<PlotGroup> pg)
{
    for (auto &t : pg->PlotMap)
    {
        auto findresult = this->PlotMap.find(t.first);
        if (findresult != this->PlotMap.end())
            findresult->second->SetLinkPlot(index, t.second);
    }
}

/// @brief  Clear the linked plotgroup.
void PlotGroup::ClearLinkPlotGroup()
{
    for (auto &t : PlotVector)
    {
        t->ClearLinkPlot();
    }
}

/// @brief Write all histograms inside this PlotGroup to a file.
void PlotGroup::WriteToFile()
{
    auto titleguard = MSGUser()->StartTitleWithGuard("Plot1D::WriteToFile");
    Process();
    if (GetOutputFileName() == "")
    {
        MSGUser()->MSG(MSGLevel::WARNING, "input an empty output file name, will be ignored");
        return;
    }

    TFile *file = new TFile(GetOutputFileName(), "UPDATE");
    file->cd();
    for (auto &t : PlotVector)
        t->Write();

    file->Close();
}

/// @brief Call NGHist::Reset() for each histogram inside this PlotGroup.
void PlotGroup::Reset()
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("PlotGroup::Reset");
    for (auto &t : PlotVector)
        t->Reset();
}

/// @brief Retrieve histograms from the input file.
/// @param filename the name of the input ROOT file.
void PlotGroup::Recover(const TString &filename)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("PlotGroup::Recover");
    TFile *file = new TFile(filename.TString::Data());
    if (file == nullptr)
    {
        MSGUser()->MSG_ERROR("ROOT file ", filename, " does not exist");
        return;
    }

    MSGUser()->MSG_DEBUG("Recover ", GetResultName(), " from file ", filename);
    Recover(file);
    file->Close();
}

/// @brief Retrieve histograms from the input file.
/// @param file the pointer to the ROOT TFile.
void PlotGroup::Recover(TFile *file)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("PlotGroup::Recover");
    if (file == nullptr)
    {
        MSGUser()->MSG_ERROR("ROOT file does not exist");
        return;
    }

    for (auto &t : PlotVector)
        t->Recover(file);
}

/// @brief Retrieve histograms from the input file.
/// @param file the pointer to the TFileHelper.
void PlotGroup::Recover(std::shared_ptr<TFileHelper> file)
{
    auto titlegurad = MSGUser()->StartTitleWithGuard("PlotGroup::Recover");
    for (auto &t : PlotVector)
        t->Recover(file);
}

/// @brief Clone this PlotGroup.
/// @param name the name of the new PlotGroup.
std::shared_ptr<PlotGroup> PlotGroup::Clone(const TString &name)
{
    std::shared_ptr<PlotGroup> newPlotGroup = std::make_shared<PlotGroup>(MSGUser(), ConfigUser(), name, GetOutputFileName());
    for (auto &t : PlotMap)
    {
        auto newplot = t.second->CloneVirtual(name + "_" + t.first);
        newPlotGroup->PlotMap.insert(std::pair<TString, std::shared_ptr<HistBase>>(t.first, newplot));
        PlotVector.emplace_back(newplot);
    }

    return newPlotGroup;
}

/// @brief Get the number of histograms stored inside this PlotGroup.
int PlotGroup::GetNPlots()
{
    return PlotVector.size();
}

/// @brief Book systematic variations for each histogram inside this PlotGroup.
/// @param name name of the systematic variation.
/// @param corr_factor correlation factor.
/// @param policy policy to deal with the systematic variation.
void PlotGroup::BookSystematicVariation(const TString &name, double corr_factor, HistBase::SystPolicy policy)
{
    for (auto &t : PlotVector)
        t->BookSystematicVariation(name, corr_factor, policy);
}

/// @brief Book systematic variations for each histogram inside this PlotGroup.
/// @param name1 name of the first systematic variation.
/// @param name2 name of the second systematic variation.
/// @param corr_factor correlation factor.
/// @param policy policy to deal with the systematic variations.
void PlotGroup::BookSystematicVariation(const TString &name1, const TString &name2, double corr_factor, HistBase::SystPolicy policy)
{
    for (auto &t : PlotVector)
        t->BookSystematicVariation(name1, name2, corr_factor, policy);
}

/// @brief Book systematic variations for each histogram inside this PlotGroup.
/// @param names vector of names of the systematic variations.
/// @param corr_factor correlation factor.
/// @param policy policy to deal with the systematic variations.
void PlotGroup::BookSystematicVariation(const std::vector<TString> &names, double corr_factor, HistBase::SystPolicy policy)
{
    for (auto &t : PlotVector)
        t->BookSystematicVariation(names, corr_factor, policy);
}

/// @brief Call NGHist::RegroupSystematicVariation for each histogram inside this PlotGroup.
void PlotGroup::RegroupSystematicVariation(const std::vector<TString> &names, HistBase::SystPolicy policy)
{
    for (auto &t : PlotVector)
        t->RegroupSystematicVariation(names, policy);
}

/// @brief Call NGHist::RemoveSystematicVariation for each histogram inside this PlotGroup.
void PlotGroup::RemoveSystematicVariation(const TString &name)
{
    for (auto &t : PlotVector)
        t->RemoveSystematicVariation(name);
}

/// @brief  Call NGHist::RenameSystematicVariation for each histogram inside this PlotGroup.
void PlotGroup::RenameSystematicVariation(const TString &name_old, const TString &name_new)
{
    for (auto &t : PlotVector)
        t->RenameSystematicVariation(name_old, name_new);
}

/// @brief Set the systematic variation for each histogram inside this PlotGroup.
/// @param name the name of the systematic variation.
void PlotGroup::SetSystematicVariation(const TString &name)
{
    for (auto &t : PlotVector)
        t->SetSystematicVariation(name);
}

/// @brief Set the systematic variation for each histogram inside this PlotGroup.
/// @param index index of the systematic variation.
void PlotGroup::SetSystematicVariation(int index)
{
    for (auto &t : PlotVector)
        t->SetSystematicVariation(index);
}
