//***************************************************************************************
/// @file NGHist.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/HistBase.h"

namespace NAGASH
{
    /** @class NAGASH::NGHist NGHist.h "NAGASH/NGHist.h"
        @ingroup Histograms ResultClasses
        @brief NAGASH interface for using ROOT histograms.
    */
    template <typename HistType>
    class NGHist : public HistBase
    {
        friend class PlotGroup;

    private:
        HistType *FillHist = nullptr;
        HistType *Nominal = nullptr;

        std::map<TString, int> SystematicIndexMap;
        std::vector<TString> SystematicVariationNames;
        std::vector<HistType *> SystematicVariations;
        std::vector<double> CorrelationFactors;
        std::vector<std::vector<size_t>> SystematicVariationIndices;
        std::vector<SystPolicy> SystematicVariationPolicies;
        std::map<TString, int> SystematicVariationBackup;

        HistType *Final = nullptr;
        TH2D *Covariance = nullptr;
        TH2D *Correlation = nullptr;
        bool Processed = false;

        // hist link
        std::vector<std::shared_ptr<HistBase>> LinkedPlots;
        TString LinkType = "";
        std::function<double(const std::vector<double> &)> LinkContentFunction;
        std::function<double(const std::vector<double> &, const std::vector<double> &)> LinkErrorFunction;
        std::function<void(const std::vector<TH1 *> &, TH1 *)> LinkHistFunction;

    protected:
        std::shared_ptr<HistBase> CloneVirtual(const TString &name) override;

    public:
        /// @brief Fill the current variation using TH1::Fill().
        template <typename... Args>
        void Fill(Args &&...args) { FillHist->Fill(std::forward<Args>(args)...); }

        /// @brief Get the pointer to the nominal variation histogram.
        HistType *GetNominalHist() { return Nominal; }

        /// @brief Get the pointer to the current variation histogram.
        HistType *GetFillHist() { return FillHist; }

        /// @brief Get the number of systematic variations.
        size_t GetNVariations() override { return SystematicVariationNames.size(); }

        /// @brief Get the pointer to the final histogram, whose contents are nominal contents and errors are the total uncertainties.
        HistType *GetFinalHist()
        {
            if (!Final)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::GetFinalHist");
                MSGUser()->MSG_WARNING("Plot ", GetResultName(), " has not been processed, please call NGHist::Process. Returning nullptr");
            }
            return Final;
        }

        /// @brief Get the pointer to the covariance histogram.
        TH2D *GetCovariance()
        {
            if (!Covariance)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::GetCovariance");
                MSGUser()->MSG_WARNING("Plot ", GetResultName(), " has not been processed, please call NGHist::Process. Returning nullptr");
            }
            return Covariance;
        }

        /// @brief Get the pointer to the correlation histogram.
        TH2D *GetCorrelation()
        {
            if (!Correlation)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::GetCorrelation");
                MSGUser()->MSG_WARNING("Plot ", GetResultName(), " has not been processed, please call NGHist::Process. Returning nullptr");
            }
            return Correlation;
        }

        /// @brief Set bin content of the current variation using TH1::SetBinContent().
        template <typename... Args>
        void SetBinContent(Args &&...args) { FillHist->SetBinContent(std::forward<Args>(args)...); }
        /// @brief Set bin error of the current variation using TH1::SetBinError().
        template <typename... Args>
        void SetBinError(Args &&...args) { FillHist->SetBinError(std::forward<Args>(args)...); }
        /// @brief Get the bin content of the current variation using TH1::GetBinContent().
        template <typename... Args>
        double GetBinContent(Args &&...args) { return FillHist->GetBinContent(std::forward<Args>(args)...); }
        /// @brief Get the bin error of the current variation using TH1::GetBinError().
        template <typename... Args>
        double GetBinError(Args &&...args) { return FillHist->GetBinError(std::forward<Args>(args)...); }
        /// @brief Get the bin error of the nominal histogram using TH1::GetBinError().
        template <typename... Args>
        double GetBinNominalError(Args &&...args) { return Nominal->GetBinError(std::forward<Args>(args)...); }

        /// @brief Get the total bin error of the final histogram using TH1::GetBinError().
        template <typename... Args>
        double GetBinTotalError(Args &&...args)
        {
            if (Final != nullptr)
                return Final->GetBinError(std::forward<Args>(args)...);
            else
                return Nominal->GetBinError(std::forward<Args>(args)...);
        }

        /// @brief Get the total bin error of the final histogram using TH1::GetBinError().
        template <typename... Args>
        double GetBinFinalError(Args &&...args)
        {
            if (Final != nullptr)
                return Final->GetBinError(std::forward<Args>(args)...);
            else
                return Nominal->GetBinError(std::forward<Args>(args)...);
        }

        /// @brief Get the total systematic error of ith bin.
        template <typename... Args>
        double GetBinSystematicError(Args &&...args)
        {
            if (Final != nullptr)
            {
                double finalerror = Final->GetBinError(std::forward<Args>(args)...);
                double staterror = Nominal->GetBinError(std::forward<Args>(args)...);
                return std::sqrt(finalerror * finalerror - staterror * staterror);
            }
            else
                return 0;
        }

        /// @brief Get the name of the ith systematic variation.
        TString GetVariationName(uint64_t index) override
        {
            if (index == 0)
                return "Nominal";
            if (index > SystematicVariationNames.size() || index < 0)
                return "UNKNOWN";
            else
                return SystematicVariationNames[index - 1];
        }

        /// @brief Get the vector of the name of all systematic variations.
        std::vector<TString> GetVariationNames()
        {
            return SystematicVariationNames;
        }

        /// @brief Get the pointer to the variation histogram based on the name
        /// @param name the name of the systematic variation
        HistType *GetVariation(const TString &name)
        {
            if (name == "Nominal")
                return Nominal;
            if (auto findresult = SystematicIndexMap.find(name); findresult != SystematicIndexMap.end())
            {
                return SystematicVariations[findresult->second];
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("NGHist::GetVariation");
                MSGUser()->MSG_WARNING("NGHist ", GetResultName(), " does not have variation ", name, ", returning nullptr");
                return nullptr;
            }
        }

        /// @brief Get the pointer to the variation histogram based on the index
        /// @param index the index of the systematic variation
        HistType *GetVariation(uint64_t index)
        {
            if (index == 0)
                return Nominal;
            else if (index >= 1 && index <= SystematicVariations.size())
                return SystematicVariations[index - 1];
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("NGHist::GetVariation");
                MSGUser()->MSG_WARNING("NGHist ", GetResultName(), " does not have variation ", index, ", returning nullptr");
                return nullptr;
            }
        }

        /// @brief Get the correlation factor for the systematic variation with the given name.
        /// @param name name of the systematic variation.
        double GetVariationCorrelationFactor(const TString &name) override
        {
            if (auto findresult = SystematicIndexMap.find(name); findresult != SystematicIndexMap.end())
            {
                return CorrelationFactors[findresult->second];
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("NGHist::GetVariationCorrelationFactor");
                MSGUser()->MSG_ERROR("NGHist ", GetResultName(), " does not have variation ", name, ", returning zero");
                return 0;
            }
        }

        /// @brief Destructor
        virtual ~NGHist()
        {
            if (Nominal)
                delete Nominal;

            for (auto &h : SystematicVariations)
                if (h)
                    delete h;

            if (Processed)
            {
                if (Final)
                    delete Final;
                if (Covariance)
                    delete Covariance;
                if (Correlation)
                    delete Correlation;
            }
        }

        // default constructor
        template <typename... Args>
        NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const Args &...args);
        // auxiliary constructor
        NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name);

        // helper function
        std::vector<std::vector<double>> GetPartialCovariance(const std::vector<TString> &systlist);
        std::shared_ptr<NGHist<HistType>> Rebin(const TString &name, int rebinnum);
        std::shared_ptr<NGHist<HistType>> Rebin2D(const TString &name, int rebinnum_x, int rebinum_y);
        std::shared_ptr<NGHist<HistType>> Rebin3D(const TString &name, int rebinnum_x, int rebinum_y, int rebinum_z);
        std::shared_ptr<NGHist<HistType>> Rebin(const TString &name, std::vector<double> bindiv);
        std::shared_ptr<NGHist<HistType>> Rebin2D(const TString &name, std::vector<double> bindiv_x, std::vector<double> bindiv_y);
        std::shared_ptr<NGHist<HistType>> Rebin3D(const TString &name, std::vector<double> bindiv_x, std::vector<double> bindiv_y, std::vector<double> bindiv_z);

        // function inherited from HistBase
        bool BookSystematicVariation(const TString &name, double corr_factor = 0, SystPolicy policy = SystPolicy::Maximum) override;
        bool BookSystematicVariation(const TString &name1, const TString &name2, double corr_factor = 0, SystPolicy policy = SystPolicy::Maximum) override;
        bool BookSystematicVariation(const std::vector<TString> &names, double corr_factor = 0, SystPolicy policy = SystPolicy::Maximum) override;
        void ClearLinkPlot() override;
        std::shared_ptr<NGHist<HistType>> Clone(const TString &name);
        void Combine(std::shared_ptr<Result> result) override;
        TH1 *GetNominalHistVirtual() override;
        std::vector<std::tuple<std::vector<TString>, double, SystPolicy>> GetVariationTypes() override;
        TH1 *GetVariationVirtual(const TString &name) override;
        TH1 *GetVariationVirtual(uint64_t index) override;
        bool IsBackupVariation(uint64_t) override;
        bool IsBackupVariation(const TString &) override;
        bool Process() override;
        bool Recover() override;
        bool Recover(TFile *file) override;
        bool Recover(const TString &filename) override;
        bool Recover(std::shared_ptr<TFileHelper>) override;
        bool RegroupSystematicVariation(const std::vector<TString> &names, SystPolicy policy) override;
        bool RemoveSystematicVariation(const TString &name) override;
        bool RenameSystematicVariation(const TString &name_old, const TString &name_new) override;
        void Reset() override;
        void Scale(double) override;
        void SetLinkPlot(uint64_t index, std::shared_ptr<HistBase> subplot) override;
        void SetLinkType(const TString &type) override;
        void SetLinkType(std::function<double(const std::vector<double> &)>, std::function<double(const std::vector<double> &, const std::vector<double> &)>) override;
        void SetLinkType(std::function<void(const std::vector<TH1 *> &, TH1 *)>) override;
        bool SetSystematicVariation(const TString &name = "Nominal") override;
        bool SetSystematicVariation(uint64_t index) override;
        void SetUnprocessed() override;
        void Write() override;
        void WriteToFile() override;

        /// @brief Calculate the variance of the given vector
        static double Variance(const std::vector<double> &v)
        {
            if (v.size() == 0)
                return 0;

            double mean = 0;
            double mean_square = 0;

            for (auto &value : v)
            {
                mean += value;
                mean_square += value * value;
            }

            mean /= v.size();
            mean_square /= v.size();

            return (mean_square - mean * mean);
        }
    };

    // protected function
    template <typename HistType>
    inline std::shared_ptr<HistBase> NGHist<HistType>::CloneVirtual(const TString &name)
    {
        std::shared_ptr<NGHist<HistType>> newPlot = std::shared_ptr<NGHist<HistType>>(new NGHist<HistType>(MSGUser(), ConfigUser(), name));
        newPlot->SetOutputFileName(GetOutputFileName());
        delete newPlot->Nominal;
        newPlot->Nominal = nullptr;
        newPlot->Nominal = (HistType *)this->Nominal->Clone(name);
        newPlot->FillHist = newPlot->Nominal;

        // copy systs
        newPlot->SystematicIndexMap = this->SystematicIndexMap;
        newPlot->SystematicVariationNames = this->SystematicVariationNames;
        newPlot->CorrelationFactors = this->CorrelationFactors;
        newPlot->SystematicVariationIndices = this->SystematicVariationIndices;
        newPlot->SystematicVariationPolicies = this->SystematicVariationPolicies;
        newPlot->SystematicVariationBackup = this->SystematicVariationBackup;

        for (int i = 0; i < this->SystematicVariationNames.size(); i++)
            newPlot->SystematicVariations.emplace_back((HistType *)(this->SystematicVariations[i]->Clone(name + "_Syst_" + this->SystematicVariationNames[i])));

        // copy links
        newPlot->LinkedPlots = this->LinkedPlots;
        newPlot->LinkType = this->LinkType;
        newPlot->LinkContentFunction = this->LinkContentFunction;
        newPlot->LinkErrorFunction = this->LinkErrorFunction;
        newPlot->LinkHistFunction = this->LinkHistFunction;

        // copy processed plots
        if (Processed)
        {
            newPlot->Final = (HistType *)this->Final->Clone("Final_" + name);
            if (this->Covariance)
                newPlot->Covariance = (TH2D *)this->Covariance->Clone("Covariance_" + name);
            if (this->Correlation)
                newPlot->Correlation = (TH2D *)this->Correlation->Clone("Correlation_" + name);
            newPlot->Processed = true;
        }

        return newPlot;
    }

    /// @brief Constructor.
    /// @tparam ...Args types of the binning info.
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name name of the histogram.
    /// @param filename name of the output file.
    /// @param ...args binning info, should be compatible with ROOT histograms.
    template <typename HistType>
    template <typename... Args>
    inline NGHist<HistType>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const Args &...args)
        : HistBase(MSG, c, name, filename)
    {
        static_assert(std::is_base_of<TH1, HistType>::value, "NGHist : the template type must be inherited from ROOT::TH1");
        constexpr size_t argcount = sizeof...(Args);
        if constexpr (argcount != 0)
            Nominal = new HistType(GetResultName(), GetResultName(), args...);
        else // if no more arguments provided, use the default constructor of TH1
            Nominal = new HistType();
        Nominal->SetDirectory(0);
        Nominal->Sumw2();
        FillHist = Nominal;
    }

    /// @brief Auxliary constructor, construct an empty NGHist object, suitable for Recover().
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name name of the NGHist object.
    template <typename HistType>
    inline NGHist<HistType>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name) : HistBase(MSG, c, name, "")
    {
        static_assert(std::is_base_of<TH1, HistType>::value, "NGHist : the template type must be inherited from ROOT::TH1");
        Nominal = new HistType();
        Nominal->SetDirectory(0);
        FillHist = Nominal;
    }

    /// @brief Get the covariance matrix for the given list of systematic variations.
    /// @param systlist vector of systematic variation names.
    /// @return covariance matrix.
    template <typename HistType>
    inline std::vector<std::vector<double>> NGHist<HistType>::GetPartialCovariance(const std::vector<TString> &systlist)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::GetPartialCovariance");

        // find the indices
        bool is_contain_nominal = false;
        std::vector<size_t> inputsystindices;
        for (size_t i = 0; i < systlist.size(); i++)
        {
            auto findresult = SystematicIndexMap.find(systlist[i]);
            if (findresult != SystematicIndexMap.end())
                inputsystindices.emplace_back(findresult->second);
            else if (systlist[i] != "Nominal")
                MSGUser()->MSG_WARNING("Syst ", systlist[i], " does not exist in plot ", GetResultName(), ", will skip that.");
            else
                is_contain_nominal = true;

            // check for backup
            findresult = SystematicVariationBackup.find(systlist[i]);
            if (findresult != SystematicVariationBackup.end())
                MSGUser()->MSG_WARNING("Syst ", systlist[i], " is in the backup list of plot ", GetResultName(), ", will skip that.");
        }

        // sort and remove duplicates
        std::sort(inputsystindices.begin(), inputsystindices.end());
        auto last = std::unique(inputsystindices.begin(), inputsystindices.end());
        inputsystindices.erase(last, inputsystindices.end());

        // check and get the correct syst pair
        std::vector<std::vector<size_t>> input_syst_indices;
        std::vector<SystPolicy> input_syst_policies;
        int systcount = 0;
        for (auto &indices : this->SystematicVariationIndices)
        {
            for (auto &index : indices)
                if (std::binary_search(inputsystindices.begin(), inputsystindices.end(), index))
                {
                    input_syst_indices.emplace_back(indices);
                    input_syst_policies.emplace_back(SystematicVariationPolicies[systcount]);
                    break;
                }
            systcount++;
        }

        // calculate cov
        int Nbins = Nominal->GetNbinsX() * Nominal->GetNbinsY() * Nominal->GetNbinsY();
        auto histtool = ToolkitUser()->template GetTool<HistTool>();
        auto convert2vector = [=](TH1 *h)
        {
            std::vector<double> content;
            content.reserve(Nbins);
            for (int i = 1; i <= h->GetXaxis()->GetNbins(); i++)
                for (int j = 1; j <= h->GetYaxis()->GetNbins(); j++)
                    for (int k = 1; k <= h->GetZaxis()->GetNbins(); k++)
                        content.emplace_back(h->GetBinContent(i, j, k));
            return content;
        };

        std::vector<std::vector<double>> cov(Nbins, std::vector<double>(Nbins, 0));
        std::vector<double> nominal_content = convert2vector(Nominal);

        systcount = 0;
        for (auto &indices : input_syst_indices)
        {
            double corr = CorrelationFactors[indices[0]];
            SystPolicy policy = input_syst_policies[systcount];
            std::vector<std::vector<double>> syst_contents;
            for (auto &index : indices)
                syst_contents.emplace_back(convert2vector(SystematicVariations[index]));

            std::vector<double> vdeltai(indices.size());
            std::vector<double> vdeltaj(indices.size());
            std::vector<double> vsystcontenti(indices.size());
            std::vector<double> vsystcontentj(indices.size());

            for (int i = 0; i < Nbins; i++)
            {
                for (int j = 0; j < Nbins; j++)
                {
                    double deltai, deltaj;
                    int signi = syst_contents[0][i] - nominal_content[i] > 0 ? 1 : -1;
                    int signj = syst_contents[0][j] - nominal_content[j] > 0 ? 1 : -1;
                    for (int k = 0; k < indices.size(); k++)
                    {
                        vdeltai[k] = fabs(syst_contents[k][i] - nominal_content[i]);
                        vdeltaj[k] = fabs(syst_contents[k][j] - nominal_content[j]);
                        vsystcontenti[k] = syst_contents[k][i];
                        vsystcontentj[k] = syst_contents[k][j];
                    }

                    if (policy == SystPolicy::Maximum)
                    {
                        deltai = *std::max_element(vdeltai.begin(), vdeltai.end());
                        deltaj = *std::max_element(vdeltaj.begin(), vdeltaj.end());
                    }
                    else if (policy == SystPolicy::Average)
                    {
                        deltai = std::accumulate(vdeltai.begin(), vdeltai.end(), (double)0) / indices.size();
                        deltaj = std::accumulate(vdeltaj.begin(), vdeltaj.end(), (double)0) / indices.size();
                    }
                    else if (policy == SystPolicy::Variance)
                    {
                        deltai = std::sqrt(std::fabs(Variance(vsystcontenti)));
                        deltaj = std::sqrt(std::fabs(Variance(vsystcontentj)));
                    }
                    else if (policy == SystPolicy::CTEQ)
                    {
                        deltai = (vsystcontenti[0] - vsystcontenti[1]) / 2;
                        deltaj = (vsystcontentj[0] - vsystcontentj[1]) / 2;
                        signi = 1;
                        signj = 1;
                    }

                    if (i != j)
                        cov[i][j] += signi * signj * deltai * deltaj * corr;
                    else
                        cov[i][j] += signi * signj * deltai * deltaj;
                }
            }

            systcount++;
        }

        // add nominal stat unc. if it is given
        if (is_contain_nominal)
        {
            std::vector<double> nominal_error;
            nominal_error.reserve(Nbins);
            for (int i = 1; i <= Nominal->GetXaxis()->GetNbins(); i++)
                for (int j = 1; j <= Nominal->GetYaxis()->GetNbins(); j++)
                    for (int k = 1; k <= Nominal->GetZaxis()->GetNbins(); k++)
                        nominal_error.emplace_back(Nominal->GetBinError(i, j, k));

            for (int i = 0; i < Nbins; i++)
                cov[i][i] += nominal_error[i] * nominal_error[i];
        }

        return cov;
    }

    /// @brief Rebin 1D histogram with the given rebin number, store the result in a new NGHist. Note that the link info will be cleared in the new NGHist.
    /// @param name Name of the new NGHist.
    /// @param rebinnum Number of bins to be combined.
    /// @return Rebinned NGHist.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Rebin(const TString &name, int rebinnum)
    {
        static_assert(!std::is_base_of<TH2, HistType>::value, "NGHist : function Rebin is not supported for 2D histograms");
        static_assert(!std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin is not supported for 3D histograms");

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Rebin");

        if (rebinnum <= 0)
        {
            MSGUser()->MSG_ERROR("Rebin number is not legal(", rebinnum, "), will do nothing for plot ", GetResultName());
            return nullptr;
        }

        if (!Processed)
            Process();

        auto newh = this->Clone(name);
        newh->ClearLinkPlot();
        newh->SetUnprocessed();

        newh->Nominal->Rebin(rebinnum);
        for (auto &h : newh->SystematicVariations)
            h->Rebin(rebinnum);

        newh->Process();

        return newh;
    }

    /// @brief Rebin 2D histogram with the given rebin numbers, store the result in a new NGHist. Note that the link info will be cleared in the new NGHist.
    /// @param name Name of the new NGHist.
    /// @param rebinnum_x Number of bins to be combined in x-axis.
    /// @param rebinnum_y Number of bins to be combined in y-axis.
    /// @return Rebinned NGHist.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Rebin2D(const TString &name, int rebinnum_x, int rebinnum_y)
    {
        static_assert(!std::is_base_of<TH1, HistType>::value || std::is_base_of<TH2, HistType>::value, "NGHist : function Rebin2D is not supported for 1D histograms");
        static_assert(!std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin2D is not supported for 3D histograms");

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Rebin2D");

        if (rebinnum_x <= 0 || rebinnum_y <= 0)
        {
            MSGUser()->MSG_ERROR("Rebin number is not legal, will do nothing for plot ", GetResultName());
            return nullptr;
        }

        if (!Processed)
            Process();

        auto newh = this->Clone(name);
        newh->ClearLinkPlot();
        newh->SetUnprocessed();

        newh->Nominal->Rebin(rebinnum_x, rebinnum_y);
        for (auto &h : newh->SystematicVariations)
            h->Rebin(rebinnum_x, rebinnum_y);

        newh->Process();

        return newh;
    }

    /// @brief Rebin 3D histogram with the given rebin numbers, store the result in a new NGHist. Note that the link info will be cleared in the new NGHist.
    /// @param name Name of the new NGHist.
    /// @param rebinnum_x Number of bins to be combined in x-axis.
    /// @param rebinnum_y Number of bins to be combined in y-axis.
    /// @param rebinnum_z Number of bins to be combined in z-axis.
    /// @return Rebinned NGHist.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Rebin3D(const TString &name, int rebinnum_x, int rebinnum_y, int rebinnum_z)
    {
        static_assert(!std::is_base_of<TH1, HistType>::value || std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin3D is not supported for 1D histograms");
        static_assert(!std::is_base_of<TH2, HistType>::value || std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin3D is not supported for 2D histograms");

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Rebin3D");

        if (rebinnum_x <= 0 || rebinnum_y <= 0 || rebinnum_z <= 0)
        {
            MSGUser()->MSG_ERROR("Rebin number is not legal, will do nothing for plot ", GetResultName());
            return nullptr;
        }

        if (!Processed)
            Process();

        auto newh = this->Clone(name);
        newh->ClearLinkPlot();
        newh->SetUnprocessed();

        newh->Nominal->Rebin(rebinnum_x, rebinnum_y, rebinnum_z);
        for (auto &h : newh->SystematicVariations)
            h->Rebin(rebinnum_x, rebinnum_y, rebinnum_z);

        newh->Process();

        return newh;
    }

    /// @brief Rebin with the given bin division, store the result in a new NGHist. Note that the link info will be cleared in the new NGHist.
    /// @param name Name of the new NGHist.
    /// @param bindiv Bin divisions of the new binning
    /// @return Rebinned NGHist.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Rebin(const TString &name, std::vector<double> bindiv)
    {
        static_assert(!std::is_base_of<TH2, HistType>::value, "NGHist : function Rebin is not supported for 2D histograms");
        static_assert(!std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin is not supported for 3D histograms");

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Rebin");

        if (bindiv.size() == 0)
        {
            MSGUser()->MSG_ERROR("No bin division is given, will do nothing for plot ", GetResultName());
            return nullptr;
        }

        if (!Processed)
            Process();

        std::shared_ptr<NGHist<HistType>> newPlot = std::shared_ptr<NGHist<HistType>>(new NGHist<HistType>(MSGUser(), ConfigUser(), name));
        newPlot->SetOutputFileName(GetOutputFileName());
        delete newPlot->Nominal;
        newPlot->Nominal = nullptr;
        newPlot->Nominal = (HistType *)this->Nominal->Rebin(bindiv.size() - 1, name, bindiv.data());
        newPlot->FillHist = newPlot->Nominal;

        // copy systs
        newPlot->SystematicIndexMap = this->SystematicIndexMap;
        newPlot->SystematicVariationNames = this->SystematicVariationNames;
        newPlot->CorrelationFactors = this->CorrelationFactors;
        newPlot->SystematicVariationIndices = this->SystematicVariationIndices;
        newPlot->SystematicVariationPolicies = this->SystematicVariationPolicies;
        newPlot->SystematicVariationBackup = this->SystematicVariationBackup;

        for (int i = 0; i < this->SystematicVariationNames.size(); i++)
            newPlot->SystematicVariations.emplace_back((HistType *)(this->SystematicVariations[i]->Rebin(bindiv.size() - 1, name + "_Syst_" + this->SystematicVariationNames[i], bindiv.data())));

        newPlot->Process();

        return newPlot;
    }

    /// @brief Rebin 2D histogram with the given bin division, store the result in a new NGHist. Note that the link info will be cleared in the new NGHist.
    /// @param name Name of the new NGHist.
    /// @param bindiv_x Bin divisions of the new binning in x-axis (empty vector for not rebinning)
    /// @param bindiv_y Bin divisions of the new binning in y-axis (empty vector for not rebinning)
    /// @return Rebinned NGHist.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Rebin2D(const TString &name, std::vector<double> bindiv_x, std::vector<double> bindiv_y)
    {
        static_assert(!std::is_base_of<TH1, HistType>::value || std::is_base_of<TH2, HistType>::value, "NGHist : function Rebin2D is not supported for 1D histograms");
        static_assert(!std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin2D is not supported for 3D histograms");

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Rebin2D");

        if (bindiv_x.size() == 0 && bindiv_y.size() == 0)
        {
            MSGUser()->MSG_ERROR("No bin division is given, will do nothing for plot ", GetResultName());
            return nullptr;
        }
        int nbinX, nbinY;
        bool fixX = false, fixY = false;
        double minX, maxX, minY, maxY;
        if (bindiv_x.size() == 0)
        {
            auto bindivarr = Nominal->GetXaxis()->GetXbins()->GetArray();
            nbinX = Nominal->GetXaxis()->GetNbins();
            if (bindivarr)
                bindiv_x = std::vector<double>(bindivarr, bindivarr + nbinX + 1);
            else
            {
                minX = Nominal->GetXaxis()->GetBinLowEdge(1);
                maxX = Nominal->GetXaxis()->GetBinUpEdge(nbinX);
                fixX = true;
            }
        }
        if (bindiv_y.size() == 0)
        {
            auto bindivarr = Nominal->GetYaxis()->GetXbins()->GetArray();
            nbinY = Nominal->GetYaxis()->GetNbins();
            if (bindivarr)
                bindiv_y = std::vector<double>(bindivarr, bindivarr + nbinY + 1);
            else
            {
                minY = Nominal->GetYaxis()->GetBinLowEdge(1);
                maxY = Nominal->GetYaxis()->GetBinUpEdge(nbinY);
                fixY = true;
            }
        }

        // check bin edge
        if (!fixX)
            for (int newX = 0, oldX = 1; newX < bindiv_x.size(); newX++)
            {
                if (bindiv_x.at(newX) < Nominal->GetXaxis()->GetBinLowEdge(1))
                    continue;
                if (bindiv_x.at(newX) > Nominal->GetXaxis()->GetBinUpEdge(Nominal->GetXaxis()->GetNbins()))
                    break;
                if (!TMath::AreEqualAbs(Nominal->GetXaxis()->GetBinLowEdge(oldX), bindiv_x.at(newX), TMath::Max(1.E-8 * Nominal->GetXaxis()->GetBinWidth(oldX), 1.E-16)) && Nominal->GetXaxis()->GetBinLowEdge(oldX) > bindiv_x.at(newX))
                    MSGUser()->MSG_WARNING("Bin X edge ", newX + 1, " of rebinned histogram does not match any bin edges of the old histogram. Result can be inconsistent");
                else
                    oldX++;
            }
        if (!fixY)
            for (int newY = 0, oldY = 1; newY < bindiv_y.size(); newY++)
            {
                if (bindiv_y.at(newY) < Nominal->GetYaxis()->GetBinLowEdge(1))
                    continue;
                if (bindiv_y.at(newY) > Nominal->GetYaxis()->GetBinUpEdge(Nominal->GetYaxis()->GetNbins()))
                    break;
                if (!TMath::AreEqualAbs(Nominal->GetYaxis()->GetBinLowEdge(oldY), bindiv_y.at(newY), TMath::Max(1.E-8 * Nominal->GetYaxis()->GetBinWidth(oldY), 1.E-16)) && Nominal->GetYaxis()->GetBinLowEdge(oldY) > bindiv_y.at(newY))
                    MSGUser()->MSG_WARNING("Bin Y edge ", newY + 1, " of rebinned histogram does not match any bin edges of the old histogram. Result can be inconsistent");
                else
                    oldY++;
            }

        if (!Processed)
            Process();

        // rebin algorithm
        auto Rebin = [](HistType *in, HistType *out)
        {
            auto xAxis = in->GetXaxis();
            auto yAxis = in->GetYaxis();
            for (int i = 1; i <= xAxis->GetNbins(); i++)
                for (int j = 1; j <= yAxis->GetNbins(); j++)
                {
                    double binCenterX = xAxis->GetBinCenter(i);
                    double binCenterY = yAxis->GetBinCenter(j);
                    int binX = out->GetXaxis()->FindBin(binCenterX);
                    int binY = out->GetYaxis()->FindBin(binCenterY);
                    out->SetBinContent(binX, binY, out->GetBinContent(binX, binY) + in->GetBinContent(i, j));
                    out->SetBinError(binX, binY, Uncertainty::AplusB(out->GetBinError(binX, binY), in->GetBinError(i, j)));
                }
        };

        std::shared_ptr<NGHist<HistType>> newPlot;
        if (fixX)
            newPlot = std::make_shared<NGHist<HistType>>(MSGUser(), ConfigUser(), name, GetOutputFileName(), nbinX, minX, maxX, bindiv_y.size() - 1, bindiv_y.data());
        else if (fixY)
            newPlot = std::make_shared<NGHist<HistType>>(MSGUser(), ConfigUser(), name, GetOutputFileName(), bindiv_x.size() - 1, bindiv_x.data(), nbinY, minY, maxY);
        else
            newPlot = std::make_shared<NGHist<HistType>>(MSGUser(), ConfigUser(), name, GetOutputFileName(), bindiv_x.size() - 1, bindiv_x.data(), bindiv_y.size() - 1, bindiv_y.data());

        Rebin(this->Nominal, newPlot->Nominal);
        newPlot->FillHist = newPlot->Nominal;

        // copy systs
        newPlot->SystematicIndexMap = this->SystematicIndexMap;
        newPlot->SystematicVariationNames = this->SystematicVariationNames;
        newPlot->CorrelationFactors = this->CorrelationFactors;
        newPlot->SystematicVariationIndices = this->SystematicVariationIndices;
        newPlot->SystematicVariationPolicies = this->SystematicVariationPolicies;
        newPlot->SystematicVariationBackup = this->SystematicVariationBackup;

        newPlot->SystematicVariations.resize(this->SystematicVariations.size());
        for (int i = 0; i < this->SystematicVariationNames.size(); i++)
        {
            newPlot->SystematicVariations.at(i) = (HistType *)newPlot->Nominal->Clone(name + "_Syst_" + newPlot->SystematicVariationNames.at(i));
            Rebin(this->SystematicVariations.at(i), newPlot->SystematicVariations.at(i));
        }

        newPlot->Process();

        return newPlot;
    }

    /// @brief Rebin 3D histogram with the given bin division, store the result in a new NGHist.
    /// @param name Name of the new NGHist.
    /// @param bindiv_x Bin divisions of the new binning in x-axis (empty vector for not rebinning)
    /// @param bindiv_y Bin divisions of the new binning in y-axis (empty vector for not rebinning)
    /// @param bindiv_z Bin divisions of the new binning in z-axis (empty vector for not rebinning)
    /// @return Rebinned NGHist.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Rebin3D(const TString &name, std::vector<double> bindiv_x, std::vector<double> bindiv_y, std::vector<double> bindiv_z)
    {
        static_assert(!std::is_base_of<TH1, HistType>::value || std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin3D is not supported for 1D histograms");
        static_assert(!std::is_base_of<TH2, HistType>::value || std::is_base_of<TH3, HistType>::value, "NGHist : function Rebin3D is not supported for 2D histograms");

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Rebin3D");

        if (bindiv_x.size() == 0 && bindiv_y.size() == 0 && bindiv_z.size() == 0)
        {
            MSGUser()->MSG_ERROR("No bin division is given, will do nothing for plot ", GetResultName());
            return nullptr;
        }
        if (bindiv_x.size() == 0)
        {
            auto bindivarr = Nominal->GetXaxis()->GetXbins()->GetArray();
            int size = Nominal->GetXaxis()->GetNbins() + 1;
            bindiv_x = std::vector<double>(bindivarr, bindivarr + size);
        }
        if (bindiv_y.size() == 0)
        {
            auto bindivarr = Nominal->GetYaxis()->GetXbins()->GetArray();
            int size = Nominal->GetYaxis()->GetNbins() + 1;
            bindiv_y = std::vector<double>(bindivarr, bindivarr + size);
        }
        if (bindiv_z.size() == 0)
        {
            auto bindivarr = Nominal->GetZaxis()->GetXbins()->GetArray();
            int size = Nominal->GetZaxis()->GetNbins() + 1;
            bindiv_z = std::vector<double>(bindivarr, bindivarr + size);
        }

        int oldX = 1, oldY = 1, oldZ = 1;
        for (int newX = 0; newX < bindiv_x.size(); newX++)
        {
            if (bindiv_x.at(newX) < Nominal->GetXaxis()->GetBinLowEdge(1))
                continue;
            if (bindiv_x.at(newX) > Nominal->GetXaxis()->GetBinUpEdge(Nominal->GetXaxis()->GetNbins()))
                break;
            if (!TMath::AreEqualAbs(Nominal->GetXaxis()->GetBinLowEdge(oldX), bindiv_x.at(newX), TMath::Max(1.E-8 * Nominal->GetXaxis()->GetBinWidth(oldX), 1.E-16)) && Nominal->GetXaxis()->GetBinLowEdge(oldX) > bindiv_x.at(newX))
                MSGUser()->MSG_WARNING("Bin X edge ", newX + 1, " of rebinned histogram does not match any bin edges of the old histogram. Result can be inconsistent");
            else
                oldX++;
        }
        for (int newY = 0; newY < bindiv_y.size(); newY++)
        {
            if (bindiv_y.at(newY) < Nominal->GetYaxis()->GetBinLowEdge(1))
                continue;
            if (bindiv_y.at(newY) > Nominal->GetYaxis()->GetBinUpEdge(Nominal->GetYaxis()->GetNbins()))
                break;
            if (!TMath::AreEqualAbs(Nominal->GetYaxis()->GetBinLowEdge(oldY), bindiv_y.at(newY), TMath::Max(1.E-8 * Nominal->GetYaxis()->GetBinWidth(oldY), 1.E-16)) && Nominal->GetYaxis()->GetBinLowEdge(oldY) > bindiv_y.at(newY))
                MSGUser()->MSG_WARNING("Bin Y edge ", newY + 1, " of rebinned histogram does not match any bin edges of the old histogram. Result can be inconsistent");
            else
                oldY++;
        }
        for (int newZ = 0; newZ < bindiv_z.size(); newZ++)
        {
            if (bindiv_z.at(newZ) < Nominal->GetZaxis()->GetBinLowEdge(1))
                continue;
            if (bindiv_z.at(newZ) > Nominal->GetZaxis()->GetBinUpEdge(Nominal->GetZaxis()->GetNbins()))
                break;
            if (!TMath::AreEqualAbs(Nominal->GetZaxis()->GetBinLowEdge(oldZ), bindiv_z.at(newZ), TMath::Max(1.E-8 * Nominal->GetZaxis()->GetBinWidth(oldZ), 1.E-16)) && Nominal->GetZaxis()->GetBinLowEdge(oldZ) > bindiv_z.at(newZ))
                MSGUser()->MSG_WARNING("Bin Z edge ", newZ + 1, " of rebinned histogram does not match any bin edges of the old histogram. Result can be inconsistent");
            else
                oldZ++;
        }

        if (!Processed)
            Process();

        // rebin algorithm
        auto Rebin = [](HistType *in, HistType *out)
        {
            auto xAxis = in->GetXaxis();
            auto yAxis = in->GetYaxis();
            auto zAxis = in->GetZaxis();
            for (int i = 1; i <= xAxis->GetNbins(); i++)
                for (int j = 1; j <= yAxis->GetNbins(); j++)
                    for (int k = 1; k <= zAxis->GetNbins(); k++)
                    {
                        double binCenterX = xAxis->GetBinCenter(i);
                        double binCenterY = yAxis->GetBinCenter(j);
                        double binCenterZ = zAxis->GetBinCenter(k);
                        int binX = out->GetXaxis()->FindBin(binCenterX);
                        int binY = out->GetYaxis()->FindBin(binCenterY);
                        int binZ = out->GetZaxis()->FindBin(binCenterZ);
                        out->SetBinContent(binX, binY, binZ, out->GetBinContent(binX, binY, binZ) + in->GetBinContent(i, j, k));
                        out->SetBinError(binX, binY, binZ, Uncertainty::AplusB(out->GetBinError(binX, binY, binZ), in->GetBinError(i, j, k)));
                    }
        };

        std::shared_ptr<NGHist<HistType>> newPlot = std::make_shared<NGHist<HistType>>(MSGUser(), ConfigUser(), name, GetOutputFileName(), bindiv_x.size() - 1, bindiv_x.data(), bindiv_y.size() - 1, bindiv_y.data(), bindiv_z.size() - 1, bindiv_z.data());
        Rebin(this->Nominal, newPlot->Nominal);
        newPlot->FillHist = newPlot->Nominal;

        // copy systs
        newPlot->SystematicIndexMap = this->SystematicIndexMap;
        newPlot->SystematicVariationNames = this->SystematicVariationNames;
        newPlot->CorrelationFactors = this->CorrelationFactors;
        newPlot->SystematicVariationIndices = this->SystematicVariationIndices;
        newPlot->SystematicVariationPolicies = this->SystematicVariationPolicies;
        newPlot->SystematicVariationBackup = this->SystematicVariationBackup;

        newPlot->SystematicVariations.resize(this->SystematicVariations.size());
        for (int i = 0; i < this->SystematicVariationNames.size(); i++)
        {
            newPlot->SystematicVariations.at(i) = (HistType *)newPlot->Nominal->Clone(name + "_Syst_" + newPlot->SystematicVariationNames.at(i));
            Rebin(this->SystematicVariations.at(i), newPlot->SystematicVariations.at(i));
        }

        newPlot->Process();

        return newPlot;
    }

    // virtual functions

    /// @brief Book a systematic variation.
    /// @tparam HistType type of NGHist.
    /// @param name name of the systematic variation.
    /// @param corr_factor correlation factor of the systematic variation.
    /// @param policy policy on how to deal with this systematic variation.
    /// @return booking procedure success or not.
    template <typename HistType>
    inline bool NGHist<HistType>::BookSystematicVariation(const TString &name, double corr_factor, SystPolicy policy)
    {
        return BookSystematicVariation(std::vector<TString>{name}, corr_factor, policy);
    }

    /// @brief Book systematic variations.
    /// @tparam HistType type of NGHist.
    /// @param name1 name of the first systematic variation.
    /// @param name2 name of the second systematic variation.
    /// @param corr_factor correlation factor of the systematic variations.
    /// @param policy policy on how to deal with this systematic variations.
    /// @return booking procedure success or not.
    template <typename HistType>
    inline bool NGHist<HistType>::BookSystematicVariation(const TString &name1, const TString &name2, double corr_factor, SystPolicy policy)
    {
        return BookSystematicVariation(std::vector<TString>{name1, name2}, corr_factor, policy);
    }

    /// @brief Book systematic variations.
    /// @tparam HistType type of NGHist.
    /// @param names systematic variation names.
    /// @param corr_factor correlation factor of the systematic variations.
    /// @param policy policy on how to deal with this systematic variations.
    /// @return booking procedure success or not.
    template <typename HistType>
    inline bool NGHist<HistType>::BookSystematicVariation(const std::vector<TString> &names, double corr_factor, SystPolicy policy)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::BookSystematicVariation");
        if (names.size() == 0)
        {
            MSGUser()->MSG_ERROR("No systematic name is given, will do nothing for plot ", GetResultName());
            return false;
        }
        // check not duplicate
        for (auto name : names)
        {
            if (SystematicIndexMap.find(name) != SystematicIndexMap.end())
            {
                MSGUser()->MSG_ERROR("Systematic variation ", name, " has already existed in plot ", GetResultName());
                return false;
            }

            if (name == "Nominal")
            {
                MSGUser()->MSG_ERROR("can not book variation named Nominal for plot ", GetResultName());
                return false;
            }
        }

        if (names.size() != 2 && policy == SystPolicy::CTEQ)
        {
            MSGUser()->MSG_ERROR("The CTEQ policy only applies to two systematic variations at the same time for plot ", GetResultName());
            return false;
        }

        SetUnprocessed();

        if (policy != SystPolicy::Backup)
        {
            std::vector<size_t> indices(names.size());
            std::iota(indices.begin(), indices.end(), CorrelationFactors.size());
            SystematicVariationIndices.emplace_back(indices);
            SystematicVariationPolicies.emplace_back(policy);
        }

        for (auto name : names)
        {
            // for backup policy, store all of them into special map backup
            if (policy == SystPolicy::Backup)
                SystematicVariationBackup.emplace(std::pair<TString, int>(name, CorrelationFactors.size()));

            SystematicIndexMap.emplace(std::pair<TString, int>(name, CorrelationFactors.size()));
            SystematicVariationNames.emplace_back(name);

            TString SystHistName = GetResultName() + "_Syst_" + name;
            HistType *SystHist = (HistType *)Nominal->Clone(SystHistName.TString::Data());
            SystHist->SetTitle(SystHistName.TString::Data());
            // SystHist->Sumw2();
            SystHist->SetDirectory(0);
            SystHist->Reset("ICESM");
            SystematicVariations.emplace_back(SystHist);
            CorrelationFactors.emplace_back(corr_factor);
        }

        return true;
    }

    /// @brief Clear all linked plots.
    template <typename HistType>
    inline void NGHist<HistType>::ClearLinkPlot()
    {
        LinkedPlots.clear();
        LinkType = "";
        LinkContentFunction = std::function<double(const std::vector<double> &)>();
        LinkErrorFunction = std::function<double(const std::vector<double> &, const std::vector<double> &)>();
        LinkHistFunction = std::function<void(const std::vector<TH1 *> &, TH1 *)>();
    }

    /// @brief Clone the current NGHist with the given name.
    template <typename HistType>
    inline std::shared_ptr<NGHist<HistType>> NGHist<HistType>::Clone(const TString &name)
    {
        return std::static_pointer_cast<NGHist<HistType>>(this->CloneVirtual(name));
    }

    /// @brief Combine the current NGHist with another NGHist.
    template <typename HistType>
    inline void NGHist<HistType>::Combine(std::shared_ptr<Result> result)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Combine");
        auto subPlot = std::dynamic_pointer_cast<typename std::remove_pointer<decltype(this)>::type>(result);
        if (subPlot != nullptr)
        {
            Nominal->Add(subPlot->GetNominalHist());

            bool systCompatible = true;
            for (auto &[varnames, corr_factor, policy] : subPlot->GetVariationTypes())
                if (SystematicIndexMap.find(varnames[0]) == SystematicIndexMap.end() && SystematicVariationBackup.find(varnames[0]) == SystematicVariationBackup.end())
                    if (systCompatible)
                        systCompatible = this->BookSystematicVariation(varnames, corr_factor, policy);

            if (!systCompatible)
            {
                MSGUser()->MSG_ERROR("the systematic variations of plot ", this->GetResultName(), " are not compatible with input plot ", subPlot->GetResultName());
                return;
            }

            for (size_t i = 1; i <= subPlot->GetNVariations(); i++)
            {
                TString varname = subPlot->GetVariationName(i);
                this->GetVariation(varname)->Add(subPlot->GetVariation(i));
            }

            SetUnprocessed();
        }
    }

    /// @brief Virtual function to get the variation histogram.
    template <typename HistType>
    inline TH1 *NGHist<HistType>::GetVariationVirtual(const TString &name)
    {
        return (TH1 *)GetVariation(name);
    }

    /// @brief Virtual function to get the variation histogram.
    template <typename HistType>
    inline TH1 *NGHist<HistType>::GetVariationVirtual(uint64_t index)
    {
        return (TH1 *)GetVariation(index);
    }

    /// @brief Virtual function to get the nominal histogram.
    template <typename HistType>
    inline TH1 *NGHist<HistType>::GetNominalHistVirtual()
    {
        return (TH1 *)Nominal;
    }

    /// @brief Get the structure of the systematic variations.
    /// @return a vector of tuples that represent a group of systematic variations, first element is the vector of systematic variation names, second element is the correlation factor, and third element is the policy.
    template <typename HistType>
    inline std::vector<std::tuple<std::vector<TString>, double, HistBase::SystPolicy>> NGHist<HistType>::GetVariationTypes()
    {
        std::vector<std::tuple<std::vector<TString>, double, SystPolicy>> types;

        int systcount = 0;
        for (auto &indices : this->SystematicVariationIndices)
        {
            double corr = CorrelationFactors[indices[0]];
            SystPolicy policy = SystematicVariationPolicies[systcount];
            std::vector<TString> varnames;

            for (auto &index : indices)
                varnames.emplace_back(SystematicVariationNames[index]);

            types.emplace_back(std::tuple<std::vector<TString>, double, SystPolicy>(varnames, corr, policy));

            ++systcount;
        }

        // emplace backup
        for (auto &iter : SystematicVariationBackup)
            types.emplace_back(std::tuple<std::vector<TString>, double, SystPolicy>({iter.first}, CorrelationFactors[iter.second], HistBase::SystPolicy::Backup));

        return types;
    }

    /// @brief If ith variation is a backup variation.
    template <typename HistType>
    inline bool NGHist<HistType>::IsBackupVariation(uint64_t index)
    {
        TString name;
        if (index == 0)
            return false;
        else if (index > 0 && index <= SystematicVariations.size())
            name = SystematicVariationNames[index - 1];
        else
        {
            auto guard = MSGUser()->StartTitleWithGuard("NGHist::IsBackupVariation");
            MSGUser()->MSG_WARNING("NGHist ", GetResultName(), " does not have variation ", index);
            FillHist = Nominal;
            return false;
        }

        if (auto findresult = SystematicVariationBackup.find(name); findresult != SystematicVariationBackup.end())
            return true;
        else
            return false;
    }

    /// @brief If the variation of given name is a backup variation.
    template <typename HistType>
    inline bool NGHist<HistType>::IsBackupVariation(const TString &name)
    {
        if (auto findresult = SystematicVariationBackup.find(name); findresult != SystematicVariationBackup.end())
            return true;
        else
            return false;
    }

    /**
        @brief process the NGHist.

        Calculate the final uncertainties for each bin, the covariance matrix and the correlation matrix. If this one is linked to other NGHists,
        also compute the contents and erros using the given link type.

        Process logic:
        - book the variation of all linked plots, if there are conflicts, that means you are doing something wrong.
        - if two link plots have the same group of variations but different policies, use the first one.
     */
    template <typename HistType>
    inline bool NGHist<HistType>::Process()
    {
        if (Processed)
            return true;

        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Process");
        auto histtool = ToolkitUser()->template GetTool<HistTool>();
        if (LinkedPlots.size() > 0)
        {
            for (size_t i = 0; i < LinkedPlots.size(); i++)
                LinkedPlots[i]->Process();

            // check for variations
            bool systCompatible = true;
            for (size_t i = 0; i < LinkedPlots.size(); i++)
                for (auto &[varnames, corr_factor, policy] : LinkedPlots[i]->GetVariationTypes())
                    if (SystematicIndexMap.find(varnames[0]) == SystematicIndexMap.end() && SystematicVariationBackup.find(varnames[0]) == SystematicVariationBackup.end())
                        if (systCompatible)
                            systCompatible = this->BookSystematicVariation(varnames, corr_factor, policy);

            if (!systCompatible)
            {
                MSGUser()->MSG_ERROR("the systematic variations of plot ", GetResultName(), " are not compatible with those of the linked plots");
                return false;
            }

            // Process as LinkedType with HistTool
            std::vector<TH1 *> LinkedHists;
            for (size_t i = 0; i < LinkedPlots.size(); i++)
            {
                if (LinkedPlots[i] == nullptr)
                    LinkedHists.emplace_back(nullptr);
                else
                    LinkedHists.emplace_back(LinkedPlots[i]->GetNominalHistVirtual());
            }
            if (LinkType != "")
                histtool->ProcessHistLink(LinkType, LinkedHists, GetNominalHist());
            else if (LinkContentFunction && LinkErrorFunction)
                histtool->ProcessHistLink(LinkContentFunction, LinkErrorFunction, LinkedHists, GetNominalHist());
            else if (LinkHistFunction)
                histtool->ProcessHistLink(LinkHistFunction, LinkedHists, GetNominalHist());
            else
            {
                MSGUser()->MSG_ERROR("Link type is not set for plot ", GetResultName(), " the hist link will not be processed");
                return false;
            }
            LinkedHists.clear();

            for (size_t i = 1; i <= GetNVariations(); i++)
            {
                TString vname = GetVariationName(i);
                auto level = MSGUser()->Level();
                MSGUser()->SetOutputLevel(MSGLevel::SILENT);
                size_t linkSystCount = 0;
                for (size_t j = 0; j < LinkedPlots.size(); j++)
                {
                    if (LinkedPlots[j] == nullptr)
                        LinkedHists.emplace_back(nullptr);
                    else
                    {
                        auto varh = LinkedPlots[j]->GetVariationVirtual(vname);
                        if (!varh)
                            LinkedHists.emplace_back(LinkedPlots[j]->GetNominalHistVirtual());
                        else
                        {
                            LinkedHists.emplace_back(varh);
                            ++linkSystCount;
                        }
                    }
                }
                MSGUser()->SetOutputLevel(level);

                if (linkSystCount == 0)
                    MSGUser()->MSG_WARNING("No variation ", vname, " is found in all linked plots for plot ", GetResultName(), ", will use nominal instead.");

                if (LinkType != "")
                    histtool->ProcessHistLink(LinkType, LinkedHists, GetVariation(vname));
                else if (LinkContentFunction && LinkErrorFunction)
                    histtool->ProcessHistLink(LinkContentFunction, LinkErrorFunction, LinkedHists, GetVariation(vname));
                else if (LinkHistFunction)
                    histtool->ProcessHistLink(LinkHistFunction, LinkedHists, GetVariation(vname));

                LinkedHists.clear();
            }
        }

        // Process final errors
        TString fname = "Final_" + GetResultName();
        Final = (HistType *)Nominal->Clone(fname.TString::Data());
        Final->SetDirectory(0);
        Final->SetTitle(fname.TString::Data());

        int Nbins = Nominal->GetXaxis()->GetNbins() * Nominal->GetYaxis()->GetNbins() * Nominal->GetZaxis()->GetNbins();

        // Process Total Uncertainty
        for (int i = 1; i <= Nominal->GetXaxis()->GetNbins(); i++)
            for (int j = 1; j <= Nominal->GetYaxis()->GetNbins(); j++)
                for (int k = 1; k <= Nominal->GetZaxis()->GetNbins(); k++)
                {
                    double val = Nominal->GetBinContent(i, j, k);
                    double unc = Nominal->GetBinError(i, j, k);

                    for (size_t ivar = 0; ivar < SystematicVariationIndices.size(); ivar++)
                    {
                        std::vector<double> deltas(SystematicVariationIndices[ivar].size());
                        std::vector<double> systcontents(SystematicVariationIndices[ivar].size());
                        // Add Syst. Unc.
                        std::transform(SystematicVariationIndices[ivar].begin(), SystematicVariationIndices[ivar].end(), deltas.begin(), [=](size_t index)
                                       { return std::fabs(SystematicVariations[index]->GetBinContent(i, j, k) - val); });
                        std::transform(SystematicVariationIndices[ivar].begin(), SystematicVariationIndices[ivar].end(), systcontents.begin(), [=](size_t index)
                                       { return SystematicVariations[index]->GetBinContent(i, j, k); });

                        if (SystematicVariationPolicies[ivar] == SystPolicy::Average)
                            unc = Uncertainty::AplusB(unc, std::accumulate(deltas.begin(), deltas.end(), (double)0) / (double)(deltas.size()));
                        if (SystematicVariationPolicies[ivar] == SystPolicy::Maximum)
                            unc = Uncertainty::AplusB(unc, *std::max_element(deltas.begin(), deltas.end()));
                        if (SystematicVariationPolicies[ivar] == SystPolicy::Variance)
                            unc = Uncertainty::AplusB(unc, std::sqrt(std::fabs(Variance(systcontents))));
                        if (SystematicVariationPolicies[ivar] == SystPolicy::CTEQ)
                            unc = Uncertainty::AplusB(unc, (systcontents[0] - systcontents[1]) / 2);
                    }

                    Final->SetBinContent(i, j, k, val);
                    Final->SetBinError(i, j, k, unc);
                }

        // Process correlation and covariance if syst booked and not too many bins
        // since it consumes so much time when bin number is great
        if (SystematicVariations.size() > 0 && Nbins <= 1e3)
        {
            fname = "Correlation_" + GetResultName();
            Correlation = new TH2D(fname.TString::Data(), fname.TString::Data(),
                                   Nbins, 0, Nbins,
                                   Nbins, 0, Nbins);
            Correlation->SetDirectory(0);
            Correlation->Sumw2();

            fname = "Covariance_" + GetResultName();
            Covariance = new TH2D(fname.TString::Data(), fname.TString::Data(),
                                  Nbins, 0, Nbins,
                                  Nbins, 0, Nbins);
            Covariance->SetDirectory(0);
            Covariance->Sumw2();

            histtool->ConvertCovToCorr(Covariance, Correlation);

            std::vector<std::vector<double>> cov(Nbins, std::vector<double>(Nbins, 0));

            auto convert2vector = [=](TH1 *h)
            {
                std::vector<double> content;
                content.reserve(Nbins);
                for (int i = 1; i <= h->GetXaxis()->GetNbins(); i++)
                    for (int j = 1; j <= h->GetYaxis()->GetNbins(); j++)
                        for (int k = 1; k <= h->GetZaxis()->GetNbins(); k++)
                            content.emplace_back(h->GetBinContent(i, j, k));
                return content;
            };

            auto converterror2vector = [=](TH1 *h)
            {
                std::vector<double> error;
                error.reserve(Nbins);
                for (int i = 1; i <= h->GetXaxis()->GetNbins(); i++)
                    for (int j = 1; j <= h->GetYaxis()->GetNbins(); j++)
                        for (int k = 1; k <= h->GetZaxis()->GetNbins(); k++)
                            error.emplace_back(h->GetBinError(i, j, k));
                return error;
            };

            std::vector<double> nominal_content = convert2vector(Nominal);
            std::vector<double> nominal_error = converterror2vector(Nominal);

            int systcount = 0;
            for (auto &indices : this->SystematicVariationIndices)
            {
                double corr = CorrelationFactors[indices[0]];
                SystPolicy policy = SystematicVariationPolicies[systcount];
                std::vector<std::vector<double>> syst_contents;
                for (auto &index : indices)
                    syst_contents.emplace_back(convert2vector(SystematicVariations[index]));

                std::vector<double> vdeltai(indices.size());
                std::vector<double> vdeltaj(indices.size());
                std::vector<double> vsystcontenti(indices.size());
                std::vector<double> vsystcontentj(indices.size());

                for (int i = 0; i < Nbins; i++)
                {
                    for (int j = 0; j < Nbins; j++)
                    {
                        double deltai, deltaj;
                        int signi = syst_contents[0][i] - nominal_content[i] > 0 ? 1 : -1;
                        int signj = syst_contents[0][j] - nominal_content[j] > 0 ? 1 : -1;
                        for (int k = 0; k < indices.size(); k++)
                        {
                            vdeltai[k] = fabs(syst_contents[k][i] - nominal_content[i]);
                            vdeltaj[k] = fabs(syst_contents[k][j] - nominal_content[j]);
                            vsystcontenti[k] = syst_contents[k][i];
                            vsystcontentj[k] = syst_contents[k][j];
                        }

                        if (policy == SystPolicy::Maximum)
                        {
                            deltai = *std::max_element(vdeltai.begin(), vdeltai.end());
                            deltaj = *std::max_element(vdeltaj.begin(), vdeltaj.end());
                        }
                        else if (policy == SystPolicy::Average)
                        {
                            deltai = std::accumulate(vdeltai.begin(), vdeltai.end(), (double)0) / indices.size();
                            deltaj = std::accumulate(vdeltaj.begin(), vdeltaj.end(), (double)0) / indices.size();
                        }
                        else if (policy == SystPolicy::Variance)
                        {
                            deltai = std::sqrt(Variance(vsystcontenti));
                            deltaj = std::sqrt(Variance(vsystcontentj));
                        }
                        else if (policy == SystPolicy::CTEQ)
                        {
                            deltai = (vsystcontenti[0] - vsystcontenti[1]) / 2;
                            deltaj = (vsystcontentj[0] - vsystcontentj[1]) / 2;
                            signi = 1;
                            signj = 1;
                        }

                        if (i != j)
                            cov[i][j] += signi * signj * deltai * deltaj * corr;
                        else
                            cov[i][j] += signi * signj * deltai * deltaj;
                    }
                }

                systcount++;
            }

            // add nominal stat unc.
            for (int i = 0; i < Nbins; i++)
                cov[i][i] += nominal_error[i] * nominal_error[i];

            // here only syst unc are considered
            histtool->ConvertVectorToTH2D(Covariance, cov);
            histtool->ConvertCovToCorr(Covariance, Correlation);
        }
        else if (SystematicVariations.size() > 0 && Nbins > 1e3)
        {
            MSGUser()->MSG_WARNING("NGHist ", GetResultName(), " has too many bins, can not effectively generate a covariance matrix");
        }

        Processed = true;

        return true;
    }

    /** @brief Recover the NGHist from a ROOT file.
        @param file the pointer of the ROOT file
        @return Recover successfully or not

        If a user somehow write a hist with trailing blanks in its name, ROOT will automatically strip these blanks for TKey. Therefore, you can never get the correct hists using its original name. See [this github issue](https://github.com/root-project/root/issues/17003) and [pull request](https://github.com/root-project/root/pull/17010)

        In `NGHist::Recover(TFile *)`, we temporarily strip the trailing blanks of the ResultName to get the correct hist.

        In `NGHist::Recover(std::shared_ptr<TFileHelper>)`, we do not need to strip the trailing blanks, because we use TIter to get iterate over the objects directly.
     */
    template <typename HistType>
    inline bool NGHist<HistType>::Recover(TFile *file)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Recover");
        if (!file)
        {
            MSGUser()->MSG_ERROR("input file does not exist for plot ", GetResultName());
            return false;
        }

        HistType *nominal_backup = nullptr;
        if (Nominal)
        {
            nominal_backup = Nominal;
            Nominal = nullptr;
        }

        /*
            If a user somehow write a hist with trailing blanks in its name, ROOT will automatically strip these blanks for TKey. Therefore, you can never get the correct hists using its original name.
            See https://github.com/root-project/root/issues/17003 and https://github.com/root-project/root/pull/17010
            In NGHist::Recover(TFile *), we temporarily strip the trailing blanks of the ResultName to get the correct hist.
            In NGHist::Recover(std::shared_ptr<TFileHelper>), we do not need to strip the trailing blanks because we use TIter to get iterate over the objects directly, more info in TFileHelper.
        */
        const TString resultName_noTrailingBlank = GetResultName().Strip();
        if (resultName_noTrailingBlank != GetResultName())
            MSGUser()->MSG_WARNING("The result name \"", GetResultName(), "\" has trailing blanks, will use \"", resultName_noTrailingBlank, "\" to recover the hist. Maybe this isn't your purpose, but ROOT doesn't work well with trailing blanks in the name of objects written in a ROOT file. Please check our document NGHist::Recover(TFile *) for more info.");

        Nominal = (HistType *)file->Get(resultName_noTrailingBlank);

        if (!Nominal)
        {
            Nominal = nominal_backup;
            MSGUser()->MSG_ERROR("Hist ", GetResultName(), " does not exist in file ", file->GetName(), " will keep the original one.");
            return false;
        }
        else
        {
            Nominal->SetDirectory(0);
            delete nominal_backup;
        }

        FillHist = Nominal;

        TTree *infotree = (TTree *)file->Get(GetResultName() + "_SystInfoTree");
        if (infotree != nullptr)
        {
            std::vector<std::string> *systnames = nullptr;
            int policy;
            double corrfactor;
            int index;
            bool doindexcheck = false;

            infotree->SetBranchAddress("SystNames", &systnames);
            infotree->SetBranchAddress("Policy", &policy);
            infotree->SetBranchAddress("CorrFactor", &corrfactor);
            // for old version
            if (infotree->SetBranchAddress("Index", &index) != TTree::ESetBranchAddressStatus::kMissingBranch)
                doindexcheck = true;

            for (int i = 0; i < infotree->GetEntries(); i++)
            {
                infotree->GetEntry(i);
                // for avoid the case that hadd auto adds syst-into tree
                if (doindexcheck && index != i)
                    break;
                std::vector<TString> tstring_systnames;
                for (int j = 0; j < systnames->size(); j++)
                    tstring_systnames.emplace_back(systnames->at(j));

                this->BookSystematicVariation(tstring_systnames, corrfactor, static_cast<SystPolicy>(policy));
            }
        }

        for (size_t i = 0; i < SystematicVariationNames.size(); i++)
        {
            TString SystHistName = GetResultName() + "_Syst_" + SystematicVariationNames[i];
            TString SystHistName_Strip = SystHistName.Strip();
            if (SystematicVariations[i])
            {
                delete SystematicVariations[i];
                SystematicVariations[i] = nullptr;
            }
            SystematicVariations[i] = (HistType *)file->Get(SystHistName_Strip.TString::Data());
            if (!SystematicVariations[i])
                MSGUser()->MSG_ERROR("Hist ", SystHistName, " does not exist");
            else
                SystematicVariations[i]->SetDirectory(0);
        }

        // get final, correlation and covariance if possible
        if (Final)
        {
            delete Final;
            Final = nullptr;
        }
        if (Covariance)
        {
            delete Covariance;
            Covariance = nullptr;
        }
        if (Correlation)
        {
            delete Correlation;
            Correlation = nullptr;
        }
        Final = (HistType *)file->Get("Final_" + resultName_noTrailingBlank);
        Covariance = (TH2D *)file->Get("Covariance_" + resultName_noTrailingBlank);
        Correlation = (TH2D *)file->Get("Correlation_" + resultName_noTrailingBlank);

        if (Final)
            Final->SetDirectory(0);
        if (Covariance)
            Covariance->SetDirectory(0);
        if (Correlation)
            Correlation->SetDirectory(0);

        FillHist = Nominal;
        Processed = true;

        return true;
    }

    /// @brief Recover the NGHist from a ROOT file.
    /// @param infile input TFileHelper.
    /// @return Recover successfully or not.
    template <typename HistType>
    inline bool NGHist<HistType>::Recover(std::shared_ptr<TFileHelper> infile)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::Recover");

        if (!infile)
        {
            MSGUser()->MSG_ERROR("input file does not exist for plot ", GetResultName());
            return false;
        }

        HistType *nominal_backup = nullptr;
        if (Nominal)
        {
            nominal_backup = Nominal;
            Nominal = nullptr;
        }

        Nominal = (HistType *)infile->Get(GetResultName().Data());

        if (!Nominal)
        {
            Nominal = nominal_backup;
            return false;
        }
        else
        {
            Nominal->SetDirectory(0);
            delete nominal_backup;
        }

        FillHist = Nominal;

        TTree *infotree = infile->GetTree((GetResultName() + "_SystInfoTree").Data());
        if (infotree != nullptr)
        {
            std::vector<std::string> *systnames = nullptr;
            int policy;
            double corrfactor;
            int index;
            bool doindexcheck = false;

            infotree->SetBranchAddress("SystNames", &systnames);
            infotree->SetBranchAddress("Policy", &policy);
            infotree->SetBranchAddress("CorrFactor", &corrfactor);
            // for old version
            if (infotree->SetBranchAddress("Index", &index) != TTree::ESetBranchAddressStatus::kMissingBranch)
                doindexcheck = true;

            for (int i = 0; i < infotree->GetEntries(); i++)
            {
                infotree->GetEntry(i);
                // for avoid the case that hadd auto adds syst-into tree
                if (doindexcheck && index != i)
                    break;

                std::vector<TString> tstring_systnames;
                for (int j = 0; j < systnames->size(); j++)
                    tstring_systnames.emplace_back(systnames->at(j));

                this->BookSystematicVariation(tstring_systnames, corrfactor, static_cast<SystPolicy>(policy));
            }

            delete systnames;
            delete infotree;
        }

        for (size_t i = 0; i < SystematicVariationNames.size(); i++)
        {
            TString SystHistName = GetResultName() + "_Syst_" + SystematicVariationNames[i];
            if (SystematicVariations[i])
            {
                delete SystematicVariations[i];
                SystematicVariations[i] = nullptr;
            }

            SystematicVariations[i] = (HistType *)infile->Get(SystHistName.Data());
        }

        // get final, correlation and covariance if possible
        if (Final)
        {
            delete Final;
            Final = nullptr;
        }
        if (Covariance)
        {
            delete Covariance;
            Covariance = nullptr;
        }
        if (Correlation)
        {
            delete Correlation;
            Correlation = nullptr;
        }

        TString finalname = "Final_" + GetResultName();
        Final = (HistType *)infile->Get(finalname.Data());

        if (Nominal)
        {
            if (Nominal->GetNbinsX() * Nominal->GetNbinsY() * Nominal->GetNbinsZ() <= 1e3 && SystematicVariations.size() > 0)
            {
                TString covname = "Covariance_" + GetResultName();
                TString corrname = "Correlation_" + GetResultName();
                Covariance = (TH2D *)infile->Get(covname.Data());
                Correlation = (TH2D *)infile->Get(corrname.Data());
            }
        }

        FillHist = Nominal;
        Processed = true;

        return true;
    }

    /// @brief Recover the NGHist from a ROOT file.
    /// @param filename name of the ROOT file.
    /// @return Recover successfully or not.
    template <typename HistType>
    inline bool NGHist<HistType>::Recover(const TString &filename)
    {
        TFile *file = new TFile(filename.TString::Data());
        if (file == nullptr)
        {
            auto guard = MSGUser()->StartTitleWithGuard("NGHist::Recover");
            MSGUser()->MSG_ERROR("ROOT file ", filename, " does not exist for plot ", GetResultName());
            return false;
        }

        auto returnvalue = Recover(file);
        file->Close();

        return returnvalue;
    }

    /// @brief Recover the NGHist from a ROOT file with the name passed to the constructor.
    /// @return Recover successfully or not.
    template <typename HistType>
    inline bool NGHist<HistType>::Recover()
    {
        return this->Recover(this->GetOutputFileName());
    }

    /// @brief Regroup the backup variations with the give name of policy.
    /// @param names variation names that you want to regroup
    /// @param policy policy for the new group of variations.
    template <typename HistType>
    inline bool NGHist<HistType>::RegroupSystematicVariation(const std::vector<TString> &names, SystPolicy policy)
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::RegroupSystematicVariation");
        if (names.size() != 2 && policy == SystPolicy::CTEQ)
        {
            MSGUser()->MSG_ERROR("The CTEQ policy only applies to two systematic variations at the same time for plot ", GetResultName());
            return false;
        }

        this->SetUnprocessed();

        std::vector<size_t> indices;
        for (auto &name : names)
        {
            auto findresult = SystematicVariationBackup.find(name);
            if (findresult != SystematicVariationBackup.end())
            {
                indices.emplace_back(findresult->second);
                SystematicVariationBackup.erase(findresult);
            }
            else
                MSGUser()->MSG_WARNING("Syst ", name, " is not in the backup list of plot ", GetResultName());
        }

        if (indices.size() > 0)
        {
            SystematicVariationIndices.emplace_back(indices);
            SystematicVariationPolicies.emplace_back(policy);
        }
        else
        {
            MSGUser()->MSG_ERROR("No suitable variation found in plot ", GetResultName());
            return false;
        }

        // check corr value same or not
        if (indices.size() > 1)
        {
            auto corr = CorrelationFactors[indices[0]];
            for (auto &index : indices)
                if (CorrelationFactors[index] != corr)
                    MSGUser()->MSG_WARNING("Correlation factors are not equal for input variations, will take the first one for plot ", GetResultName());
        }

        return true;
    }

    /// @brief Move a systematic variation to backup
    /// the variations in the same group will also be moved to the backup.
    template <typename HistType>
    inline bool NGHist<HistType>::RemoveSystematicVariation(const TString &name)
    {
        // move the input variation to backup
        auto guard = MSGUser()->StartTitleWithGuard("NGHist::RemoveSystematicVariation");

        if (auto findresult_backup = SystematicVariationBackup.find(name); findresult_backup != SystematicVariationBackup.end())
        {
            MSGUser()->MSG_WARNING("variation ", name, " has already been in the backup list of plot ", GetResultName());
            return false;
        }

        if (auto findresult = this->SystematicIndexMap.find(name); findresult != SystematicIndexMap.end())
        {
            for (int i = 0; i < SystematicVariationIndices.size(); i++)
            {
                if (std::find(SystematicVariationIndices[i].begin(), SystematicVariationIndices[i].end(), findresult->second) != SystematicVariationIndices[i].end())
                {
                    for (auto &index : SystematicVariationIndices[i])
                        SystematicVariationBackup[SystematicVariationNames[index]] = index;

                    SystematicVariationIndices.erase(SystematicVariationIndices.begin() + i);
                    SystematicVariationPolicies.erase(SystematicVariationPolicies.begin() + i);

                    break;
                }
            }

            return true;
        }
        else
        {
            MSGUser()->MSG_WARNING("variation ", name, " does not exist in plot ", GetResultName());
            return false;
        }
    }

    /// @brief Rename a systematic variation.
    /// @param name_old old name.
    /// @param name_new new name.
    template <typename HistType>
    inline bool NGHist<HistType>::RenameSystematicVariation(const TString &name_old, const TString &name_new)
    {
        auto guard = MSGUser()->StartTitleWithGuard("NGHist::RenameSystematicVariation");

        if (auto findresult = this->SystematicIndexMap.find(name_old); findresult != SystematicIndexMap.end())
        {
            SystematicIndexMap[name_new] = findresult->second;
            SystematicVariationNames[findresult->second] = name_new;
            TString SystHistName = GetResultName() + "_Syst_" + name_new;
            SystematicVariations[findresult->second]->SetName(SystHistName.Data());
            SystematicVariations[findresult->second]->SetTitle(SystHistName.Data());
            SystematicIndexMap.erase(findresult);

            if (auto findresult2 = this->SystematicVariationBackup.find(name_old); findresult2 != SystematicVariationBackup.end())
            {
                SystematicVariationBackup[name_new] = findresult2->second;
                SystematicVariationBackup.erase(findresult2);
            }

            return true;
        }
        else
        {
            MSGUser()->MSG_WARNING("variation ", name_old, " does not exist in plot ", GetResultName());
            return false;
        }
    }

    /// @brief Reset all histograms.
    template <typename HistType>
    inline void NGHist<HistType>::Reset()
    {
        Nominal->Reset("ICESM");
        for (auto &sh : SystematicVariations)
            sh->Reset("ICESM");

        if (Final)
            Final->Reset("ICESM");
        if (Covariance)
            Covariance->Reset("ICESM");
        if (Correlation)
            Correlation->Reset("ICESM");

        ClearLinkPlot();
        SetUnprocessed();
    }

    /// @brief Scale all histograms using TH1::Scale().
    /// @param sf scale factor.
    template <typename HistType>
    inline void NGHist<HistType>::Scale(double sf)
    {
        if (Nominal)
            Nominal->Scale(sf);
        if (Final)
            Final->Scale(sf);
        if (Covariance)
            Covariance->Scale(sf);

        for (size_t i = 0; i < SystematicVariationNames.size(); i++)
            if (SystematicVariations[i])
                SystematicVariations[i]->Scale(sf);
    }

    /// @brief Link this NGHist to anothe one.
    ///
    /// The link will be calculated in Process() via NAGASH::HistTool::ProcessHistLink()
    /// @param index the index of the link.
    /// @param subplot the linked NGHist.
    template <typename HistType>
    inline void NGHist<HistType>::SetLinkPlot(uint64_t index, std::shared_ptr<HistBase> subplot)
    {
        if (subplot != nullptr)
        {
            SetUnprocessed();
            if (LinkedPlots.size() < (index + 1))
            {
                int num_needed = index + 1 - LinkedPlots.size();
                for (int i = 0; i < num_needed; i++)
                    LinkedPlots.emplace_back(nullptr);
            }

            LinkedPlots[index] = subplot;
        }
        else
        {
            auto guard = MSGUser()->StartTitleWithGuard("NGHist::SetLinkPlot");
            MSGUser()->MSG_WARNING("for plot ", GetResultName(), ", given link plot ", index, " is null");
            return;
        }
    }

    /** @brief Set the type of the link.
        @param type the type of the link. Possible values are: `Asymmetry`, `Delta`, `Ratio`, `Chi2` and `Pull`.

    The link will be calculated in Process() via NAGASH::HistTool::ProcessHistLink(). For example, if you want to
    generate the forward-backward asymmetry plot, you can do it by:
    @code {.cxx}
    AsymHist->SetLinkPlot(0, Forward);
    AsymHist->SetLinkPlot(1, Backward);
    AysmHist->SetLinkType("Asymmetry");
    AysmHist->Process();
    @endcode
    */
    template <typename HistType>
    inline void NGHist<HistType>::SetLinkType(const TString &type)
    {
        SetUnprocessed();
        LinkType = type;
    }

    /** @brief Set the type of the links.
        @param contentfunction function to calculate the content of the links, the argument is the vector of the contents of all linked histograms.
        @param errorfunction function to calculate the error of the links, the first and the second arguments are the vector of the contents and erros of all linked histograms.

    The link will be calculated in Process() via NAGASH::HistTool::ProcessHistLink(). For example, if you want to substract background from data, you can do it by:
    @code {.cxx}
    auto SubstractBkgContentFunc = [](const std::vector<double> &contents) -> double
    {
        if (contents.size() > 1)
        {
            double c = contents[0];
            for (size_t i = 1; i < contents.size(); i++)
                c -= contents[i];
            return c;
        }
        else
            return contents[0];
    };

    auto SubstractBkgErrorFunc = [](const std::vector<double> &contents, const std::vector<double> &errors) -> double
    {
        double e = 0;
        for (auto &error : errors)
            e += error * error;
        return std::sqrt(std::fabs(e));
    };

    DataSubstractBkg->SetLinkType(SubstractBkgContentFunc, SubstractBkgErrorFunc);
    DataSubstractBkg->SetLinkPlot(0, Data);
    DataSubstractBkg->SetLinkPlot(1, Bkg1);
    DataSubstractBkg->SetLinkPlot(2, Bkg2);
    DataSubstractBkg->SetLinkPlot(3, Bkg3);
    DataSubstractBkg->Process();
    @endcode
    */
    template <typename HistType>
    inline void NGHist<HistType>::SetLinkType(std::function<double(const std::vector<double> &)> contentfunction, std::function<double(const std::vector<double> &, const std::vector<double> &)> errorfunction)
    {
        SetUnprocessed();
        LinkContentFunction = contentfunction;
        LinkErrorFunction = errorfunction;
    }

    /** @brief Set the type of the links.
        @param f the function to calculate the link. The first argument is the vector of all linked histograms. The second argument is the target histogram.

    The link will be calculated in Process() via NAGASH::HistTool::ProcessHistLink(). For example, if you want to substract background from data, you can do it by:
    @code {.cxx}
    auto SubstractBkgFunc = [&](const std::vector<TH1 *> &vh, TH1 *h_target) -> void
    {
        for (auto &h : vh)
            h_target->Add(h, -1);
    };

    DataSubstractBkg->SetLinkType(SubstractBkgFunc);
    DataSubstractBkg->SetLinkPlot(0, Data);
    DataSubstractBkg->SetLinkPlot(1, Bkg1);
    DataSubstractBkg->SetLinkPlot(2, Bkg2);
    DataSubstractBkg->SetLinkPlot(3, Bkg3);
    DataSubstractBkg->Process();
    @endcode
    */
    template <typename HistType>
    inline void NGHist<HistType>::SetLinkType(std::function<void(const std::vector<TH1 *> &, TH1 *)> f)
    {
        SetUnprocessed();
        LinkHistFunction = f;
    }

    /// @brief Set the NGHist to the give variation name.
    template <typename HistType>
    inline bool NGHist<HistType>::SetSystematicVariation(const TString &name)
    {
        if (name == "Nominal")
        {
            FillHist = Nominal;
            return true;
        }
        auto findresult = SystematicIndexMap.find(name);
        if (findresult != SystematicIndexMap.end())
        {
            FillHist = SystematicVariations[findresult->second];
            return true;
        }
        else
        {
            auto guard = MSGUser()->StartTitleWithGuard("NGHist::SetSystematicVariation");
            MSGUser()->MSG_WARNING("NGHist ", GetResultName(), " does not have variation ", name, ", will set to nominal");
            FillHist = Nominal;
            return false;
        }
    }

    /// @brief Set the NGHist to the give variation index.
    template <typename HistType>
    inline bool NGHist<HistType>::SetSystematicVariation(uint64_t index)
    {
        if (index == 0)
        {
            FillHist = Nominal;
            return true;
        }
        else if (index > 0 && index <= SystematicVariations.size())
        {
            FillHist = SystematicVariations[index - 1];
            return true;
        }
        else
        {
            auto guard = MSGUser()->StartTitleWithGuard("NGHist::SetSystematicVariation");
            MSGUser()->MSG_WARNING("NGHist ", GetResultName(), " does not have variation ", index, ", will set to nominal");
            FillHist = Nominal;
            return false;
        }
    }

    /// @brief Set this NGHist to a unprocessed state.
    template <typename HistType>
    inline void NGHist<HistType>::SetUnprocessed()
    {
        Processed = false;
        if (Final)
        {
            delete Final;
            Final = nullptr;
        }

        if (Covariance)
        {
            delete Covariance;
            Covariance = nullptr;
        }

        if (Correlation)
        {
            delete Correlation;
            Correlation = nullptr;
        }
    }

    /** @brief Write the NGHist to a ROOT file.

        When storing in a ROOT file, the name of the nominal histogram is the same as the name of the NGHist. For systematic variations,
        the name is in the form as "histname_Syst_systname", where "systname" is the name of the systematic variations. The final histogram,
        covariance histogram and correlation histogram are stored in the name as "Final_histname", "Covariance_histname" and "Correlation_histname".
        Also the structure of systematic variations is stored as a TTree object with the name as "histname_SystInfoTree".
     */
    template <typename HistType>
    inline void NGHist<HistType>::Write()
    {
        Nominal->Write();
        for (size_t i = 0; i < SystematicVariations.size(); i++)
            SystematicVariations[i]->Write();

        if (Final != nullptr)
            Final->Write();
        if (Covariance != nullptr)
            Covariance->Write();
        if (Correlation != nullptr)
            Correlation->Write();

        // write syst info for fast recover
        if (SystematicVariations.size() > 0)
        {
            TTree *infotree = new TTree(GetResultName() + "_SystInfoTree", GetResultName() + "_SystInfoTree");
            std::vector<std::string> systnames;
            int policy;
            double corrfactor;
            int index = 0;
            infotree->Branch("SystNames", &systnames);
            infotree->Branch("Policy", &policy);
            infotree->Branch("CorrFactor", &corrfactor);
            infotree->Branch("Index", &index);

            for (size_t i = 0; i < SystematicVariationIndices.size(); i++)
            {
                systnames.clear();
                policy = static_cast<std::underlying_type<SystPolicy>::type>(SystematicVariationPolicies[i]);
                corrfactor = CorrelationFactors[SystematicVariationIndices[i][0]];
                for (size_t j = 0; j < SystematicVariationIndices[i].size(); j++)
                    systnames.emplace_back(SystematicVariationNames[SystematicVariationIndices[i][j]]);

                infotree->Fill();
                ++index;
            }

            // write backup
            for (auto &iter : SystematicVariationBackup)
            {
                systnames.clear();
                policy = static_cast<std::underlying_type<SystPolicy>::type>(SystPolicy::Backup);
                corrfactor = CorrelationFactors[iter.second];
                systnames.emplace_back(iter.first.Data());
                infotree->Fill();
                ++index;
            }

            infotree->Write();
            delete infotree;
        }
    }

    /// @brief Write the NGHist to a ROOT file.
    template <typename HistType>
    inline void NGHist<HistType>::WriteToFile()
    {
        auto titleguard = MSGUser()->StartTitleWithGuard("NGHist::WriteToFile");
        Process();
        if (GetOutputFileName() == "")
        {
            MSGUser()->MSG_WARNING("input an empty output file name for plot ", GetResultName(), " will be ignored");
            return;
        }
        TFile *file = new TFile(GetOutputFileName(), "UPDATE");
        file->cd();
        Write();
        file->Close();
    }
}
