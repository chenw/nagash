// to do list:
// 1. richer info in errors

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"
#include "NAGASH/NNGHist.h"
#include "NAGASH/NGCount.h"

namespace NAGASH
{
    template <typename... Args>
    inline std::string string_format(const std::string &format, Args... args)
    {
        int size_s = std::snprintf(nullptr, 0, format.c_str(), args...) + 1; // Extra space for '\0'
        if (size_s <= 0)
        {
            throw std::runtime_error("error during formatting.");
        }
        auto size = static_cast<size_t>(size_s);
        auto buf = std::make_unique<char[]>(size);
        std::snprintf(buf.get(), size, format.c_str(), args...);
        return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
    }

    class NAGASHFile : public Tool
    {
    public:
        enum class Mode
        {
            READ,
            WRITE,
            UPDATE
        };

        NAGASHFile(std::shared_ptr<MSGTool>, const std::string &, Mode option = Mode::READ);
        ~NAGASHFile();
        bool IsOpen();
        Mode OpenMode();
        void Close();

        template <typename FirstAxis, typename... MoreAxis>
        bool SaveHist(const std::string &, NNGHist<FirstAxis, MoreAxis...> &);

        template <typename FirstAxis, typename... MoreAxis>
        bool LoadHist(const std::string &, NNGHist<FirstAxis, MoreAxis...> &);

        template <typename FirstAxis, typename... MoreAxis>
        NNGHist<FirstAxis, MoreAxis...> LoadHist(const std::string &);

        void PrintListOfHists(std::ostream &outs = std::cout);

        static void hadd(const std::string &, const std::vector<std::string> &);

    private:
        bool ReadFile();

        template <size_t Index = 0, typename TTuple,
                  size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value>
        bool ReadAxes(TTuple &&tuple);

        template <typename T>
        void WriteVector(const std::vector<T> &);
        template <typename T>
        void ReadVector(std::vector<T> &);

        void WriteString(const std::string &);
        void ReadString(std::string &);

        std::tuple<bool, bool, size_t> FindHistPosition(const std::string &);

        std::fstream nagashfile;
        std::string file_name;
        std::map<std::string, std::vector<size_t>> name_position_map;
        Mode file_open_mode;
        bool is_open = false;
        std::string magic_number = "whma";
        size_t list_position = 36; // position of histogram list
        size_t list_size = 0;
        size_t main_version = NAGASHMainVersion;
        size_t sub_version = NAGASHSubVersion;

        // for hadd
        class AuxHist
        {
        public:
            AuxHist(size_t);
            std::pair<bool, bool> Add(const AuxHist &);

            size_t dim;
            std::vector<char> axesinfo;
            OverflowOption overflow_option;
            double SumW;
            double SumW2;
            std::vector<double> SumX;
            std::vector<std::vector<double>> SumX2;
            size_t nvar;
            std::vector<double> content;
            std::vector<double> error;
        };

        AuxHist LoadAuxHist(size_t);
        void SaveAuxHist(const std::string &, AuxHist &);
    };

    inline bool NAGASHFile::IsOpen() { return this->is_open; }

    inline NAGASHFile::Mode NAGASHFile::OpenMode() { return this->file_open_mode; }

    template <size_t Index, typename TTuple, size_t Size>
    inline bool NAGASHFile::ReadAxes(TTuple &&tuple)
    {
        if constexpr (Index < Size - 1)
        {
            if (std::get<Index>(tuple).Read(this->nagashfile))
                return ReadAxes<Index + 1>(std::forward<TTuple>(tuple));
            else
                return false;
        }
        else
            return std::get<Index>(tuple).Read(this->nagashfile);
    }

    template <typename T>
    inline void NAGASHFile::WriteVector(const std::vector<T> &v)
    {
        size_t vectorsize = v.size() * sizeof(T);
        nagashfile.write(reinterpret_cast<const char *>(&vectorsize), sizeof(size_t));
        nagashfile.write(reinterpret_cast<const char *>(v.data()), vectorsize);
    }

    template <typename T>
    inline void NAGASHFile::ReadVector(std::vector<T> &v)
    {
        size_t vectorsize;
        nagashfile.read(reinterpret_cast<char *>(&vectorsize), sizeof(size_t));
        v.resize(vectorsize / sizeof(T));
        nagashfile.read(reinterpret_cast<char *>(v.data()), vectorsize);
    }

    template <typename FirstAxis, typename... MoreAxis>
    inline bool NAGASHFile::SaveHist(const std::string &name, NNGHist<FirstAxis, MoreAxis...> &inhist)
    {
        if (this->file_open_mode == Mode::READ)
        {
            MSGUser()->StartTitle("NAGASHFile::SaveHist");
            MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened for write");
            MSGUser()->EndTitle();
            return false;
        }

        if (IsOpen())
        {
            auto findresult = name_position_map.find(name);
            if (findresult != name_position_map.end())
                findresult->second.emplace_back(list_position);
            else
                name_position_map.insert(std::pair<std::string, std::vector<size_t>>(name, std::vector<size_t>{list_position}));

            if (inhist.isResetStatsNeeded) // to get the correct counter info
            {
                inhist.ResetStatsCounter_impl();
                inhist.isResetStatsNeeded = false;
            }

            // write the histogram
            nagashfile.seekp(list_position, std::ios::beg);
            // write dimension and axisnames
            nagashfile.write(reinterpret_cast<const char *>(&(inhist.dimension)), sizeof(size_t));
            // write axis info size
            size_t axisinfosize = 0;
            nagashfile.write(reinterpret_cast<const char *>(&axisinfosize), sizeof(size_t));

            // write axis info
            TupleManipulation::tuple_for_each(inhist.myaxis, [&](auto &&item)
                                              { item.Write(nagashfile); });

            // rewrite axis info size
            size_t axisinfoendpos = nagashfile.tellp();
            nagashfile.seekp(list_position + sizeof(size_t), std::ios::beg);
            axisinfosize = axisinfoendpos - (list_position + sizeof(size_t) + sizeof(size_t));
            nagashfile.write(reinterpret_cast<const char *>(&axisinfosize), sizeof(size_t));
            nagashfile.seekp(axisinfoendpos, std::ios::beg);

            // write number of variations(intend for future)
            size_t nvar = 1;
            nagashfile.write(reinterpret_cast<const char *>(&nvar), sizeof(size_t));

            // write stats info
            nagashfile.write(reinterpret_cast<const char *>(&(inhist.StatsOption)), sizeof(OverflowOption));
            nagashfile.write(reinterpret_cast<const char *>(&(inhist.StatsCounter.SumW)), sizeof(double));
            nagashfile.write(reinterpret_cast<const char *>(&(inhist.StatsCounter.SumW2)), sizeof(double));
            for (int i = 0; i < inhist.dimension; i++)
                nagashfile.write(reinterpret_cast<const char *>(&(inhist.StatsCounter.SumX[i])), sizeof(double));
            for (int i = 0; i < inhist.dimension; i++)
                for (int j = 0; j < inhist.dimension; j++)
                    nagashfile.write(reinterpret_cast<const char *>(&(inhist.StatsCounter.SumX2[i][j])), sizeof(double));

            // write content and error
            WriteVector<double>(inhist.content);
            WriteVector<double>(inhist.error);

            list_position = nagashfile.tellp();
            return true;
        }

        MSGUser()->StartTitle("NAGASHFile::SaveHist");
        MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened");
        MSGUser()->EndTitle();
        return false;
    }

    template <typename FirstAxis, typename... MoreAxis>
    inline bool NAGASHFile::LoadHist(const std::string &name, NNGHist<FirstAxis, MoreAxis...> &inhist)
    {
        if (this->file_open_mode == Mode::WRITE)
        {
            MSGUser()->StartTitle("NAGASHFile::LoadHist");
            MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened for read");
            MSGUser()->EndTitle();
            return false;
        }

        if (IsOpen())
        {
            // if not exist, check if there are multiple hists with the same name
            // if exist, give the last written histogram
            auto [histexist, isoutrange, position] = this->FindHistPosition(name);

            if (histexist)
            {
                // read the histogram
                nagashfile.seekg(position, std::ios::beg);

                // check on the bin sizes
                // dimension check
                size_t dim;
                nagashfile.read(reinterpret_cast<char *>(&dim), sizeof(size_t));

                if (dim != inhist.dimension)
                {
                    MSGUser()->StartTitle("NAGASHFile::LoadHist");
                    MSGUser()->MSG_ERROR("dimension of histogram ", name, " does not match what stored in file(", dim, " stored in file but ", inhist.dimension, " given), aborted");
                    MSGUser()->EndTitle();
                    return false;
                }

                // read axis info size
                size_t axisinfosize;
                nagashfile.read(reinterpret_cast<char *>(&axisinfosize), sizeof(size_t));

                // check axis
                typename std::decay_t<decltype(inhist)>::axis_type newaxis;
                if (!ReadAxes(newaxis))
                {
                    MSGUser()->StartTitle("NAGASHFile::LoadHist");
                    MSGUser()->MSG_ERROR("axes type of histogram ", name, " does not match what stored in file, aborted");
                    MSGUser()->EndTitle();
                    return false;
                }

                if (!(newaxis == inhist.myaxis))
                {
                    MSGUser()->StartTitle("NAGASHFile::LoadHist");
                    MSGUser()->MSG_ERROR("axes info of histogram ", name, " does not match what stored in file, aborted");
                    MSGUser()->EndTitle();
                    return false;
                }

                // read number of variations(intend for future)
                size_t nvar;
                nagashfile.read(reinterpret_cast<char *>(&nvar), sizeof(size_t));

                // read stats info
                nagashfile.read(reinterpret_cast<char *>(&(inhist.StatsOption)), sizeof(OverflowOption));
                nagashfile.read(reinterpret_cast<char *>(&(inhist.StatsCounter.SumW)), sizeof(double));
                nagashfile.read(reinterpret_cast<char *>(&(inhist.StatsCounter.SumW2)), sizeof(double));
                for (int i = 0; i < inhist.dimension; i++)
                    nagashfile.read(reinterpret_cast<char *>(&(inhist.StatsCounter.SumX[i])), sizeof(double));
                for (int i = 0; i < inhist.dimension; i++)
                    for (int j = 0; j < inhist.dimension; j++)
                        nagashfile.read(reinterpret_cast<char *>(&(inhist.StatsCounter.SumX2[i][j])), sizeof(double));

                // read content and error
                ReadVector<double>(inhist.content);
                ReadVector<double>(inhist.error);

                return true;
            }
            else
            {
                MSGUser()->StartTitle("NAGASHFile::LoadHist");
                if (!isoutrange)
                    MSGUser()->MSG_ERROR("histogram ", name, " does not exist");
                else
                    MSGUser()->MSG_ERROR("histogram ", name, " does not exist(input index out of range ", name_position_map.find(name)->second.size(), " )");
                MSGUser()->EndTitle();
                return false;
            }
        }

        MSGUser()->StartTitle("NAGASHFile::LoadHist");
        MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened");
        MSGUser()->EndTitle();
        return false;
    }

    template <typename FirstAxis, typename... MoreAxis>
    inline NNGHist<FirstAxis, MoreAxis...> NAGASHFile::LoadHist(const std::string &name)
    {
        std::tuple<FirstAxis, MoreAxis...> outaxis;

        if (this->file_open_mode == Mode::WRITE)
        {
            MSGUser()->StartTitle("NAGASHFile::LoadHist");
            MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened for read, a null histogram returned");
            MSGUser()->EndTitle();
            return NNGHist<FirstAxis, MoreAxis...>(std::move(outaxis));
        }

        if (IsOpen())
        {
            // if not exist, check if there are multiple hists with the same name
            // if exist, give the last written histogram
            auto [histexist, isoutrange, position] = this->FindHistPosition(name);

            if (histexist)
            {
                // read the histogram
                nagashfile.seekg(position, std::ios::beg);

                // check on the bin sizes
                // dimension check
                size_t dim;
                nagashfile.read(reinterpret_cast<char *>(&dim), sizeof(size_t));

                if (dim != std::tuple_size_v<std::tuple<FirstAxis, MoreAxis...>>)
                {
                    MSGUser()->StartTitle("NAGASHFile::LoadHist");
                    MSGUser()->MSG_ERROR("dimension of histogram ", name, " does not match what stored in file(", dim, " stored in file but ",
                                         std::tuple_size_v<std::tuple<FirstAxis, MoreAxis...>>, " given), a null histogram returned");
                    MSGUser()->EndTitle();
                    return NNGHist<FirstAxis, MoreAxis...>(std::move(outaxis));
                }

                // read axis info size
                size_t axisinfosize;
                nagashfile.read(reinterpret_cast<char *>(&axisinfosize), sizeof(size_t));

                // check axis
                if (!ReadAxes(outaxis))
                {
                    MSGUser()->StartTitle("NAGASHFile::LoadHist");
                    MSGUser()->MSG_ERROR("axes type of histogram ", name, " does not match what stored in file, a null histogram returned");
                    MSGUser()->EndTitle();
                    return NNGHist<FirstAxis, MoreAxis...>(std::move(outaxis));
                }

                NNGHist<FirstAxis, MoreAxis...> outhist(std::move(outaxis));
                // read number of variations(intend for future)
                size_t nvar;
                nagashfile.read(reinterpret_cast<char *>(&nvar), sizeof(size_t));

                // read stats info
                nagashfile.read(reinterpret_cast<char *>(&(outhist.StatsOption)), sizeof(OverflowOption));
                nagashfile.read(reinterpret_cast<char *>(&(outhist.StatsCounter.SumW)), sizeof(double));
                nagashfile.read(reinterpret_cast<char *>(&(outhist.StatsCounter.SumW2)), sizeof(double));
                for (int i = 0; i < outhist.dimension; i++)
                    nagashfile.read(reinterpret_cast<char *>(&(outhist.StatsCounter.SumX[i])), sizeof(double));
                for (int i = 0; i < outhist.dimension; i++)
                    for (int j = 0; j < outhist.dimension; j++)
                        nagashfile.read(reinterpret_cast<char *>(&(outhist.StatsCounter.SumX2[i][j])), sizeof(double));

                // read content and error
                ReadVector<double>(outhist.content);
                ReadVector<double>(outhist.error);

                return outhist;
            }
            else
            {
                MSGUser()->StartTitle("NAGASHFile::LoadHist");
                if (!isoutrange)
                    MSGUser()->MSG_ERROR("histogram ", name, " does not exist, a null histogram returned");
                else
                    MSGUser()->MSG_ERROR("histogram ", name, " does not exist(input index out of max value ", name_position_map.find(name)->second.size(), " ), a null histogram returned");
                MSGUser()->EndTitle();
                return NNGHist<FirstAxis, MoreAxis...>(std::move(outaxis));
            }
        }

        MSGUser()->StartTitle("NAGASHFile::LoadHist");
        MSGUser()->MSG_ERROR("file ", this->file_name, " is not opened, a null histogram returned");
        MSGUser()->EndTitle();
        return NNGHist<FirstAxis, MoreAxis...>(std::move(outaxis));
    }

}
