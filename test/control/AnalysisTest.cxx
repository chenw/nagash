#include "NAGASH/Global.h"
#include "NAGASH/Analysis.h"
#include "NAGASH/NGFigure.h"

#include "LoopEventTest.h"

using namespace NAGASH;
using namespace std;

int main(int argc, char **argv)
{
    if (argc != 3 && argc != 2)
    {
        cout << "Usage: " << argv[0] << " (file list) (config file)" << endl;
        cout << "Usage: " << argv[0] << " (file list)" << endl;
        return 0;
    }

    ConfigTool config;
    if (argc == 3)
        config = ConfigTool(argv[2]);
    auto MSG = config.MSGUser();
    Analysis MyAnalysis(config);
    auto myresult = MyAnalysis.ProcessEventLoop<LoopEventTest>(argv[1]);

    auto Mass = myresult->Get<Count>("Mass");

    MSG->StartTitle("Main");
    MSG->MSG(MSGLevel::INFO, "Events Looped : ", myresult->Get<Count>("NEvents")->GetSum());
    MSG->MSG(MSGLevel::INFO, Mass->GetMean(), " ", Mass->GetMeanStatUnc());
    MSG->MSG(MSGLevel::INFO, myresult->Get<Count>("Test")->GetMean());

    auto MassPlots = myresult->Get<PlotGroup>("MassHist");
    MSG->MSG(MSGLevel::INFO, "Total Number WMass Hist: ", MassPlots->GetPlot1D("WMass")->GetNominalHist()->Integral());
    MSG->MSG(MSGLevel::INFO, "Total Number WPt Hist: ", MassPlots->GetPlot1D("WPt")->GetNominalHist()->Integral());
    MSG->MSG(MSGLevel::INFO, "Total Number WRapidity Hist: ", MassPlots->GetPlot1D("WRapidity")->GetNominalHist()->Integral());

    return 0;
}
