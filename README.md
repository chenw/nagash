To All the Developer:

Project NAGASH is for our next generation framework.
It is organized as the structure below.

Significant change: all the headers originally in the `include` dir have been moved to `include/NAGASH` to resolve potential conflicts.

When you build your framework based on NAGASH, run `GenerateMakefile (your project name)` to generate the standard makefile.

The [document](https://nagash.docs.cern.ch/) will be automatically pipelined through CI/CD when the master branch is updated. Or you may locally run `doxygen Doxyfile` and check the document in `nagashdoc/index.html`.

Todo list:

What we need to do now!

1. Need comments from all the possible users, raise questions of instructions and requests for more Tools and Results
2. Need comments on class NNGHist defined in include/NNGHist.h, which will be the standard histogram storage in NAGASH

BUG Report (Every one can edit here)
