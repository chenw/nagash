#include "NAGASH/NAGASH.h"
#include <sys/resource.h> // Include for getrusage

using namespace NAGASH;

void printMemoryUsage(const char *when)
{
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    std::cout << when << " Memory Usage: " << usage.ru_maxrss << " KB\n";
}

int main(int argc, char **argv)
{
    printMemoryUsage("start of the program");
    auto msg = std::make_shared<MSGTool>();
    printMemoryUsage("creation of msgtool");
    auto file = std::make_shared<TFileHelper>(msg, argv[1]);
    // TFile *file = new TFile(argv[1], "READ");
    printMemoryUsage("opened file");
    file.reset();
    // file->Close();
    // delete file;
    printMemoryUsage("closed file");

    return 0;
}