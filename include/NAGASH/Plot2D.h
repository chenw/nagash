//***************************************************************************************
/// @file Plot2D.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/NGHist.h"

namespace NAGASH
{
    /// @ingroup Histograms
    /// @brief alias for frequently used NGHist<TH2D>.
    using Plot2D = NGHist<TH2D>;

    /// @brief partial specialization for the constructor of NGHist<TH2D>.
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name the name of the histogram.
    /// @param filename  the file name of the histogram to be saved in.
    /// @param binnumx number of bins in the x axis.
    /// @param startx start value of the x axis.
    /// @param endx end value of the x axis.
    /// @param bindivy binning in the y axis.
    template <>
    template <>
    inline NGHist<TH2D>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const int &binnumx, const double &startx, const double &endx, const std::vector<double> &bindivy)
        : HistBase(MSG, c, name, filename)
    {
        Nominal = new TH2D(GetResultName(), GetResultName(), binnumx, startx, endx, (int)bindivy.size() - 1, bindivy.data());
        Nominal->Sumw2();
        Nominal->SetDirectory(0);
        FillHist = Nominal;
    }

    /// @brief partial specialization for the constructor of NGHist<TH2D>.
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name the name of the histogram.
    /// @param filename the file name of the histogram to be saved in.
    /// @param bindivx binning in the x axis.
    /// @param binnumy number of bins in the y axis.
    /// @param starty start value of the y axis.
    /// @param endy end value of the y axis.
    template <>
    template <>
    inline NGHist<TH2D>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const std::vector<double> &bindivx, const int &binnumy, const double &starty, const double &endy)
        : HistBase(MSG, c, name, filename)
    {
        Nominal = new TH2D(GetResultName(), GetResultName(), (int)bindivx.size() - 1, bindivx.data(), binnumy, starty, endy);
        Nominal->Sumw2();
        Nominal->SetDirectory(0);
        FillHist = Nominal;
    }

    /// @brief partial specialization for the constructor of NGHist<TH2D>.
    /// @param MSG input MSGTool.
    /// @param c input ConfigTool.
    /// @param name the name of the histogram.
    /// @param filename the file name of the histogram to be saved in.
    /// @param bindivx binning in the x axis.
    /// @param bindivy binning in the y axis.
    template <>
    template <>
    inline NGHist<TH2D>::NGHist(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &name, const TString &filename, const std::vector<double> &bindivx, const std::vector<double> &bindivy)
        : HistBase(MSG, c, name, filename)
    {
        Nominal = new TH2D(GetResultName(), GetResultName(), (int)bindivx.size() - 1, bindivx.data(), (int)bindivy.size() - 1, bindivy.data());
        Nominal->Sumw2();
        Nominal->SetDirectory(0);
        FillHist = Nominal;
    }

}
