// This is the demo of histogram class that will be used in the next generation of NAGASH
// Author : Wenhao Ma
// Comments on more features are welcome
//
// Basic Usage :
// 1D histogram with fixed bin width : NNGHist myhist(Axis::Regular(100, 0, 100));
// 2D histogram with varied bin width : NNGHist myhist(Axis::Variable({0, 2, 5, 6}), Axis::Variable({0, 2, 5, 9}));
// in principle you can define histogram with any dimension.
//
// Currently supported functions:
// Fill, GetBin, FindBin, GetBinContent, GetBinError, SetBinContent, SetBinError, SetStatOverflows,
// GetAxis<>, Integral, IntegrateOver<>, Rebin, Scale, GetMaximum, GetMinimum, GetMaximumBin, GetMinimumBin,
// Reset, Projection<>, ...
//
// Also a stats counter of type NGCount<dim> is supported, one can get the statistics of the filled histogram
// using GetStats()
// NGCount provides a series of functions like GetMean(), GetStdVar() ..., see NGCount.h for more details.
//
// To-do list:
// 1. Add more functions.
//

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/NGCount.h"
#include "NAGASH/NAGASHFile.h"

namespace NAGASH
{
    class NAGASHFile;

    // helpers
    namespace SequenceManipulation
    {
        template <std::size_t F(std::size_t), typename Seq>
        struct modify_sequence;

        template <std::size_t F(std::size_t), std::size_t... Ints>
        struct modify_sequence<F, std::index_sequence<Ints...>>
        {
            using type = std::index_sequence<F(Ints)...>;
        };

        template <std::size_t F(std::size_t), typename Seq>
        using modify_sequence_t = typename modify_sequence<F, Seq>::type;

        template <std::size_t n>
        constexpr std::size_t add(std::size_t v) { return v + n; }

        template <size_t N, typename Seq>
        using offset_sequence_t = typename modify_sequence<add<N>, Seq>::type;
    }

    namespace TupleManipulation
    {
        // do something for each element of a tuple
        template <
            size_t Index = 0,                                                      // start iteration at 0 index
            typename TTuple,                                                       // the tuple type
            size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value, // tuple size
            typename TCallable,                                                    // the callable to bo invoked for each tuple item
            typename... TArgs                                                      // other arguments to be passed to the callable
            >
        inline void tuple_for_each(TTuple &&tuple, TCallable &&callable, TArgs &&...args)
        {
            if constexpr (Index < Size)
            {
                std::invoke(callable, args..., std::get<Index>(tuple));

                if constexpr (Index + 1 < Size)
                    tuple_for_each<Index + 1>(
                        std::forward<TTuple>(tuple),
                        std::forward<TCallable>(callable),
                        std::forward<TArgs>(args)...);
            }
        }

        // remove ith type of a tuple
        template <size_t I, typename T>
        struct remove_ith_type
        {
        };

        template <typename T, typename... Ts>
        struct remove_ith_type<0, std::tuple<T, Ts...>>
        {
            using type = std::tuple<Ts...>;
        };

        template <size_t I, typename T, typename... Ts>
        struct remove_ith_type<I, std::tuple<T, Ts...>>
        {
            using type = decltype(std::tuple_cat(
                std::declval<std::tuple<T>>(),
                std::declval<typename remove_ith_type<I - 1, std::tuple<Ts...>>::type>()));
        };

        // replace ith type of a tuple
        template <size_t I, typename R, typename T>
        struct replace_ith_type
        {
        };

        template <typename R, typename T, typename... Ts>
        struct replace_ith_type<0, R, std::tuple<T, Ts...>>
        {
            using type = std::tuple<R, Ts...>;
        };

        template <size_t I, typename R, typename T, typename... Ts>
        struct replace_ith_type<I, R, std::tuple<T, Ts...>>
        {
            using type = decltype(std::tuple_cat(
                std::declval<std::tuple<T>>(),
                std::declval<typename replace_ith_type<I - 1, R, std::tuple<Ts...>>::type>()));
        };

        // remove ith element of a tuple
        template <typename Tuple, std::size_t... Ints>
        auto select_tuple_elements(Tuple &&tuple, std::index_sequence<Ints...>)
        {
            return std::tuple<std::tuple_element_t<Ints, std::decay_t<Tuple>>...>(
                std::get<Ints>(std::forward<Tuple>(tuple))...);
        }

        template <std::size_t N, typename Tuple>
        auto remove_ith_element(Tuple &&tuple)
        {
            constexpr auto size = std::tuple_size_v<std::decay_t<Tuple>>;
            using first = std::make_index_sequence<N>;
            using rest = SequenceManipulation::offset_sequence_t<N + 1, std::make_index_sequence<size - N - 1>>;
            return std::tuple_cat(
                select_tuple_elements(std::forward<Tuple>(tuple), first{}),
                select_tuple_elements(std::forward<Tuple>(tuple), rest{}));
        }

        // replace ith element of a tuple
        template <std::size_t N, typename R, typename Tuple>
        auto replace_ith_element(R &&replacement, Tuple &&tuple)
        {
            constexpr auto size = std::tuple_size_v<std::decay_t<Tuple>>;
            using first = std::make_index_sequence<N>;
            using rest = SequenceManipulation::offset_sequence_t<N + 1, std::make_index_sequence<size - N - 1>>;
            return std::tuple_cat(
                select_tuple_elements(std::forward<Tuple>(tuple), first{}),
                std::make_tuple(std::forward<R>(replacement)),
                select_tuple_elements(std::forward<Tuple>(tuple), rest{}));
        }

    }

    // control variable
    enum class OverflowOption
    {
        WithOverflows,
        WithoutOverflows
    };

    namespace Axis
    {
        class Regular;
        class Variable;
    }

    // class definition
    template <typename FirstAxis, typename... MoreAxis>
    class NNGHist
    {
        template <typename AnotherAxis, typename... AnotherMoreAxis>
        friend class NNGHist; // to access to private member functions

        friend ::NAGASH::NAGASHFile;

        using axis_type = std::tuple<std::decay_t<FirstAxis>, std::decay_t<MoreAxis>...>;
        using counter_type = NGCount<sizeof...(MoreAxis) + 1>;
        using axis_indices = std::make_index_sequence<sizeof...(MoreAxis) + 1>;

        // meta for NNGHist::IntegrateOver and NNGHist::Rebin
        template <typename T, typename U>
        struct axis_selection;

        template <typename TTuple, std::size_t... Is>
        struct axis_selection<TTuple, std::index_sequence<Is...>>
        {
            using type = NNGHist<typename std::tuple_element<Is, TTuple>::type...>;
        };

    public:
        template <typename... Args>
        static std::shared_ptr<NNGHist<std::decay_t<FirstAxis>, std::decay_t<MoreAxis>...>> MakeHist(Args &&...args)
        {
            return std::make_shared<NNGHist<std::decay_t<FirstAxis>, std::decay_t<MoreAxis>...>>(std::forward<Args>(args)...);
        }

        NNGHist(FirstAxis &&axis, MoreAxis &&...moreaxis)
            : myaxis(std::forward<FirstAxis>(axis), std::forward<MoreAxis>(moreaxis)...),
              dimension(sizeof...(MoreAxis) + 1)
        {
            int contentsize = 1;
            TupleManipulation::tuple_for_each(myaxis, [&](auto &item)
                                              { contentsize *= item.Nbins(OverflowOption::WithOverflows); });
            TupleManipulation::tuple_for_each(myaxis, [&](auto &item)
                                              { nbins.emplace_back(item.Nbins(OverflowOption::WithOverflows)); });
            content = std::vector<double>(contentsize);
            error = std::vector<double>(contentsize);
        }

        NNGHist(TH1 *h) // construct NNGHist from a ROOT histogram
            : myaxis(FirstAxis(), MoreAxis()...),
              dimension(sizeof...(MoreAxis) + 1)
        {
            // check axis type initially
            constexpr size_t HistogramDimension = std::tuple_size_v<axis_type>;
            static_assert(HistogramDimension <= 3, "must less than or equal to 3");

            // check the axis type compatible with the input hist
            if (!h)
            {
                std::cout << "input hist is null" << std::endl;
                return;
            }

            if (this->dimension != h->GetDimension())
            {
                std::cout << "input hist's dimension does not match" << std::endl;
                return;
            }

            if constexpr (HistogramDimension == 1)
            {
                std::get<0>(myaxis) = typename std::tuple_element<0, axis_type>::type(h->GetXaxis());
            }

            if constexpr (HistogramDimension == 2)
            {
                std::get<0>(myaxis) = typename std::tuple_element<0, axis_type>::type(h->GetXaxis());
                std::get<1>(myaxis) = typename std::tuple_element<1, axis_type>::type(h->GetYaxis());
            }

            if constexpr (HistogramDimension == 3)
            {
                std::get<0>(myaxis) = typename std::tuple_element<0, axis_type>::type(h->GetXaxis());
                std::get<1>(myaxis) = typename std::tuple_element<1, axis_type>::type(h->GetYaxis());
                std::get<2>(myaxis) = typename std::tuple_element<2, axis_type>::type(h->GetZaxis());
            }

            int contentsize = 1;
            TupleManipulation::tuple_for_each(myaxis, [&](auto &item)
                                              { contentsize *= item.Nbins(OverflowOption::WithOverflows); });
            TupleManipulation::tuple_for_each(myaxis, [&](auto &item)
                                              { nbins.emplace_back(item.Nbins(OverflowOption::WithOverflows)); });
            content = std::vector<double>(contentsize);
            error = std::vector<double>(contentsize);

            ConvertFromTHXD(h);
        }

        template <typename... FillPar>
        int Fill(FillPar &&...par)
        {
            constexpr std::size_t naxis = sizeof...(MoreAxis) + 1;
            constexpr std::size_t npar = sizeof...(FillPar);
            static_assert(npar == naxis || npar == naxis + 1, "must be match");
            int binnumber = 0;
            bool outrange = false;
            double weight = 1;
            if constexpr (naxis == npar)
                FindBin_impl(std::forward_as_tuple(par...), binnumber, outrange);
            if constexpr (naxis + 1 == npar)
                FindBinWithWeight_impl(std::forward_as_tuple(par...), binnumber, weight, outrange);
            content[binnumber] += weight;
            error[binnumber] += weight * weight;
            if (!outrange || StatsOption == OverflowOption::WithOverflows)
                StatsCounter.Add(par...);
            return binnumber;
        }

        template <typename... FillPar>
        int FindBin(FillPar &&...par) const
        {
            static_assert(sizeof...(FillPar) == std::tuple_size<std::remove_reference_t<axis_type>>::value, "must match");
            int binnumber = 0;
            bool outrange = false;
            FindBin_impl(std::forward_as_tuple(par...), binnumber, outrange);
            return binnumber;
        }

        template <typename... BinIndex>
        double GetBinContent(BinIndex &&...indices) const
        {
            static_assert(sizeof...(BinIndex) == std::tuple_size<std::remove_reference_t<axis_type>>::value, "must match");
            return content[GetBin_impl(std::forward_as_tuple(indices...))];
        }

        template <typename... BinIndex>
        double GetBinError(BinIndex &&...indices) const
        {
            static_assert(sizeof...(BinIndex) == std::tuple_size<std::remove_reference_t<axis_type>>::value, "must match");
            return sqrt(fabs(error[GetBin_impl(std::forward_as_tuple(indices...))]));
        }

        template <typename... BinIndex>
        int GetBin(BinIndex &&...indices) const
        {
            static_assert(sizeof...(BinIndex) == std::tuple_size<std::remove_reference_t<axis_type>>::value, "must match");
            return GetBin_impl(std::forward_as_tuple(indices...));
        }

        template <size_t Index>
        auto GetAxis() const -> const typename std::tuple_element<Index, axis_type>::type &
        {
            static_assert(Index < std::tuple_size<std::remove_reference_t<axis_type>>::value, "index must be lower than dimension");
            return std::cref(std::get<Index>(myaxis));
        }

        void SetStatOverflows(OverflowOption option = OverflowOption::WithOverflows)
        {
            this->StatsOption = option;
        }

        const counter_type &GetStats()
        {
            // check if SetBinContent or SetBinError called
            if (!isResetStatsNeeded)
                return StatsCounter;
            else
            {
                // recalculate stats info
                StatsCounter.Reset();
                ResetStatsCounter_impl();
                isResetStatsNeeded = false;
                return StatsCounter;
            }
        }

        double Integral(OverflowOption option = OverflowOption::WithoutOverflows) const
        {
            if (option == OverflowOption::WithoutOverflows)
            {
                double sum;
                this->Integral_impl(sum);
                return sum;
            }
            else
                return std::accumulate(content.begin(), content.end(), typename decltype(content)::value_type(0));
        }

        template <size_t AxisIndex>
        auto IntegrateOver(OverflowOption option = OverflowOption::WithoutOverflows)
        {
            static_assert(AxisIndex < std::tuple_size<std::remove_reference_t<axis_type>>::value, "index must be lower than dimension");

            if constexpr (std::tuple_size<std::remove_reference_t<axis_type>>::value == 1)
                // if only one axis return the integrate value
                return Integral(option);
            else
            {
                // or integrate over certain axis
                using reduced_tuple_type = typename TupleManipulation::remove_ith_type<AxisIndex, axis_type>::type;
                using reduced_tuple_index_sequence = std::make_index_sequence<std::tuple_size_v<reduced_tuple_type>>;
                typename axis_selection<reduced_tuple_type, reduced_tuple_index_sequence>::type newhist(TupleManipulation::remove_ith_element<AxisIndex>(myaxis));
                if (option == OverflowOption::WithoutOverflows)
                    IntegrateOverWithoutOverflows_impl<AxisIndex>(newhist);
                if (option == OverflowOption::WithOverflows)
                    IntegrateOverWithOverflows_impl<AxisIndex>(newhist);

                // set the counter
                newhist.StatsCounter = this->GetStats().template IntegrateOver<AxisIndex>();
                return newhist;
            }
        }

        template <typename... BinIndexAndContent>
        void SetBinContent(BinIndexAndContent &&...indices_and_content)
        {
            static_assert(sizeof...(BinIndexAndContent) - 1 == std::tuple_size<std::remove_reference_t<axis_type>>::value, "must match");
            content[GetBin_impl<decltype(std::forward_as_tuple(indices_and_content...)), 0,
                                sizeof...(BinIndexAndContent) - 1>(std::forward_as_tuple(indices_and_content...))] =
                std::get<sizeof...(BinIndexAndContent) - 1>(std::forward_as_tuple(indices_and_content...));

            // reset counter
            isResetStatsNeeded = true;
        }

        template <typename... BinIndexAndError>
        void SetBinError(BinIndexAndError &&...indices_and_error)
        {
            static_assert(sizeof...(BinIndexAndError) - 1 == std::tuple_size<std::remove_reference_t<axis_type>>::value, "must match");
            error[GetBin_impl<decltype(std::forward_as_tuple(indices_and_error...)), 0,
                              sizeof...(BinIndexAndError) - 1>(std::forward_as_tuple(indices_and_error...))] =
                std::get<sizeof...(BinIndexAndError) - 1>(std::forward_as_tuple(indices_and_error...)) *
                std::get<sizeof...(BinIndexAndError) - 1>(std::forward_as_tuple(indices_and_error...));

            // reset counter
            isResetStatsNeeded = true;
        }

        template <size_t AxisIndex, typename NewAxis>
        auto Rebin(NewAxis &&newaxis)
        {
            static_assert(AxisIndex < std::tuple_size<std::remove_reference_t<axis_type>>::value, "index must be lower than dimension");
            using reduced_tuple_type = typename TupleManipulation::replace_ith_type<AxisIndex, typename std::decay_t<NewAxis>, axis_type>::type;
            using reduced_tuple_index_sequence = std::make_index_sequence<std::tuple_size_v<reduced_tuple_type>>;
            typename axis_selection<reduced_tuple_type, reduced_tuple_index_sequence>::type newhist(TupleManipulation::replace_ith_element<AxisIndex>(std::forward<NewAxis>(newaxis), myaxis));

            Rebin_impl<AxisIndex>(newhist, std::forward<NewAxis>(newaxis));
            newhist.StatsCounter = this->GetStats();
            return newhist;
        }

        void Scale(double factor)
        {
            // scale factor must be greater than zero
            factor = fabs(factor);
            if (factor != 0)
            {
                for (int i = 0; i < this->content.size(); i++)
                {
                    this->content[i] /= factor;
                    this->error[i] /= factor * factor;
                }
                StatsCounter.Scale(factor);
            }
        }

        void Reset()
        {
            this->StatsOption = OverflowOption::WithoutOverflows;
            for (int i = 0; i < this->content.size(); i++)
            {
                this->content[i] = 0;
                this->error[i] = 0;
            }

            this->StatsCounter.Reset();
        }

        int GetMaximumBin(OverflowOption option = OverflowOption::WithoutOverflows) const
        {
            if (option == OverflowOption::WithoutOverflows)
            {
                int maxpos = 0;
                GetMaximumBin_impl(maxpos);
                return maxpos;
            }
            else
            {
                auto maxpos = std::max_element(content.begin(), content.end());
                return std::distance(content.begin(), maxpos);
            }
        }

        int GetMinimumBin(OverflowOption option = OverflowOption::WithoutOverflows) const
        {
            if (option == OverflowOption::WithoutOverflows)
            {
                int minpos = 0;
                GetMinimumBin_impl(minpos);
                return minpos;
            }
            else
            {
                auto minpos = std::min_element(content.begin(), content.end());
                return std::distance(content.begin(), minpos);
            }
        }

        double GetMaximum(OverflowOption option = OverflowOption::WithoutOverflows) const
        {
            return this->content[GetMaximumBin(option)];
        }

        double GetMinimum(OverflowOption option = OverflowOption::WithoutOverflows) const
        {
            return this->content[GetMinimumBin(option)];
        }

        template <size_t AxisIndex>
        auto Projection(OverflowOption option = OverflowOption::WithoutOverflows)
        {
            static_assert(AxisIndex < std::tuple_size<std::remove_reference_t<axis_type>>::value, "index must be lower than dimension");
            using newhisttype = typename std::tuple_element<AxisIndex, axis_type>::type;
            NNGHist<newhisttype> newhist(std::get<AxisIndex>(myaxis));

            if (option == OverflowOption::WithoutOverflows)
                ProjectionWithoutOverflows_impl<AxisIndex>(newhist);
            else
                ProjectionWithOverflows_impl<AxisIndex>(newhist);

            this->GetStats();
            newhist.StatsCounter.Entries = this->StatsCounter.Entries;
            newhist.StatsCounter.SumW = this->StatsCounter.SumW;
            newhist.StatsCounter.SumW2 = this->StatsCounter.SumW2;
            newhist.StatsCounter.SumX[0] = this->StatsCounter.SumX[AxisIndex];
            newhist.StatsCounter.SumX2[0][0] = this->StatsCounter.SumX2[AxisIndex][AxisIndex];
            return newhist;
        }

        // the following functions should be modified
        // but I'm too lazy
        template <size_t AxisIndex>
        double GetMomentAboutOrigin(size_t order, OverflowOption option = OverflowOption::WithoutOverflows)
        {
            return 0;
        }

        template <size_t AxisIndex>
        double GetCentralMoment(size_t order, OverflowOption option = OverflowOption::WithoutOverflows)
        {
            return 0;
        }

        template <size_t AxisIndex>
        double GetSkewness(OverflowOption option = OverflowOption::WithoutOverflows)
        {
            static_assert(AxisIndex < std::tuple_size<std::remove_reference_t<axis_type>>::value, "index must be lower than dimension");
            using newhisttype = typename std::tuple_element<AxisIndex, axis_type>::type;

            NNGHist<newhisttype> *histptr;
            if (dimension == 1)
                histptr = this;
            else
                histptr = new NNGHist<newhisttype>(this->Projection<AxisIndex>());

            // calculation based on the bin content and bin error
            int ilow, iup;
            if (option == OverflowOption::WithoutOverflows)
            {
                ilow = 1;
                iup = std::get<AxisIndex>(myaxis).Nbins();
            }
            else
            {
                ilow = 0;
                iup = std::get<AxisIndex>(myaxis).Nbins(option) - 1;
            }

            // should consider if the mean contains the overflows, but I omit it here
            double mean = histptr->GetStats().GetMean(0);
            double stdvar = histptr->GetStats().GetStdVar(0);
            double sumx = 0;
            double sumw = 0;
            for (int i = ilow; i <= iup; i++)
            {
                sumx += pow(histptr->template GetAxis<0>().GetBinCenter(i) - mean, 3) * histptr->GetBinContent(i);
                sumw += histptr->GetBinContent(i);
            }

            if (dimension != 1)
                delete histptr;

            return stdvar != 0 ? sumx / (sumw * pow(stdvar, 3)) : 0;
        }

        template <size_t AxisIndex>
        double GetKurtosis(OverflowOption option = OverflowOption::WithoutOverflows)
        {
            return 0;
        }

        // Convert to THXD
        auto ConvertToTHXD(const char *name)
        {
            constexpr size_t HistogramDimension = std::tuple_size_v<axis_type>;
            static_assert(HistogramDimension <= 3, "must less than or equal to 3");

            TH1 *rh = nullptr;

            if constexpr (HistogramDimension == 1)
            {
                using axis0_type = typename std::tuple_element<0, axis_type>::type;
                static_assert(std::is_same<axis0_type, Axis::Regular>::value || std::is_same<axis0_type, Axis::Variable>::value, "Axis type not match");

                if constexpr (std::is_same<axis0_type, Axis::Regular>::value)
                    rh = new TH1D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).min, std::get<0>(myaxis).max);
                else if constexpr (std::is_same<axis0_type, Axis::Variable>::value)
                    rh = new TH1D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).GetBinDivisions().data());

                rh->Sumw2();
                rh->SetDirectory(0);

                if (this->StatsOption == OverflowOption::WithOverflows)
                    rh->SetStatOverflows(TH1::EStatOverflows::kConsider);

                rh->SetContent(this->content.data());
                rh->SetError(this->content.data());

                this->GetStats();
                double stats[4];
                stats[0] = this->StatsCounter.SumW;
                stats[1] = this->StatsCounter.SumW2;
                stats[2] = this->StatsCounter.SumX[0];
                stats[3] = this->StatsCounter.SumX2[0][0];
                rh->PutStats(stats);
            }
            else if constexpr (HistogramDimension == 2)
            {
                using axis0_type = typename std::tuple_element<0, axis_type>::type;
                using axis1_type = typename std::tuple_element<1, axis_type>::type;
                static_assert((std::is_same<axis0_type, Axis::Regular>::value || std::is_same<axis0_type, Axis::Variable>::value ||
                               std::is_same<axis1_type, Axis::Regular>::value || std::is_same<axis1_type, Axis::Variable>::value),
                              "Axis type not match");

                if constexpr (std::is_same<axis0_type, Axis::Regular>::value)
                {
                    if constexpr (std::is_same<axis1_type, Axis::Regular>::value)
                        rh = new TH2D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).min, std::get<0>(myaxis).max,
                                      std::get<1>(myaxis).Nbins(), std::get<1>(myaxis).min, std::get<1>(myaxis).max);
                    else if constexpr (std::is_same<axis1_type, Axis::Variable>::value)
                        rh = new TH2D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).min, std::get<0>(myaxis).max,
                                      std::get<1>(myaxis).Nbins(), std::get<1>(myaxis).GetBinDivisions().data());
                }
                else if constexpr (std::is_same<axis0_type, Axis::Variable>::value)
                {
                    if constexpr (std::is_same<axis1_type, Axis::Regular>::value)
                        rh = new TH2D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).GetBinDivisions().data(),
                                      std::get<1>(myaxis).Nbins(), std::get<1>(myaxis).min, std::get<1>(myaxis).max);
                    else if constexpr (std::is_same<axis1_type, Axis::Variable>::value)
                        rh = new TH2D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).GetBinDivisions().data(),
                                      std::get<1>(myaxis).Nbins(), std::get<1>(myaxis).GetBinDivisions().data());
                }

                rh->Sumw2();
                rh->SetDirectory(0);

                if (this->StatsOption == OverflowOption::WithOverflows)
                    rh->SetStatOverflows(TH1::EStatOverflows::kConsider);

                rh->SetContent(this->content.data());
                rh->SetError(this->error.data());

                this->GetStats();
                double stats[7];
                stats[0] = this->StatsCounter.SumW;
                stats[1] = this->StatsCounter.SumW2;
                stats[2] = this->StatsCounter.SumX[0];
                stats[3] = this->StatsCounter.SumX2[0][0];
                stats[4] = this->StatsCounter.SumX[1];
                stats[5] = this->StatsCounter.SumX2[1][1];
                stats[6] = this->StatsCounter.SumX2[0][1];
                rh->PutStats(stats);
            }
            else if constexpr (HistogramDimension == 3)
            {
                using axis0_type = typename std::tuple_element<0, axis_type>::type;
                using axis1_type = typename std::tuple_element<1, axis_type>::type;
                using axis2_type = typename std::tuple_element<2, axis_type>::type;
                static_assert((std::is_same<axis0_type, Axis::Regular>::value && std::is_same<axis1_type, Axis::Regular>::value && std::is_same<axis1_type, Axis::Regular>::value) ||
                                  (std::is_same<axis0_type, Axis::Variable>::value && std::is_same<axis1_type, Axis::Variable>::value && std::is_same<axis1_type, Axis::Variable>::value),
                              "Axis type not match");

                if constexpr (std::is_same<axis0_type, Axis::Regular>::value && std::is_same<axis1_type, Axis::Regular>::value && std::is_same<axis2_type, Axis::Regular>::value)
                {
                    rh = new TH3D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).min, std::get<0>(myaxis).max,
                                  std::get<1>(myaxis).Nbins(), std::get<1>(myaxis).min, std::get<1>(myaxis).max,
                                  std::get<2>(myaxis).Nbins(), std::get<2>(myaxis).min, std::get<2>(myaxis).max);
                }
                else if constexpr (std::is_same<axis0_type, Axis::Variable>::value && std::is_same<axis1_type, Axis::Variable>::value && std::is_same<axis2_type, Axis::Variable>::value)
                {
                    rh = new TH3D(name, name, std::get<0>(myaxis).Nbins(), std::get<0>(myaxis).GetBinDivisions().data(),
                                  std::get<1>(myaxis).Nbins(), std::get<1>(myaxis).GetBinDivisions().data(),
                                  std::get<2>(myaxis).Nbins(), std::get<2>(myaxis).GetBinDivisions().data());
                }

                rh->Sumw2();
                rh->SetDirectory(0);

                if (this->StatsOption == OverflowOption::WithOverflows)
                    rh->SetStatOverflows(TH1::EStatOverflows::kConsider);

                rh->SetContent(this->content.data());
                rh->SetError(this->error.data());

                this->GetStats();
                double stats[11];
                stats[0] = this->StatsCounter.SumW;
                stats[1] = this->StatsCounter.SumW2;
                stats[2] = this->StatsCounter.SumX[0];
                stats[3] = this->StatsCounter.SumX2[0][0];
                stats[4] = this->StatsCounter.SumX[1];
                stats[5] = this->StatsCounter.SumX2[1][1];
                stats[6] = this->StatsCounter.SumX2[0][1];
                stats[7] = this->StatsCounter.SumX[2];
                stats[8] = this->StatsCounter.SumX2[2][2];
                stats[9] = this->StatsCounter.SumX2[0][2];
                stats[10] = this->StatsCounter.SumX2[1][2];
                rh->PutStats(stats);
            }

            if constexpr (HistogramDimension == 1)
                return (TH1D *)rh;
            else if constexpr (HistogramDimension == 2)
                return (TH2D *)rh;
            else if constexpr (HistogramDimension == 3)
                return (TH3D *)rh;
        }

        bool ConvertFromTHXD(TH1 *rh)
        {
            constexpr size_t HistogramDimension = std::tuple_size_v<axis_type>;
            static_assert(HistogramDimension <= 3, "must less than or equal to 3");

            // check the sizes of the rh and this histogram
            if constexpr (HistogramDimension == 1)
            {
                using axis0_type = typename std::tuple_element<0, axis_type>::type;
                static_assert(std::is_same_v<axis0_type, Axis::Variable> || std::is_same_v<axis0_type, Axis::Regular>, "axis type must be Regular or Variable");
                if (rh->GetNbinsX() != std::get<0>(myaxis).Nbins())
                    return false;
                else
                {
                    this->StatsCounter.Reset();
                    for (int i = 0; i < std::get<0>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                        this->SetBinContent(i, rh->GetBinContent(i));

                    if (rh->GetStatOverflows() == TH1D::EStatOverflows::kConsider)
                        this->StatsOption = OverflowOption::WithOverflows;
                    this->isResetStatsNeeded = false;

                    double stats[4];
                    rh->GetStats(stats);
                    this->StatsCounter.SumW = stats[0];
                    this->StatsCounter.SumW2 = stats[1];
                    this->StatsCounter.SumX[0] = stats[2];
                    this->StatsCounter.SumX2[0][0] = stats[3];

                    return true;
                }
            }

            if constexpr (HistogramDimension == 2)
            {
                using axis0_type = typename std::tuple_element<0, axis_type>::type;
                using axis1_type = typename std::tuple_element<1, axis_type>::type;
                static_assert(std::is_same_v<axis0_type, Axis::Variable> || std::is_same_v<axis0_type, Axis::Regular>, "axis type must be Regular or Variable");
                static_assert(std::is_same_v<axis1_type, Axis::Variable> || std::is_same_v<axis1_type, Axis::Regular>, "axis type must be Regular or Variable");
                if (rh->GetNbinsX() != std::get<0>(myaxis).Nbins() || rh->GetNbinsY() != std::get<1>(myaxis).Nbins())
                    return false;
                else
                {
                    this->StatsCounter.Reset();
                    for (int i = 0; i < std::get<0>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                        for (int j = 0; j < std::get<1>(myaxis).Nbins(OverflowOption::WithOverflows); j++)
                            this->SetBinContent(i, j, rh->GetBinContent(i, j));

                    if (rh->GetStatOverflows() == TH1D::EStatOverflows::kConsider)
                        this->StatsOption = OverflowOption::WithOverflows;
                    this->isResetStatsNeeded = false;

                    double stats[7];
                    rh->GetStats(stats);
                    this->StatsCounter.SumW = stats[0];
                    this->StatsCounter.SumW2 = stats[1];
                    this->StatsCounter.SumX[0] = stats[2];
                    this->StatsCounter.SumX2[0][0] = stats[3];
                    this->StatsCounter.SumX[1] = stats[4];
                    this->StatsCounter.SumX2[1][1] = stats[5];
                    this->StatsCounter.SumX2[0][1] = stats[6];

                    return true;
                }
            }

            if constexpr (HistogramDimension == 3)
            {
                using axis0_type = typename std::tuple_element<0, axis_type>::type;
                using axis1_type = typename std::tuple_element<1, axis_type>::type;
                using axis2_type = typename std::tuple_element<2, axis_type>::type;
                static_assert((std::is_same_v<axis0_type, Axis::Regular> && std::is_same_v<axis1_type, Axis::Regular> && std::is_same_v<axis2_type, Axis::Regular>) ||
                                  (std::is_same_v<axis0_type, Axis::Variable> && std::is_same_v<axis1_type, Axis::Variable> && std::is_same_v<axis2_type, Axis::Variable>),
                              "axis type must all be Regular");

                if (rh->GetNbinsX() != std::get<0>(myaxis).Nbins() || rh->GetNbinsY() != std::get<1>(myaxis).Nbins() || rh->GetNbinsZ() != std::get<2>(myaxis).Nbins())
                    return false;
                else
                {
                    this->StatsCounter.Reset();
                    for (int i = 0; i < std::get<0>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                        for (int j = 0; j < std::get<1>(myaxis).Nbins(OverflowOption::WithOverflows); j++)
                            for (int k = 0; k < std::get<1>(myaxis).Nbins(OverflowOption::WithOverflows); k++)
                                this->SetBinContent(i, j, k, rh->GetBinContent(i, j, k));

                    if (rh->GetStatOverflows() == TH1D::EStatOverflows::kConsider)
                        this->StatsOption = OverflowOption::WithOverflows;
                    this->isResetStatsNeeded = false;

                    double stats[11];
                    rh->GetStats(stats);
                    this->StatsCounter.SumW = stats[0];
                    this->StatsCounter.SumW2 = stats[1];
                    this->StatsCounter.SumX[0] = stats[2];
                    this->StatsCounter.SumX2[0][0] = stats[3];
                    this->StatsCounter.SumX[1] = stats[4];
                    this->StatsCounter.SumX2[1][1] = stats[5];
                    this->StatsCounter.SumX2[0][1] = stats[6];
                    this->StatsCounter.SumX[2] = stats[7];
                    this->StatsCounter.SumX2[2][2] = stats[8];
                    this->StatsCounter.SumX2[0][2] = stats[9];
                    this->StatsCounter.SumX2[1][2] = stats[10];

                    return true;
                }
            }

            return false;
        }

        bool Add(NNGHist<FirstAxis, MoreAxis...> &inh, double factor = 1)
        {
            // check whether the axes are equal
            if (this->myaxis == inh.myaxis)
            {
                if (inh.isResetStatsNeeded)
                {
                    inh.ResetStatsCounter_impl();
                    inh.isResetStatsNeeded = false;
                }

                if (this->StatsOption != inh.StatsOption)
                {
                    // print out some warnings
                }

                this->StatsCounter.Combine(inh.StatsCounter, factor);
                for (int i = 0; i < this->content.size(); i++)
                {
                    this->content[i] += factor * inh.content[i];
                    this->error[i] += factor * factor * inh.error[i];
                }

                return true;
            }
            else
            {
                // print out sth
                return false;
            }
        }

    private:
        NNGHist(axis_type &&tuple_axis)
            : myaxis(std::forward<axis_type>(tuple_axis)), dimension(std::tuple_size_v<std::remove_reference_t<axis_type>>)
        {
            int contentsize = 1;
            TupleManipulation::tuple_for_each(myaxis, [&](auto &item)
                                              { contentsize *= item.Nbins(OverflowOption::WithOverflows); });
            TupleManipulation::tuple_for_each(myaxis, [&](auto &item)
                                              { nbins.emplace_back(item.Nbins(OverflowOption::WithOverflows)); });
            content = std::vector<double>(contentsize);
            error = std::vector<double>(contentsize);
        }

        template <
            typename TTuple,                                                            // the tuple type
            size_t Index = std::tuple_size<std::remove_reference_t<TTuple>>::value - 1, // start iteration at Size - 1 index
            size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value       // tuple size
            >
        inline void FindBin_impl(TTuple &&value, int &binnumber, bool &outrange) const
        {
            if constexpr (Index >= 0)
            {
                if constexpr (Index == Size - 1)
                {
                    int temp = std::get<Index>(myaxis).FindBin(std::get<Index>(value));
                    binnumber += temp;
                    if (temp == 0 || temp > std::get<Index>(myaxis).Nbins())
                        outrange = true;
                }

                if constexpr (Index < Size - 1)
                {
                    binnumber *= std::get<Index>(myaxis).Nbins(OverflowOption::WithOverflows);
                    int temp = std::get<Index>(myaxis).FindBin(std::get<Index>(value));
                    binnumber += temp;
                    if (temp == 0 || temp > std::get<Index>(myaxis).Nbins())
                        outrange = true;
                }

                if constexpr (Index >= 1)
                    FindBin_impl<TTuple, Index - 1, Size>(std::forward<TTuple>(value), std::ref(binnumber), outrange);
            }
        }

        template <
            typename TTuple,                                                            // the tuple type
            size_t Index = std::tuple_size<std::remove_reference_t<TTuple>>::value - 2, // start iteration at Size - 2 index
            size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value       // tuple size
            >
        inline void FindBinWithWeight_impl(TTuple &&value, int &binnumber, double &weight, bool &outrange) const
        {
            // the last of the value is the weight
            weight = std::get<Size - 1>(value);
            if constexpr (Index >= 0)
                FindBin_impl<TTuple, Index, Size - 1>(std::forward<TTuple>(value), std::ref(binnumber), outrange);
        }

        template <
            typename TTuple,                                                      // the tuple type
            size_t Index = 0,                                                     // start iteration at Size - 1 index
            size_t Size = std::tuple_size<std::remove_reference_t<TTuple>>::value // tuple size
            >
        inline int GetBin_impl(TTuple &&value) const
        {
            int nbins = std::get<Index>(myaxis).Nbins(OverflowOption::WithOverflows);
            int getbin;
            if (std::get<Index>(value) >= 0 && std::get<Index>(value) < nbins)
                getbin = std::get<Index>(value);
            else if (std::get<Index>(value) < 0)
                getbin = 0;
            else if (std::get<Index>(value) >= nbins)
                getbin = nbins - 1;

            if constexpr (Index == Size - 1)
                return getbin;
            else
                return nbins * GetBin_impl<TTuple, Index + 1, Size>(std::forward<TTuple>(value)) + getbin;
        }

        template <size_t Index = std::tuple_size<axis_type>::value - 1,
                  typename... BinIndices>
        inline void Integral_impl(double &sum, BinIndices &&...indices) const
        {
            if constexpr (Index == 0)
            {
                for (size_t i = 1; i <= std::get<Index>(myaxis).Nbins(OverflowOption::WithoutOverflows); i++)
                    sum += GetBinContent(i, std::forward<BinIndices>(indices)...);
            }
            else
            {
                for (size_t i = 1; i <= std::get<Index>(myaxis).Nbins(OverflowOption::WithoutOverflows); i++)
                    Integral_impl<Index - 1>(sum, i, std::forward<BinIndices>(indices)...);
            }
        }

        template <size_t IntegrateIndex,                                       // axis be integrated over
                  size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename... MoreNewAxis,                                     // return new histogram
                  typename... BinIndices>                                      // Bin indices
        inline void IntegrateOverWithoutOverflows_impl(NNGHist<MoreNewAxis...> &newh, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex != IntegrateIndex && CurrentIndex == 0)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                {
                    int newbinnumber = newh.GetBin_impl(TupleManipulation::remove_ith_element<IntegrateIndex>(std::forward_as_tuple(i, indices...)));
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else if constexpr (CurrentIndex == IntegrateIndex && CurrentIndex == 0)
            {
                for (size_t i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithoutOverflows); i++)
                {
                    int newbinnumber = newh.GetBin_impl(TupleManipulation::remove_ith_element<IntegrateIndex>(std::forward_as_tuple(i, indices...)));
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else if constexpr (CurrentIndex != IntegrateIndex)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                    IntegrateOverWithoutOverflows_impl<IntegrateIndex, CurrentIndex - 1>(newh, i, std::forward<BinIndices>(indices)...);
            }
            else if constexpr (CurrentIndex == IntegrateIndex)
            {
                for (size_t i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithoutOverflows); i++)
                    IntegrateOverWithoutOverflows_impl<IntegrateIndex, CurrentIndex - 1>(newh, i, std::forward<BinIndices>(indices)...);
            }
        }

        template <size_t IntegrateIndex,                                       // axis be integrated over
                  size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename... MoreNewAxis,                                     // return new histogram
                  typename... BinIndices>                                      // Bin indices
        inline void IntegrateOverWithOverflows_impl(NNGHist<MoreNewAxis...> &newh, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex == 0)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(this->myaxis).Nbins(OverflowOption::WithOverflows); i++)
                {
                    int newbinnumber = newh.GetBin_impl(TupleManipulation::remove_ith_element<IntegrateIndex>(std::forward_as_tuple(i, indices...)));
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else
                for (size_t i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                    IntegrateOverWithOverflows_impl<IntegrateIndex, CurrentIndex - 1>(newh, i, std::forward<BinIndices>(indices)...);
        }

        template <size_t RIndex,                                               // axis be replaced over
                  size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename NewAxis,                                            // New axis type
                  typename... MoreNewAxis,                                     // return new histogram
                  typename... BinIndices>                                      // Bin indices
        inline void Rebin_impl(NNGHist<MoreNewAxis...> &newh, NewAxis &&newaxis, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex == 0)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(this->myaxis).Nbins(OverflowOption::WithOverflows); i++)
                {
                    int newbinnumber = 0;
                    bool outrange;
                    newh.FindBin_impl(this->GetBinCenter_impl(std::forward_as_tuple(i, indices...), axis_indices{}), newbinnumber, outrange);
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else
                for (size_t i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                    Rebin_impl<RIndex, CurrentIndex - 1>(newh, std::forward<NewAxis>(newaxis), i, std::forward<BinIndices>(indices)...);
        }

        template <typename TTuple, std::size_t... I>
        auto GetBinCenter_impl(TTuple &&indices, std::index_sequence<I...>) const
        {
            // no check on the size of BinIndices
            return std::make_tuple(std::get<I>(myaxis).GetBinCenter(std::get<I>(indices))...);
        }

        template <size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename... BinIndices>                                      // Bin indices
        void GetMaximumBin_impl(int &maxpos, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex == 0)
            {
                for (int i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                {
                    int currentbinnumber = GetBin_impl(std::forward_as_tuple(i, indices...));
                    if (content[maxpos] < content[currentbinnumber])
                        maxpos = currentbinnumber;
                }
            }
            else
                for (int i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                    GetMaximumBin_impl<CurrentIndex - 1>(maxpos, i, std::forward<BinIndices>(indices)...);
        }

        template <size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename... BinIndices>                                      // Bin indices
        void GetMinimumBin_impl(int &minpos, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex == 0)
            {
                for (int i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                {
                    int currentbinnumber = GetBin_impl(std::forward_as_tuple(i, indices...));
                    if (content[minpos] > content[currentbinnumber])
                        minpos = currentbinnumber;
                }
            }
            else
                for (int i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                    GetMinimumBin_impl<CurrentIndex - 1>(minpos, i, std::forward<BinIndices>(indices)...);
        }

        template <size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename... BinIndices>                                      // Bin indices
        void ResetStatsCounter_impl(BinIndices &&...indices)
        {
            if constexpr (CurrentIndex == 0)
            {
                if (StatsOption == OverflowOption::WithoutOverflows)
                    for (int i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                    {
                        double currentcontent = this->GetBinContent(i, std::forward<BinIndices>(indices)...);
                        double currenterror = this->GetBinError(i, std::forward<BinIndices>(indices)...);
                        StatsCounter.Add_tuple(this->GetBinCenter_impl(std::forward_as_tuple(i, indices...), axis_indices{}), currentcontent);
                        // add sumw2
                        StatsCounter.SumW2 += currenterror * currenterror - currentcontent * currentcontent;
                    }
                else
                {
                    for (int i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                    {
                        double currentcontent = this->GetBinContent(i, std::forward<BinIndices>(indices)...);
                        double currenterror = this->GetBinError(i, std::forward<BinIndices>(indices)...);
                        StatsCounter.Add_tuple(this->GetBinCenter_impl(std::forward_as_tuple(i, indices...), axis_indices{}), currentcontent);
                        // add sumw2
                        StatsCounter.SumW2 += currenterror * currenterror - currentcontent * currentcontent;
                    }
                }
            }
            else
            {
                if (StatsOption == OverflowOption::WithoutOverflows)
                    for (int i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                        ResetStatsCounter_impl<CurrentIndex - 1>(i, std::forward<BinIndices>(indices)...);
                else
                    for (int i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                        ResetStatsCounter_impl<CurrentIndex - 1>(i, std::forward<BinIndices>(indices)...);
            }
        }

        template <size_t ProjectionIndex,                                      // axis be project onto
                  size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename NewAxis,                                            // return new histogram
                  typename... BinIndices>                                      // Bin indices
        inline void ProjectionWithoutOverflows_impl(NNGHist<NewAxis> &newh, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex == 0 && CurrentIndex != ProjectionIndex)
            {
                for (size_t i = 1; i <= std::get<CurrentIndex>(this->myaxis).Nbins(); i++)
                {
                    int newbinnumber = std::get<ProjectionIndex>(std::forward_as_tuple(i, indices...));
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else if constexpr (CurrentIndex == 0 && CurrentIndex == ProjectionIndex)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(this->myaxis).Nbins(OverflowOption::WithOverflows); i++)
                {
                    int newbinnumber = std::get<ProjectionIndex>(std::forward_as_tuple(i, indices...));
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else if constexpr (CurrentIndex != ProjectionIndex)
            {
                for (size_t i = 1; i <= std::get<CurrentIndex>(myaxis).Nbins(); i++)
                    ProjectionWithoutOverflows_impl<ProjectionIndex, CurrentIndex - 1>(newh, i, std::forward<BinIndices>(indices)...);
            }
            else if constexpr (CurrentIndex == ProjectionIndex)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                    ProjectionWithoutOverflows_impl<ProjectionIndex, CurrentIndex - 1>(newh, i, std::forward<BinIndices>(indices)...);
            }
        }

        template <size_t ProjectionIndex,                                      // axis be project onto
                  size_t CurrentIndex = std::tuple_size<axis_type>::value - 1, // current axis index
                  typename NewAxis,                                            // return new histogram
                  typename... BinIndices>                                      // Bin indices
        inline void ProjectionWithOverflows_impl(NNGHist<NewAxis> &newh, BinIndices &&...indices) const
        {
            if constexpr (CurrentIndex == 0)
            {
                for (size_t i = 0; i < std::get<CurrentIndex>(this->myaxis).Nbins(OverflowOption::WithOverflows); i++)
                {
                    int newbinnumber = std::get<ProjectionIndex>(std::forward_as_tuple(i, indices...));
                    int thisbinnumber = this->GetBin_impl(std::forward_as_tuple(i, indices...));
                    newh.content[newbinnumber] += this->content[thisbinnumber];
                    newh.error[newbinnumber] += this->error[thisbinnumber];
                }
            }
            else
                for (size_t i = 0; i < std::get<CurrentIndex>(myaxis).Nbins(OverflowOption::WithOverflows); i++)
                    ProjectionWithOverflows_impl<ProjectionIndex, CurrentIndex - 1>(newh, i, std::forward<BinIndices>(indices)...);
        }

        axis_type myaxis;
        std::size_t dimension;
        std::vector<int> nbins;
        std::vector<double> content;
        std::vector<double> error;
        OverflowOption StatsOption = OverflowOption::WithoutOverflows;

        bool isResetStatsNeeded = false;
        counter_type StatsCounter;
    };

    // Axis
    namespace Axis
    {
        class Regular
        {
        public:
            template <typename FirstAxis, typename... MoreAxis>
            friend class ::NAGASH::NNGHist; // to access to private member functions

            friend class ::NAGASH::NAGASHFile;

            Regular() = default;
            Regular(const Regular &) = default;
            Regular(Regular &&) = default;
            Regular &operator=(const Regular &) = default;
            Regular &operator=(Regular &&) = default;

            Regular(int _nbins_, double _min_, double _max_)
                : nbins(_nbins_ + 2), min(_min_), max(_max_)
            {
                if (min > max)
                    std::swap(min, max);
                width = _nbins_ != 0 ? (max - min) / _nbins_ : 0;
            }

            Regular(TAxis *axis)
            {
                if (!axis)
                    return;

                if (axis->GetXbins()->GetSize()) // the input axis is not fixed size
                {
                    nbins = axis->GetXbins()->GetSize() + 1;
                    min = axis->GetXbins()->At(0);
                    max = axis->GetXbins()->At(nbins - 2);
                }
                else
                {
                    nbins = axis->GetNbins() + 2;
                    min = axis->GetXmin();
                    max = axis->GetXmax();
                }
                width = nbins - 2 != 0 ? (max - min) / (nbins - 2) : 0;
            }

            int FindBin(double val) const
            {
                int __binnum = val < min ? 0 : (val - min) / width + 1;
                if (width != 0)
                    return __binnum > nbins - 2 ? nbins - 1 : __binnum;
                else
                    return 0;
            }

            int Nbins(OverflowOption option = OverflowOption::WithoutOverflows) const
            {
                if (option == OverflowOption::WithoutOverflows)
                    return nbins - 2;
                else
                    return nbins;
            }

            // note here the interpretation of underflow and overflow bin is not quite correct
            // however it does the job
            double GetBinCenter(int bin) const
            {
                if (bin > nbins - 1)
                    bin = nbins - 1;
                if (bin < 0)
                    bin = 0;

                return min + width * bin - width / 2;
            }

            double GetBinLowEdge(int bin) const
            {
                if (bin > nbins - 1)
                    bin = nbins - 1;
                if (bin < 0)
                    bin = 0;

                return min + width * (bin - 1);
            }

            double GetBinUpEdge(int bin) const
            {
                if (bin > nbins - 1)
                    bin = nbins - 1;
                if (bin < 0)
                    bin = 0;

                return min + width * bin;
            }

            double GetBinWidth(int bin) const
            {
                return width;
            }

            bool operator==(const Regular &inA) const
            {
                return this->nbins == inA.nbins && this->min == inA.min && this->max == inA.max;
            }

            // for read from and written to NAGASHFile
            bool Read(std::fstream &inf)
            {
                // check file
                if (!inf.is_open())
                    return false;

                // check if it's Regular type
                std::string namecheck;
                size_t stringsize;
                inf.read(reinterpret_cast<char *>(&stringsize), sizeof(size_t));
                namecheck.resize(stringsize);
                inf.read(reinterpret_cast<char *>(&namecheck[0]), namecheck.size());

                if (namecheck != this->Name)
                    return false;

                // read variables
                size_t controlblocksize;
                inf.read(reinterpret_cast<char *>(&controlblocksize), sizeof(size_t));
                inf.read(reinterpret_cast<char *>(&nbins), sizeof(int));
                inf.read(reinterpret_cast<char *>(&min), sizeof(double));
                inf.read(reinterpret_cast<char *>(&max), sizeof(double));
                width = nbins - 2 != 0 ? (max - min) / (nbins - 2) : 0;

                return true;
            }

            bool Write(std::fstream &inf)
            {
                // check file
                if (!inf.is_open())
                    return false;

                size_t stringsize = this->Name.size();
                inf.write(reinterpret_cast<char *>(&stringsize), sizeof(size_t));
                inf.write(reinterpret_cast<const char *>(&(this->Name[0])), stringsize);

                size_t controlblocksize = sizeof(int) + 2 * sizeof(double);
                inf.write(reinterpret_cast<char *>(&controlblocksize), sizeof(size_t));
                inf.write(reinterpret_cast<char *>(&nbins), sizeof(int));
                inf.write(reinterpret_cast<char *>(&min), sizeof(double));
                inf.write(reinterpret_cast<char *>(&max), sizeof(double));

                return true;
            }

        private:
            static const std::string Name;
            int nbins = 2;
            double min = 0;
            double max = 0;
            double width = 0;
        };

        inline const std::string Regular::Name = "Regular";

        class Variable
        {
            template <typename FirstAxis, typename... MoreAxis>
            friend class NAGASH::NNGHist; // to access to private member functions

            friend class ::NAGASH::NAGASHFile;

        public:
            // should warn if input vector is null
            Variable() = default;
            Variable(const Variable &) = default;
            Variable(Variable &&) = default;
            Variable &operator=(const Variable &) = default;
            Variable &operator=(Variable &&) = default;

            Variable(const std::vector<double> &_bindiv_)
                : bindiv(_bindiv_)
            {
                std::sort(bindiv.begin(), bindiv.end());
            }

            Variable(std::vector<double> &&_bindiv_)
                : bindiv(_bindiv_)
            {
                std::sort(bindiv.begin(), bindiv.end());
            }

            Variable(TAxis *axis)
            {
                if (!axis)
                    return;

                if (axis->GetXbins()->GetSize()) // the input axis is not fixed size
                {
                    bindiv = std::vector<double>(axis->GetXbins()->GetArray(), axis->GetXbins()->GetArray() + axis->GetXbins()->GetSize());
                }
                else
                {
                    int nbins = axis->GetNbins() - 1;
                    double min = axis->GetXmin();
                    double max = axis->GetXmax();
                    double width = nbins != 0 ? (max - min) / nbins : 0;

                    bindiv.reserve(nbins + 1);
                    for (int i = 0; i <= nbins; i++)
                        bindiv.emplace_back(min + i * width);
                }
            }

            int FindBin(double val) const
            {
                return std::upper_bound(bindiv.begin(), bindiv.end(), val) - bindiv.begin();
            }

            int Nbins(OverflowOption option = OverflowOption::WithoutOverflows) const
            {
                if (option == OverflowOption::WithoutOverflows)
                    return bindiv.size() - 1;
                else
                    return bindiv.size() + 1;
            }

            // note here the interpretation of underflow and overflow bin is not quite correct
            // however it does the job
            double GetBinCenter(uint64_t bin) const
            {
                if (bin > 0 && bin < bindiv.size())
                    return (bindiv[bin - 1] + bindiv[bin]) / 2;
                else if (bin <= 0) // underflow
                    return bindiv[0] - GetBinWidth(0) / 2;
                else // overflow
                    return bindiv.back() + GetBinWidth(bindiv.size()) / 2;
            }

            double GetBinWidth(uint64_t bin) const
            {
                if (bin > 0 && bin < bindiv.size())
                    return bindiv[bin] - bindiv[bin - 1];
                else if (bin <= 0) // underflow
                    return GetBinWidth(1);
                else // overflow
                    return GetBinWidth((int)bindiv.size() - 1);
            }

            double GetBinLowEdge(uint64_t bin) const
            {
                if (bin > 0 && bin < bindiv.size())
                    return bindiv[bin - 1];
                else if (bin <= 0)
                    return bindiv[0] - GetBinWidth(1);
                else if (bin >= bindiv.size())
                    return bindiv.back();
            }

            double GetBinUpEdge(uint64_t bin) const
            {
                if (bin > 0 && bin < bindiv.size())
                    return bindiv[bin];
                else if (bin <= 0)
                    return bindiv[0];
                else if (bin >= bindiv.size())
                    return bindiv.back() + GetBinWidth(bin);
            }

            const std::vector<double> GetBinDivisions() const
            {
                return bindiv;
            }

            bool operator==(const Variable &inA) const
            {
                return this->bindiv == inA.bindiv;
            }

        private:
            // for read from and written to NAGASHFile
            bool Read(std::fstream &inf)
            {
                // check file
                if (!inf.is_open())
                    return false;

                // check if it's Variable type
                std::string namecheck;
                size_t stringsize;
                inf.read(reinterpret_cast<char *>(&stringsize), sizeof(size_t));
                namecheck.resize(stringsize);
                inf.read(reinterpret_cast<char *>(&namecheck[0]), namecheck.size());

                if (namecheck != this->Name)
                    return false;

                // read variables
                size_t vecsize;
                inf.read(reinterpret_cast<char *>(&vecsize), sizeof(size_t));
                bindiv.resize(vecsize / sizeof(double));
                inf.read(reinterpret_cast<char *>(bindiv.data()), vecsize);

                return true;
            }

            bool Write(std::fstream &inf)
            {
                // check file
                if (!inf.is_open())
                    return false;

                size_t stringsize = this->Name.size();
                inf.write(reinterpret_cast<char *>(&stringsize), sizeof(size_t));
                inf.write(reinterpret_cast<const char *>(&(this->Name[0])), stringsize);
                size_t vecsize = this->bindiv.size() * sizeof(double);
                inf.write(reinterpret_cast<char *>(&vecsize), sizeof(size_t));
                inf.write(reinterpret_cast<char *>(bindiv.data()), vecsize);

                return true;
            }

            static const std::string Name;
            std::vector<double> bindiv;
        };

        inline const std::string Variable::Name = "Variable";
    }
}
