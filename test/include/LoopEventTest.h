#pragma once

#include "NAGASH/LoopEvent.h"
#include "NAGASH/Count.h"
#include "NAGASH/PlotGroup.h"
#include "NAGASH/Uncertainty.h"
#include "NAGASH/Kinematics.h"
#include "TProfile.h"

class LoopEventTest : public NAGASH::LoopEvent
{
public:
    LoopEventTest(const NAGASH::ConfigTool &c) : NAGASH::LoopEvent(c) {}
    virtual NAGASH::StatusCode InitializeData() override;
    virtual NAGASH::StatusCode InitializeUser() override;
    virtual NAGASH::StatusCode Execute() override;
    virtual NAGASH::StatusCode Finalize() override;

private:
    std::shared_ptr<NAGASH::Count> CountNEvents;
    std::shared_ptr<NAGASH::Count> CountMass;
    std::shared_ptr<NAGASH::Count> CountTest;
    std::shared_ptr<NAGASH::Plot1D> PlotWMass;
    std::shared_ptr<NAGASH::Plot1D> PlotWPt;
    std::shared_ptr<NAGASH::Plot1D> PlotWRapidity;
    std::shared_ptr<NAGASH::Plot2D> PlotWRapidity_LeptonEta;
    std::shared_ptr<NAGASH::Plot3D> PlotWp_LeptonPt_MET_LeptonEta;
    std::shared_ptr<NAGASH::Plot3D> PlotWm_LeptonPt_MET_LeptonEta;
    std::shared_ptr<NAGASH::Plot1D> PlotWp_LeptonEta;
    std::shared_ptr<NAGASH::Plot1D> PlotWm_LeptonEta;
    std::shared_ptr<NAGASH::Plot1D> PlotAsym_LeptonEta;
    std::shared_ptr<NAGASH::NGHist<TProfile>> PlotX_LeptonEta;

    Double_t MuonPxFinal;
    Double_t MuonPyFinal;
    Double_t MuonPzFinal;
    Double_t MuonEFinal;
    Double_t MuonPx;
    Double_t MuonPy;
    Double_t MuonPz;
    Double_t MuonE;
    Double_t MuonCharge;
    Double_t NeutrinoPx;
    Double_t NeutrinoPy;
    Double_t NeutrinoPz;
    Double_t NeutrinoE;
    Double_t WbosonPx;
    Double_t WbosonPy;
    Double_t WbosonPz;
    Double_t WbosonE;
    Int_t QuarkFlavour[2];
    Double_t QuarkPx[2];
    Double_t QuarkPy[2];
    Double_t QuarkPz[2];
    Double_t QuarkE[2];

    // List of branches
    TBranch *b_MuonPxFinal;  //!
    TBranch *b_MuonPyFinal;  //!
    TBranch *b_MuonPzFinal;  //!
    TBranch *b_MuonEFinal;   //!
    TBranch *b_MuonPx;       //!
    TBranch *b_MuonPy;       //!
    TBranch *b_MuonPz;       //!
    TBranch *b_MuonE;        //!
    TBranch *b_MuonCharge;   //!
    TBranch *b_NeutrinoPx;   //!
    TBranch *b_NeutrinoPy;   //!
    TBranch *b_NeutrinoPz;   //!
    TBranch *b_NeutrinoE;    //!
    TBranch *b_WbosonPx;     //!
    TBranch *b_WbosonPy;     //!
    TBranch *b_WbosonPz;     //!
    TBranch *b_WbosonE;      //!
    TBranch *b_QuarkFlavour; //!
    TBranch *b_QuarkPx;      //!
    TBranch *b_QuarkPy;      //!
    TBranch *b_QuarkPz;      //!
    TBranch *b_QuarkE;       //!
};
