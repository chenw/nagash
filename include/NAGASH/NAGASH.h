/** @mainpage Introduction to %NAGASH
## What is NAGASH?

%NAGASH is short for "Next Generation Analysis System", which is a collection of useful c++ classes based on ROOT including multi-thread support.
It enables you building your analysis easily. All classes and functions are defined within `%NAGASH` namespace.
Nagash is the Great Necromancer in popular video game Warhammer.

## Installation

Simply run following command in your console

@code{.sh}
git clone https://gitlab.cern.ch/chenw/nagash.git
cd nagash
make clean
make -j
. config.sh
@endcode

Then all executables are made in `bin ` directory and `libNAGASH.so` in `lib` directory alone with a shell script named `setupNAGASH.sh`.  Use

@code{.sh}
. setupNAGASH.sh
@endcode

to setup the enviroment variables.

## How to start?
We have several topics now which help you to build your own analysis framework via NAGASH, you can find them in \ref AnalysisClasses, \ref Histograms, \ref ResultClasses and \ref ToolClasses.

## Useful executables
%NAGASH provides several useful executables to help you fast develop your framework.

### NAGASHMakeClass

Defined in \link NAGASHMakeClass.cxx \endlink

This enables you to quickly make a LoopEvent class based on given `Root` file and `TTree` name, you may take the documenation of \link NAGASH::Analysis \endlink as an example.

### CodeGenerator

Defined in \link CodeGenerator.cxx \endlink

This allow you to fast generate some code use substitution, see following example:

`input.dat`:

@code
One Two:
1 2
3 4
5 6

Three Four Five:
11 22 33
44 55 66

Six:
7
8
9
10

Code:
One_Two_Three_Four_Five_Six_One_Two_Three
@endcode

By using command:

@code{.sh}
CodeGenerator input.dat output.dat
@endcode

You will get `output.dat` with following content:

@code
1_2_11_22_33_7_1_2_11
1_2_11_22_33_8_1_2_11
1_2_11_22_33_9_1_2_11
1_2_11_22_33_10_1_2_11
1_2_44_55_66_7_1_2_44
1_2_44_55_66_8_1_2_44
1_2_44_55_66_9_1_2_44
1_2_44_55_66_10_1_2_44
3_4_11_22_33_7_3_4_11
3_4_11_22_33_8_3_4_11
3_4_11_22_33_9_3_4_11
3_4_11_22_33_10_3_4_11
3_4_44_55_66_7_3_4_44
3_4_44_55_66_8_3_4_44
3_4_44_55_66_9_3_4_44
3_4_44_55_66_10_3_4_44
5_6_11_22_33_7_5_6_11
5_6_11_22_33_8_5_6_11
5_6_11_22_33_9_5_6_11
5_6_11_22_33_10_5_6_11
5_6_44_55_66_7_5_6_44
5_6_44_55_66_8_5_6_44
5_6_44_55_66_9_5_6_44
5_6_44_55_66_10_5_6_44
@endcode

It will substitute the given tokens with replacements with possible combinations.

## Development
If you want to contribute to %NAGASH, you may fork the repository and make your changes. Or you can submit to the issues if you enconter any problem.
 */

/** @defgroup Advance Advance Usages
    @brief Some tips to make your framework run faster.

### Avoid using "Get" in NAGASH::LoopEvent::Execute()

The NAGASH::LoopEvent::Execute() will be called for each entry in the `TTree`, and methods like NAGASH::ResultGroup::Get(),
NAGASH::ConfigTool::GetPar() and NAGASH::PlotGroup::GetPlot1D() involves a `std::map::find` called which is unnecessary and time-consuming.
To avoid the unnecessary cost, you can store the pointer of your objects or value of booked parameters as member variables.
An example is shown in [this](@ref PlotGroup_example).

### Use NAGASH::TFileHelper instead of file name to `Recover`

The opening and close of `TFile` are time-consuming. If you want to recover massive plots in your program,
you should create a NAGASH::TFileHelper object and pass it to methods like NAGASH::PlotGroup::Recover() instead of the file name.

### Use fixed-bin-size histograms whenever possible

Consider the following two ways of defining a histogram:

@code {.cxx}
// fixed bin size
TH1D *h1 = new TH1D("h1", "h1", 100, 0, 100);

// varied bin size
double bindiv[101];
for (int i=0;i <= 100;i++)
    bindiv[i] = i;
TH1D *h2 = new TH1D("h2", "h2", 100, bindiv);
@endcode

The two histograms are "identical". But the filling of `h2` will be much slower because `ROOT` uses binary search to decide the bin number.
For `h1`, since the size of bins are fixed, `ROOT` can directly calculate the bin number, thus the filling will be faster.
 */

/// @file NAGASH.h
/// This is a common header file for NAGASH. You can include this once for all.
/// @author Wenhao Ma
///

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Analysis.h"
#include "NAGASH/Chi2Fitter.h"
#include "NAGASH/ConfigTool.h"
#include "NAGASH/Count.h"
#include "NAGASH/CutFlowCounter.h"
#include "NAGASH/EventVector.h"
#include "NAGASH/FigureTool.h"
#include "NAGASH/FigureX.h"
#include "NAGASH/Graph2DTool.h"
#include "NAGASH/HistTool.h"
#include "NAGASH/Job.h"
#include "NAGASH/Kinematics.h"
#include "NAGASH/LoopEvent.h"
#include "NAGASH/MapTool.h"
#include "NAGASH/MSGTool.h"
#include "NAGASH/NAGASHFile.h"
#include "NAGASH/NGFigure.h"
#include "NAGASH/PCATool.h"
#include "NAGASH/ProfileFitter.h"
#include "NAGASH/PlotGroup.h"
#include "NAGASH/ResultGroup.h"
#include "NAGASH/Result.h"
#include "NAGASH/SystTool.h"
#include "NAGASH/TemplateFitter.h"
#include "NAGASH/TFileHelper.h"
#include "NAGASH/ThreadPool.h"
#include "NAGASH/Timer.h"
#include "NAGASH/Tool.h"
#include "NAGASH/Toolkit.h"
#include "NAGASH/Uncertainty.h"
#include "NAGASH/UnfoldTool.h"
