#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Job.h"

namespace NAGASH
{
    class TestJob : public Job
    {
    public:
        TestJob(const ConfigTool &config) : Job(config) {}
        void Change(std::string &);
        static StatusCode Process(const ConfigTool &config, std::shared_ptr<ResultGroup> result, std::string &);
    };

    void TestJob::Change(std::string &inputstr)
    {
        inputstr = "TestJob::Modified";
        MSGUser()->MSG_INFO("address of input string : ", &inputstr);
    }

    StatusCode TestJob::Process(const ConfigTool &config, std::shared_ptr<ResultGroup> result, std::string &inputstr)
    {
        TestJob j(config);
        j.Change(inputstr);
        result->Merge(j.ResultGroupUser());
        return StatusCode::SUCCESS;
    }
} // namespace NAGASH
