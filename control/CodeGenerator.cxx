//***************************************************************************************
//
//! \file CodeGenerator.cxx
//! This allow you to fast generate some code use substitution, see following example:
//! \author    Chen Wang, Wenhao Ma
//
//***************************************************************************************
#include "NAGASH/Global.h"

std::vector<std::vector<std::string>> Token;
std::vector<std::vector<std::vector<std::string>>> TokenReplacement;
std::string line;
std::string Code{""};

void trim_back(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch)
                         { return !std::isspace(ch); })
                .base(),
            s.end());
}

std::size_t replace_all(std::string &inout, std::string_view what, std::string_view with)
{
    std::size_t count{};
    for (std::string::size_type pos{};
         inout.npos != (pos = inout.find(what.data(), pos, what.length()));
         pos += with.length(), ++count)
    {
        inout.replace(pos, what.length(), with.data(), with.length());
    }
    return count;
}

void nestedforloop(size_t layer, std::string newcode, std::ofstream &out)
{
    if (layer == 0)
        newcode = Code;
    ++layer;

    size_t replacementsize = TokenReplacement[layer - 1].size();
    for (size_t i = 0; i < replacementsize; ++i)
    {
        std::string nnewcode = newcode;
        for (size_t j = 0; j < Token[layer - 1].size(); j++)
        {
            // std::cout << TokenReplacement[layer - 1][i][j] << " ";
            replace_all(nnewcode, Token[layer - 1][j], TokenReplacement[layer - 1][i][j]);
        }

        if (layer != Token.size())
            nestedforloop(layer, nnewcode, out);
        else
            out << nnewcode;
    }
}

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Usage: " << argv[0] << " (input file) (output file)" << std::endl;
        return 0;
    }

    std::ifstream infile;
    infile.open(argv[1]);
    if (!infile.is_open())
    {
        std::cout << "Error: the input file is corrupted" << std::endl;
        return 0;
    }

    bool ReadCode = false;
    bool ReadReplacement = false;
    size_t linecount = 0;
    while (std::getline(infile, line))
    {
        ++linecount;

        // remove all the trailing spaces, tabs, and carriage returns
        trim_back(line);

        if (line.size() == 0)
            continue;

        if (line.back() == ':')
        {
            std::string tokens = line;
            tokens.pop_back();

            // Parse the tokens
            std::regex word_regex("([^\\s]+)");
            auto words_begin = std::sregex_iterator(tokens.begin(), tokens.end(), word_regex);
            auto words_end = std::sregex_iterator();
            std::vector<std::string> matched_words;
            for (auto i = words_begin; i != words_end; ++i)
            {
                matched_words.push_back((*i).str());
            }

            if (matched_words.size() == 0)
            {
                std::cout << "Warning: no tokens found in line " << linecount << std::endl;
                continue;
            }

            if (matched_words.size() == 1 && matched_words[0] == "Code")
            {
                ReadCode = true;
                ReadReplacement = false;
            }
            else
            {
                Token.emplace_back(matched_words);
                TokenReplacement.emplace_back(std::vector<std::vector<std::string>>());

                ReadReplacement = true;
                ReadCode = false;
            }

            continue;
        }

        if (ReadCode)
            Code.append(line + "\n");

        if (ReadReplacement)
        {
            // Parse the replacements
            std::regex word_regex("([^\\s]+)");
            auto words_begin = std::sregex_iterator(line.begin(), line.end(), word_regex);
            auto words_end = std::sregex_iterator();
            std::vector<std::string> vreplacement;
            for (auto i = words_begin; i != words_end; ++i)
            {
                vreplacement.push_back((*i).str());
            }

            if (vreplacement.size() > Token.back().size())
            {
                std::cout << "Warning: more replacements than needed in line " << linecount << std::endl;
                for (size_t i = 0; i < vreplacement.size() - Token.back().size(); i++)
                    vreplacement.pop_back();
            }

            if (vreplacement.size() < Token.back().size())
            {
                std::cout << "Warning: less replacements than needed in line " << linecount << std::endl;
                for (size_t i = 0; i < Token.back().size() - vreplacement.size(); i++)
                    vreplacement.emplace_back("");
            }

            TokenReplacement.back().emplace_back(vreplacement);
        }
    }

    infile.close();

    /*
    std::cout << Code << std::endl;

    for (size_t i = 0; i < Token.size(); i++)
    {
        for (size_t j = 0; j < Token[i].size(); j++)
            std::cout << Token[i][j] << " ";
        std::cout << std::endl;
    }

    for (size_t i = 0; i < TokenReplacement.size(); i++)
    {
        for (size_t j = 0; j < TokenReplacement[i].size(); j++)
        {
            for (size_t k = 0; k < TokenReplacement[i][j].size(); k++)
                std::cout << TokenReplacement[i][j][k] << " ";
            std::cout << std::endl;
        }
    }
    */

    std::ofstream outfile;
    outfile.open(argv[2]);
    nestedforloop(0, "", outfile);
    outfile.close();

    return 0;
}