//***************************************************************************************
/// @file FigureTool.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    class FigureSquareRange
    {
    public:
        int LB_X = 0;
        int LB_Y = 0;
        int RT_X = 0;
        int RT_Y = 0;
    };

    class FigureTool;
    class FigureCanvas;
    class FigurePad;
    class FigureHist1D;
    class FigureHist2D;
    class FigureHStack;
    class FigureFunc1D;
    class FigureGraph1D;
    class FigureLatex;

    /// @class NAGASH::FigureStyleHelper FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Class for control the style of the FigureTool, you can define your own FigureStyleHelper by inheriting this class.
    class FigureStyleHelper
    {
    public:
        FigureStyleHelper(std::shared_ptr<MSGTool> MSG);
        virtual void SetPadStyle(FigurePad *pad);
        virtual void SetHist1DStyle(FigureHist1D *hist);
        virtual void SetHist1DGraphStyle(FigureHist1D *hist);
        virtual void SetGraph1DStyle(FigureGraph1D *graph);
        virtual void SetFunc1DStyle(FigureFunc1D *func);
        virtual void SetHist2DStyle(FigureHist2D *hist);
        virtual void SetLatexStyle(FigureLatex *latex);
        virtual ~FigureStyleHelper();
        std::shared_ptr<MSGTool> MSGUser();

        double TITLE_MARGIN_PERCENT = 0.15;       ///< The margin size for the title(left and bottom).
        double COLZ_MARGIN_PERCENT = 0.05;        ///< The margin size for the z axis for 2D histogram COLZ draw option.
        double TOP_BLANK_MARGIN_PERCENT = 0.05;   ///< The margin size on the top.
        double RIGHT_BLANK_MARGIN_PERCENT = 0.05; ///< The margin size on the right.

        double TICK_LENGTH_PERCENT = 0.03;       ///< The length of the tick on the axis.
        double TEXT_SIZE_PERCENT = 0.035;        ///< The size of the text on the axis.
        double LEGEND_TEXT_SIZE_PERCENT = 0.055; ///< The size of the legend text as well as other text in the pad.
        double LEGEND_COLUMN_WIDTH = 0.23;       ///< The size of the each legend column.

        int TEXT_FONT = 4; ///< The font for the text.

        int DIVISION_N1 = 20; ///< The number of primary divisions on the axis.
        int DIVISION_N2 = 5;  ///< The number of secondary divisions on the axis.
        int DIVISION_N3 = 0;  ///< The number of tertiary divisions on the axis.

        int NCOLORDIV;

        double LeftPercentage = 0.07; ///< Control the left space to automatically place the legend.

    private:
        std::shared_ptr<MSGTool> msg;

        int colorstart;
    };

    inline std::shared_ptr<MSGTool> FigureStyleHelper::MSGUser() { return msg; }

    // base class for different figure elements
    class FigureElement
    {
    public:
        FigureElement(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const TString &tyname, FigureElement *mother = 0);
        void Draw();
        virtual void CD();
        virtual void DrawElement();
        virtual void SetStyle();
        // find the element from the root element
        std::shared_ptr<FigureElement> TraceElement(const TString &name);
        // find the element from this node
        std::shared_ptr<FigureElement> FindLinkElement(const TString &name);
        // get the ith element in this link only
        std::shared_ptr<FigureElement> GetLinkElement(int index);
        FigureElement *GetRootElement();
        bool IsType(const TString &tyname);
        TString GetName();
        TString GetTypeName();
        int GetLinkIndex(const TString &name);
        FigureElement *GetMotherElement();
        int GetNLinkElement();

        FigureSquareRange Range_Total;

    protected:
        bool CheckDuplicate(const TString &name);
        std::shared_ptr<MSGTool> MSGUser();
        std::shared_ptr<FigureStyleHelper> StyleUser();

        bool Contain(std::shared_ptr<FigureElement> subelement);
        bool Contain(FigureSquareRange A, FigureSquareRange SubA);

        std::vector<FigureSquareRange> Range_vec;

        std::deque<std::shared_ptr<FigureElement>> LinkedElement;
        std::map<TString, uint64_t> MapLinkedElement;
        FigureElement *MotherElement;

    private:
        std::shared_ptr<MSGTool> msg;
        std::shared_ptr<FigureStyleHelper> stylehelper;
        TString name;
        TString castname = "FigureElement";
    };

    inline std::shared_ptr<MSGTool> FigureElement::MSGUser() { return msg; }
    inline std::shared_ptr<FigureStyleHelper> FigureElement::StyleUser() { return stylehelper; }
    inline TString FigureElement::GetName() { return name; }
    inline TString FigureElement::GetTypeName() { return castname; }
    inline bool FigureElement::IsType(const TString &tyname) { return castname.TString::EqualTo(tyname); }
    inline FigureElement *FigureElement::GetMotherElement() { return MotherElement; }
    inline int FigureElement::GetNLinkElement() { return LinkedElement.size(); }

    /// @class NAGASH::FigureHist1D FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Figure class for TH1D
    class FigureHist1D : public FigureElement
    {
        friend class FigureHStack;
        friend class FigurePad;
        friend class FigureCanvas;
        friend class FigureStyleHelper;

    public:
        virtual ~FigureHist1D();
        void Scale(double); ///< Scale the histogram.
        void Rebin(int);    ///< Rebin the histogram.

    private:
        FigureHist1D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TH1D *hist, const TString &option, FigureElement *mother, std::function<void(TH1D *)> optionfunc = [](TH1D *) {});
        FigureHist1D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const TString &linkinfo, const TString &option, FigureElement *mother, std::function<void(TH1D *)> optionfunc = [](TH1D *) {});
        FigureHist1D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TH1D *hist, const TString &option, FigureElement *mother, std::function<void(TGraph *)> optionfunc);
        FigureHist1D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const TString &linkinfo, const TString &option, FigureElement *mother, std::function<void(TGraph *)> optionfunc);

        std::pair<double, double> GetMinAndMax(double, double);
        TH1D *GetHist();
        void CreateHistFromHistLink();
        virtual void DrawElement() override;
        virtual void SetStyle() override;

        TH1D *myhist = nullptr;
        TGraphErrors *mygraph = nullptr;
        TString DrawOption;
        std::function<void(TH1D *)> OptionFunc;
        std::function<void(TGraph *)> TGraphOptionFunc;
        bool DrawAsTGraph = false;
        bool FromHistLink = false;
        bool ForHStack = false;
        TString LinkInfo;

        int index_in_pad = 0;

        TString GetLinkMode();
        std::vector<TString> GetLinkObject();
    };

    inline TH1D *FigureHist1D::GetHist() { return myhist; }

    /// @class NAGASH::FigureHStack FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Figure class for THStack.
    class FigureHStack : public FigureElement
    {
        friend class FigurePad;
        friend class FigureCanvas;
        friend class FigureStyleHelper;

    public:
        virtual void DrawElement() override;
        virtual void SetStyle() override;
        virtual ~FigureHStack();

        void Scale(double); ///< Scale the histogram.
        void Rebin(int);    ///< Rebin the histogram.
        void DrawTotalHist(
            const TString &option, std::function<void(TH1D *)> optionfunc = [](TH1D *) {});

        TH1D *GetTotalHist();

    private:
        FigureHStack(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, const std::vector<TString> &linkhists, const TString &option, FigureElement *mother);
        THStack *GetHist();
        std::pair<double, double> GetMinAndMax(double, double);
        void CalculateTotalHist();

        THStack *myhstack = nullptr;
        TString DrawOption;
        std::vector<TString> LinkedHists;

        // for adjusting the axis
        TH1D *empty_hist = nullptr;
        TH1D *total_hist = nullptr;
        TGraphErrors *total_hist_graph = nullptr;

        bool draw_total_hist = false;
        TString total_hist_drawoption;
        std::function<void(TH1D *)> total_hist_optionfunc;
    };

    inline THStack *FigureHStack::GetHist() { return myhstack; }

    /// @class NAGASH::FigureGraph1D FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Figure class for TGraph.
    class FigureGraph1D : public FigureElement
    {
        friend class FigurePad;
        friend class FigureCanvas;
        friend class FigureStyleHelper;

    public:
        FigureGraph1D();
        virtual void DrawElement() override;
        virtual void SetStyle() override;
        virtual ~FigureGraph1D();

    private:
        FigureGraph1D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TGraph *graph, const TString &option, FigureElement *mother, std::function<void(TGraph *)> optionfunc = [](TGraph *) {});
        TGraph *GetGraph();
        std::pair<double, double> GetMinAndMax(double, double);

        TGraph *mygraph = nullptr;
        TString DrawOption;
        std::function<void(TGraph *)> OptionFunc;
        int index_in_pad = 0;
    };

    inline TGraph *FigureGraph1D::GetGraph() { return mygraph; }

    /// @class NAGASH::FigureFunc1D FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Figure class for TF1.
    class FigureFunc1D : public FigureElement
    {
        friend class FigurePad;
        friend class FigureCanvas;
        friend class FigureStyleHelper;

    public:
        virtual void DrawElement() override;
        virtual void SetStyle() override;
        virtual ~FigureFunc1D();

    private:
        FigureFunc1D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TF1 *func, const TString &option, FigureElement *mother, std::function<void(TF1 *)> optionfunc = [](TF1 *) {});
        TF1 *GetFunc();
        std::pair<double, double> GetMinAndMax(double, double);

        TF1 *myfunc = nullptr;
        TString DrawOption;
        std::function<void(TF1 *)> OptionFunc;
        int index_in_pad = 0;
    };

    inline TF1 *FigureFunc1D::GetFunc() { return myfunc; }

    /// @class NAGASH::FigureHist2D FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Figure class for TH2D.
    class FigureHist2D : public FigureElement
    {
        friend class FigurePad;
        friend class FigureCanvas;
        friend class FigureStyleHelper;

    public:
        virtual void DrawElement() override;
        virtual void SetStyle() override;
        virtual ~FigureHist2D();

    private:
        FigureHist2D(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, TH2D *hist, const TString &option, FigureElement *mother, std::function<void(TH2D *)> optionfunc = [](TH2D *) {});
        TH2D *GetHist();
        void Rebin(int, int);
        void Scale(double);

        TH2D *myhist;
        TString DrawOption;
        std::function<void(TH2D *)> OptionFunc;
        int index_in_pad = 0;
    };

    inline TH2D *FigureHist2D::GetHist() { return myhist; }

    /// @class NAGASH::FigureLatex FigureTool.h "NAGASH/FigureTool.h"
    /// @brief Figure class for TLatex.
    class FigureLatex : public FigureElement
    {
        friend class FigurePad;
        friend class FigureCanvas;
        friend class FigureStyleHelper;

    public:
        virtual void DrawElement() override;
        virtual void SetStyle() override;
        virtual ~FigureLatex();

    private:
        FigureLatex(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper,
            const TString &content, double posx, double poxy, Color_t color, FigureElement *mother, std::function<void(TLatex *)> optionfunc = [](TLatex *) {});

        TLatex *mylatex;
        std::function<void(TLatex *)> OptionFunc;
    };

    /** @class NAGASH::FigurePad FigureTool.h "NAGASH/FigureTool.h"
        @brief Figure class for TPad.
        This is the class for drawing all the other elements(histograms, graphs, function, etc.) in a figure.
    */
    class FigurePad : public FigureElement
    {
        friend class FigureStyleHelper;
        friend class FigureCanvas;

    public:
        virtual void DrawElement() override;
        virtual void CD() override;
        virtual void SetStyle() override;
        virtual ~FigurePad();

        // set input elements
        std::shared_ptr<FigurePad> BookPad(
            const TString &padname, int lbx, int lby, int rtx, int rty, std::function<void(TPad *)> optionfunc = [](TPad *) {});
        std::shared_ptr<FigureHist1D> SetInputHist1D(
            const TString &elementname, TH1D *hist, const TString &option, std::function<void(TH1D *)> optionfunc = [](TH1D *) {});
        std::shared_ptr<FigureHist1D> SetInputHist1D(
            const TString &elementname, const TString &linkinfo, const TString &option, std::function<void(TH1D *)> optionfunc = [](TH1D *) {});
        std::shared_ptr<FigureHist1D> SetInputHist1DGraph(
            const TString &elementname, TH1D *hist, const TString &option, std::function<void(TGraph *)> optionfunc = [](TGraph *) {});
        std::shared_ptr<FigureHist1D> SetInputHist1DGraph(
            const TString &elementname, const TString &linkinfo, const TString &option, std::function<void(TGraph *)> optionfunc = [](TGraph *) {});
        std::shared_ptr<FigureGraph1D> SetInputGraph1D(
            const TString &elementname, TGraph *graph, const TString &option, std::function<void(TGraph *)> optionfunc = [](TGraph *) {});
        std::shared_ptr<FigureFunc1D> SetInputFunc1D(
            const TString &elementname, TF1 *func, const TString &option, std::function<void(TF1 *)> optionfunc = [](TF1 *) {});
        std::shared_ptr<FigureHist2D> SetInputHist2D(
            const TString &elementname, TH2D *hist, const TString &option, std::function<void(TH2D *)> optionfunc = [](TH2D *) {});
        void SetLatex(
            const TString &content, double posx, double poxy, Color_t color = 1, std::function<void(TLatex *)> optionfunc = [](TLatex *) {});
        void DrawATLASLabel(const TString &content, double posx, double poxy, Color_t color = 1, double space = -1);

        std::shared_ptr<FigureHStack> SetHStack(const TString &elementname, const std::vector<TString> &linkinfo, const TString &option);

        // normalization
        void Norm(const TString &, const TString &, double xmin = 0, double xmax = 0);

        // set legend
        void SetLegend(const TString &header = "", const TString &option = "");
        void SetLegend(const TString &header, const TString &option, double, double, double, double);
        void SetLegendColumn(int);
        void SetLegendEntries(const std::vector<TString> &entries, const std::vector<TString> &options);

        // set pad style
        void SetAxisTitle(const TString &xname = "UNKNOWN", const TString &yname = "UNKNOWN", const TString &zname = "UNKNOWN");
        void SetAxisTitleOffset(double off_x = 1.4, double off_y = 1.4, double off_z = 1);
        void SetAxisStyle(std::function<void(TAxis *, TAxis *, TAxis *)>);
        void SetGridx(bool gridx = true);
        void SetGridy(bool gridy = true);
        void SetLogx(int value = 1);
        void SetLogy(int value = 1);
        void SetLogz(int value = 1);
        void SetXaxisRange(double min, double max);
        void SetYaxisRange(double min, double max);
        void SetZaxisRange(double min, double max);

        bool NeedLeftMargin();
        bool NeedRightMargin();
        bool NeedBottomMargin();
        TString GetXTitle();
        TString GetYTitle();
        TString GetZTitle();
        double GetXTitleOffset();
        double GetYTitleOffset();
        double GetZTitleOffset();
        int GetNColoredObjects();

    private:
        FigurePad(
            std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, int lbx, int lby, int rtx, int rty, FigureElement *mother, std::function<void(TPad *)> optionfunc = [](TPad *) {});
        TPad *mypad = nullptr;
        TPad *GetPad();

        std::function<void(TPad *)> OptionFunc;
        std::function<void(TAxis *, TAxis *, TAxis *)> AxisOptionFunc;

        bool waitforfirstlink = true;
        bool hashstack = false;
        bool user_set_title_offset = false;
        TString xtitle = "UNKNOWN";
        TString ytitle = "UNKNOWN";
        TString ztitle = "UNKNOWN";

        double xoffset = 4.2;
        double yoffset = 4.5;
        double zoffset = 1;

        int n_colored_objects = 0;

        bool user_set_rangex = false;
        bool user_set_rangey = false;
        bool user_set_rangez = false;

        double max_xaxis = 0;
        double min_xaxis = 0;
        double max_yaxis = 0;
        double min_yaxis = 0;
        double max_zaxis = 0;
        double min_zaxis = 0;

        // legend parameters
        TLegend *legend = nullptr;
        bool plot_legend = false;
        std::vector<TString> legend_entries;
        std::vector<TString> legend_options;
        TString legend_header;
        TString legend_header_option;
        bool user_set_legend_position = false;
        double legend_xmin = 0;
        double legend_ymin = 0;
        double legend_xmax = 0;
        double legend_ymax = 0;
        int legend_column = 1;
    };

    inline bool FigurePad::NeedLeftMargin() { return !(ytitle.EqualTo("UNKNOWN")); }
    inline bool FigurePad::NeedRightMargin() { return !(ztitle.EqualTo("UNKNOWN")); }
    inline bool FigurePad::NeedBottomMargin() { return !(xtitle.EqualTo("UNKNOWN")); }
    inline TString FigurePad::GetXTitle() { return xtitle; }
    inline TString FigurePad::GetYTitle() { return ytitle; }
    inline TString FigurePad::GetZTitle() { return ztitle; }
    inline double FigurePad::GetXTitleOffset() { return xoffset; }
    inline double FigurePad::GetYTitleOffset() { return yoffset; }
    inline double FigurePad::GetZTitleOffset() { return zoffset; }
    inline TPad *FigurePad::GetPad() { return mypad; }
    inline int FigurePad::GetNColoredObjects() { return n_colored_objects; }

    /// @brief Set the offset of the axis title manually.
    /// @param off_x offset of the x-axis title.
    /// @param off_y offset of the y-axis title.
    /// @param off_z offset of the z-axis title.
    inline void FigurePad::SetAxisTitleOffset(double off_x, double off_y, double off_z)
    {
        user_set_title_offset = true;
        xoffset = off_x;
        yoffset = off_y;
        zoffset = off_z;
    }

    /// @brief Set the axis title.
    /// @param xname axis title for the x-axis.
    /// @param yname axis title for the y-axis.
    /// @param zname axis title for the z-axis.
    inline void FigurePad::SetAxisTitle(const TString &xname, const TString &yname, const TString &zname)
    {
        xtitle = xname;
        ytitle = yname;
        ztitle = zname;
    }

    /// @brief Set the style of the axis manually.
    /// @param func function to set the style of the axis, which takes the pointer of the x,y,z TAxis as input.
    inline void FigurePad::SetAxisStyle(std::function<void(TAxis *, TAxis *, TAxis *)> func)
    {
        AxisOptionFunc = func;
    }

    /** @class NAGASH::FigureCanvas FigureTool.h "NAGASH/FigureTool.h"
        @brief Figure class for TCanvas.
        A canvas is the base of your figure where you can define multiple NAGASH::FigurePad objects for drawing all the ingredients.
    */
    class FigureCanvas : public FigureElement
    {
        friend class FigureTool;

    public:
        virtual void DrawElement() override;
        virtual void CD() override;
        void SaveAs(const TString &filename);
        virtual ~FigureCanvas();

        std::shared_ptr<FigurePad> BookPad(
            const TString &padname, int lbx, int lby, int rtx, int rty, std::function<void(TPad *)> optionfunc = [](TPad *) {});
        std::shared_ptr<FigurePad> BookPadNDC(
            const TString &padname, double lbx, double lby, double rtx, double rty, std::function<void(TPad *)> optionfunc = [](TPad *) {});

    private:
        FigureCanvas(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper, const TString &elementname, int npx, int npy);
        TCanvas *mycanvas = nullptr;
    };

    inline void FigureCanvas::SaveAs(const TString &filename) { mycanvas->SaveAs(filename.TString::Data()); }

    class FigureTool : public Tool
    {
    public:
        FigureTool(std::shared_ptr<MSGTool> MSG);
        FigureTool(std::shared_ptr<MSGTool> MSG, std::shared_ptr<FigureStyleHelper> shelper);
        std::shared_ptr<FigureCanvas> BookFigure(const TString &figurename, int size_x, int size_y);
        std::shared_ptr<FigureCanvas> BookATLASSquare(const TString &figurename);
        std::shared_ptr<FigureCanvas> BookATLASRectangular(const TString &figurename);
        void DrawFigures();
        void SetFigureDirectory(const TString &dirname);
        std::shared_ptr<FigureStyleHelper> GetFigureStyleHelper();

    private:
        // All Figures
        std::shared_ptr<FigureStyleHelper> stylehelper;
        std::map<TString, std::shared_ptr<FigureCanvas>> BookedFigure;
        TString figuredir = ".";
    };

    inline void FigureTool::SetFigureDirectory(const TString &dirname) { figuredir = dirname; }          ///< Set the output directory of the figures produced by this tool.
    inline std::shared_ptr<FigureStyleHelper> FigureTool::GetFigureStyleHelper() { return stylehelper; } ///< Return the style helper of this tool.
} // namespace NAGASH
