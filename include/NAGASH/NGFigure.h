///***************************************************************************************
/// @file NGFigure.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/HistTool.h"
#include "NAGASH/Tool.h"
#include "NAGASH/Uncertainty.h"

namespace NAGASH
{
    class NGFigure : public Tool
    {
    private:
        TString SaveFileName;

        bool isFor_Hist2D = false;
        TH2D *Hist2D = nullptr;
        TH2D *Matrix_Save = nullptr;
        TH2D *Text2D = nullptr;
        int asmb = 3;
        bool DoMatrix = false;
        bool DoTextOnly = false;
        bool DoColorOnly = false;
        int RebinNumX;
        int RebinNumY;
        int binnumx;
        int binnumy;
        bool isNeed_SetBinNameX = false;
        bool isNeed_SetBinNameY = false;
        std::vector<TString> Name_X_Bin;
        std::vector<TString> Name_Y_Bin;

        TCanvas *figure = nullptr;
        bool Main_Pad_isNeeded;
        bool Sub_Pad_isNeeded;
        TPad *Main_Pad = nullptr;
        TPad *Sub_Pad = nullptr;
        TLegend *Main_Legend = nullptr;
        TString HeaderName = "ATLAS Internal";
        bool ShowChi2 = false;

        TAxis *Main_Pad_Z_Axis = nullptr;
        TAxis *Main_Pad_Y_Axis = nullptr;
        TAxis *Main_Pad_X_Axis = nullptr;
        TAxis *Sub_Pad_Y_Axis = nullptr;
        TAxis *Sub_Pad_X_Axis = nullptr;

        bool isSet_X_Range = false;
        double Max_X_Range;
        double Min_X_Range;

        bool isSet_Norm_Range = false;
        bool isNeed_Norm = true;
        bool isFixed_Norm = false;
        bool isExtern_Norm = false;
        bool isUnity_Norm = false;
        double Max_Norm_Range;
        double Min_Norm_Range;
        double Norm_Factor;

        bool isSet_Mode = false;
        int Style_Index = 0;

        double LeftPercentage = 0.07;

        // For Main Pad
        TString Mode_Tag_Main;
        std::vector<TH1D *> Main_Hist;
        std::vector<double> ExternNorm_Factor;
        std::vector<bool> isNeed_ExternNorm;
        std::vector<TString> LegendName;
        std::vector<int> RebinNum;

        std::vector<TGraph *> Main_Graph;
        std::vector<TString> LegendName_Graph;
        std::vector<TString> DrawOption_Graph;
        std::vector<int> Graph_Marker_Style;
        std::vector<int> Graph_Marker_Size;
        std::vector<int> Graph_Marker_Color;
        std::vector<int> Graph_Line_Style;
        std::vector<int> Graph_Line_Width;
        std::vector<int> Graph_Line_Color;

        TH1D *Total_Hist_With_Error = nullptr;
        TH1D *Total_Hist = nullptr;
        TH1D *Empty_Hist = nullptr;
        TH1D *Empty_Hist_Sub = nullptr;

        THStack *Main_Stack = nullptr;
        THStack *Sub_Stack = nullptr;

        int Hist_Line_Width;
        int Hist_Marker_Size;
        int Sub_Fill_Style;
        int Hist_Fill_Style;
        int Sub_Fill_Style_First;
        int Hist_Fill_Style_First;

        double *X_Binning = nullptr;
        int Num_X_Bin;

        double RightMargin_Main;
        double LeftMargin_Main;
        double TopMargin_Main;
        double BottomMargin_Main;

        double X_Label_Size_Main;
        double X_Label_Font_Main;
        double X_Title_Size_Main;
        double X_Title_Font_Main;
        double X_Title_Offset_Main;

        double Y_Label_Size_Main;
        double Y_Label_Font_Main;
        double Y_Title_Size_Main;
        double Y_Title_Font_Main;
        double Y_Title_Offset_Main;

        double Z_Label_Size_Main;
        double Z_Label_Font_Main;
        double Z_Label_Offset_Main;

        TString Y_Title_Main_Pad;
        TString X_Title_Main_Pad;
        bool isNeed_X_Title_Main_Pad;

        bool isSet_Y_Range_Main = false;
        double Max_Y_Range_Main;
        double Min_Y_Range_Main;

        bool isSet_Z_Range_Main = false;
        double Max_Z_Range_Main;
        double Min_Z_Range_Main;

        bool NeedGridX_Main = true;
        bool NeedGridY_Main = true;

        double Legend_Text_Size;
        int Legend_Text_Font;
        int Legend_Column = 1;

        size_t Num_Main_Hist;

        // For Sub Pad
        TString Mode_Tag_Sub;
        std::vector<TH1D *> Sub_Hist;
        std::vector<bool> isExtern_Sub_Hist;

        TString Y_Title_Sub_Pad;
        TString X_Title_Sub_Pad;
        bool isNeed_X_Title_Sub_Pad;

        bool isSet_Y_Range_Sub = false;
        double Max_Y_Range_Sub;
        double Min_Y_Range_Sub;

        double RightMargin_Sub;
        double LeftMargin_Sub;
        double TopMargin_Sub;
        double BottomMargin_Sub;

        double X_Label_Size_Sub;
        double X_Label_Font_Sub;
        double X_Title_Size_Sub;
        double X_Title_Font_Sub;
        double X_Title_Offset_Sub;

        double Y_Label_Size_Sub;
        double Y_Label_Font_Sub;
        double Y_Title_Size_Sub;
        double Y_Title_Font_Sub;
        double Y_Title_Offset_Sub;

        bool NeedGridX_Sub = true;
        bool NeedGridY_Sub = true;

        bool DoStagger = false;
        double *Stagger_X_Binning = nullptr;
        int Stagger_Num_X_Bin;
        std::vector<TH1D *> Stagger_Save_Main_Hist;
        std::vector<TH1D *> Stagger_Save_Sub_Hist;
        bool DoSeparate = false;

        bool DoMainLogY = false;

        bool DoLegend = true;

        char name[200];

        // auxiliary tools
        Uncertainty unc;
        HistTool histtool;

    public:
        NGFigure(std::shared_ptr<MSGTool> msg, const char *FigureName, const char *TitleX, const char *TitleY);
        void SetMode(const char *main_mode_tag, const char *sub_mode_tag = "UNKNOWN", const char *SubTitleY = "UNKNOWN");
        void SetInputHist(const char *filename, const char *histname, const char *legendname, int rebinnum = -1, double scale = 1);
        void SetInputHist(TFile *infile, const char *histname, const char *legendname, int rebinnum = -1, double scale = 1);
        void SetInputHist(TH1D *hist, const char *legendnamem, int rebinnum = -1, double scale = 1);
        void SetInputHist2D(const char *filename, const char *histname, const char *legendname, int rebinnumx = -1, int rebinnumy = -1);
        void SetInputHist2D(TH2D *hist, const char *legendnamem, int rebinnumx = -1, int rebinnumy = -1);
        void SetInputHist2D(TFile *infile, const char *histname, const char *legendname, int rebinnumx = -1, int rebinnumy = -1);
        void SetInputGraph(TGraph *graph, const char *legendname, const char *drawoption);
        void SetGraphMarker(int marker_style, int marker_size, int marker_color);
        void SetGraphLine(int line_style, int line_width, int line_color);
        void SetXRange(double min, double max);
        void SetMainYRange(double min, double max);
        void SetMainZRange(double min, double max);
        void SetSubYRange(double min, double max);
        void SetNormRange(double min, double max);
        void SetGridX(bool grid = true);
        void SetGridY(bool grid = true);
        void SetSubGridX(bool grid = true);
        void SetSubGridY(bool grid = true);
        void SetMainGridX(bool grid = true);
        void SetMainGridY(bool grid = true);
        void SetGrid(bool grid = true);
        void SetMatrixStyle(bool domatrix = true);
        void Set2DTextOnly(bool dotextonly = true);
        void Set2DColorOnly(bool docoloronly = true);
        void SetAsmble(int index);
        void SetStyleIndex(int index);
        void SetLeftPercentage(double percent);
        void SetStagger(bool dostagger = true);
        void SetFixedNormFactor(bool fix = true, double scale = -1);
        void SetHeaderName(const char *header);
        void SetShowChi2(bool show = true);
        void SetXBinName(const char *binname);
        void SetYBinName(const char *binname);
        void SeparateUncertainty(bool doseparate = true);
        void SetNotNorm();
        void SetMainLogY(bool dosetlogy = true);
        void SetLegendColumn(int num);
        void SetLegend(bool);

        void DrawFigure(); // Main Code
        bool Check();
        bool RebinHist();
        bool Normalize();
        bool ConfigParameters();
        bool DefinePad();
        bool DrawHist();
        bool DrawHStack();
        bool DrawHist2D();
        bool HackStagger();

        void SetMargin();
        void SetAxis();
        void SetHist();
        bool SaveHist();

        ~NGFigure();
    };
}
