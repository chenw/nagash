#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"
#include "NAGASH/ConfigTool.h"
#include "NAGASH/MSGTool.h"

#include "Minuit2/FCNBase.h"

namespace NAGASH
{
    namespace DevelFeature
    {
        class FigureX;
        class FigureElement;

        enum class FigureParameterType
        {
            FLOAT,
            FIXED,
            LINKED
        };

        class FigureParameter
        {
        public:
            double Val;
            double Init;
            double Min;
            double Max;
            double Step;

            TString Name;
            FigureParameterType Type;
            FigureParameter(const TString &name, FigureParameterType type);
        };

        inline FigureParameter::FigureParameter(const TString &name, FigureParameterType type)
        {
            Name = name;
            Type = type;
        }

        class FigureProfile;
        class FigureCanvas;

        // FigureX will book all the FigureProfile you want just like ResultGroup will book all the result you need
        // FigureProfile containes everything needed for this figure, it will build the tree structure of FigureElement
        // Starting from <FigureCanvas> Canvas, It will define the FigureParameters and Fit them before drawing the figure

        class FigureX : public Tool
        {
        public:
            FigureX(std::shared_ptr<MSGTool> MSG,
                    const TString &style_config);

            template <typename profiletype>
            std::shared_ptr<profiletype> BookFigure(const TString &figurename, const TString &modefile);
            void ConfigFigures();
            void DrawFigures();

        private:
            std::shared_ptr<ConfigTool> Style;
            std::map<TString, std::shared_ptr<FigureProfile>> Figure_Map;
        };

        template <typename profiletype>
        std::shared_ptr<profiletype> FigureX::BookFigure(const TString &figurename, const TString &modefile)
        {
            if (Figure_Map.find(figurename) == Figure_Map.end())
            {
                auto figure = std::make_shared<profiletype>(MSGUser(), figurename, modefile);
                Figure_Map.emplace(std::pair<TString, std::shared_ptr<profiletype>>(figurename, figure));
                return figure;
            }

            return nullptr;
        }

        class FigureProfile
        {
        public:
            TString Name;

            std::shared_ptr<ConfigTool> Mode = nullptr;
            std::shared_ptr<FigureCanvas> Canvas;
            std::map<TString, std::shared_ptr<FigureParameter>> Par_Map;

            FigureProfile(std::shared_ptr<MSGTool> msg, const TString &name, const TString &modefile = "");

            virtual void ConfigFigure(){}; // This is used to build the tree structure
            void DrawFigure();             // This is used to tune parameters and save figures
            void TuneFreeParameters();
            bool InitParameter(std::shared_ptr<FigureParameter> par);
            std::shared_ptr<FigureParameter> BookParameter(const TString &name);
            void SetParameter(const TString &name, double val, FigureParameterType par_type = FigureParameterType::FIXED);

            std::shared_ptr<MSGTool> MSGUser();

            class FCN : public ROOT::Minuit2::FCNBase
            {
            public:
                FCN(std::shared_ptr<FigureCanvas> m_canvas, std::vector<std::shared_ptr<FigureParameter>> par_vec);
                double operator()(const std::vector<double> &) const override;
                double Up() const { return 1.0; }

                mutable std::shared_ptr<FigureCanvas> Canvas;
                mutable std::vector<std::shared_ptr<FigureParameter>> Par_Vec;

                mutable int NCalled;
                mutable double Min_Chi2;
            };

        private:
            std::shared_ptr<MSGTool> m_msg;
        };

        inline FigureProfile::FCN::FCN(std::shared_ptr<FigureCanvas> m_canvas, std::vector<std::shared_ptr<FigureParameter>> par_vec)
        {
            Canvas = m_canvas;
            Par_Vec = par_vec;
        }
        inline std::shared_ptr<MSGTool> FigureProfile::MSGUser() { return m_msg; }

        class Figure_MultiCompare : public FigureProfile
        {
        public:
            std::map<TString, TH1D *> Hist_Map;

            TString X_Axis_Title;
            TString Y_Axis_Title;

            TString Main_Legend_Header = "";

            Figure_MultiCompare(std::shared_ptr<MSGTool> msg, const TString &name, const TString &modefile = "") : FigureProfile(msg, name, modefile){};

            void BookHist(const TString &name, TH1D *hist);
            void SetHeader(const TString &header) { Main_Legend_Header = header; };
            void SetTitle(const TString &x_title, const TString &y_title)
            {
                X_Axis_Title = x_title;
                Y_Axis_Title = y_title;
            };
            void ConfigFigure();
        };

        class FigureElement
        {
        public:
            TString Name;
            TString TypeName;

            std::vector<int> Left_Down_X;
            std::vector<int> Left_Down_Y;
            std::vector<int> Right_Up_X;
            std::vector<int> Right_Up_Y;

            bool NeedAxis = false;

            FigureElement *MotherElement = nullptr;
            std::vector<std::shared_ptr<FigureElement>> LinkedElement;

            FigureElement(const TString &name, FigureElement *mother = nullptr);
            virtual void UpdateOwnPosition(){};
            void UpdatePosition();
            void ClearPosition();
            virtual void CD(){};
            virtual TObject *GetObject() { return nullptr; };

            void Draw();
            virtual void DrawElement(){};
            virtual double OwnScore();
            double Score();

            bool Check();
            virtual bool OwnCheck() { return true; };
            bool CheckCollision();

            template <typename R, typename... Args>
            std::shared_ptr<R> Book(const TString &name, Args &&...args);
        };

        inline FigureElement::FigureElement(const TString &name, FigureElement *mother)
        {
            Name = name;
            MotherElement = mother;
        }

        template <typename R, typename... Args>
        std::shared_ptr<R> FigureElement::Book(const TString &name, Args &&...args)
        {
            std::shared_ptr<R> newelement = std::make_shared<R>(name, this, std::forward<Args>(args)...);

            LinkedElement.emplace_back(std::shared_ptr<FigureElement>(std::dynamic_pointer_cast<R>(newelement)));

            return newelement;
        }

        inline void FigureElement::ClearPosition()
        {
            Left_Down_X.clear();
            Left_Down_Y.clear();
            Right_Up_X.clear();
            Right_Up_Y.clear();
        }

        class FigureCanvas : public FigureElement
        {
        public:
            FigureCanvas(const TString &name, FigureElement *mother, std::shared_ptr<FigureParameter> x, std::shared_ptr<FigureParameter> y);

            std::shared_ptr<FigureParameter> Size_X;
            std::shared_ptr<FigureParameter> Size_Y;

            TCanvas *m_canvas = nullptr;

            void DrawElement();
            void CD()
            {
                if (m_canvas != nullptr)
                    m_canvas->cd();
            }

            void UpdateOwnPosition();
            double OwnScore();
            TObject *GetObject() { return m_canvas; };
        };

        inline FigureCanvas::FigureCanvas(const TString &name, FigureElement *mother,
                                          std::shared_ptr<FigureParameter> x, std::shared_ptr<FigureParameter> y) : FigureElement(name, mother)
        {
            Size_X = x;
            Size_Y = y;
            TypeName = "Canvas";
        }

        class FigurePad : public FigureElement
        {
        public:
            FigurePad(const TString &name, FigureElement *mother,
                      std::shared_ptr<FigureParameter> ld_x, std::shared_ptr<FigureParameter> ld_y,
                      std::shared_ptr<FigureParameter> ru_x, std::shared_ptr<FigureParameter> ru_y);

            std::shared_ptr<FigureParameter> X_Axis_Min;
            std::shared_ptr<FigureParameter> X_Axis_Max;
            std::shared_ptr<FigureParameter> Y_Axis_Min;
            std::shared_ptr<FigureParameter> Y_Axis_Max;

            std::shared_ptr<FigureParameter> LD_X;
            std::shared_ptr<FigureParameter> LD_Y;
            std::shared_ptr<FigureParameter> RU_X;
            std::shared_ptr<FigureParameter> RU_Y;

            std::shared_ptr<FigureParameter> Top_Margin_Rate;
            std::shared_ptr<FigureParameter> Left_Margin_Rate;
            std::shared_ptr<FigureParameter> Right_Margin_Rate;
            std::shared_ptr<FigureParameter> Bottom_Margin_Rate;

            bool HaveAxis = false;
            TString X_Axis_Title = "";
            TString Y_Axis_Title = "";

            bool NeedCollideCheck(const TString &type_i, const TString &type_j);

            void UpdateOwnPosition();
            bool OwnCheck();
            double OwnScore();
            void SetAxis(std::shared_ptr<FigureParameter> min_x, std::shared_ptr<FigureParameter> min_y,
                         std::shared_ptr<FigureParameter> max_x, std::shared_ptr<FigureParameter> max_y);
            void SetMargin(std::shared_ptr<FigureParameter> top, std::shared_ptr<FigureParameter> bottom,
                           std::shared_ptr<FigureParameter> left, std::shared_ptr<FigureParameter> right);
            void GetAxisRange(double &draw_ld_x, double &draw_ld_y, double &draw_ru_x, double &draw_ru_y);
            void SetTitle(const TString &x_title, const TString &y_title)
            {
                X_Axis_Title = x_title;
                Y_Axis_Title = y_title;
            };

            TPad *m_pad = nullptr;
            void CD()
            {
                if (m_pad != nullptr)
                    m_pad->cd();
            }
            void DrawElement();
            TObject *GetObject() { return m_pad; };
        };

        inline void FigurePad::SetAxis(std::shared_ptr<FigureParameter> min_x, std::shared_ptr<FigureParameter> min_y,
                                       std::shared_ptr<FigureParameter> max_x, std::shared_ptr<FigureParameter> max_y)
        {
            X_Axis_Min = min_x;
            X_Axis_Max = max_x;
            Y_Axis_Min = min_y;
            Y_Axis_Max = max_y;

            HaveAxis = true;
        }

        inline void FigurePad::SetMargin(std::shared_ptr<FigureParameter> top, std::shared_ptr<FigureParameter> bottom,
                                         std::shared_ptr<FigureParameter> left, std::shared_ptr<FigureParameter> right)
        {
            Top_Margin_Rate = top;
            Bottom_Margin_Rate = bottom;
            Left_Margin_Rate = left;
            Right_Margin_Rate = right;

            HaveAxis = true;
        }

        inline FigurePad::FigurePad(const TString &name, FigureElement *mother,
                                    std::shared_ptr<FigureParameter> ld_x, std::shared_ptr<FigureParameter> ld_y,
                                    std::shared_ptr<FigureParameter> ru_x, std::shared_ptr<FigureParameter> ru_y) : FigureElement(name, mother)
        {
            TypeName = "Pad";
            LD_X = ld_x;
            LD_Y = ld_y;
            RU_X = ru_x;
            RU_Y = ru_y;
        }

        class FigureHist1D : public FigureElement
        {
        public:
            FigureHist1D(const TString &name, FigureElement *mother, TH1D *hist);
            TH1D *Hist;
            void DrawElement();
            void UpdateOwnPosition();
            double OwnScore();
            TObject *GetObject() { return Hist; };
        };

        inline FigureHist1D::FigureHist1D(const TString &name, FigureElement *mother, TH1D *hist) : FigureElement(name, mother)
        {
            Hist = hist;
            TypeName = "Hist1D";
            NeedAxis = true;
        }

        class FigureLegend : public FigureElement
        {
        public:
            FigureLegend(const TString &name, FigureElement *mother, const TString &header,
                         std::shared_ptr<FigureParameter> pos_x, std::shared_ptr<FigureParameter> pos_y);
            void SetEntry(std::shared_ptr<FigureElement> entry) { Entry_Vec.push_back(entry); };
            TLegend *Legend;
            TString Header;
            std::vector<std::shared_ptr<FigureElement>> Entry_Vec;

            std::shared_ptr<FigureParameter> Pos_X;
            std::shared_ptr<FigureParameter> Pos_Y;

            double GetWidth();
            double GetHeight();

            void DrawElement();
            void UpdateOwnPosition();
            double OwnScore();
            TObject *GetObject() { return Legend; };
        };

        inline FigureLegend::FigureLegend(const TString &name, FigureElement *mother, const TString &header,
                                          std::shared_ptr<FigureParameter> pos_x, std::shared_ptr<FigureParameter> pos_y) : FigureElement(name, mother)
        {
            TypeName = "Legend";
            Header = header;
            Pos_X = pos_x;
            Pos_Y = pos_y;
        }
    } // namespace DevelFeature
} // namespace NAGASH
