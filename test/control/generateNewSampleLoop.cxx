// Generate LoopEvent Class under NAGASH Framework
// Similar to TTree::MakeClass
// Modified by : Wenhao Ma
#include "NAGASH/Global.h"
#include "TLeaf.h"
#include "TLeafObject.h"
#include "TObjString.h"
#include "TBranch.h"
#include "TBranchElement.h"
#include "TBranchObject.h"
#include "TChainElement.h"
#include "TStreamerElement.h"

using namespace std;

// Return the name of the branch pointer needed by MakeClass
static TString R__GetBranchPointerName(TLeaf *leaf, Bool_t replace = kTRUE)
{
    TLeaf *leafcount = leaf->GetLeafCount();
    TBranch *branch = leaf->GetBranch();

    TString branchname(branch->GetName());

    if (branch->GetNleaves() <= 1)
    {
        if (branch->IsA() != TBranchObject::Class())
        {
            if (!leafcount)
            {
                TBranch *mother = branch->GetMother();
                const char *ltitle = leaf->GetTitle();
                if (mother && mother != branch)
                {
                    branchname = mother->GetName();
                    if (branchname[branchname.Length() - 1] != '.')
                    {
                        branchname += ".";
                    }
                    if (strncmp(branchname.Data(), ltitle, branchname.Length()) == 0)
                    {
                        branchname = "";
                    }
                }
                else
                {
                    branchname = "";
                }
                branchname += ltitle;
            }
        }
    }
    if (replace)
    {
        char *bname = (char *)branchname.Data();
        char *twodim = (char *)strstr(bname, "[");
        if (twodim)
            *twodim = 0;
        while (*bname)
        {
            if (*bname == '.')
                *bname = '_';
            if (*bname == ',')
                *bname = '_';
            if (*bname == ':')
                *bname = '_';
            if (*bname == '<')
                *bname = '_';
            if (*bname == '>')
                *bname = '_';
            bname++;
        }
    }
    return branchname;
}

// add std:: to STL container type
// It's weird that ROOT did nothing on this
TString AddStdPrefix(const char *TypeName)
{
    TString type(TypeName);
    type.ReplaceAll("string", "std::string");
    type.ReplaceAll("vector", "std::vector");
    type.ReplaceAll("list", "std::list");
    type.ReplaceAll("deque", "std::deque");
    type.ReplaceAll("set", "std::set");
    type.ReplaceAll("multiset", "std::multiset");

    return type;
}

int main(int argc, char **argv)
{
    // initial tree
    if (argc != 4)
    {
        cout << "Usage : " << argv[0] << " (file name) (tree name) (class name)" << endl;
        return -1;
    }

    TFile *infile = new TFile(argv[1]);
    if (!infile)
    {
        Error("MakeClass", "Input file does not exist");
        return -1;
    }

    TTree *tree = (TTree *)infile->Get(argv[2]);
    if (!tree)
    {
        Error("MakeClass", "Input tree does not exist");
        return -1;
    }

    const char *classname = argv[3];

    // the following code are modified from TTreePlayer::MakeClass
    TString opt = "";
    opt.ToLower();

    // Connect output files
    if (!classname)
        classname = tree->GetName();

    TString thead;
    thead.Form("%s.h", classname);
    FILE *fp = fopen(thead, "w");
    if (!fp)
    {
        Error("MakeClass", "cannot open output file %s", thead.Data());
        return 3;
    }
    TString tcimp;
    tcimp.Form("%s.cxx", classname);
    FILE *fpc = fopen(tcimp, "w");
    if (!fpc)
    {
        Error("MakeClass", "cannot open output file %s", tcimp.Data());
        fclose(fp);
        return 3;
    }
    TString treefile;
    if (tree->GetDirectory() && tree->GetDirectory()->GetFile())
    {
        treefile = tree->GetDirectory()->GetFile()->GetName();
    }
    else
    {
        treefile = "Memory Directory";
    }
    // In the case of a chain, the GetDirectory information usually does
    // pertain to the Chain itself but to the currently loaded tree.
    // So we can not rely on it.
    Bool_t ischain = tree->InheritsFrom(TChain::Class());
    Bool_t isHbook = tree->InheritsFrom("THbookTree");
    if (isHbook)
        treefile = tree->GetTitle();

    //======================Generate classname.h=====================
    // Print header
    TObjArray *leaves = tree->GetListOfLeaves();
    Int_t nleaves = leaves ? leaves->GetEntriesFast() : 0;
    TDatime td;
    /*
    fprintf(fp, "//////////////////////////////////////////////////////////\n");
    fprintf(fp, "// This class has been automatically generated on\n");
    fprintf(fp, "// %s by ROOT version %s\n", td.AsString(), gROOT->GetVersion());
    if (!ischain)
    {
        fprintf(fp, "// from TTree %s/%s\n", tree->GetName(), tree->GetTitle());
        fprintf(fp, "// found on file: %s\n", treefile.Data());
    }
    else
    {
        fprintf(fp, "// from TChain %s/%s\n", tree->GetName(), tree->GetTitle());
    }
    fprintf(fp, "//////////////////////////////////////////////////////////\n");
    fprintf(fp, "\n");
    fprintf(fp, "#ifndef %s_HEADER\n", classname);
    fprintf(fp, "#define %s_HEADER\n", classname);
    fprintf(fp, "\n");
    fprintf(fp, "#include \"NAGASH.h\"\n");
    */
    // fprintf(fp, "using namespace std; // I don't know why ROOT generated this file without std:: prefix on STL objects");

    /*
    if (isHbook)
        fprintf(fp, "#include <THbookFile.h>\n");
    if (opt.Contains("selector"))
        fprintf(fp, "#include <TSelector.h>\n");
        */

    // See if we can add any #include about the user data.
    Int_t l;
    /*
    fprintf(fp, "\n// Header file for the classes stored in the TTree if any.\n");
    TList listOfHeaders;
    listOfHeaders.SetOwner();
    for (l = 0; l < nleaves; l++)
    {
        TLeaf *leaf = (TLeaf *)leaves->UncheckedAt(l);
        TBranch *branch = leaf->GetBranch();
        TClass *cl = TClass::GetClass(branch->GetClassName());
        if (cl && cl->IsLoaded() && !listOfHeaders.FindObject(cl->GetName()))
        {
            const char *declfile = cl->GetDeclFileName();
            if (declfile && declfile[0])
            {
                static const char *precstl = "prec_stl/";
                static const unsigned int precstl_len = strlen(precstl);
                static const char *rootinclude = "include/";
                static const unsigned int rootinclude_len = strlen(rootinclude);
                if (strncmp(declfile, precstl, precstl_len) == 0)
                {
                    fprintf(fp, "#include <%s>\n", declfile + precstl_len);
                    listOfHeaders.Add(new TNamed(cl->GetName(), declfile + precstl_len));
                }
                else if (strncmp(declfile, "/usr/include/", 13) == 0)
                {
                    fprintf(fp, "#include <%s>\n", declfile + strlen("/include/c++/"));
                    listOfHeaders.Add(new TNamed(cl->GetName(), declfile + strlen("/include/c++/")));
                }
                else if (strstr(declfile, "/include/c++/") != 0)
                {
                    fprintf(fp, "#include <%s>\n", declfile + strlen("/include/c++/"));
                    listOfHeaders.Add(new TNamed(cl->GetName(), declfile + strlen("/include/c++/")));
                }
                else if (strncmp(declfile, rootinclude, rootinclude_len) == 0)
                {
                    fprintf(fp, "#include <%s>\n", declfile + rootinclude_len);
                    listOfHeaders.Add(new TNamed(cl->GetName(), declfile + rootinclude_len));
                }
                else
                {
                    fprintf(fp, "#include \"%s\"\n", declfile);
                    listOfHeaders.Add(new TNamed(cl->GetName(), declfile));
                }
            }
        }
    }
    */

    // First loop on all leaves to generate dimension declarations
    Int_t len, lenb;
    char blen[1024];
    char *bname;
    Int_t *leaflen = new Int_t[nleaves];
    TObjArray *leafs = new TObjArray(nleaves);
    for (l = 0; l < nleaves; l++)
    {
        TLeaf *leaf = (TLeaf *)leaves->UncheckedAt(l);
        leafs->AddAt(new TObjString(leaf->GetName()), l);
        leaflen[l] = leaf->GetMaximum();
    }
    if (ischain)
    {
        // In case of a chain, one must find the maximum dimension of each leaf
        // One must be careful and not assume that all Trees in the chain
        // have the same leaves and in the same order!
        TChain *chain = (TChain *)tree;
        Int_t ntrees = chain->GetNtrees();
        for (Int_t file = 0; file < ntrees; file++)
        {
            Long64_t first = chain->GetTreeOffset()[file];
            chain->LoadTree(first);
            for (l = 0; l < nleaves; l++)
            {
                TObjString *obj = (TObjString *)leafs->At(l);
                TLeaf *leaf = chain->GetLeaf(obj->GetName());
                if (leaf)
                {
                    leaflen[l] = TMath::Max(leaflen[l], leaf->GetMaximum());
                }
            }
        }
        chain->LoadTree(0);
    }

    /*
    fprintf(fp, "\n");
    if (opt.Contains("selector"))
    {
        fprintf(fp, "class %s : public TSelector {\n", classname);
        fprintf(fp, "public :\n");
        fprintf(fp, "   TTree          *fChain;   //!pointer to the analyzed TTree or TChain\n");
    }
    else
    {
        fprintf(fp, "class %s : public NAGASH::LoopEvent{\n", classname);
        fprintf(fp, "public :\n");
        fprintf(fp, "   %s(const NAGASH::ConfigTool &c) : NAGASH::LoopEvent(c) {}  // Constructor\n", classname);
        fprintf(fp, "   virtual NAGASH::StatusCode InitializeData() override;\n");
        fprintf(fp, "   virtual NAGASH::StatusCode InitializeUser() override;\n");
        fprintf(fp, "   virtual NAGASH::StatusCode Execute() override;\n");
        fprintf(fp, "   virtual NAGASH::StatusCode Finalize() override;\n");
    }

    fprintf(fp, "\n// Fixed size dimensions of array or collections stored in the TTree if any.\n");
    */
    fprintf(fp, "    // %s\n", tree->GetName());
    fprintf(fp, "   TTree *%s = nullptr;\n", tree->GetName());
    leaves = tree->GetListOfLeaves();
    for (l = 0; l < nleaves; l++)
    {
        TLeaf *leaf = (TLeaf *)leaves->UncheckedAt(l);
        strlcpy(blen, leaf->GetName(), sizeof(blen));
        bname = &blen[0];
        while (*bname)
        {
            if (*bname == '.')
                *bname = '_';
            if (*bname == ',')
                *bname = '_';
            if (*bname == ':')
                *bname = '_';
            if (*bname == '<')
                *bname = '_';
            if (*bname == '>')
                *bname = '_';
            bname++;
        }
        lenb = strlen(blen);
        if (blen[lenb - 1] == '_')
        {
            blen[lenb - 1] = 0;
            len = leaflen[l];
            if (len <= 0)
                len = 1;
            fprintf(fp, "   static constexpr Int_t kMax%s = %d;\n", blen, len);
        }
    }
    delete[] leaflen;
    leafs->Delete();
    delete leafs;

    // second loop on all leaves to generate type declarations
    // fprintf(fp, "\n   // Declaration of leaf types\n");
    TLeaf *leafcount;
    TLeafObject *leafobj;
    TBranchElement *bre = 0;
    const char *headOK = "   ";
    const char *headcom = " //";
    const char *head;
    char branchname[1024];
    char aprefix[1024];
    TObjArray branches(100);
    TObjArray mustInit(100);
    TObjArray mustInitArr(100);
    mustInitArr.SetOwner(kFALSE);
    Int_t *leafStatus = new Int_t[nleaves];
    for (l = 0; l < nleaves; l++)
    {
        Int_t kmax = 0;
        head = headOK;
        leafStatus[l] = 0;
        TLeaf *leaf = (TLeaf *)leaves->UncheckedAt(l);
        len = leaf->GetLen();
        if (len <= 0)
            len = 1;
        leafcount = leaf->GetLeafCount();
        TBranch *branch = leaf->GetBranch();
        branchname[0] = 0;
        strlcpy(branchname, branch->GetName(), sizeof(branchname));
        strlcpy(aprefix, branch->GetName(), sizeof(aprefix));
        if (!branches.FindObject(branch))
            branches.Add(branch);
        else
            leafStatus[l] = 1;
        if (branch->GetNleaves() > 1)
        {
            // More than one leaf for the branch we need to distinguish them
            strlcat(branchname, ".", sizeof(branchname));
            strlcat(branchname, leaf->GetTitle(), sizeof(branchname));
            if (leafcount)
            {
                // remove any dimension in title
                char *dim = (char *)strstr(branchname, "[");
                if (dim)
                    dim[0] = 0;
            }
        }
        else
        {
            strlcpy(branchname, branch->GetName(), sizeof(branchname));
        }
        char *twodim = (char *)strstr(leaf->GetTitle(), "][");
        bname = branchname;
        while (*bname)
        {
            if (*bname == '.')
                *bname = '_';
            if (*bname == ',')
                *bname = '_';
            if (*bname == ':')
                *bname = '_';
            if (*bname == '<')
                *bname = '_';
            if (*bname == '>')
                *bname = '_';
            bname++;
        }
        if (branch->IsA() == TBranchObject::Class())
        {
            if (branch->GetListOfBranches()->GetEntriesFast())
            {
                leafStatus[l] = 1;
                continue;
            }
            leafobj = (TLeafObject *)leaf;
            if (!leafobj->GetClass())
            {
                leafStatus[l] = 1;
                head = headcom;
            }
            auto ModifiedType = AddStdPrefix(leafobj->GetTypeName());
            fprintf(fp, "%s%-15s *%s;\n", head, ModifiedType.Data(), leafobj->GetName());
            if (leafStatus[l] == 0)
                mustInit.Add(leafobj);
            continue;
        }
        if (leafcount)
        {
            len = leafcount->GetMaximum();
            if (len <= 0)
                len = 1;
            strlcpy(blen, leafcount->GetName(), sizeof(blen));
            bname = &blen[0];
            while (*bname)
            {
                if (*bname == '.')
                    *bname = '_';
                if (*bname == ',')
                    *bname = '_';
                if (*bname == ':')
                    *bname = '_';
                if (*bname == '<')
                    *bname = '_';
                if (*bname == '>')
                    *bname = '_';
                bname++;
            }
            lenb = strlen(blen);
            if (blen[lenb - 1] == '_')
            {
                blen[lenb - 1] = 0;
                kmax = 1;
            }
            else
                snprintf(blen, sizeof(blen), "%d", len);
        }
        if (branch->IsA() == TBranchElement::Class())
        {
            bre = (TBranchElement *)branch;
            if (bre->GetType() != 3 && bre->GetType() != 4 && bre->GetStreamerType() <= 0 && bre->GetListOfBranches()->GetEntriesFast())
            {
                leafStatus[l] = 0;
            }
            if (bre->GetType() == 3 || bre->GetType() == 4)
            {
                fprintf(fp, "   %-15s %s_;\n", "Int_t", branchname);
                continue;
            }
            if (bre->IsBranchFolder())
            {
                auto ModifiedType = AddStdPrefix(bre->GetClassName());
                fprintf(fp, "   %-15s *%s;\n", ModifiedType.Data(), branchname);
                mustInit.Add(bre);
                continue;
            }
            else
            {
                if (branch->GetListOfBranches()->GetEntriesFast())
                {
                    leafStatus[l] = 1;
                }
            }
            if (bre->GetStreamerType() < 0)
            {
                if (branch->GetListOfBranches()->GetEntriesFast())
                {
                    auto ModifiedType = AddStdPrefix(bre->GetClassName());
                    fprintf(fp, "%s%-15s *%s;\n", headcom, ModifiedType.Data(), branchname);
                }
                else
                {
                    auto ModifiedType = AddStdPrefix(bre->GetClassName());
                    fprintf(fp, "%s%-15s *%s;\n", head, ModifiedType.Data(), branchname);
                    mustInit.Add(bre);
                }
                continue;
            }
            if (bre->GetStreamerType() == 0)
            {
                if (!TClass::GetClass(bre->GetClassName())->HasInterpreterInfo())
                {
                    leafStatus[l] = 1;
                    head = headcom;
                }
                auto ModifiedType = AddStdPrefix(bre->GetClassName());
                fprintf(fp, "%s%-15s *%s;\n", head, ModifiedType.Data(), branchname);
                if (leafStatus[l] == 0)
                    mustInit.Add(bre);
                continue;
            }
            if (bre->GetStreamerType() > 60)
            {
                TClass *cle = TClass::GetClass(bre->GetClassName());
                if (!cle)
                {
                    leafStatus[l] = 1;
                    continue;
                }
                if (bre->GetStreamerType() == 66)
                    leafStatus[l] = 0;
                char brename[256];
                strlcpy(brename, bre->GetName(), 255);
                char *bren = brename;
                char *adot = strrchr(bren, '.');
                if (adot)
                    bren = adot + 1;
                char *brack = strchr(bren, '[');
                if (brack)
                    *brack = 0;
                TStreamerElement *elem = (TStreamerElement *)cle->GetStreamerInfo()->GetElements()->FindObject(bren);
                if (elem)
                {
                    if (elem->IsA() == TStreamerBase::Class())
                    {
                        leafStatus[l] = 1;
                        continue;
                    }
                    if (!TClass::GetClass(elem->GetTypeName()))
                    {
                        leafStatus[l] = 1;
                        continue;
                    }
                    if (!TClass::GetClass(elem->GetTypeName())->HasInterpreterInfo())
                    {
                        leafStatus[l] = 1;
                        head = headcom;
                    }
                    if (leafcount)
                    {
                        auto ModifiedType = AddStdPrefix(elem->GetTypeName());
                        fprintf(fp, "%s%-15s %s[kMax%s];\n", head, ModifiedType.Data(), branchname, blen);
                    }
                    else
                    {
                        auto ModifiedType = AddStdPrefix(elem->GetTypeName());
                        fprintf(fp, "%s%-15s %s;\n", head, ModifiedType.Data(), branchname);
                    }
                }
                else
                {
                    if (!TClass::GetClass(bre->GetClassName())->HasInterpreterInfo())
                    {
                        leafStatus[l] = 1;
                        head = headcom;
                    }
                    auto ModifiedType = AddStdPrefix(bre->GetClassName());
                    fprintf(fp, "%s%-15s %s;\n", head, ModifiedType.Data(), branchname);
                }
                continue;
            }
        }
        if (strlen(leaf->GetTypeName()) == 0)
        {
            leafStatus[l] = 1;
            continue;
        }
        if (leafcount)
        {
            // len = leafcount->GetMaximum();
            // strlcpy(blen,leafcount->GetName(),sizeof(blen));
            // bname = &blen[0];
            // while (*bname) {if (*bname == '.') *bname='_'; bname++;}
            // lenb = strlen(blen);
            // Int_t kmax = 0;
            // if (blen[lenb-1] == '_') {blen[lenb-1] = 0; kmax = 1;}
            // else                     sprintf(blen,"%d",len);

            const char *stars = " ";
            if (bre && bre->GetBranchCount2())
            {
                stars = "*";
            }
            // Dimensions can be in the branchname for a split Object with a fix length C array.
            // Theses dimensions HAVE TO be placed after the dimension explicited by leafcount
            TString dimensions;
            char *dimInName = (char *)strstr(branchname, "[");
            if (twodim || dimInName)
            {
                if (dimInName)
                {
                    dimensions = dimInName;
                    dimInName[0] = 0; // terminate branchname before the array dimensions.
                }
                if (twodim)
                    dimensions += (char *)(twodim + 1);
            }
            const char *leafcountName = leafcount->GetName();
            char b2len[1024];
            if (bre && bre->GetBranchCount2())
            {
                TLeaf *l2 = (TLeaf *)bre->GetBranchCount2()->GetListOfLeaves()->At(0);
                strlcpy(b2len, l2->GetName(), sizeof(b2len));
                bname = &b2len[0];
                while (*bname)
                {
                    if (*bname == '.')
                        *bname = '_';
                    if (*bname == ',')
                        *bname = '_';
                    if (*bname == ':')
                        *bname = '_';
                    if (*bname == '<')
                        *bname = '_';
                    if (*bname == '>')
                        *bname = '_';
                    bname++;
                }
                leafcountName = b2len;
            }
            if (dimensions.Length())
            {
                if (kmax)
                {
                    auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                    fprintf(fp, "   %-14s %s%s[kMax%s]%s;   //[%s]\n", ModifiedType.Data(), stars,
                            branchname, blen, dimensions.Data(), leafcountName);
                }
                else
                {
                    auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                    fprintf(fp, "   %-14s %s%s[%d]%s;   //[%s]\n", ModifiedType.Data(), stars,
                            branchname, len, dimensions.Data(), leafcountName);
                }
            }
            else
            {
                if (kmax)
                {
                    auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                    fprintf(fp, "   %-14s %s%s[kMax%s];   //[%s]\n", ModifiedType.Data(), stars, branchname, blen, leafcountName);
                }
                else
                {
                    auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                    fprintf(fp, "   %-14s %s%s[%d];   //[%s]\n", ModifiedType.Data(), stars, branchname, len, leafcountName);
                }
            }
            if (stars[0] == '*')
            {
                TNamed *n;
                if (kmax)
                    n = new TNamed(branchname, Form("kMax%s", blen));
                else
                    n = new TNamed(branchname, Form("%d", len));
                mustInitArr.Add(n);
            }
        }
        else
        {
            if (strstr(branchname, "["))
                len = 1;
            if (len < 2)
            {
                auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                fprintf(fp, "   %-15s %s;\n", ModifiedType.Data(), branchname);
            }
            else
            {
                if (twodim)
                {
                    auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                    fprintf(fp, "   %-15s %s%s;\n", ModifiedType.Data(), branchname, (char *)strstr(leaf->GetTitle(), "["));
                }
                else
                {
                    auto ModifiedType = AddStdPrefix(leaf->GetTypeName());
                    fprintf(fp, "   %-15s %s[%d];\n", ModifiedType.Data(), branchname, len);
                }
            }
        }
    }

    // generate list of branches
    fprintf(fp, "\n");
    // fprintf(fp, "   // List of branches\n");
    for (l = 0; l < nleaves; l++)
    {
        if (leafStatus[l])
            continue;
        TLeaf *leaf = (TLeaf *)leaves->UncheckedAt(l);
        fprintf(fp, "   TBranch        *b_%s;   //!\n", R__GetBranchPointerName(leaf).Data());
    }
    fprintf(fp, "\n");

    // generate class member functions prototypes
    if (opt.Contains("selector"))
    {
        fprintf(fp, "\n");
        fprintf(fp, "   %s(TTree * /*tree*/ =0) : fChain(0) { }\n", classname);
        fprintf(fp, "   virtual ~%s() { }\n", classname);
        fprintf(fp, "   virtual Int_t   Version() const { return 2; }\n");
        fprintf(fp, "   virtual void    Begin(TTree *tree);\n");
        fprintf(fp, "   virtual void    SlaveBegin(TTree *tree);\n");
        fprintf(fp, "   virtual void    Init(TTree *tree);\n");
        fprintf(fp, "   virtual Bool_t  Notify();\n");
        fprintf(fp, "   virtual Bool_t  Process(Long64_t entry);\n");
        fprintf(fp, "   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }\n");
        fprintf(fp, "   virtual void    SetOption(const char *option) { fOption = option; }\n");
        fprintf(fp, "   virtual void    SetObject(TObject *obj) { fObject = obj; }\n");
        fprintf(fp, "   virtual void    SetInputList(TList *input) { fInput = input; }\n");
        fprintf(fp, "   virtual TList  *GetOutputList() const { return fOutput; }\n");
        fprintf(fp, "   virtual void    SlaveTerminate();\n");
        fprintf(fp, "   virtual void    Terminate();\n\n");
        fprintf(fp, "   ClassDef(%s,0);\n", classname);
        fprintf(fp, "};\n");
        fprintf(fp, "\n");
        fprintf(fp, "#endif\n");
        fprintf(fp, "\n");
    }
    else
    {
        /*
        fprintf(fp, "\n\n");
        fprintf(fp, "private:\n");
        fprintf(fp, "// here user can define their own variables\n\n");
        fprintf(fp, "};\n\n");
        fprintf(fp, "\n");
        fprintf(fp, "#endif\n");
        fprintf(fp, "\n");
        */
    }
    // generate code for class constructor
    /*
    fprintf(fp, "#ifdef %s_cxx\n", classname);
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "%s::%s(TTree *tree) : fChain(0) \n", classname, classname);
        fprintf(fp, "{\n");
        fprintf(fp, "// if parameter tree is not specified (or zero), connect the file\n");
        fprintf(fp, "// used to generate this class and read the Tree.\n");
        fprintf(fp, "   if (tree == 0) {\n");
        if (ischain)
        {
            fprintf(fp, "\n#ifdef SINGLE_TREE\n");
            fprintf(fp, "      // The following code should be used if you want this class to access\n");
            fprintf(fp, "      // a single tree instead of a chain\n");
        }
        if (isHbook)
        {
            fprintf(fp, "      THbookFile *f = (THbookFile*)gROOT->GetListOfBrowsables()->FindObject(\"%s\");\n",
                    treefile.Data());
            fprintf(fp, "      if (!f) {\n");
            fprintf(fp, "         f = new THbookFile(\"%s\");\n", treefile.Data());
            fprintf(fp, "      }\n");
            Int_t hid;
            sscanf(tree->GetName(), "h%d", &hid);
            fprintf(fp, "      tree = (TTree*)f->Get(%d);\n\n", hid);
        }
        else
        {
            fprintf(fp, "      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(\"%s\");\n", treefile.Data());
            fprintf(fp, "      if (!f || !f->IsOpen()) {\n");
            fprintf(fp, "         f = new TFile(\"%s\");\n", treefile.Data());
            fprintf(fp, "      }\n");
            if (tree->GetDirectory() != tree->GetCurrentFile())
            {
                fprintf(fp, "      TDirectory * dir = (TDirectory*)f->Get(\"%s\");\n", tree->GetDirectory()->GetPath());
                fprintf(fp, "      dir->GetObject(\"%s\",tree);\n\n", tree->GetName());
            }
            else
            {
                fprintf(fp, "      f->GetObject(\"%s\",tree);\n\n", tree->GetName());
            }
        }
        if (ischain)
        {
            fprintf(fp, "#else // SINGLE_TREE\n\n");
            fprintf(fp, "      // The following code should be used if you want this class to access a chain\n");
            fprintf(fp, "      // of trees.\n");
            fprintf(fp, "      TChain * chain = new TChain(\"%s\",\"%s\");\n",
                    tree->GetName(), tree->GetTitle());
            {
                R__LOCKGUARD(gROOTMutex);
                TIter next(((TChain *)tree)->GetListOfFiles());
                TChainElement *element;
                while ((element = (TChainElement *)next()))
                {
                    fprintf(fp, "      chain->Add(\"%s/%s\");\n", element->GetTitle(), element->GetName());
                }
            }
            fprintf(fp, "      tree = chain;\n");
            fprintf(fp, "#endif // SINGLE_TREE\n\n");
        }
        fprintf(fp, "   }\n");
        fprintf(fp, "   Init(tree);\n");
        fprintf(fp, "}\n");
        fprintf(fp, "\n");
    }

    // generate code for class destructor()
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "%s::~%s()\n", classname, classname);
        fprintf(fp, "{\n");
        fprintf(fp, "   if (!fChain) return;\n");
        if (isHbook)
        {
            //fprintf(fp,"   delete fChain->GetCurrentFile();\n");
        }
        else
        {
            fprintf(fp, "   delete fChain->GetCurrentFile();\n");
        }
        fprintf(fp, "}\n");
        fprintf(fp, "\n");
    }
    // generate code for class member function GetEntry()
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "Int_t %s::GetEntry(Long64_t entry)\n", classname);
        fprintf(fp, "{\n");
        fprintf(fp, "// Read contents of entry.\n");

        fprintf(fp, "   if (!fChain) return 0;\n");
        fprintf(fp, "   return fChain->GetEntry(entry);\n");
        fprintf(fp, "}\n");
    }
    // generate code for class member function LoadTree()
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "Long64_t %s::LoadTree(Long64_t entry)\n", classname);
        fprintf(fp, "{\n");
        fprintf(fp, "// Set the environment to read one entry\n");
        fprintf(fp, "   if (!fChain) return -5;\n");
        fprintf(fp, "   Long64_t centry = fChain->LoadTree(entry);\n");
        fprintf(fp, "   if (centry < 0) return centry;\n");
        fprintf(fp, "   if (fChain->GetTreeNumber() != fCurrent) {\n");
        fprintf(fp, "      fCurrent = fChain->GetTreeNumber();\n");
        fprintf(fp, "      Notify();\n");
        fprintf(fp, "   }\n");
        fprintf(fp, "   return centry;\n");
        fprintf(fp, "}\n");
        fprintf(fp, "\n");
    }
    */
    // generate code for class member function Init(), first pass = get branch pointer
    /*
    fprintf(fp, "inline NAGASH::StatusCode %s::InitializeData()\n", classname);
    fprintf(fp, "{\n");
    fprintf(fp, "   // The InitializeData() function is used to open ROOT file, get TTree into\n"
                "   // RootTreeUser. And call TTree::SetBranchAddress to set the address of variables\n"
                "   // User need not to modify this function.\n\n");
    fprintf(fp, "    RootFileUser()->GetObject(\"%s\", RootTreeUser());\n", argv[2]);
    fprintf(fp, "    if (RootTreeUser() == nullptr)\n");
    fprintf(fp, "    {\n");
    fprintf(fp, "        MSGUser()->MSG(NAGASH::MSGLevel::ERROR,\"Wrong TTree name\");\n");
    fprintf(fp, "        return NAGASH::StatusCode::FAILURE;\n");
    fprintf(fp, "    }\n\n");
    */

    fprintf(fpc, "    // %s\n", tree->GetName());
    fprintf(fpc, "    RootFileUser()->GetObject(\"%s\", %s);\n", tree->GetName(), tree->GetName());
    fprintf(fpc, "    if (%s)\n", tree->GetName());
    fprintf(fpc, "    {\n");
    if (mustInit.Last())
    {
        TIter next(&mustInit);
        TObject *obj;
        fprintf(fpc, "   // Set object pointer\n");
        while ((obj = next()))
        {
            if (obj->InheritsFrom(TBranch::Class()))
            {
                strlcpy(branchname, ((TBranch *)obj)->GetName(), sizeof(branchname));
            }
            else if (obj->InheritsFrom(TLeaf::Class()))
            {
                strlcpy(branchname, ((TLeaf *)obj)->GetName(), sizeof(branchname));
            }
            branchname[1023] = 0;
            bname = branchname;
            while (*bname)
            {
                if (*bname == '.')
                    *bname = '_';
                if (*bname == ',')
                    *bname = '_';
                if (*bname == ':')
                    *bname = '_';
                if (*bname == '<')
                    *bname = '_';
                if (*bname == '>')
                    *bname = '_';
                bname++;
            }
            fprintf(fpc, "   %s = 0;\n", branchname);
        }
    }
    if (mustInitArr.Last())
    {
        TIter next(&mustInitArr);
        TNamed *info;
        fprintf(fpc, "   // Set array pointer\n");
        while ((info = (TNamed *)next()))
        {
            fprintf(fpc, "   for(int i=0; i<%s; ++i) %s[i] = 0;\n", info->GetTitle(), info->GetName());
        }
        fprintf(fpc, "\n");
    }
    // fprintf(fpc, "   // Set branch addresses and branch pointers\n");
    /*
    fprintf(fp, "   if (!tree) return;\n");
    fprintf(fp, "   fChain = tree;\n");
    if (!opt.Contains("selector"))
        fprintf(fp, "   fCurrent = -1;\n");
    fprintf(fp, "   fChain->SetMakeClass(1);\n");
    fprintf(fp, "\n");
    */
    for (l = 0; l < nleaves; l++)
    {
        if (leafStatus[l])
            continue;
        TLeaf *leaf = (TLeaf *)leaves->UncheckedAt(l);
        len = leaf->GetLen();
        leafcount = leaf->GetLeafCount();
        TBranch *branch = leaf->GetBranch();
        strlcpy(aprefix, branch->GetName(), sizeof(aprefix));

        if (branch->GetNleaves() > 1)
        {
            // More than one leaf for the branch we need to distinguish them
            strlcpy(branchname, branch->GetName(), sizeof(branchname));
            strlcat(branchname, ".", sizeof(branchname));
            strlcat(branchname, leaf->GetTitle(), sizeof(branchname));
            if (leafcount)
            {
                // remove any dimension in title
                char *dim = (char *)strstr(branchname, "[");
                if (dim)
                    dim[0] = 0;
            }
        }
        else
        {
            strlcpy(branchname, branch->GetName(), sizeof(branchname));
            if (branch->IsA() == TBranchElement::Class())
            {
                bre = (TBranchElement *)branch;
                if (bre->GetType() == 3 || bre->GetType() == 4)
                    strlcat(branchname, "_", sizeof(branchname));
            }
        }
        bname = branchname;
        char *brak = strstr(branchname, "[");
        if (brak)
            *brak = 0;
        char *twodim = (char *)strstr(bname, "[");
        if (twodim)
            *twodim = 0;
        while (*bname)
        {
            if (*bname == '.')
                *bname = '_';
            if (*bname == ',')
                *bname = '_';
            if (*bname == ':')
                *bname = '_';
            if (*bname == '<')
                *bname = '_';
            if (*bname == '>')
                *bname = '_';
            bname++;
        }
        const char *maybedisable = "";
        if (branch != tree->GetBranch(branch->GetName()))
        {
            Error("MakeClass", "The branch named %s (full path name: %s) is hidden by another branch of the same name and its data will not be loaded.", branch->GetName(), R__GetBranchPointerName(leaf, kFALSE).Data());
            maybedisable = "// ";
        }
        if (branch->IsA() == TBranchObject::Class())
        {
            if (branch->GetListOfBranches()->GetEntriesFast())
            {
                fprintf(fpc, "%s       %s->SetBranchAddress(\"%s\",(void*)-1,&b_%s);\n", maybedisable, tree->GetName(), branch->GetName(), R__GetBranchPointerName(leaf).Data());
                continue;
            }
            strlcpy(branchname, branch->GetName(), sizeof(branchname));
        }
        if (branch->IsA() == TBranchElement::Class())
        {
            if (((TBranchElement *)branch)->GetType() == 3)
                len = 1;
            if (((TBranchElement *)branch)->GetType() == 4)
                len = 1;
        }
        if (leafcount)
            len = leafcount->GetMaximum() + 1;
        if (len > 1)
            fprintf(fpc, "%s       %s->SetBranchAddress(\"%s\", %s, &b_%s);\n",
                    maybedisable, tree->GetName(), branch->GetName(), branchname, R__GetBranchPointerName(leaf).Data());
        else
            fprintf(fpc, "%s       %s->SetBranchAddress(\"%s\", &%s, &b_%s);\n",
                    maybedisable, tree->GetName(), branch->GetName(), branchname, R__GetBranchPointerName(leaf).Data());
    }
    // must call Notify in case of MakeClass
    /*
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "   Notify();\n");
    }
    */

    fprintf(fpc, "    }\n\n");
    /*
    fprintf(fp, "    return NAGASH::StatusCode::SUCCESS;\n");
    fprintf(fp, "}\n");
    fprintf(fp, "\n");
    fprintf(fp, "#endif\n");
    fprintf(fp, "\n");
    */

    /*
    // generate code for class member function Notify()
    fprintf(fp, "Bool_t %s::Notify()\n", classname);
    fprintf(fp, "{\n");
    fprintf(fp, "   // The Notify() function is called when a new file is opened. This\n"
                "   // can be either for a new TTree in a TChain or when when a new TTree\n"
                "   // is started when using PROOF. It is normally not necessary to make changes\n"
                "   // to the generated code, but the routine can be extended by the\n"
                "   // user if needed. The return value is currently not used.\n\n");
    fprintf(fp, "   return kTRUE;\n");
    fprintf(fp, "}\n");
    fprintf(fp, "\n");

    // generate code for class member function Show()
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "void %s::Show(Long64_t entry)\n", classname);
        fprintf(fp, "{\n");
        fprintf(fp, "// Print contents of entry.\n");
        fprintf(fp, "// If entry is not specified, print current entry\n");

        fprintf(fp, "   if (!fChain) return;\n");
        fprintf(fp, "   fChain->Show(entry);\n");
        fprintf(fp, "}\n");
    }
    // generate code for class member function Cut()
    if (!opt.Contains("selector"))
    {
        fprintf(fp, "Int_t %s::Cut(Long64_t entry)\n", classname);
        fprintf(fp, "{\n");
        fprintf(fp, "// This function may be called from Loop.\n");
        fprintf(fp, "// returns  1 if entry is accepted.\n");
        fprintf(fp, "// returns -1 otherwise.\n");

        fprintf(fp, "   return 1;\n");
        fprintf(fp, "}\n");
    }
    fprintf(fp, "#endif // #ifdef %s_cxx\n", classname);
    */

    //======================Generate classname.C=====================
    /*
    if (!opt.Contains("selector"))
    {
        // generate code for class member function Loop()
        fprintf(fpc, "#include \"%s.h\"\n", classname);
        fprintf(fpc, "\n");
        fprintf(fpc, "// User can use \"using namespace NAGASH;\" to avoid extra work.\n\n");
        fprintf(fpc, "NAGASH::StatusCode %s::InitializeUser()\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // In this function, User can initialize their own variables, e.g. PlotGroup\n");
        fprintf(fpc, "    return NAGASH::StatusCode::SUCCESS;\n");
        fprintf(fpc, "}\n\n");

        fprintf(fpc, "NAGASH::StatusCode %s::Execute()\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // In this function, User do what they want for each entry in the TTree\n");
        fprintf(fpc, "    return NAGASH::StatusCode::SUCCESS;\n");
        fprintf(fpc, "}\n\n");

        fprintf(fpc, "NAGASH::StatusCode %s::Finalize()\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // In this function, User can do something after process the entries in a single file\n");
        fprintf(fpc, "    return NAGASH::StatusCode::SUCCESS;\n");
        fprintf(fpc, "}\n\n");
    }
    */
    if (opt.Contains("selector"))
    {
        // generate usage comments and list of includes
        fprintf(fpc, "#define %s_cxx\n", classname);
        fprintf(fpc, "// The class definition in %s.h has been generated automatically\n", classname);
        fprintf(fpc, "// by the ROOT utility TTree::MakeSelector(). This class is derived\n");
        fprintf(fpc, "// from the ROOT class TSelector. For more information on the TSelector\n"
                     "// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.\n\n");
        fprintf(fpc, "// The following methods are defined in this file:\n");
        fprintf(fpc, "//    Begin():        called every time a loop on the tree starts,\n");
        fprintf(fpc, "//                    a convenient place to create your histograms.\n");
        fprintf(fpc, "//    SlaveBegin():   called after Begin(), when on PROOF called only on the\n"
                     "//                    slave servers.\n");
        fprintf(fpc, "//    Process():      called for each event, in this function you decide what\n");
        fprintf(fpc, "//                    to read and fill your histograms.\n");
        fprintf(fpc, "//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF\n"
                     "//                    called only on the slave servers.\n");
        fprintf(fpc, "//    Terminate():    called at the end of the loop on the tree,\n");
        fprintf(fpc, "//                    a convenient place to draw/fit your histograms.\n");
        fprintf(fpc, "//\n");
        fprintf(fpc, "// To use this file, try the following session on your Tree T:\n");
        fprintf(fpc, "//\n");
        fprintf(fpc, "// root> T->Process(\"%s.C\")\n", classname);
        fprintf(fpc, "// root> T->Process(\"%s.C\",\"some options\")\n", classname);
        fprintf(fpc, "// root> T->Process(\"%s.C+\")\n", classname);
        fprintf(fpc, "//\n\n");
        fprintf(fpc, "#include \"%s\"\n", thead.Data());
        fprintf(fpc, "#include <TH2.h>\n");
        fprintf(fpc, "#include <TStyle.h>\n");
        fprintf(fpc, "\n");
        // generate code for class member function Begin
        fprintf(fpc, "\n");
        fprintf(fpc, "void %s::Begin(TTree * /*tree*/)\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // The Begin() function is called at the start of the query.\n");
        fprintf(fpc, "   // When running with PROOF Begin() is only called on the client.\n");
        fprintf(fpc, "   // The tree argument is deprecated (on PROOF 0 is passed).\n");
        fprintf(fpc, "\n");
        fprintf(fpc, "   TString option = GetOption();\n");
        fprintf(fpc, "\n");
        fprintf(fpc, "}\n");
        // generate code for class member function SlaveBegin
        fprintf(fpc, "\n");
        fprintf(fpc, "void %s::SlaveBegin(TTree * /*tree*/)\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // The SlaveBegin() function is called after the Begin() function.\n");
        fprintf(fpc, "   // When running with PROOF SlaveBegin() is called on each slave server.\n");
        fprintf(fpc, "   // The tree argument is deprecated (on PROOF 0 is passed).\n");
        fprintf(fpc, "\n");
        fprintf(fpc, "   TString option = GetOption();\n");
        fprintf(fpc, "\n");
        fprintf(fpc, "}\n");
        // generate code for class member function Process
        fprintf(fpc, "\n");
        fprintf(fpc, "Bool_t %s::Process(Long64_t entry)\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // The Process() function is called for each entry in the tree (or possibly\n"
                     "   // keyed object in the case of PROOF) to be processed. The entry argument\n"
                     "   // specifies which entry in the currently loaded tree is to be processed.\n"
                     "   // It can be passed to either %s::GetEntry() or TBranch::GetEntry()\n"
                     "   // to read either all or the required parts of the data. When processing\n"
                     "   // keyed objects with PROOF, the object is already loaded and is available\n"
                     "   // via the fObject pointer.\n"
                     "   //\n"
                     "   // This function should contain the \"body\" of the analysis. It can contain\n"
                     "   // simple or elaborate selection criteria, run algorithms on the data\n"
                     "   // of the event and typically fill histograms.\n"
                     "   //\n"
                     "   // The processing can be stopped by calling Abort().\n"
                     "   //\n"
                     "   // Use fStatus to set the return value of TTree::Process().\n"
                     "   //\n"
                     "   // The return value is currently not used.\n\n",
                classname);
        fprintf(fpc, "\n");
        fprintf(fpc, "   return kTRUE;\n");
        fprintf(fpc, "}\n");
        // generate code for class member function SlaveTerminate
        fprintf(fpc, "\n");
        fprintf(fpc, "void %s::SlaveTerminate()\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // The SlaveTerminate() function is called after all entries or objects\n"
                     "   // have been processed. When running with PROOF SlaveTerminate() is called\n"
                     "   // on each slave server.");
        fprintf(fpc, "\n");
        fprintf(fpc, "\n");
        fprintf(fpc, "}\n");
        // generate code for class member function Terminate
        fprintf(fpc, "\n");
        fprintf(fpc, "void %s::Terminate()\n", classname);
        fprintf(fpc, "{\n");
        fprintf(fpc, "   // The Terminate() function is the last function to be called during\n"
                     "   // a query. It always runs on the client, it can be used to present\n"
                     "   // the results graphically or save the results to file.");
        fprintf(fpc, "\n");
        fprintf(fpc, "\n");
        fprintf(fpc, "}\n");
    }
    Info("MakeClass", "Files: %s and %s generated from TTree: %s", thead.Data(), tcimp.Data(), tree->GetName());
    delete[] leafStatus;

    fclose(fp);
    fclose(fpc);

    return 0;
}
