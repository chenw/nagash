//***************************************************************************************
/// @file Uncertainty.cxx
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#include "NAGASH/Uncertainty.h"

using namespace NAGASH;

/// @brief Calculate the uncertainty of \f$ A+B \f$.
/// @param A_err uncertainty of \f$ A \f$.
/// @param B_err uncertainty of \f$ B \f$.
double Uncertainty::AplusB(double A_err, double B_err)
{
    return sqrt(A_err * A_err + B_err * B_err);
}

/// @brief Calculate the uncertainty of \f$ A-B \f$.
/// @param A_err uncertainty of \f$ A \f$.
/// @param B_err uncertainty of \f$ B \f$
double Uncertainty::AminusB(double A_err, double B_err)
{
    return sqrt(A_err * A_err + B_err * B_err);
}

/// @brief Calculate the uncertainty of \f$ A/(A+B) \f$.
/// @param A the value of \f$ A \f$.
/// @param AplusB  the value of \f$ A+B \f$.
/// @param A_err the uncertainty of \f$ A \f$.
/// @param AplusB_err the uncertainty of \f$ A+B \f$.
double Uncertainty::AoverAplusB(double A, double AplusB, double A_err, double AplusB_err)
{
    double B = AplusB - A;
    double B_err = A_err < AplusB_err ? sqrt(pow(AplusB_err, 2) - pow(A_err, 2)) : 0;

    if (AplusB != 0)
        return sqrt(B * B * A_err * A_err + A * A * B_err * B_err) / pow(A + B, 2);
    else
        return 0;
}

/// @brief Calculate the uncertainty of \f$ A/B \f$.
/// @param A the value of \f$ A \f$.
/// @param B the value of \f$ B \f$.
/// @param A_err the uncertainty of \f$ A \f$.
/// @param B_err  the uncertainty of \f$ B \f$.
double Uncertainty::AoverB(double A, double B, double A_err, double B_err)
{
    if (B != 0)
        return sqrt(B * B * A_err * A_err + A * A * B_err * B_err) / pow(B, 2);
    else
        return 0;
}

/// @brief Calculate the uncertainty of \f$ (A-B)/(A+B) \f$.
/// @param A the value of \f$ A \f$.
/// @param B the value of \f$ B \f$.
/// @param A_err the uncertainty of \f$ A \f$.
/// @param B_err the uncertainty of \f$ B \f$.
/// @return
double Uncertainty::AminusBoverAplusB(double A, double B, double A_err, double B_err)
{
    if (A != 0 || B != 0)
        return 2 * sqrt(B * B * A_err * A_err + A * A * B_err * B_err) / pow(A + B, 2);
    else
        return 0;
}
