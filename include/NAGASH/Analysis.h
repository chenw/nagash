//***************************************************************************************
/// @file Analysis.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

/** @defgroup AnalysisClasses Analysis Classes
    @brief Classes for constructing a analysis framework.

## Your First Analysis Framework

###HelloWorld

Firstly, let's print out "hello world !" in the framework of NAGASH, create a file `HelloWorld.cxx` after you setup NAGASH with following content:

@code {.cxx}
#include "NAGASH.h"
#include <iostream>

class HelloWorld : public NAGASH::Job
{
public:
    // constructor of the class, inherited from NAGASH::Job
    HelloWorld(const NAGASH::ConfigTool &config) : NAGASH::Job(config) {}
    // User defined function
    void Print();
    // The function called in NAGASH::Analysis::SubmitJob, must be static
    static NAGASH::StatusCode Process(const NAGASH::ConfigTool &config, std::shared_ptr<NAGASH::ResultGroup> result);
};

void HelloWorld::Print()
{
    // use NAGASH::MSGTool to print messages
    MSGUser()->StartTitle("HelloWorld::Print");
    MSGUser()->MSG(NAGASH::MSGLevel::INFO, "Hello World ! (from Job Number ", JobNumber(), ")");
    MSGUser()->EndTitle();
}

NAGASH::StatusCode HelloWorld::Process(const NAGASH::ConfigTool &config, std::shared_ptr<NAGASH::ResultGroup> result)
{
    // the first argument is the ConfigTool that you should pass to constructor
    // the second argument is the ResultGroup you want to save.
    // you should call
    // result->Merge(job.ResultGroupUser())
    // after your job finishes

    HelloWorld temph(config);
    // add lock to avoid messy output
    NAGASH::Job::DefaultMutex.lock();
    temph.Print();
    NAGASH::Job::DefaultMutex.unlock();
    return NAGASH::StatusCode::SUCCESS;
}

int main()
{
    NAGASH::ConfigTool config;
    // let the threadpool of MyAnalysis be the size of 5
    config.BookPar<int>("Nthread", 5);
    NAGASH::Analysis MyAnalysis(config);
    for (int i = 0; i < 5; i++)
    {
        // submit your job
        MyAnalysis.SubmitJob<HelloWorld>(false);
    }
    // join all the submitted jobs
    MyAnalysis.Join();
}
@endcode

Then  compile and run with following commands:

@code{.sh}
g++ -std=c++17 `root-config --cflags --libs` -lMinuit -lMinuit2 -lpthread -lm -lNAGASH -O3 HelloWorld.cxx -o HelloWorld
./HelloWorld
@endcode

You will see a output like:

@code
Analysis->Timer::PrintCurrent     INFO     Submit job 0 at 2021-02-14 19:16:57
Analysis->Timer::PrintCurrent     INFO     Submit job 1 at 2021-02-14 19:16:57
Analysis->Timer::PrintCurrent     INFO     Submit job 2 at 2021-02-14 19:16:57
Analysis->Timer::PrintCurrent     INFO     Submit job 3 at 2021-02-14 19:16:57
Analysis->Timer::PrintCurrent     INFO     Submit job 4 at 2021-02-14 19:16:57
Analysis->HelloWorld::Print     INFO     Hello World ! (from Job Number 0)
Analysis->HelloWorld::Print     INFO     Hello World ! (from Job Number 2)
Analysis->HelloWorld::Print     INFO     Hello World ! (from Job Number 1)
Analysis->HelloWorld::Print     INFO     Hello World ! (from Job Number 4)
Analysis->HelloWorld::Print     INFO     Hello World ! (from Job Number 3)
Analysis     INFO     Finished all submitted jobs
Analysis     INFO     Finished Time : 2021-02-14 19:16:57
Analysis->Timer::Duration     INFO     Duration between (First Submit) and (Finish Submitted Jobs) : 918 us
@endcode

You can modify `HelloWorld.cxx` and see how the output changes.

### Creating your first analysis framework

Let's create the basic directory structure first:

@code {.sh}
mkdir MyAnalysis
cd MyAnalysis
mkdir include src control share
@endcode

Then use `NAGASHMakeClass` to make class for processing ROOT file:

@code {.sh}
NAGASHMakeClass /Data2/whma/Pythia8_Wmunu_pp_13TeV/Pythia8_Wmunu_pp_13TeV_CT10_2M_1.root Tree LoopPythiaWSample
@endcode

You will get `LoopPythiaWSample.h` and `LoopPythiaWSample.cxx` in current directory, move `.h` to `include` and `.cxx` to  `src`. Then edit `src/LoopPythiaWSample.cxx`  as follows:

@code {.cxx}
#include "LoopPythiaWSample.h"

// User can use "using namespace NAGASH;" to avoid extra typing.

NAGASH::StatusCode LoopPythiaWSample::InitializeUser()
{
    // In this function, User can initialize their own variables, e.g. PlotGroup
    ResultGroupUser()->BookResult<NAGASH::Count>("Mass");
    auto MyPlotGroup = ResultGroupUser()->BookResult<NAGASH::PlotGroup>("MassHist", "MyAnalysis_MassHist.root");
    MyPlotGroup->BookPlot1D("WMass", 200, 0, 100);
    MyPlotGroup->BookPlot1D("WPt", 200, 0, 100);
    MyPlotGroup->BookPlot1D("WRapidity", 100, -5, 5);

    return NAGASH::StatusCode::SUCCESS;
}

NAGASH::StatusCode LoopPythiaWSample::Execute()
{
    // In this function, User do what they want for each entry in the TTree
    TLorentzVector WbosonP4;
    WbosonP4.SetPxPyPzE(WbosonPx, WbosonPy, WbosonPz, WbosonE);
    ResultGroupUser()->Get<NAGASH::Count>("Mass")->Add(WbosonP4.M());

    // Fill booked histograms
    // In real analysis, users are recommanded to store the pointer of histograms as member variables
    // Since ResultGroup::Get are quite slow and will slow down your program if you call it for each entry
    std::shared_ptr<NAGASH::PlotGroup> MyPlotGroup = ResultGroupUser()->Get<NAGASH::PlotGroup>("MassHist");
    MyPlotGroup->GetPlot1D("WMass")->Fill(WbosonP4.M());
    MyPlotGroup->GetPlot1D("WPt")->Fill(WbosonP4.Pt());
    MyPlotGroup->GetPlot1D("WRapidity")->Fill(WbosonP4.Rapidity());

    // Debug info
    MSGUser()->MSG(NAGASH::MSGLevel::DEBUG, WbosonP4.M());

    return NAGASH::StatusCode::SUCCESS;
}

NAGASH::StatusCode LoopPythiaWSample::Finalize()
{
    // In this function, User can do something after process the entries
    return NAGASH::StatusCode::SUCCESS;
}
@endcode

Create `control/MyAnalysis.cxx` with:

@code {.cxx}
#include "Analysis.h"
#include "LoopPythiaWSample.h"

using namespace NAGASH;
using namespace std;

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        cout << "Usage: " << argv[0] << " (file list) (config file)" << endl;
        return 0;
    }

    Analysis MyAnalysis(argv[2]);
    auto myresult = MyAnalysis.ProcessEventLoop<LoopPythiaWSample>(argv[1]);
}
@endcode

Create `makefile` with the following command:

@code {.sh}
GenerateMakefile MyAnalysis
@endcode

In general, you can call `GenerateMakefile (project name)` to generate `makefile` of your project in current directory.

Create `share/filelist.dat`  with:

@code
/Data2/whma/Pythia8_Wmunu_pp_13TeV/Pythia8_Wmunu_pp_13TeV_CT10_2M_1.root
/Data2/whma/Pythia8_Wmunu_pp_13TeV/Pythia8_Wmunu_pp_13TeV_CT10_2M_2.root
/Data2/whma/Pythia8_Wmunu_pp_13TeV/Pythia8_Wmunu_pp_13TeV_CT10_2M_3.root
/Data2/whma/Pythia8_Wmunu_pp_13TeV/Pythia8_Wmunu_pp_13TeV_CT10_2M_4.root
/Data2/whma/Pythia8_Wmunu_pp_13TeV/Pythia8_Wmunu_pp_13TeV_CT10_2M_5.root
@endcode

Create `share/config.dat` with:

@code
int Nthread 5 # number of threads
long EvtMax 1000 # number of entry you want to read in
bool CreateLog 0 # create log file or print to screen
string LogFileName MyAnaOut.log # log file name
string MSGLevel INFO # message level
@endcode

Then run with the following command:

@code {.sh}
make -j
./bin/MyAnalysis ./share/filelist.dat ./share/config.dat
@endcode

After it finishes, you will get a ROOT file `MyAnalysis_MassHist.root`, check its content.

Then switch `CreateLog` to `1` and `MSGLevel` to `DEBUG` and rerun. Check the log file you get.
 */

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/ThreadPool.h"
#include "NAGASH/ConfigTool.h"
#include "NAGASH/LoopEvent.h"
#include "NAGASH/ResultGroup.h"
#include "NAGASH/Job.h"

namespace NAGASH
{

    class Analysis
    {
    public:
        Analysis();
        Analysis(const Analysis &ana) = delete;
        Analysis(Analysis &&ana) = delete;
        Analysis &operator=(const Analysis &ana) = delete;
        Analysis &operator=(Analysis &&ana) = delete;

        Analysis(const ConfigTool &c);
        Analysis(const char *configfilename);
        ~Analysis();
        void SetThreadNumber(int num);
        std::shared_ptr<ConfigTool> ConfigUser();

        // deal with eventloop
        // for multi thread
        template <typename LE>
        std::shared_ptr<ResultGroup> ProcessEventLoop(const char *filelist, bool dosave = true);
        template <typename LE>
        std::shared_ptr<ResultGroup> SubmitEventLoop(const char *filelist, bool dosave = true);
        // for multi process
        template <typename LE>
        std::shared_ptr<ResultGroup> ProcessEventLoopMainThread(const char *filename, bool dosave = true);

        // deal with user defined functions
        template <typename J, typename... Args>
        std::shared_ptr<ResultGroup> SubmitJob(bool doSave, Args &&...args);
        template <typename J, typename JS, typename... Args>
        std::shared_ptr<ResultGroup> SubmitJob(bool doSave, Args &&...args);

        std::shared_ptr<MSGTool> MSGUser();
        void Join();

    protected:
        std::shared_ptr<ConfigTool> config;
        std::vector<std::shared_ptr<ResultGroup>> ResultsToSave;
        std::shared_ptr<Timer> TimerUser();

    private:
        int ThreadNumber = 1;
        std::vector<int> SubmittedJobCount;
        int SubmittedJobs = 0;
        std::shared_ptr<MSGTool> msg;
        std::shared_ptr<Timer> timer;
        std::shared_ptr<ThreadPool> pool;
        std::vector<std::future<StatusCode>> vf;
        void ConfigThreadPool();

        template <typename F, typename... Args>
        void ThreadPoolEnqueue(F &&f, Args &&...args);
    };

    inline std::shared_ptr<ConfigTool> Analysis::ConfigUser() { return this->config; } ///< return the \link ConfigTool \endlink
    inline std::shared_ptr<Timer> Analysis::TimerUser() { return timer; }              ///< return the \link Timer \endlink
    inline std::shared_ptr<MSGTool> Analysis::MSGUser() { return msg; }                ///< return the \link MSGTool \endlink

    /// @brief Process the user defined event loop.
    /// This function will automatically call Join() after submit the event loop.
    /// @tparam LE user-define event loop class, must inherit from \link LoopEvent \endlink.
    /// @param[in] filelist Input file list.
    /// @param[in] dosave Whether save the result, i.e. automatically call \link ResultGroup::Write() \endlink after
    ///            finish the event loop.
    /// @return The \link ResultGroup \endlink of the event loop class defined by the user that stores the results
    ///         after the entire event loop finished.
    template <typename LE>
    std::shared_ptr<ResultGroup> Analysis::ProcessEventLoop(const char *filelist, bool dosave)
    {
        auto result = SubmitEventLoop<LE>(filelist, dosave);
        Join();
        return result;
    }

    /// @brief Submit the user defined event loop.
    /// This function will not automatically call Join() after submit the event loop. So user can submit multiple event loops.
    /// @tparam LE user-define event loop class, must inherit from \link LoopEvent \endlink.
    /// @param[in] filelist Input file list.
    /// @param[in] dosave Whether save the result, i.e. automatically call \link ResultGroup::Write() \endlink after
    ///            finish the event loop.
    /// @return The \link ResultGroup \endlink of the event loop class defined by the user that stores the results
    ///         after the entire event loop finished.
    template <typename LE>
    std::shared_ptr<ResultGroup> Analysis::SubmitEventLoop(const char *filelist, bool dosave)
    {
        // check LE type
        static_assert(!std::is_pointer<LE>::value, "Analysis::SubmitEventLoop : type can not be a pointer");
        static_assert(std::is_base_of<LoopEvent, LE>::value, "Analysis::SubmitEventLoop : Input type must be or be inherited from LoopEvent class");

        std::ifstream infile;
        infile.open(filelist, std::ios::in);
        if (!infile.is_open())
        {
            MSGUser()->StartTitle("Analysis::SubmitEventLoop");
            MSGUser()->MSG_WARNING("file list ", filelist, " does not exist, returning a null result");
            MSGUser()->EndTitle();
            return nullptr;
        }

        auto origin = msg->OutputLevel;
        bool existfileselection = false;
        bool existfileselectionpattern = false;
        msg->OutputLevel = MSGLevel::SILENT;
        std::vector<int> fileselection = config->GetPar<std::vector<int>>("FileSelection", &existfileselection);
        std::string fileselectionpattern(config->GetPar<TString>("FileSelectionPattern", &existfileselectionpattern).Data());
        std::regex fileselectionregex(fileselectionpattern);
        msg->OutputLevel = origin;

        auto result = std::make_shared<ResultGroup>(this->msg, this->config);
        std::string line;
        int count = 0;
        while (std::getline(infile, line))
        {
            ++count;

            if (existfileselection && std::find(fileselection.begin(), fileselection.end(), count) == fileselection.end())
                continue;

            if (existfileselectionpattern && !std::regex_search(line, fileselectionregex))
                continue;

            // remove comment and check if the file exists
            auto commentstart = line.find("#");
            if (commentstart != std::string::npos)
                line.erase(commentstart, line.size());

            // remove the carriage return
            if (line.back() == '\r')
                line.pop_back();

            int isexist = access(line.c_str(), F_OK);
            if (isexist == 0)
            {
                SubmitJob<LoopEvent, LE>(false, result, TString(line.c_str()));
            }
            else if (line.size() != 0)
            {
                MSGUser()->StartTitle("Analysis::SubmitEventLoop");
                MSGUser()->MSG_WARNING("file ", line, " does not exist, will skip this file");
                MSGUser()->EndTitle();
            }
        }

        if (dosave)
            ResultsToSave.push_back(result);

        infile.close();
        return result;
    }

    /// @brief Single-thread mode of processing the user defined event loop.
    /// The event loop will be run in the main thread.
    /// @tparam LE user-define event loop class, must inherit from \link LoopEvent \endlink.
    /// @param[in] filename Input file name.
    /// @param[in] dosave Whether save the result, i.e. automatically call \link NAGASH::ResultGroup::Write() \endlink after
    ///            finish the event loop.
    /// @return The \link ResultGroup \endlink of the event loop class defined by the user that stores the results
    ///         after the entire event loop finished.
    template <typename LE>
    inline std::shared_ptr<ResultGroup> Analysis::ProcessEventLoopMainThread(const char *filename, bool dosave)
    {
        // check LE type
        static_assert(!std::is_pointer<LE>::value, "Analysis::SubmitEventLoop : type can not be a pointer");
        static_assert(std::is_base_of<LoopEvent, LE>::value, "Analysis::SubmitEventLoop : Input type must be or be inherited from LoopEvent class");

        ConfigTool configCopy(*config);
        configCopy.BookPar<int>("JobNumber", -1);
        auto r = std::make_shared<ResultGroup>(this->msg, this->config);
        LoopEvent::Process<LE>(configCopy, nullptr, r, filename);
        return r;
    }

    /// @brief Submit the user defined job.
    /// The Analysis will process the submitted jobs in multi-thread mode.
    /// @tparam J user-defined job class, must inherit from \link Job \endlink.
    /// @tparam ...Args types of other arguments to be passed to the job class. Can be automatically deduced by the compiler.
    /// @param doSave Whether save the result, i.e. automatically call \link ResultGroup::Write() \endlink after
    ///        finish the job.
    /// @param ...args other arguments to be passed to the job class, should be compatible with the definition of the Process
    ///        function of the job class.
    /// @return The \link ResultGroup \endlink of the job class defined by the user that stores the results
    ///         after the job finished.
    template <typename J, typename... Args>
    inline std::shared_ptr<ResultGroup> Analysis::SubmitJob(bool doSave, Args &&...args)
    {
        static_assert(!std::is_pointer<J>::value, "Analysis::SubmitJob : type can not be a pointer");
        static_assert(std::is_base_of<Job, J>::value, "Analysis::SubmitJob : Input type must be or be inherited from Job class");

        ConfigTool configCopy(*config);
        configCopy.BookPar<int>("JobNumber", SubmittedJobs);
        // copy msg
        configCopy.SetMSG(std::make_shared<MSGTool>());
        configCopy.MSGUser()->OutputLevel = config->MSGUser()->OutputLevel;
        configCopy.MSGUser()->DefinedLevel = config->MSGUser()->DefinedLevel;
        configCopy.MSGUser()->FocusName = config->MSGUser()->FocusName;
        configCopy.MSGUser()->isCreateLog = config->MSGUser()->isCreateLog;
        configCopy.MSGUser()->isOpenLog = config->MSGUser()->isOpenLog;
        configCopy.MSGUser()->LogFileName = config->MSGUser()->LogFileName;
        configCopy.MSGUser()->FuncTitle = config->MSGUser()->FuncTitle;

        auto r = std::make_shared<ResultGroup>(this->msg, this->config);
        ThreadPoolEnqueue(J::Process, configCopy, r, std::forward<Args>(args)...);
        if (doSave)
        {
            ResultsToSave.emplace_back(r);
        }

        return r;
    }

    /// @brief Submit the user defined job.
    /// The Analysis will process the submitted jobs in multi-thread mode.
    /// @tparam J user-defined job class, must inherit from \link Job \endlink.
    /// @tparam JS sometimes the Job::Process() function is a template function, this is the template type.
    /// @tparam ...Args types of other arguments to be passed to the job class. Can be automatically deduced by the compiler.
    /// @param doSave Whether save the result, i.e. automatically call \link ResultGroup::Write() \endlink after
    ///        finish the job.
    /// @param ...args other arguments to be passed to the job class, should be compatible with the definition of the Process
    ///        function of the job class.
    /// @return The \link ResultGroup \endlink of the job class defined by the user that stores the results
    ///         after the job finished.
    template <typename J, typename JS, typename... Args>                          // Job::Process with template
    std::shared_ptr<ResultGroup> Analysis::SubmitJob(bool doSave, Args &&...args) // use config from Analysis
    {
        static_assert(!std::is_pointer<J>::value, "Analysis::SubmitJob : type can not be a pointer");
        static_assert(std::is_base_of<Job, J>::value, "Analysis::SubmitJob : Input type must be or be inherited from Job class");

        ConfigTool configCopy(*config);
        configCopy.BookPar<int>("JobNumber", SubmittedJobs);
        // copy msg
        configCopy.SetMSG(std::make_shared<MSGTool>());
        configCopy.MSGUser()->OutputLevel = config->MSGUser()->OutputLevel;
        configCopy.MSGUser()->DefinedLevel = config->MSGUser()->DefinedLevel;
        configCopy.MSGUser()->FocusName = config->MSGUser()->FocusName;
        configCopy.MSGUser()->isCreateLog = config->MSGUser()->isCreateLog;
        configCopy.MSGUser()->isOpenLog = config->MSGUser()->isOpenLog;
        configCopy.MSGUser()->LogFileName = config->MSGUser()->LogFileName;
        configCopy.MSGUser()->FuncTitle = config->MSGUser()->FuncTitle;

        auto r = std::make_shared<ResultGroup>(this->msg, this->config);
        ThreadPoolEnqueue(J::template Process<JS>, configCopy, r, std::forward<Args>(args)...);
        if (doSave)
        {
            ResultsToSave.emplace_back(r);
        }

        return r;
    }

    /// @brief General function to enqueue a function into the thread pool inside Analysis class.
    /// @tparam F the type of the function to be enqueued.
    /// @tparam ...Args the type of the arguments of the function.
    /// @param f the function to be enqueued.
    /// @param ...args the arguments of the function.
    template <typename F, typename... Args>
    void Analysis::ThreadPoolEnqueue(F &&f, Args &&...args)
    {
        if (pool == nullptr)
        {
            msg->MSG(MSGLevel::WARNING, "Thread pool not defined, only one thread will be created");
            pool = std::make_shared<ThreadPool>(1);
        }

        TString record;
        record.Form("(Submit Job %d)", SubmittedJobs);
        TimerUser()->Record(record);
        TString jobnum;
        jobnum.Form("Submit job %d at ", SubmittedJobs);
        TimerUser()->PrintCurrent(jobnum);
        vf.emplace_back(pool->enqueue(f, std::forward<Args>(args)...));
        SubmittedJobCount.push_back(SubmittedJobs);
        SubmittedJobs++;
    }

} // namespace NAGASH
