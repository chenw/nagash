#include "NAGASH/NAGASH.h"

using namespace NAGASH;

int main(int argc, char **argv)
{
    auto msg = std::make_shared<MSGTool>(MSGLevel::INFO);
    auto config = std::make_shared<ConfigTool>();
    auto plot = std::make_shared<Plot1D>(msg, config, "plot", "test.root", 1, 0., 1.);
    auto plot2d = std::make_shared<Plot2D>(msg, config, "plot2d", "test.root", 1, 0., 1., 2, 0., 2.);

    plot->Rebin("aaa", 1);
    // plot2d->Rebin("aaa", 1);

    return 0;
}
