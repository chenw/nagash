//***************************************************************************************
/// @file TFileHelper.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Tool.h"
#include "NAGASH/Global.h"
#include "NAGASH/MapTool.h"

namespace NAGASH
{
    /** @class NAGASH::TFileHelper TFileHelper.h "NAGASH/TFileHelper.h"
        @ingroup ToolClasses
        @brief Provide fast access to ROOT objects inside a TFile.
     */
    class TFileHelper : public Tool
    {
    public:
        /// @brief Constructor
        /// @param msg input MSGTool.
        /// @param name name of the ROOT file.
        TFileHelper(std::shared_ptr<MSGTool> msg, const std::string &name)
            : Tool(msg)
        {
            TH1::AddDirectory(kFALSE);
            auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
            infile = new TFile(name.c_str(), "READ");
            if (infile == nullptr)
            {
                MSGUser()->MSG_ERROR("can't open ROOT file ", name);
                return;
            }

            if (infile->IsZombie())
            {
                MSGUser()->MSG_ERROR("can't open ROOT file ", name);
                return;
            }

            isopen = true;
            if (isopen)
            {
                TIter next(infile->GetListOfKeys());
                TKey *key;

                while ((key = (TKey *)next()))
                {
                    if (TString(key->GetClassName()).Contains("TH1") ||
                        TString(key->GetClassName()).Contains("TH2") ||
                        TString(key->GetClassName()).Contains("TH3") ||
                        TString(key->GetClassName()).Contains("THn") ||
                        TString(key->GetClassName()).Contains("TProfile"))
                    {
                        std::unique_ptr<TObject> obj(key->ReadObj());
                        map_name_hist.emplace(std::pair<std::string, TKey *>(obj->GetName(), key));
                    }

                    if (TString(key->GetClassName()) == "TTree")
                    {
                        std::unique_ptr<TObject> obj(key->ReadObj());
                        map_name_tree.emplace(std::pair<std::string, TKey *>(obj->GetName(), key));
                    }

                    if (TString(key->GetClassName()).Contains("TGraph"))
                    {
                        std::unique_ptr<TObject> obj(key->ReadObj());
                        map_name_graph.emplace(std::pair<std::string, TKey *>(obj->GetName(), key));
                    }
                }
            }

            MapTool tool(MSGUser());
            histnames = tool.GetListOfKeys(map_name_hist);
        }

        /// @brief Get TH1 object
        /// @param name name of the TH1 object.
        TH1 *Get(const std::string &name)
        {
            if (!isopen)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
                MSGUser()->MSG_ERROR("file not open");
                return nullptr;
            }
            else
            {
                auto findresult = map_name_hist.find(name);
                if (findresult != map_name_hist.end())
                    return (TH1 *)findresult->second->ReadObj();
                else
                {
                    auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
                    MSGUser()->MSG_ERROR("hist ", name, " not found");
                    return nullptr;
                }
            }
        }

        /// @brief Get TTree object
        /// @param name name of the TTree object.
        TTree *GetTree(const std::string &name)
        {

            if (!isopen)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
                MSGUser()->MSG_ERROR("file not open");
                return nullptr;
            }
            else
            {
                auto findresult = map_name_tree.find(name);
                if (findresult != map_name_tree.end())
                    return (TTree *)findresult->second->ReadObj();
                else
                {
                    // auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
                    // MSGUser()->MSG_ERROR("tree ", name, " not found");
                    return nullptr;
                }
            }
        }

        /// @brief Get TGraph object
        /// @param name name of the TGraph object
        TGraph *GetTGraph(const std::string &name)
        {

            if (!isopen)
            {
                auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
                MSGUser()->MSG_ERROR("file not open");
                return nullptr;
            }
            else
            {
                auto findresult = map_name_graph.find(name);
                if (findresult != map_name_graph.end())
                    return (TGraph *)findresult->second->ReadObj();
                else
                {
                    auto titleguard = MSGUser()->StartTitleWithGuard("TFileHelper");
                    MSGUser()->MSG_ERROR("graph ", name, " not found");
                    return nullptr;
                }
            }
        }

        /// @brief Close the ROOT file.
        void Close()
        {
            if (infile)
            {
                delete infile;
                infile = nullptr;
                isopen = false;

                map_name_hist.clear();
                map_name_graph.clear();
                map_name_tree.clear();
                histnames.clear();
            }
        }

        /// @brief Get a vector stores the list of the names of histogram.
        /// @return
        const std::vector<std::string> &GetListOfHistNames()
        {
            return histnames;
        }

        /// @brief Destructor
        virtual ~TFileHelper()
        {
            Close();
        }

    private:
        std::map<std::string, TKey *> map_name_hist;
        std::map<std::string, TKey *> map_name_graph;
        std::map<std::string, TKey *> map_name_tree;
        std::vector<std::string> histnames;
        TFile *infile = nullptr;
        bool isopen = false;
    };
}
