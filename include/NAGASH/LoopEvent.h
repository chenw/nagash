//***************************************************************************************
/// @file LoopEvent.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/ConfigTool.h"
#include "NAGASH/ResultGroup.h"
#include "NAGASH/MSGTool.h"
#include "NAGASH/Toolkit.h"
#include "NAGASH/Timer.h"
#include "NAGASH/EventVector.h"
#include "NAGASH/Job.h"

namespace NAGASH
{
    /** @class NAGASH::LoopEvent LoopEvent.h "NAGASH/LoopEvent.h"
        @ingroup AnalysisClasses
        @brief Virtual base class for event loops.

        Example of usage of LoopEvent can be found in topic @ref AnalysisClasses.
    */
    class LoopEvent : public Job
    {
    public:
        /// @brief Constructor.
        /// @param c input ConfigTool.
        LoopEvent(const ConfigTool &c) : Job(c) {}
        virtual ~LoopEvent();

        /// @brief Initialize the input data.
        /// Most times, this function can be automatically generated using NAGASHMakeClass command.
        virtual StatusCode InitializeData() = 0;

        virtual StatusCode InitializeUser() = 0; ///< Initialize user-defined variables.
        virtual StatusCode Execute() = 0;        ///< Execute this function for each event
        virtual StatusCode Finalize() = 0;       ///< you can do something here after the loop for this file.
        StatusCode OpenRootFile(const TString &);
        StatusCode Run();
        void SetEventVector(std::shared_ptr<EventVector> ev);

        template <typename LE>
        static StatusCode Process(const ConfigTool &, std::shared_ptr<ResultGroup>, std::shared_ptr<ResultGroup>, const TString &);

    protected:
        TTree *&RootTreeUser();
        TFile *RootFileUser();
        int CurrentEntry();
        const TString &InputRootFileName();
        std::shared_ptr<EventVector> EventVectorUser();

    private:
        TTree *fChain = nullptr;
        int NEventsToLoop = 0;
        TFile *infile = nullptr;
        std::shared_ptr<EventVector> eventvector;
        TString inputrootfilename;
        int entry = -1;
    };

    /// @brief Get the pointer to the used Root TTree
    inline TTree *&LoopEvent::RootTreeUser() { return fChain; }
    /// @brief Get the pointer to the used Root TFile
    inline TFile *LoopEvent::RootFileUser() { return infile; }
    /// @brief Get the current entry number in the Root TTree
    inline int LoopEvent::CurrentEntry() { return entry; }
    inline void LoopEvent::SetEventVector(std::shared_ptr<EventVector> ev) { eventvector = ev; }
    inline std::shared_ptr<EventVector> LoopEvent::EventVectorUser() { return eventvector; }
    /// @brief Get the input root file name
    inline const TString &LoopEvent::InputRootFileName() { return inputrootfilename; }

    /// @brief The function used by Analysis to process the event loop.
    /// @tparam LE the type of the user-defined LoopEvent.
    /// @param config_child the ConfigTool, given by Analysis.
    /// @param result the ResultGroup to be saved, handed by Analysis.
    /// @param wholeresult the ResultGroup to be merged together for each sub event loop, handleed by Analysis.
    /// @param inputfilename the input root file name.
    /// @return the status of this event loop.
    template <typename LE>
    StatusCode LoopEvent::Process(const ConfigTool &config_child, std::shared_ptr<ResultGroup> result, std::shared_ptr<ResultGroup> wholeresult, const TString &inputfilename)
    {
        static_assert(!std::is_pointer<LE>::value, "LoopEvent::Process : type can not be a pointer");
        static_assert(std::is_base_of<LoopEvent, LE>::value, "LoopEvent::Process : Input type must be or be inherited from LoopEvent class");

        Job::DefaultMutex.lock();
        auto childLE = std::make_shared<LE>(config_child);
        if (childLE->OpenRootFile(inputfilename) == StatusCode::FAILURE)
        {
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        auto msg_child = childLE->MSGUser();
        TString threadsuffix;
        if (childLE->JobNumber() >= 0)
            threadsuffix.Form("(Job %d)", childLE->JobNumber());

        msg_child->StartTitle("LE::InitializeData" + threadsuffix);
        if (childLE->InitializeData() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();

        msg_child->StartTitle("LE::InitializeUser" + threadsuffix);
        if (childLE->InitializeUser() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();

        Job::DefaultMutex.unlock();

        msg_child->StartTitle("LE::Run" + threadsuffix);
        if (childLE->Run() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            Job::DefaultMutex.lock();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();

        Job::DefaultMutex.lock();
        // Add Child Result to Mother
        msg_child->StartTitle("LE::Finalize" + threadsuffix);
        if (childLE->Finalize() == StatusCode::FAILURE)
        {
            msg_child->EndTitle();
            childLE.reset();
            Job::DefaultMutex.unlock();
            return StatusCode::FAILURE;
        }
        msg_child->EndTitle();
        wholeresult->Merge(childLE->ResultGroupUser());
        childLE.reset();
        Job::DefaultMutex.unlock();

        return StatusCode::SUCCESS;
    }

} // namespace NAGASH
