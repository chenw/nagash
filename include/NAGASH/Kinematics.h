//***************************************************************************************
/// @file Kinematics.h
/// @author Chen Wang, Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Global.h"
#include "NAGASH/Tool.h"

namespace NAGASH
{
    /// @class NAGASH::Kinematics Kinematics.h "NAGASH/Kinematics.h"
    /// @ingroup ToolClasses
    /// @brief Provide some static functions to calculate kinematic variables.
    class Kinematics : public Tool
    {
    public:
        Kinematics(std::shared_ptr<MSGTool> MSG) : Tool(MSG) {}
        static double MT(const TLorentzVector &l1, const TLorentzVector &l2);
        static double CosthStar(const TLorentzVector &Lepton, const TLorentzVector &AntiLepton);
        static double DeltaR(double, double, double, double);
        static double DeltaPhi(double, double);

        static void GetCSFAngles(const TLorentzVector &lep1, const int &charge1, const TLorentzVector &lep2, double ebeam, double &costh, double &phi);
        static void GetVHFAngles(const TLorentzVector &lep1, const int &charge1, const TLorentzVector &lep2, double &costh, double &phi);
        static void GetBDFAngles(const TLorentzVector &tlv1, const int &charge1, const TLorentzVector &tlv2, double &costh, double &phi);

        static void GetAiPolynominals(double costh, double phi, std::vector<double> &aipols);
        static std::vector<double> GetAiPolynominals(double costh, double phi);
        static double SumAiPolynominals(std::vector<double> &ai, std::vector<double> &aimom);

        // projections
        static void GetProjections(const TLorentzVector &proj, const TLorentzVector &axis, double &par, double &perp);
    };

    /// @brief Calculate the transverse momentum of a two-particle-system
    /// @param l1 four-vector of particle one.
    /// @param l2 four-vector particle two.
    /// @return the transverse momentum value.
    inline double Kinematics::MT(const TLorentzVector &l1, const TLorentzVector &l2)
    {
        return sqrt(2 * l1.Pt() * l2.Pt() * (1 - cos(l1.DeltaPhi(l2))));
    }

    /// @brief Calculate the Collins-Soper angle of a two-particle-system
    /// @param Lepton four-vector of the lepton.
    /// @param AntiLepton four-vector of the anti-lepton.
    /// @return the cosine value of the angle.
    inline double Kinematics::CosthStar(const TLorentzVector &Lepton, const TLorentzVector &AntiLepton)
    {
        double costh, P1_plus, P1_minus, P2_plus, P2_minus, Q, QT;
        TLorentzVector Zboson = Lepton + AntiLepton;

        P1_plus = 1 / sqrt(2) * (Lepton.E() + Lepton.Pz());
        P1_minus = 1 / sqrt(2) * (Lepton.E() - Lepton.Pz());
        P2_plus = 1 / sqrt(2) * (AntiLepton.E() + AntiLepton.Pz());
        P2_minus = 1 / sqrt(2) * (AntiLepton.E() - AntiLepton.Pz());
        Q = Zboson.M();
        QT = Zboson.Pt();
        costh = 2 / (Q * sqrt(Q * Q + QT * QT)) * (P1_plus * P2_minus - P2_plus * P1_minus);
        return costh;
    }

    /// @brief Get the parallel and perpendicular components of a vector corresponding to an axis.
    /// @param[in] proj the vector to be projected.
    /// @param[in] axis the axis.
    /// @param[out] par the parallel component of the vector.
    /// @param[out] perp the perpendicular component of the vector.
    inline void Kinematics::GetProjections(const TLorentzVector &proj, const TLorentzVector &axis, double &par, double &perp)
    {
        par = (proj.Px() * axis.Px() + proj.Py() * axis.Py()) / axis.Pt();
        perp = (proj.Py() * axis.Px() - proj.Px() * axis.Py()) / axis.Pt();
    }

    /// @brief Calculate the angular polynomials.
    /// @param ai the \f$A_i\f$s
    /// @param aimom the value of each angular term.
    /// @return
    inline double Kinematics::SumAiPolynominals(std::vector<double> &ai, std::vector<double> &aimom)
    {
        ai.resize(8);
        aimom.resize(8);

        static const double norm = 3. / (16. * TMath::Pi());
        double xsec = 1 + aimom[4] * aimom[4];
        for (int imom = 0; imom < 8; imom++)
            xsec += ((imom == 2) ? 0.5 : 1) * ai[imom] * aimom[imom];
        return xsec * norm;
    }

    /// @brief Calculate the \f$\Delta R\f$ between two particles.
    /// @param eta1 the \f$\eta\f$ of the first particle.
    /// @param phi1 the \f$\phi\f$ of the first particle.
    /// @param eta2 the \f$\eta\f$ of the second particle.
    /// @param phi2 the \f$\phi\f$ of the second particle.
    /// @return \f$\Delta R\f$ value.
    inline double Kinematics::DeltaR(double eta1, double phi1, double eta2, double phi2)
    {
        return sqrt((eta1 - eta2) * (eta1 - eta2) + pow(DeltaPhi(phi1, phi2), 2));
    }

    /// @brief Calculate the \f$\Delta\phi\f$ between two angles.
    /// @param phi1 angle 1.
    /// @param phi2 angle 2.
    /// @return the \f$\Delta\phi\f$ between two them, in the range between \f$[-\pi,\pi]\f$.
    inline double Kinematics::DeltaPhi(double phi1, double phi2)
    {
        return TVector2::Phi_mpi_pi(phi1 - phi2);
    }

} // namespace NAGASH
