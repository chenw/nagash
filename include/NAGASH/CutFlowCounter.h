//***************************************************************************************
/// @file CutFlowCounter.h
/// @author Wenhao Ma
//***************************************************************************************

#pragma once

#include "NAGASH/Result.h"

namespace NAGASH
{
    /** @class NAGASH::CutFlowCounter CutFlowCounter.h "NAGASH/CutFlowCounter.h"
        @ingroup ResultClasses
        @brief Store the cut flow with user defined cuts

        Basic usage:
        ~~~ {c++}
        // suppose you are using the CutFlowCounter inside a LoopEvent.
        NAGASH::StatusCode UserLoopEvent::InitializeUser()
        {
            // ......... other initialization.........
            MyCutFlow = ResultGroupUser()->BookResult<CutFlowCounter>("MyCutFlow");
            MyCutFlow->BookCutPoint("c0");
            MyCutFlow->BookCutPoint("c1");
            MyCutFlow->BookCutPoint("c2");
        }

        NAGASH::StatusCode UserLoopEvent::Execute()
        {
            if(pass_cut_0)
            {
                // ....... other stuff ......
                MyCutFlow->Record("c0", 1);
                if (pass_cut_1)
                {
                    // ...... other stuff......
                    MyCutFlow->Record("c1", 1);
                    if (pass_cut_2)
                    {
                        // ....... other stuff ......
                        MyCutFlow->Record("c2", 1);
                    }
                }
            }
        }
        ~~~

        After that you can get your cut flow as:
        ~~~ {c++}
        MyCutFlow->PrintSummary();
        ~~~
        This will show the entries, sum of weights after each cut, with the effiency of each cut and
        the relative efficiency between each adjacent cut.
    */
    class CutFlowCounter : public Result
    {
    private:
        std::vector<double> SumE;
        std::vector<double> SumW;
        std::map<std::string, uint64_t> CutPointNames;
        std::vector<std::string> CutPointNamesVector;

    public:
        /// @brief Constructor.
        /// @param msg input NAGASH::MSGTool.
        /// @param config input NAGASH::ConfigTool.
        /// @param name the name of the result.
        /// @param fname the output file name of the result, not used in this class.
        CutFlowCounter(std::shared_ptr<MSGTool> msg, std::shared_ptr<ConfigTool> config, const TString &name, const TString &fname = "")
            : Result(msg, config, name, fname) {}

        /// @brief Book a cut point.
        /// @param name name of the cut point.
        void BookCutPoint(const std::string &name)
        {
            CutPointNames.emplace(std::pair<std::string, uint64_t>(name, SumE.size()));
            SumE.emplace_back(0);
            SumW.emplace_back(0);
            CutPointNamesVector.emplace_back(name);
        }

        /// @brief Record a entry that pass a cut.
        /// @param point the index of the cut point.
        /// @param w the weight of this entry.
        void Record(uint64_t point, double w = 1)
        {
            if (point < SumE.size())
            {
                SumE[point] += 1;
                SumW[point] += w;
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Record");
                MSGUser()->MSG_WARNING("Cut point ", point, " does not exist!");
            }
        }

        /// @brief Record a entry that pass a cut.
        /// @param point the name of the cut point.
        /// @param w the weight of this entry.
        void Record(const std::string &point, double w = 1)
        {
            if (auto result = CutPointNames.find(point); result != CutPointNames.end())
            {
                SumE[result->second] += 1;
                SumW[result->second] += w;
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Record");
                MSGUser()->MSG_WARNING("Cut point ", point, " does not exist!");
            }
        }

        /// @brief Combine the cut flow with another cut flow.
        /// @param result input CutFlowCounter to be combined.
        void Combine(std::shared_ptr<Result> result)
        {
            auto subCounter = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
            auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Combine");
            if (subCounter != nullptr)
            {
                if (CutPointNames != subCounter->CutPointNames)
                {
                    MSGUser()->MSG_WARNING("Two cut flow counter are note the same (", GetResultName(), " and ", subCounter->GetResultName(), ")");
                    return;
                }

                for (size_t i = 0; i < SumE.size(); ++i)
                {
                    SumE[i] += subCounter->SumE[i];
                    SumW[i] += subCounter->SumW[i];
                }
            }
        }

        /// @brief Get the total entries of a cut point.
        /// @param point index of the cut point.
        double GetEntries(uint32_t point)
        {
            if (point < SumE.size())
            {
                return SumE[point];
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Record");
                MSGUser()->MSG_WARNING("Cut point ", point, " does not exist!");
            }
        }

        /// @brief Get the total entries of a cut point.
        /// @param point the name of the cut point.
        double GetEntries(const std::string &point)
        {
            if (auto result = CutPointNames.find(point); result != CutPointNames.end())
            {
                return SumE[result->second];
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Record");
                MSGUser()->MSG_WARNING("Cut point ", point, " does not exist!");
            }
        }

        /// @brief Get the sum of weights of a cut point.
        /// @param point the index of the cut point.
        double GetSumW(uint32_t point)
        {
            if (point < SumW.size())
            {
                return SumW[point];
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Record");
                MSGUser()->MSG_WARNING("Cut point ", point, " does not exist!");
            }
        }

        /// @brief Get the sum of weights of a cut point.
        /// @param point the name of the cut point.
        double GetSumW(const std::string &point)
        {
            if (auto result = CutPointNames.find(point); result != CutPointNames.end())
            {
                return SumW[result->second];
            }
            else
            {
                auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::Record");
                MSGUser()->MSG_WARNING("Cut point ", point, " does not exist!");
            }
        }

        /// @brief Print the summary of the cut flow.
        void PrintSummary()
        {
            auto guard = MSGUser()->StartTitleWithGuard("CutFlowCounter::PrintSummary");
            MSGUser()->MSG_INFO("Summary of cut flow counter ", GetResultName());
            for (size_t i = 0; i < SumE.size(); ++i)
                MSGUser()->MSG_INFO("Cut point ", CutPointNamesVector[i], "  Entries : ", SumE[i], "  SumW : ", SumW[i], "  Eff : ", SumW[i] / SumW[0], " Relative Eff : ", i == 0 ? 1 : SumW[i] / SumW[i - 1]);
        }
    };

}
